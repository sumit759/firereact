package com.cscs.raaz_asymmetric_grid_fuction;

public interface PoolObjectFactory<T> {
  T createObject();
}

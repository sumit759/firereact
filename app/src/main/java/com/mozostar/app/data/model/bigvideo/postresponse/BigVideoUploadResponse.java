package com.mozostar.app.data.model.bigvideo.postresponse;

import com.google.gson.annotations.SerializedName;

public class BigVideoUploadResponse{

	@SerializedName("video")
	private Video video;

	@SerializedName("message")
	private String message;

	public void setVideo(Video video){
		this.video = video;
	}

	public Video getVideo(){
		return video;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}
package com.mozostar.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Safetydetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("qrcode")
    @Expose
    private String qrcode;
    @SerializedName("account_type")
    @Expose
    private Integer accountType;
    @SerializedName("comment_status")
    @Expose
    private Integer commentStatus;
    @SerializedName("like_status")
    @Expose
    private Integer likeStatus;
    @SerializedName("react_status")
    @Expose
    private Integer reactStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Integer getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(Integer commentStatus) {
        this.commentStatus = commentStatus;
    }

    public Integer getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(Integer likeStatus) {
        this.likeStatus = likeStatus;
    }

    public Integer getReactStatus() {
        return reactStatus;
    }

    public void setReactStatus(Integer reactStatus) {
        this.reactStatus = reactStatus;
    }

}

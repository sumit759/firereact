package com.mozostar.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoList_Pojo1 {


    @SerializedName("videos")
    @Expose
    private Videos videos;

    public Videos getVideos() {
        return videos;
    }

    public void setVideos(Videos videos) {
        this.videos = videos;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("user_id")
        @Expose
        private Integer userId;

        @SerializedName("category_id")
        @Expose
        private Integer categoryId;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("video")
        @Expose
        private String video;

        @SerializedName("replaced_video")
        @Expose
        private Object replacedVideo;

        @SerializedName("audio")
        @Expose
        private String audio;

        @SerializedName("view_type")
        @Expose
        private Integer viewType;

        @SerializedName("duration")
        @Expose
        private Object duration;

        @SerializedName("status")
        @Expose
        private Integer status;

        @SerializedName("block_status")
        @Expose
        private Integer blockStatus;

        @SerializedName("likes_count")
        @Expose
        private Integer likesCount;


        @SerializedName("dislikes_count")
        @Expose
        private Integer dislikes_count;

        @SerializedName("comments_count")
        @Expose
        private Integer commentsCount;

        @SerializedName("videosound")
        @Expose
        private Videosound videosound;

        @SerializedName("userdetails")
        @Expose
        private Userdetails userdetails;

        @SerializedName("tagfriends")
        @Expose
        private Tagfriends tagfriends;

        @SerializedName("safetydetails")
        @Expose
        private Object safetydetails;

        @SerializedName("followers")
        @Expose
        private Object followers;

        @SerializedName("video_image_url")
        @Expose
        private String video_image_url;

        public String getVideo_image_url() {
            return video_image_url;
        }

        public void setVideo_image_url(String video_image_url) {
            this.video_image_url = video_image_url;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public Object getReplacedVideo() {
            return replacedVideo;
        }

        public void setReplacedVideo(Object replacedVideo) {
            this.replacedVideo = replacedVideo;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public Integer getViewType() {
            return viewType;
        }

        public void setViewType(Integer viewType) {
            this.viewType = viewType;
        }

        public Object getDuration() {
            return duration;
        }

        public void setDuration(Object duration) {
            this.duration = duration;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getBlockStatus() {
            return blockStatus;
        }

        public void setBlockStatus(Integer blockStatus) {
            this.blockStatus = blockStatus;
        }

        public Integer getLikesCount() {
            return likesCount;
        }

        public void setLikesCount(Integer likesCount) {
            this.likesCount = likesCount;
        }

        public Integer getCommentsCount() {
            return commentsCount;
        }

        public void setCommentsCount(Integer commentsCount) {
            this.commentsCount = commentsCount;
        }

        public Videosound getVideosound() {
            return videosound;
        }

        public void setVideosound(Videosound videosound) {
            this.videosound = videosound;
        }

        public Userdetails getUserdetails() {
            return userdetails;
        }

        public void setUserdetails(Userdetails userdetails) {
            this.userdetails = userdetails;
        }

        public Tagfriends getTagfriends() {
            return tagfriends;
        }

        public void setTagfriends(Tagfriends tagfriends) {
            this.tagfriends = tagfriends;
        }

        public Object getSafetydetails() {
            return safetydetails;
        }

        public void setSafetydetails(Object safetydetails) {
            this.safetydetails = safetydetails;
        }

        public Object getFollowers() {
            return followers;
        }

        public void setFollowers(Object followers) {
            this.followers = followers;
        }

        public Integer getDislikes_count() {
            return dislikes_count;
        }

        public void setDislikes_count(Integer dislikes_count) {
            this.dislikes_count = dislikes_count;
        }

    }

    public class Tagfriends {

        @SerializedName("friends_id")
        @Expose
        private String friendsId;

        public String getFriendsId() {
            return friendsId;
        }

        public void setFriendsId(String friendsId) {
            this.friendsId = friendsId;
        }

    }

    public class Userdetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("country_code")
        @Expose
        private String countryCode;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("user_bio")
        @Expose
        private Object userBio;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("email_verified_at")
        @Expose
        private Object emailVerifiedAt;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("instagram_link")
        @Expose
        private Object instagramLink;
        @SerializedName("youtube_link")
        @Expose
        private Object youtubeLink;
        @SerializedName("website_link")
        @Expose
        private Object websiteLink;
        @SerializedName("block_status")
        @Expose
        private Integer blockStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Object getUserBio() {
            return userBio;
        }

        public void setUserBio(Object userBio) {
            this.userBio = userBio;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getInstagramLink() {
            return instagramLink;
        }

        public void setInstagramLink(Object instagramLink) {
            this.instagramLink = instagramLink;
        }

        public Object getYoutubeLink() {
            return youtubeLink;
        }

        public void setYoutubeLink(Object youtubeLink) {
            this.youtubeLink = youtubeLink;
        }

        public Object getWebsiteLink() {
            return websiteLink;
        }

        public void setWebsiteLink(Object websiteLink) {
            this.websiteLink = websiteLink;
        }

        public Integer getBlockStatus() {
            return blockStatus;
        }

        public void setBlockStatus(Integer blockStatus) {
            this.blockStatus = blockStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Videos {

        @SerializedName("current_page")
        @Expose
        private Integer currentPage;
        @SerializedName("data")
        @Expose
        private List<Data> data = null;
        @SerializedName("first_page_url")
        @Expose
        private String firstPageUrl;
        @SerializedName("from")
        @Expose
        private Integer from;
        @SerializedName("last_page")
        @Expose
        private Integer lastPage;
        @SerializedName("last_page_url")
        @Expose
        private String lastPageUrl;
        @SerializedName("next_page_url")
        @Expose
        private String nextPageUrl;
        @SerializedName("path")
        @Expose
        private String path;
        @SerializedName("per_page")
        @Expose
        private Integer perPage;
        @SerializedName("prev_page_url")
        @Expose
        private Object prevPageUrl;
        @SerializedName("to")
        @Expose
        private Integer to;
        @SerializedName("total")
        @Expose
        private Integer total;

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        public String getFirstPageUrl() {
            return firstPageUrl;
        }

        public void setFirstPageUrl(String firstPageUrl) {
            this.firstPageUrl = firstPageUrl;
        }

        public Integer getFrom() {
            return from;
        }

        public void setFrom(Integer from) {
            this.from = from;
        }

        public Integer getLastPage() {
            return lastPage;
        }

        public void setLastPage(Integer lastPage) {
            this.lastPage = lastPage;
        }

        public String getLastPageUrl() {
            return lastPageUrl;
        }

        public void setLastPageUrl(String lastPageUrl) {
            this.lastPageUrl = lastPageUrl;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public Integer getPerPage() {
            return perPage;
        }

        public void setPerPage(Integer perPage) {
            this.perPage = perPage;
        }

        public Object getPrevPageUrl() {
            return prevPageUrl;
        }

        public void setPrevPageUrl(Object prevPageUrl) {
            this.prevPageUrl = prevPageUrl;
        }

        public Integer getTo() {
            return to;
        }

        public void setTo(Integer to) {
            this.to = to;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

    }

    public class Videosound {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("audio")
        @Expose
        private String audio;
        @SerializedName("duration")
        @Expose
        private Integer duration;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public Integer getDuration() {
            return duration;
        }

        public void setDuration(Integer duration) {
            this.duration = duration;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}

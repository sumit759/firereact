package com.mozostar.app.data.model.bigvideo.getlist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BigVideoGetListResponse{

	@SerializedName("videos")
	private List<VideosItem> videos;

	public void setVideos(List<VideosItem> videos){
		this.videos = videos;
	}

	public List<VideosItem> getVideos(){
		return videos;
	}
}
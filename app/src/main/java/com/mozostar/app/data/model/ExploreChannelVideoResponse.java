package com.mozostar.app.data.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ExploreChannelVideoResponse {

    @SerializedName("data")
    private List<ExploreChannelData> data;

    @SerializedName("message")
    private String message;

    public List<ExploreChannelData> getData() {
        return data;
    }

    public String getMessage() {
        return message;
}


    public class ExploreChannelData {

        @SerializedName("block_status")
        private int blockStatus;

        @SerializedName("channel")
        private int channel;

        @SerializedName("description")
        private String description;

        @SerializedName("view_type")
        private int viewType;

        @SerializedName("replaced_video")
        private String replacedVideo;

        @SerializedName("video")
        private String video;

        @SerializedName("title")
        private String title;

        @SerializedName("channel_category")
        private int channelCategory;

        @SerializedName("duration")
        private String duration;

        @SerializedName("likes_count")
        private int likesCount;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("userdetails")
        private Userdetails userdetails;

        @SerializedName("comments_count")
        private int commentsCount;

        @SerializedName("id")
        private int id;

        @SerializedName("status")
        private int status;

        @SerializedName("video_image_url")
        private String  video_image_url;


        public String getVideo_image_url() {
            return video_image_url;
        }

        public void setVideo_image_url(String video_image_url) {
            this.video_image_url = video_image_url;
        }

        public int getBlockStatus() {
            return blockStatus;
        }

        public int getChannel() {
            return channel;
        }

        public String getDescription() {
            return description;
        }

        public int getViewType() {
            return viewType;
        }

        public String getReplacedVideo() {
            return replacedVideo;
        }

        public String getVideo() {
            return video;
        }

        public String getTitle() {
            return title;
        }

        public int getChannelCategory() {
            return channelCategory;
        }

        public String getDuration() {
            return duration;
        }

        public int getLikesCount() {
            return likesCount;
        }

        public int getUserId() {
            return userId;
        }

        public Userdetails getUserdetails() {
            return userdetails;
        }

        public int getCommentsCount() {
            return commentsCount;
        }

        public int getId() {
            return id;
        }

        public int getStatus() {
            return status;
        }
    }

    public class Userdetails {

        @SerializedName("website_link")
        private String websiteLink;

        @SerializedName("image")
        private String image;

        @SerializedName("block_status")
        private int blockStatus;

        @SerializedName("device_id")
        private String deviceId;

        @SerializedName("mobile")
        private String mobile;

        @SerializedName("last_name")
        private String lastName;

        @SerializedName("created_at")
        private String createdAt;

        @SerializedName("device_type")
        private String deviceType;

        @SerializedName("email_verified_at")
        private String emailVerifiedAt;

        @SerializedName("youtube_link")
        private String youtubeLink;

        @SerializedName("country_code")
        private String countryCode;

        @SerializedName("updated_at")
        private String updatedAt;

        @SerializedName("user_bio")
        private String userBio;

        @SerializedName("device_token")
        private String deviceToken;

        @SerializedName("instagram_link")
        private String instagramLink;

        @SerializedName("id")
        private int id;

        @SerializedName("first_name")
        private String firstName;

        @SerializedName("email")
        private String email;

        public String getWebsiteLink() {
            return websiteLink;
        }

        public String getImage() {
            return image;
        }

        public int getBlockStatus() {
            return blockStatus;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public String getMobile() {
            return mobile;
        }

        public String getLastName() {
            return lastName;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public String getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public String getYoutubeLink() {
            return youtubeLink;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getUserBio() {
            return userBio;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public String getInstagramLink() {
            return instagramLink;
        }

        public int getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getEmail() {
            return email;
        }
    }
}
package com.mozostar.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoUpload_pojo {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("video")
    @Expose
    private Video video;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public class Video {

        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("view_type")
        @Expose
        private String viewType;
        @SerializedName("audio")
        @Expose
        private String audio;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("video")
        @Expose
        private String video;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getViewType() {
            return viewType;
        }

        public void setViewType(String viewType) {
            this.viewType = viewType;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }
}

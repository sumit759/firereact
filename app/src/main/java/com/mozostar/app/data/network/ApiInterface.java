package com.mozostar.app.data.network;


import com.google.gson.JsonObject;
import com.mozostar.app.data.ChatUserList_Pojo;
import com.mozostar.app.data.model.AddCategory_Pojo;
import com.mozostar.app.data.model.CategoryListPojo;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.data.model.CommentsList_Pojo;
import com.mozostar.app.data.model.DeletePojo;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.Follower_list;
import com.mozostar.app.data.model.FollowingLIst_pojo;
import com.mozostar.app.data.model.Guest_Pojo;
import com.mozostar.app.data.model.HashSearch_Pojo;
import com.mozostar.app.data.model.Login_Pojo;
import com.mozostar.app.data.model.ProfileChannelList_pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.model.Profile_Pojo;
import com.mozostar.app.data.model.Register_Pojo;
import com.mozostar.app.data.model.Reset_Pojo;
import com.mozostar.app.data.model.Safety_Pojo;
import com.mozostar.app.data.model.SoundsPlayList_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.data.model.UpdateProfile_Pojo;
import com.mozostar.app.data.model.UserVideosList_Pojo;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.data.model.VideoList_Pojo1;
import com.mozostar.app.data.model.VideoReportResponse;
import com.mozostar.app.data.model.VideoUpload_pojo;
import com.mozostar.app.data.model.bigvideo.getlist.BigVideoGetListResponse;
import com.mozostar.app.data.model.bigvideo.postresponse.BigVideoUploadResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;


public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/user/chat")
    Observable<Object> chatPush(@FieldMap HashMap<String, Object> params);

    @POST("fcm/send")
    Observable<Object> sendFcm(@Body JsonObject jsonObject);

    @Multipart
    @POST("api/user/signup")
    Observable<Register_Pojo> register(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file);

    @Multipart
    @POST("api/user/user_edit")
    Observable<UpdateProfile_Pojo> update_profile(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("api/user/videolike")
    Observable<Object> like(@FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("api/user/follow_person")
    Observable<Follow_Pojo> follow(@FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("api/user/safetydetails")
    Observable<Safety_Pojo> getSafetyCount (@FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("api/user/sound_track_video")
    Observable<ArrayList<ProfileVideoList_Pojo>> sound_list(@FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("api/user/videocomment")
    Observable<CommentPost_Pojo> post_comment(@FieldMap HashMap<String, Object> params);

    @Multipart
    @POST("api/user/video")
    Observable<VideoUpload_pojo> video_upload(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file, @Part("tag") ArrayList<RequestBody> tag);

    @Multipart
    @POST("api/user/video")
    Observable<VideoUpload_pojo> video_upload(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file,@Part MultipartBody.Part file1,@Part("tag") ArrayList<RequestBody> tag);

    @Multipart
    @POST("api/user/channel_video_upload")
    Observable<VideoUpload_pojo> video_upload_channal(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file,@Part("tag") ArrayList<RequestBody> tag);

    @FormUrlEncoded
    @POST("api/user/login")
    Observable<Login_Pojo> login(@FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("api/user/reset_password_otp")
    Observable<Reset_Pojo> reset(@FieldMap HashMap<String, Object> params);


    @FormUrlEncoded
    @POST("api/user/changed_password")
    Observable<Follow_Pojo> change_pswd(@FieldMap HashMap<String, Object> params);

    @GET("api/user/profile")
    Observable<ArrayList<Profile_Pojo>> profile();

    @GET("api/user/channel_category_list")
    Observable<CategoryListPojo> getCategory();

    @GET("api/user/guest_profile/{id}")
    Observable<Guest_Pojo> guest_profile(@Path("id") String id);

    @GET("api/user/chat_user_following_person")
    Observable<ArrayList<ChatUserList_Pojo>> getChatList();

    @DELETE("api/user/user_videos_delete/{id}")
    Observable<DeletePojo> delete_video(@Path("id") String id);

    @FormUrlEncoded
    @POST("api/user/allvideo")
    Observable<VideoList_Pojo1> video_list(@FieldMap HashMap<String, Object> params);

    @GET("api/user/hastag_list")
    Observable<Discover_Pojo> trending_list();

    @GET("api/user/follow_person_video")
    Observable<ArrayList<ProfileVideoList_Pojo>> follow_video_list();

    @GET("api/user/user_like_videos")
    Observable<ArrayList<ProfileVideoList_Pojo>> user_like_videos();

    @GET("api/user/user_videos")
    Observable<ArrayList<ProfileVideoList_Pojo>> user_videos();

    @GET("api/user/guest_like_videos/{id}")
    Observable<ArrayList<ProfileVideoList_Pojo>> guest_like_videos(@Path("id") String id);

    @GET("api/user/user_following")
    Observable<ArrayList<FollowingLIst_pojo>> following_list();

    @GET("api/user/user_follower")
    Observable<ArrayList<Follower_list>> followers_list();

    @GET("api/user/user_tag_search/{name}")
    Observable<ArrayList<HashSearch_Pojo>> search_tag(@Path("name") String name);

    @GET("api/user/profile_search/{name}")
    Observable<ArrayList<User_Pojo>> search_user(@Path("name") String name);

    @GET("api/user/guest_following/{id}")
    Observable<ArrayList<FollowingLIst_pojo>> guest_following_list(@Path("id") String id);

    @GET("api/user/guest_follower/{id}")
    Observable<ArrayList<Follower_list>> guest_followers_list(@Path("id") String id);

    @GET("api/user/guest_videos/{id}")
    Observable<ArrayList<ProfileVideoList_Pojo>> guest_videos(@Path("id") String id);

    @GET("api/user/viewcomments/{id}")
    Observable<ArrayList<CommentsList_Pojo>> comments_list(@Path("id") String id);

    @Multipart
    @POST("api/user/update/profile")
    Observable<Object> profile(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part filename);

    @Multipart
    @POST("api/user/create_channel")
    Observable<AddCategory_Pojo> add_category(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part filename);

    @GET("api/user/sounds")
    Observable<List<SoundsPlayList_Pojo>> sound_playlist();

    @GET("api/user/channel_list")
    Observable<ProfileChannelList_pojo> channel_list();

    @GET("api/user/channel_video_get/{id}")
    Observable<ChannelVideoList_Pojo> channel_video_list(@Path("id") String id);

    @FormUrlEncoded
    @POST("api/user/business_account")
    Observable<Follow_Pojo> activateBusiness(@FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("api/user/user_video_report")
    Observable<VideoReportResponse> reportVideo(@FieldMap HashMap<String, Object> params);

    @GET("/api/user/video_search/{name}")
    Observable<ArrayList<UserVideosList_Pojo>> searchVideo(@Path("name") String name);

    @GET("api/user/sound_search/{name}")
    Observable<ArrayList<Sounds_Pojo>> searchSound(@Path("name") String name);

    @POST("/api/user/explore_channel_video")
    Observable<ExploreChannelVideoResponse> exploreChannelVideo();

    @Multipart
    @POST("/public/api/user/bigvideo_upload")
    Observable<BigVideoUploadResponse> uploadBigVideo(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file, @Part("tag") ArrayList<RequestBody> tag);

    @GET("/public/api/user/big_video_get")
    Observable<BigVideoGetListResponse> getBigVideoList();

    @FormUrlEncoded
    @POST("api/user/videodislike")
    Observable<Object> disLike(@FieldMap HashMap<String, Object> params);

}

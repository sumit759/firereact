package com.mozostar.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Profile_Pojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("user_bio")
    @Expose
    private String userBio;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("instagram_link")
    @Expose
    private String instagramLink;
    @SerializedName("youtube_link")
    @Expose
    private String youtubeLink;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("uservideo_count")
    @Expose
    private Integer uservideoCount;
    @SerializedName("userlikes_count")
    @Expose
    private Integer userlikesCount;
    @SerializedName("userfollowing_count")
    @Expose
    private Integer userfollowingCount;
    @SerializedName("userfollower_count")
    @Expose
    private Integer userfollowerCount;

    @SerializedName("uservideo")
    @Expose
    private List<Uservideo> uservideo = null;
    @SerializedName("safetydetails")
    @Expose
    private Safetydetails safetydetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setUserBio(String userBio) {
        this.userBio = userBio;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }



    public String getCountryCode() {
        return countryCode;
    }

    public String getUserBio() {
        return userBio;
    }

    public Object getOtp() {
        return otp;
    }

    public String getImage() {
        return image;
    }


    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }

    public Object getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUservideoCount() {
        return uservideoCount;
    }

    public void setUservideoCount(Integer uservideoCount) {
        this.uservideoCount = uservideoCount;
    }

    public Integer getUserlikesCount() {
        return userlikesCount;
    }

    public void setUserlikesCount(Integer userlikesCount) {
        this.userlikesCount = userlikesCount;
    }

    public Integer getUserfollowingCount() {
        return userfollowingCount;
    }

    public void setUserfollowingCount(Integer userfollowingCount) {
        this.userfollowingCount = userfollowingCount;
    }

    public Integer getUserfollowerCount() {
        return userfollowerCount;
    }

    public void setUserfollowerCount(Integer userfollowerCount) {
        this.userfollowerCount = userfollowerCount;
    }

    public List<Uservideo> getUservideo() {
        return uservideo;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Safetydetails getSafetydetails() {
        return safetydetails;
    }

    public void setSafetydetails(Safetydetails safetydetails) {
        this.safetydetails = safetydetails;
    }

    public void setUservideo(List<Uservideo> uservideo) {
        this.uservideo = uservideo;
    }
    public class Uservideo {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("video")
        @Expose
        private String video;
        @SerializedName("replaced_video")
        @Expose
        private Object replacedVideo;
        @SerializedName("audio")
        @Expose
        private String audio;
        @SerializedName("view_type")
        @Expose
        private Integer viewType;
        @SerializedName("duration")
        @Expose
        private Object duration;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public Object getReplacedVideo() {
            return replacedVideo;
        }

        public void setReplacedVideo(Object replacedVideo) {
            this.replacedVideo = replacedVideo;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public Integer getViewType() {
            return viewType;
        }

        public void setViewType(Integer viewType) {
            this.viewType = viewType;
        }

        public Object getDuration() {
            return duration;
        }

        public void setDuration(Object duration) {
            this.duration = duration;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }
}

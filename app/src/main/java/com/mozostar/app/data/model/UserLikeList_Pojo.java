package com.mozostar.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserLikeList_Pojo {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("video_id")
    @Expose
    private Integer videoId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("likevideo")
    @Expose
    private List<Likevideo> likevideo = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Likevideo> getLikevideo() {
        return likevideo;
    }

    public void setLikevideo(List<Likevideo> likevideo) {
        this.likevideo = likevideo;
    }

    public class Likevideo {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("video")
        @Expose
        private String video;
        @SerializedName("replaced_video")
        @Expose
        private Object replacedVideo;
        @SerializedName("audio")
        @Expose
        private String audio;
        @SerializedName("view_type")
        @Expose
        private Object viewType;
        @SerializedName("duration")
        @Expose
        private Object duration;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public Object getReplacedVideo() {
            return replacedVideo;
        }

        public void setReplacedVideo(Object replacedVideo) {
            this.replacedVideo = replacedVideo;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public Object getViewType() {
            return viewType;
        }

        public void setViewType(Object viewType) {
            this.viewType = viewType;
        }

        public Object getDuration() {
            return duration;
        }

        public void setDuration(Object duration) {
            this.duration = duration;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }
}

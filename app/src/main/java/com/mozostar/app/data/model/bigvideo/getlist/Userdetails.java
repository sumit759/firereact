package com.mozostar.app.data.model.bigvideo.getlist;

import com.google.gson.annotations.SerializedName;

public class Userdetails{

	@SerializedName("website_link")
	private Object websiteLink;

	@SerializedName("image")
	private String image;

	@SerializedName("block_status")
	private int blockStatus;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("email_verified_at")
	private Object emailVerifiedAt;

	@SerializedName("youtube_link")
	private Object youtubeLink;

	@SerializedName("country_code")
	private String countryCode;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_bio")
	private String userBio;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("instagram_link")
	private String instagramLink;

	@SerializedName("id")
	private int id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	public void setWebsiteLink(Object websiteLink){
		this.websiteLink = websiteLink;
	}

	public Object getWebsiteLink(){
		return websiteLink;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setBlockStatus(int blockStatus){
		this.blockStatus = blockStatus;
	}

	public int getBlockStatus(){
		return blockStatus;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setEmailVerifiedAt(Object emailVerifiedAt){
		this.emailVerifiedAt = emailVerifiedAt;
	}

	public Object getEmailVerifiedAt(){
		return emailVerifiedAt;
	}

	public void setYoutubeLink(Object youtubeLink){
		this.youtubeLink = youtubeLink;
	}

	public Object getYoutubeLink(){
		return youtubeLink;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserBio(String userBio){
		this.userBio = userBio;
	}

	public String getUserBio(){
		return userBio;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setInstagramLink(String instagramLink){
		this.instagramLink = instagramLink;
	}

	public String getInstagramLink(){
		return instagramLink;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}
}
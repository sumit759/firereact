package com.mozostar.app.data.model.bigvideo.postresponse;

import com.google.gson.annotations.SerializedName;

public class Video{

	@SerializedName("user_details")
	private UserDetails userDetails;

	@SerializedName("video_details")
	private VideoDetails videoDetails;

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	public void setVideoDetails(VideoDetails videoDetails){
		this.videoDetails = videoDetails;
	}

	public VideoDetails getVideoDetails(){
		return videoDetails;
	}
}
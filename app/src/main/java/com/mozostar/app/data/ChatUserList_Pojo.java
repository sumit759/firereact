package com.mozostar.app.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatUserList_Pojo {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("follower_id")
    @Expose
    private Integer followerId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("chat_following")
    @Expose
    private ChatFollowing chatFollowing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFollowerId() {
        return followerId;
    }

    public void setFollowerId(Integer followerId) {
        this.followerId = followerId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ChatFollowing getChatFollowing() {
        return chatFollowing;
    }

    public void setChatFollowing(ChatFollowing chatFollowing) {
        this.chatFollowing = chatFollowing;
    }

    public class ChatFollowing {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("country_code")
        @Expose
        private String countryCode;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("user_bio")
        @Expose
        private Object userBio;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("email_verified_at")
        @Expose
        private Object emailVerifiedAt;
        @SerializedName("image")
        @Expose
        private Object image;
        @SerializedName("instagram_link")
        @Expose
        private Object instagramLink;
        @SerializedName("youtube_link")
        @Expose
        private Object youtubeLink;
        @SerializedName("website_link")
        @Expose
        private Object websiteLink;
        @SerializedName("block_status")
        @Expose
        private Integer blockStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Object getUserBio() {
            return userBio;
        }

        public void setUserBio(Object userBio) {
            this.userBio = userBio;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public Object getInstagramLink() {
            return instagramLink;
        }

        public void setInstagramLink(Object instagramLink) {
            this.instagramLink = instagramLink;
        }

        public Object getYoutubeLink() {
            return youtubeLink;
        }

        public void setYoutubeLink(Object youtubeLink) {
            this.youtubeLink = youtubeLink;
        }

        public Object getWebsiteLink() {
            return websiteLink;
        }

        public void setWebsiteLink(Object websiteLink) {
            this.websiteLink = websiteLink;
        }

        public Integer getBlockStatus() {
            return blockStatus;
        }

        public void setBlockStatus(Integer blockStatus) {
            this.blockStatus = blockStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}

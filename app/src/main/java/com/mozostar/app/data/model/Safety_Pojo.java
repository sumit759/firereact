package com.mozostar.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Safety_Pojo {

    @SerializedName("comment_status")
    @Expose
    private Integer commentStatus;
    @SerializedName("like_status")
    @Expose
    private Integer likeStatus;
    @SerializedName("react_status")
    @Expose
    private Integer reactStatus;

    public Integer getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(Integer commentStatus) {
        this.commentStatus = commentStatus;
    }

    public Integer getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(Integer likeStatus) {
        this.likeStatus = likeStatus;
    }

    public Integer getReactStatus() {
        return reactStatus;
    }

    public void setReactStatus(Integer reactStatus) {
        this.reactStatus = reactStatus;
    }
}

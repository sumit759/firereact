package com.mozostar.app.data.model.bigvideo.getlist;

import com.google.gson.annotations.SerializedName;

public class VideosItem{

	@SerializedName("block_status")
	private int blockStatus;

	@SerializedName("description")
	private String description;

	@SerializedName("view_type")
	private int viewType;

	@SerializedName("replaced_video")
	private Object replacedVideo;

	@SerializedName("video")
	private String video;

	@SerializedName("title")
	private String title;

	@SerializedName("video_image_url")
	private String videoImageUrl;

	@SerializedName("duration")
	private Object duration;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("userdetails")
	private Userdetails userdetails;

	@SerializedName("dislikes_count")
	private int dislikesCount;

	@SerializedName("id")
	private int id;

	@SerializedName("audio")
	private String audio;

	@SerializedName("status")
	private int status;

	public void setBlockStatus(int blockStatus){
		this.blockStatus = blockStatus;
	}

	public int getBlockStatus(){
		return blockStatus;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setViewType(int viewType){
		this.viewType = viewType;
	}

	public int getViewType(){
		return viewType;
	}

	public void setReplacedVideo(Object replacedVideo){
		this.replacedVideo = replacedVideo;
	}

	public Object getReplacedVideo(){
		return replacedVideo;
	}

	public void setVideo(String video){
		this.video = video;
	}

	public String getVideo(){
		return video;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setVideoImageUrl(String videoImageUrl){
		this.videoImageUrl = videoImageUrl;
	}

	public String getVideoImageUrl(){
		return videoImageUrl;
	}

	public void setDuration(Object duration){
		this.duration = duration;
	}

	public Object getDuration(){
		return duration;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setUserdetails(Userdetails userdetails){
		this.userdetails = userdetails;
	}

	public Userdetails getUserdetails(){
		return userdetails;
	}

	public void setDislikesCount(int dislikesCount){
		this.dislikesCount = dislikesCount;
	}

	public int getDislikesCount(){
		return dislikesCount;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setAudio(String audio){
		this.audio = audio;
	}

	public String getAudio(){
		return audio;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
}
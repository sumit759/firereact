package com.mozostar.app.data.model.bigvideo.postresponse;

import com.google.gson.annotations.SerializedName;

public class VideoDetails{

	@SerializedName("video_image_url")
	private String videoImageUrl;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("description")
	private String description;

	@SerializedName("view_type")
	private String viewType;

	@SerializedName("video")
	private String video;

	@SerializedName("audio")
	private String audio;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public void setVideoImageUrl(String videoImageUrl){
		this.videoImageUrl = videoImageUrl;
	}

	public String getVideoImageUrl(){
		return videoImageUrl;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setViewType(String viewType){
		this.viewType = viewType;
	}

	public String getViewType(){
		return viewType;
	}

	public void setVideo(String video){
		this.video = video;
	}

	public String getVideo(){
		return video;
	}

	public void setAudio(String audio){
		this.audio = audio;
	}

	public String getAudio(){
		return audio;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}
}
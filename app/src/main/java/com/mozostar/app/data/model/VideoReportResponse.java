package com.mozostar.app.data.model;

import com.google.gson.annotations.SerializedName;

public class VideoReportResponse{

	@SerializedName("data")
	private ReportVideoData data;

	@SerializedName("message")
	private String message;

	public void setData(ReportVideoData data){
		this.data = data;
	}

	public ReportVideoData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public class ReportVideoData {

		@SerializedName("user_id")
		private String userId;

		@SerializedName("report")
		private String report;

		@SerializedName("id")
		private int id;

		@SerializedName("video_user_id")
		private String video_user_id;

		@SerializedName("video_id")
		private String video_id;

		public void setUserId(String userId){
			this.userId = userId;
		}

		public String getUserId(){
			return userId;
		}

		public void setReport(String report){
			this.report = report;
		}

		public String getReport(){
			return report;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setVideoUserId(String videoUserId){
			this.video_user_id = videoUserId;
		}

		public String getVideoUserId(){
			return video_user_id;
		}

		public void setVideoId(String videoId){
			this.video_id = videoId;
		}

		public String getVideoId(){
			return video_id;
		}
	}
}
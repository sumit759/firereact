package com.mozostar.app.custom_ui;
import com.mozostar.app.data.model.AsymRecyleItems;

import java.util.ArrayList;
import java.util.List;

final class AsymmetricUtils {
  int currentOffset;

  AsymmetricUtils() {
  }

  public List<AsymRecyleItems> assymItem(int qty) {
    List<AsymRecyleItems> items = new ArrayList<>();

    for (int i = 0; i < qty; i++) {
      int colSpan = Math.random() < 0.2f ? 2 : 1;
      // Swap the next 2 lines to have items with variable
      // column/row span.
      // int rowSpan = Math.random() < 0.2f ? 2 : 1;
      int rowSpan = colSpan;
      AsymRecyleItems item = new AsymRecyleItems(colSpan, rowSpan, currentOffset + i);
      items.add(item);
    }

    currentOffset += qty;

    return items;
  }
}

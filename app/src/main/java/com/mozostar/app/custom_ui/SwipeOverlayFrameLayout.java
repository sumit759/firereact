package com.mozostar.app.custom_ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.widget.FrameLayout;


public class SwipeOverlayFrameLayout extends FrameLayout {

    /* renamed from: a */
    private GestureDetector f20504a;

    /* renamed from: b */
    private OnSwipeListener f20505b;

    /* renamed from: c */
    private float f20506c;

    /* renamed from: d */
    private float f20507d;

    /* renamed from: e */
    private boolean f20508e = true;

    /* renamed from: f */
    private boolean f20509f;

    /* renamed from: g */
    private boolean f20510g;

    public interface OnSwipeListener {
        boolean onSwipeLeft();

        boolean onSwipeRight();
    }

    public void setDisllowInterceptEnabled(boolean z) {
        this.f20509f = z;
    }

    public void setOnSwipeListener(OnSwipeListener onSwipeListener) {
        this.f20505b = onSwipeListener;
    }

    public void setSwipeEnabled(boolean z) {
        this.f20508e = z;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.f20510g = z;
    }

    public SwipeOverlayFrameLayout(Context context) {
        super(context);
        mo30323a(context);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo30323a(Context context) {
        SimpleOnGestureListener r0 = new SimpleOnGestureListener() {
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                return SwipeOverlayFrameLayout.this.mo30324a(motionEvent, motionEvent2, f, f2);
            }
        };
        this.f20506c =m14099b(context, 45.0f);
        this.f20507d =m14099b(context, 65.0f);
        this.f20504a = new GestureDetector(context.getApplicationContext(), r0);
        this.f20504a.setOnDoubleTapListener(null);
        this.f20504a.setIsLongpressEnabled(false);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            boolean z = false;
            if (motionEvent.getAction() == 0) {
                this.f20510g = false;
            }
            if (this.f20510g && this.f20509f) {
                z = true;
            }
            if (!this.f20508e || this.f20504a == null || z || !this.f20504a.onTouchEvent(motionEvent)) {
                return super.dispatchTouchEvent(motionEvent);
            }
            motionEvent.setAction(3);
            super.dispatchTouchEvent(motionEvent);
            return true;
        } catch (Exception unused) {
            return true;
        }
    }

    public SwipeOverlayFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo30323a(context);
    }

    public SwipeOverlayFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        mo30323a(context);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo30324a(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        boolean z = false;
        if (this.f20505b == null || Math.abs(motionEvent2.getY() - motionEvent.getY()) > this.f20506c) {
            return false;
        }
        float abs = Math.abs(f);
        float abs2 = Math.abs(f2);
        float abs3 = Math.abs(motionEvent2.getX() - motionEvent.getX());
        float abs4 = Math.abs(motionEvent2.getY() - motionEvent.getY());
        if (abs2 >= abs || abs4 >= abs3) {
            return false;
        }
        if (abs3 > this.f20507d) {
            if (f > 0.0f) {
                z = this.f20505b.onSwipeRight();
            } else if (f < 0.0f) {
                z = this.f20505b.onSwipeLeft();
            }
        }
        return z;
    }

    public static float m14099b(Context context, float f) {
        if (context != null) {
            return (f * context.getResources().getDisplayMetrics().density) + 0.5f;
        }
        return 0.0f;
    }
}

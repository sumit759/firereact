package com.mozostar.app;

import android.app.Application;
import android.location.Location;

import androidx.multidex.MultiDex;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.facebook.stetho.Stetho;

public class MvpApplication extends Application {

    private static MvpApplication mInstance;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 100;
    public static final int PICK_LOCATION_REQUEST_CODE = 3;
    public static final int PERMISSIONS_REQUEST_PHONE = 4;
    public static final int REQUEST_CHECK_SETTINGS = 5;
    public static final int PICK_PAYMENT_METHOD = 12;
    public static final int PICK_PROMO_CODE_METHOD = 13;
    public static float DEFAULT_ZOOM = 12;
    public static Location mLastKnownLocation;


    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        mInstance = this;
        MultiDex.install(this);
        // Setting timeout globally for the download network requests:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);
    }

    public static synchronized MvpApplication getInstance() {
        return mInstance;
    }


}

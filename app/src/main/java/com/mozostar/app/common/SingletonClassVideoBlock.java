package com.mozostar.app.common;

import com.mozostar.app.data.model.VideoList_Pojo1;

public class SingletonClassVideoBlock {
    private static SingletonClassVideoBlock singletonClassVideoBlock;

    VideoList_Pojo1.Data mVideoData;

    public VideoList_Pojo1.Data getmVideoData() {
        return mVideoData;
    }

    public void setmVideoData(VideoList_Pojo1.Data mVideoData) {
        this.mVideoData = mVideoData;
    }

    /**
     * The private constructor for the Customer List Singleton class
     */
    private SingletonClassVideoBlock() {}

    public static SingletonClassVideoBlock getInstance() {
        //instantiate a new CustomerLab if we didn't instantiate one yet
        if (singletonClassVideoBlock == null) {
            singletonClassVideoBlock = new SingletonClassVideoBlock();
        }
        return singletonClassVideoBlock;
    }
}

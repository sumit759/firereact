package com.mozostar.app.common;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.mozostar.app.R;
import com.mozostar.app.data.model.VideoList_Pojo1;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomVideoReportDialog extends Dialog {

    @BindView(R.id.button_submit_report)
    Button button_submit_report;

    @BindView(R.id.block_other_et)
    EditText block_other_et;

    @BindView(R.id.main_radio_group)
    RadioGroup main_radio_group;
    Context context;
    public DialogInterface dialogInterface;
    String selectedStr = "";
    VideoList_Pojo1.Data mVideoDatas;
    String radiobuttonStr;


    public CustomVideoReportDialog(@NonNull Context context, VideoList_Pojo1.Data mVideoData, DialogInterface dialogInterface) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.dialogInterface = dialogInterface;
        this.mVideoDatas = mVideoData;
    }

    public CustomVideoReportDialog(Context context) {
        super(context);
        this.context = context;
    }

    public interface DialogInterface {
        void onSubmit(String reportTxt, VideoList_Pojo1.Data mVideoDatas);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));*/
        setContentView(R.layout.custom_video_block_dialog);
        ButterKnife.bind(this);
        main_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                radiobuttonStr = checkedRadioButton.getText().toString();
                if (radiobuttonStr.equals("Others")) {
                    block_other_et.setVisibility(View.VISIBLE);
                    selectedStr = block_other_et.getText().toString();
                } else {
                    block_other_et.setVisibility(View.GONE);
                    selectedStr = radiobuttonStr;
                }
            }
        });

    }

    @OnClick(R.id.button_submit_report)
    public void onYesClick() {
        try {
            if (radiobuttonStr.equalsIgnoreCase("Others")) {
                if (!TextUtils.isEmpty(block_other_et.getText().toString())) {
                    dialogInterface.onSubmit(block_other_et.getText().toString(), mVideoDatas);
                    dismiss();
                } else {
                    Toast.makeText(context, "Please type any comments", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!TextUtils.isEmpty(selectedStr)) {
                    dialogInterface.onSubmit(selectedStr, mVideoDatas);
                    dismiss();
                } else {
                    Toast.makeText(context, "Please choose any field", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

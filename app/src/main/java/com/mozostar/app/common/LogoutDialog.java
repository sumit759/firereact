package com.mozostar.app.common;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.mozostar.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogoutDialog extends Dialog {

    public TextView yes, no;


    @BindView(R.id.yes_dialog)
    TextView yes_dialog;

    @BindView(R.id.no_dialog)
    TextView no_dialog;
    Context context;
    public DialogInterface dialogInterface;

    public interface DialogInterface {
        void onYes();

        void onNo();
    }

    public LogoutDialog(@NonNull Context context, DialogInterface dialogInterface) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.dialogInterface = dialogInterface;
    }

    public LogoutDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));*/
        setContentView(R.layout.custom_logout_dialog);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.no_dialog)
    public void onNoClick() {
        try {
            dialogInterface.onNo();
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.yes_dialog)
    public void onYesClick() {
        try {
            dialogInterface.onYes();
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

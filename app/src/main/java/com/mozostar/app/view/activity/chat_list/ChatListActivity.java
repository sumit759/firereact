package com.mozostar.app.view.activity.chat_list;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.custom_ui.DialogManager;
import com.mozostar.app.data.ChatUserList_Pojo;
import com.mozostar.app.view.activity.chat.models.User;
import com.mozostar.app.view.activity.chat.users.get.all.GetUsersContract;
import com.mozostar.app.view.activity.chat.users.get.all.GetUsersPresenter;
import com.mozostar.app.view.adapter.ChatListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListActivity extends BaseActivity implements ChatIView, GetUsersContract.View {

    GetUsersPresenter mGetUsersPresenter;

    List<User> chatUser;

    ChatPresenter<ChatListActivity> presenter = new ChatPresenter<>();

    @BindView(R.id.chat_list)
    RecyclerView chat_list;
    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;
    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;
    @BindView(R.id.back)
    ImageView back;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chat_list;
    }
    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        tvTitle_ToolBarLay.setText("Direct message");
        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);
        mGetUsersPresenter = new GetUsersPresenter(this);
        DialogManager.showProgress(this);
        mGetUsersPresenter.getAllUsers();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    @Override
    public void onSuccess(ArrayList<ChatUserList_Pojo> trendsResponse) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(ChatListActivity.this, RecyclerView.VERTICAL, false);
        chat_list.setLayoutManager(layoutManager);
        chat_list.setHasFixedSize(false);
        int size = trendsResponse.size();
        ChatListAdapter homeScreen_adapter = new ChatListAdapter(this, trendsResponse,chatUser);
        chat_list.setAdapter(homeScreen_adapter);
        if (trendsResponse != null && size != 0) {

//            no_data.setVisibility(View.GONE);
        } else {
//            no_data.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetAllUsersSuccess(List<User> users) {
        chatUser = users;
        presenter.getList();
        DialogManager.hideProgress();
    }

    @Override
    public void onGetAllUsersFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetChatUsersSuccess(List<User> users) {

    }

    @Override
    public void onGetChatUsersFailure(String message) {

    }
}

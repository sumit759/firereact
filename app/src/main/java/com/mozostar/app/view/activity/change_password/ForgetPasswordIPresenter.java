package com.mozostar.app.view.activity.change_password;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface ForgetPasswordIPresenter<V extends ForgetPasswordIView> extends MvpPresenter<V> {

    @SuppressLint("CheckResult")
    void reset(HashMap<String, Object> obj);
}

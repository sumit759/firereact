package com.mozostar.app.view.fragment.discover;

import android.graphics.drawable.Drawable;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.view.adapter.DiscoverTrending_Adapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingDiscover_Fragment extends BaseFragment implements DiscoverIView {

    @BindView(R.id.rvSoundTrack_SoundTrackLay)
    RecyclerView rvSoundTrack_SoundTrackLay;
    @BindView(R.id.rv_SoundTrackLay)
    RecyclerView rv_SoundTrackLay;
    ArrayList<Drawable> cover_ArrayList = new ArrayList<>();
    DiscoverPresenter<TrendingDiscover_Fragment> presenter = new DiscoverPresenter<>();
    ArrayList<Discover_Pojo.Video> discover_pojos = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_trending_discover_;
    }

    @Override
    public View initView(View view) {
        ButterKnife.bind(this,view);

        presenter.attachView(this);
        presenter.getList();

        rv_SoundTrackLay.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rv_SoundTrackLay.setItemAnimator(new DefaultItemAnimator());

        rvSoundTrack_SoundTrackLay.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvSoundTrack_SoundTrackLay.setItemAnimator(new DefaultItemAnimator());


        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbline));
        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbnail2));
        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbnail3));
        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbnail4));
        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbnail5));
        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbnail6));
        cover_ArrayList.add(getResources().getDrawable(R.drawable.thumbnail7));

//        TreandingExploreAdapter soundTrack_adapter = new TreandingExploreAdapter(getContext(), cover_ArrayList);
//        rv_SoundTrackLay.setAdapter(soundTrack_adapter);
//        TrendingAdapter trendingAdapter = new TrendingAdapter(getContext(), cover_ArrayList);
//        rvSoundTrack_SoundTrackLay.setAdapter(trendingAdapter);

        return view;
    }

    @Override
    public void onSuccess(Discover_Pojo trendsResponse) {
        discover_pojos = (ArrayList<Discover_Pojo.Video>) trendsResponse.getVideos();
        DiscoverTrending_Adapter homeScreen_adapter = new DiscoverTrending_Adapter(getActivity(), discover_pojos);
        rvSoundTrack_SoundTrackLay.setAdapter(homeScreen_adapter);
    }

    @Override
    public void onSuccessUser(ArrayList<User_Pojo> trendsResponse) {

    }

    @Override
    public void onSuccessExploreVideoList(ExploreChannelVideoResponse exploreList) {

    }
}

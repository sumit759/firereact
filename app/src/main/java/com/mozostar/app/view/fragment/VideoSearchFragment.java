package com.mozostar.app.view.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.HashSearch_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.data.model.UserVideosList_Pojo;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.view.activity.search.SearchIView;
import com.mozostar.app.view.activity.search.SearchPresenter;
import com.mozostar.app.view.adapter.VideoSearchAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoSearchFragment extends BaseFragment implements SearchIView {

    @BindView(R.id.searchUserRV)
    RecyclerView searchUserRV;

    @BindView(R.id.no_data_search_user)
    TextView no_data_search_user;

    @BindView(R.id.fragment_name_tv)
    TextView fragment_name_tv;

    SearchPresenter<VideoSearchFragment> presenter = new SearchPresenter<VideoSearchFragment>();

    @Override
    public int getLayoutId() {
        return R.layout.search_user_fragment;
    }

    Bundle extras;
    ArrayList<UserVideosList_Pojo> videoResponseArrayList = new ArrayList<>();

    LinearLayoutManager layoutManager;

    @Override
    public View initView(View view) {
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        try {
            fragment_name_tv.setText("Video");
            Bundle bundle = this.getArguments();
            String strtext = bundle.getString("searchingKey");
            callFromActivity(strtext);
            //  presenter.getUserList(extras.getString("fromAct"));
            layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            searchUserRV.setLayoutManager(layoutManager);
            searchUserRV.setHasFixedSize(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void callFromActivity(String searchingKey) {
        try {
            if (!TextUtils.isEmpty(searchingKey)) {
                presenter.getVideoList(searchingKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessUser(ArrayList<User_Pojo> userResponse) {

    }

    @Override
    public void onSuccessHash(ArrayList<HashSearch_Pojo> hashSearchPojos) {

    }

    @Override
    public void onSuccessVideo(ArrayList<UserVideosList_Pojo> videoResponseList) {
        try {
            if (videoResponseList.size() > 0) {
                videoResponseArrayList = videoResponseList;
                searchUserRV.setVisibility(View.VISIBLE);
                VideoSearchAdapter videoSearchAdapter = new VideoSearchAdapter(getContext(), videoResponseArrayList);
                searchUserRV.setAdapter(videoSearchAdapter);
            } else {
                searchUserRV.setVisibility(View.GONE);
                no_data_search_user.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessSound(ArrayList<Sounds_Pojo> sounds_pojoList) {

    }
}

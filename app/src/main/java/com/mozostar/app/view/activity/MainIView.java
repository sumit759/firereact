package com.mozostar.app.view.activity;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.view.adapter.CommentsAdapter;

public interface MainIView extends MvpView {
    void onSuccess(CommentsAdapter list);

    void onSuccessComment(CommentPost_Pojo trendsResponse);
}

package com.mozostar.app.view.activity.privacy;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Safety_Pojo;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrivacyActivity extends BaseActivity implements PrivacyIView {

    @BindView(R.id.like_safety_layout)
    RelativeLayout like_safety_layout;

    @BindView(R.id.comment_safety_layout)
    RelativeLayout comment_safety_layout;

    @BindView(R.id.react_safety_layout)
    RelativeLayout react_safety_layout;

    String likedSafetyResult, commentSafetyResult, reactSafetyResult;

    @BindView(R.id.liked_status_text)
    TextView liked_status_text;

    @BindView(R.id.comment_status_text)
    TextView comment_status_text;

    @BindView(R.id.react_status_text)
    TextView react_status_text;

    int likedStatusCount, commentStatusCount, reactStatusCount;
    PrivacyPresenter<PrivacyActivity> presenter = new PrivacyPresenter<>();

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @Override
    public int getLayoutId() {
        return R.layout.activity_privacy;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);

        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);
        tvTitle_ToolBarLay.setText(R.string.privacy_safety);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        likedStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "likedStatusCount");
        commentStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "commentStatusCount");
        reactStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "reactStatusCount");

        like_safety_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingPrivacy = new Intent(PrivacyActivity.this, SafetyStatusActivity.class);
                settingPrivacy.putExtra("action_activity","like_activity");
                startActivityForResult(settingPrivacy, 1);
            }
        });

        comment_safety_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingPrivacy = new Intent(PrivacyActivity.this, SafetyStatusActivity.class);
                settingPrivacy.putExtra("action_activity","comment_activity");
                startActivityForResult(settingPrivacy, 2);
            }
        });

        react_safety_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingPrivacy = new Intent(PrivacyActivity.this, SafetyStatusActivity.class);
                settingPrivacy.putExtra("action_activity","react_activity");
                startActivityForResult(settingPrivacy, 3);
            }
        });



        getStatusCount();


    }

    private void getStatusCount() {
        if (likedStatusCount == 0) {
            liked_status_text.setText("Everyone");
        }

        if (commentStatusCount == 0) {
            comment_status_text.setText("Everyone");
        }

        if (reactStatusCount == 0) {
            react_status_text.setText("Everyone");
        }

        if (likedStatusCount == 1) {
            liked_status_text.setText("Friends");
        }

        if (commentStatusCount == 1) {
            comment_status_text.setText("Friends");
        }

        if (reactStatusCount == 1) {
            react_status_text.setText("Friends");
        }

        if (likedStatusCount == 2) {
            liked_status_text.setText("Only me");
        }

        if (commentStatusCount == 2) {
            comment_status_text.setText("Only me");
        }

        if (reactStatusCount == 2) {
            react_status_text.setText("Only me");
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 1:
                if (resultCode == RESULT_OK && data != null) {

                    likedSafetyResult = data.getStringExtra("likedStatus");

                    if (likedSafetyResult != null && !likedSafetyResult.equalsIgnoreCase("")) {
                        if (likedSafetyResult.equalsIgnoreCase("Everyone")) {
                            likedStatusCount = 0;
                        } else if (likedSafetyResult.equalsIgnoreCase("Friends")) {
                            likedStatusCount = 1;
                        } else if (likedSafetyResult.equalsIgnoreCase("Only me")) {
                            likedStatusCount = 2;
                        }
                    } else {
                        likedStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "likedStatusCount");
                        getStatusCount();
                    }
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("like_status", likedStatusCount);
                    // map.put("comment_status", 0);
                    // map.put("react_status", 0);
                    presenter.getSafetyCount(map);

                    if (likedSafetyResult != null && !likedSafetyResult.equalsIgnoreCase("")) {
                        liked_status_text.setText(likedSafetyResult);
                    } else {
                        likedStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "likedStatusCount");
                        getStatusCount();
                    }
                }
                break;
            case 2:
                if (resultCode == RESULT_OK && data != null) {
                    commentSafetyResult = data.getStringExtra("commentStatus");

                    if (commentSafetyResult != null && !commentSafetyResult.equalsIgnoreCase("")) {
                        if (commentSafetyResult.equalsIgnoreCase("Everyone")) {
                            commentStatusCount = 0;
                        } else if (commentSafetyResult.equalsIgnoreCase("Friends")) {
                            commentStatusCount = 1;
                        } else if (commentSafetyResult.equalsIgnoreCase("Only me")) {
                            commentStatusCount = 2;
                        }
                    } else {
                        commentStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "commentStatusCount");
                        getStatusCount();
                    }
                    if (commentSafetyResult != null && !commentSafetyResult.equalsIgnoreCase("")) {
                        comment_status_text.setText(commentSafetyResult);
                        HashMap<String, Object> map = new HashMap<>();
                        //  map.put("like_status", 0);
                        map.put("comment_status", commentStatusCount);
                        //  map.put("react_status", 0);
                        presenter.getSafetyCount(map);
                    } else {
                        commentStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "commentStatusCount");
                        getStatusCount();
                    }
                }
                break;
            case 3:
                if (resultCode == RESULT_OK && data != null) {
                    reactSafetyResult = data.getStringExtra("reactStatus");

                    if (reactSafetyResult != null && !reactSafetyResult.equalsIgnoreCase("")) {
                        if (reactSafetyResult.equalsIgnoreCase("Everyone")) {
                            reactStatusCount = 0;
                        } else if (reactSafetyResult.equalsIgnoreCase("Friends")) {
                            reactStatusCount = 1;
                        } else if (reactSafetyResult.equalsIgnoreCase("Only me")) {
                            reactStatusCount = 2;
                        }
                    } else {
                        reactStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "reactStatusCount");
                        getStatusCount();
                    }
                    if (reactSafetyResult != null && !reactSafetyResult.equalsIgnoreCase("")) {
                        react_status_text.setText(reactSafetyResult);
                        HashMap<String, Object> map = new HashMap<>();
                        //  map.put("like_status", 0);
                        //  map.put("comment_status", 0);
                        map.put("react_status", reactStatusCount);
                        presenter.getSafetyCount(map);
                    } else {
                        reactStatusCount = SharedHelper.getIntKey(PrivacyActivity.this, "reactStatusCount");
                        getStatusCount();
                    }

                }
                    break;
                }
        }

    @Override
    public void onSuccess(Safety_Pojo safetyResponse) {
        Log.e("Success Safety activity","Success Safety activity");
        Toast.makeText(PrivacyActivity.this, "Updated Successfully", Toast.LENGTH_LONG).show();
    }
}

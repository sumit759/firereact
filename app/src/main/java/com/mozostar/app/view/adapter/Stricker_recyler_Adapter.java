package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.common.FilterType;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Stricker_recyler_Adapter extends RecyclerView.Adapter<Stricker_recyler_Adapter.MyViewHolder> {
    Context context;
    List<FilterType> filterList;
    int[] imgList = {R.drawable.sticker_1, R.drawable.sticker_2, R.drawable.sticker_3, R.drawable.sticker_4};
    Boolean[] selection = {false, false, false, false};


    int clickPosition;


    public Stricker_recyler_Adapter(Context context, List<FilterType> filterTypes) {
        this.context = context;
        this.filterList = filterTypes;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.strickers_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.name.setText("F1" + i);
        myViewHolder.name.setVisibility(View.GONE);
        myViewHolder.image.setImageResource(imgList[i]);


        myViewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selection[clickPosition] = false;
                selection[i] = true;
                notifyDataSetChanged();
            }
        });


        if (selection[i]) {
            clickPosition = i;
            myViewHolder.itemLayout.setBackgroundResource(R.drawable.selection);
        } else {
            myViewHolder.itemLayout.setBackgroundResource(0);
        }


    }

    @Override
    public int getItemCount() {
        return imgList.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.filter_item_layout)
        LinearLayout itemLayout;
        @BindView(R.id.filter_item_text)
        TextView name;
        @BindView(R.id.filter_item_image)
        ImageView image;

        MyViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "ok", Toast.LENGTH_SHORT).show();
                }
            });


        }


    }
}

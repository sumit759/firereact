package com.mozostar.app.view.fragment.guestfragment;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GuestVideoPresenter<V extends GuestVideoIView> extends BasePresenter<V> implements GuestVideoIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void getList(String id) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().guest_like_videos(id);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccess((ArrayList<ProfileVideoList_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    @Override
    public void getDubList(String id) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().guest_videos(id);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessDub((ArrayList<ProfileVideoList_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }
}

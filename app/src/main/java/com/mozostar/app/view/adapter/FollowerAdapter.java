package com.mozostar.app.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.R;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Follower_list;
import com.mozostar.app.view.activity.follow.InteractionListener;
import com.mozostar.app.view.activity.guest.GuestProfile_Activity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.MyViewHolder> {

    ArrayList<Follower_list> mlist_arraylist;
    Context mContext;
    String from;
    InteractionListener listener;

    public FollowerAdapter(Context context, ArrayList<Follower_list> list_arraylist) {
        mContext = context;
        mlist_arraylist = list_arraylist;
        this.listener = (InteractionListener) context;
    }


    @NonNull
    @Override
    public FollowerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.follow_adapter, parent, false);
        return new FollowerAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull FollowerAdapter.MyViewHolder myViewHolder, int i) {
        try {
            from = SharedHelper.getKey(mContext, "on_click_follow");

            int user_id = SharedHelper.getIntKey(mContext, "user_id");

            myViewHolder.user_prfl_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent guest = new Intent(mContext, GuestProfile_Activity.class);
                    guest.putExtra("guest_id", "" + mlist_arraylist.get(i).getUserId());
                    mContext.startActivity(guest);
                }
            });

            if (mlist_arraylist.get(i).getFollowers() != null) {
                if (mlist_arraylist.get(i).getFollowers().getImage() != null)
                    Glide.with(mContext).load(mlist_arraylist.get(i).getFollowers().getImage()).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate().error(R.drawable.user_profile)).into(myViewHolder.user_prfl_img);

                myViewHolder.user_id.setText(mlist_arraylist.get(i).getFollowers().getFirstName() + " " + mlist_arraylist.get(i).getFollowers().getFirstName());

            }
            if (mlist_arraylist.get(i).getStatus() != null) {
                if (mlist_arraylist.get(i).getUserId() == user_id) {
                    myViewHolder.follow_btn.setVisibility(View.GONE);
                } else if (mlist_arraylist.get(i).getStatus().equals("following")) {
                    myViewHolder.follow_btn.setText("Following");
                    myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                    myViewHolder.follow_btn.setBackgroundResource(R.drawable.button_border);
                } else if (mlist_arraylist.get(i).getStatus().equals("friends")) {
                    myViewHolder.follow_btn.setText("Friends");
                    myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                    myViewHolder.follow_btn.setBackgroundResource(R.drawable.button_border);
                } else if (mlist_arraylist.get(i).getStatus().equals("unfollow")) {
                    myViewHolder.follow_btn.setText("Follow");
                    myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                    myViewHolder.follow_btn.setBackgroundResource(R.drawable.ovel_gradient_button);

                } else if (mlist_arraylist.get(i).getStatus().equals("0")) {
                    myViewHolder.follow_btn.setText("Follow");
                    myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                    myViewHolder.follow_btn.setBackgroundResource(R.drawable.ovel_gradient_button);

                } else if (mlist_arraylist.get(i).getStatus().equals("1")) {
                    myViewHolder.follow_btn.setText("Friends");
                    myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                    myViewHolder.follow_btn.setBackgroundResource(R.drawable.button_border);
                } else {
                    myViewHolder.follow_btn.setText("Follow");
                    myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                    myViewHolder.follow_btn.setBackgroundResource(R.drawable.ovel_gradient_button);

                }
            } else {
                myViewHolder.follow_btn.setText("Follow");
                myViewHolder.follow_btn.setTextColor(mContext.getResources().getColor(R.color.white));
                myViewHolder.follow_btn.setBackgroundResource(R.drawable.ovel_gradient_button);

            }

            myViewHolder.follow_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.follow("" + mlist_arraylist.get(i).getFollowers().getId());
                }
            });
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mlist_arraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.user_prfl_img)
        CircleImageView user_prfl_img;

        @BindView(R.id.follow_btn)
        Button follow_btn;

        @BindView(R.id.user_id)
        TextView user_id;

        @BindView(R.id.following_lay)
        LinearLayout following_lay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

package com.mozostar.app.view.login;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;

import com.google.firebase.auth.FirebaseUser;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Login_Pojo;
import com.mozostar.app.data.model.Reset_Pojo;
import com.mozostar.app.view.activity.MainActivity;
import com.mozostar.app.view.activity.change_password.ForgetPasswordActivity;
import com.mozostar.app.view.activity.chat.login.LoginContract;
import com.mozostar.app.view.activity.chat.login.LoginPresenterchat;
import com.mozostar.app.view.activity.chat.registration.RegisterContract;
import com.mozostar.app.view.activity.chat.registration.RegisterPresenter;
import com.mozostar.app.view.activity.chat.users.add.AddUserContract;
import com.mozostar.app.view.activity.chat.users.add.AddUserPresenter;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Login_Activity extends BaseFragment implements LoginIView, LoginContract.View, RegisterContract.View, AddUserContract.View {
    Context context;
    private static String TAG = Login_Activity.class.getSimpleName();

    private LoginPresenterchat mLoginPresenter;   //for chat
    private RegisterPresenter mRegisterPresenter;  //for chat
    private AddUserPresenter mAddUserPresenter;   // for chat

    private String UserName, password;
    @BindView(R.id.ivBack_LoginLay)
    ImageView ivBack_LoginLay;
    @BindView(R.id.etUserName_LoginLay)
    AppCompatEditText etUserName_LoginLay;
    @BindView(R.id.etPassword_LoginLay)
    AppCompatEditText etPassword_LoginLay;
    @BindView(R.id.sign_up)
    TextView sign_up;
    @BindView(R.id.email_login_field_layout)
    LinearLayout email_login_field_layout;
    @BindView(R.id.phone_login_field_layout)
    LinearLayout phone_login_field_layout;
    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;
    @BindView(R.id.etOtpPhoneNumber)
    AppCompatEditText etOtpPhoneNumber;
    @BindView(R.id.forgot_pswd_txt)
    TextView forgot_pswd_txt;
    @BindView(R.id.phone_login_layout)
    LinearLayout phone_login_layout;
    @BindView(R.id.email_login_layout)
    LinearLayout email_login_layout;
    @BindView(R.id.email_username_view)
    View email_username_view;
    @BindView(R.id.phone_login_view)
    View phone_login_view;
    @BindView(R.id.email_username_txt)
    TextView email_username_txt;
    @BindView(R.id.phone_login_txt)
    TextView phone_login_txt;
    @BindView(R.id.facebook_layout)
    RelativeLayout facebook_layout;
    @BindView(R.id.gmail_layout)
    RelativeLayout gmail_layout;

    private LoginPresenter<Login_Activity> presenter = new LoginPresenter<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);

        phone_login_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email_username_txt.setTextColor(getResources().getColor(R.color.dark_grey));
                email_username_view.setBackgroundColor(getResources().getColor(R.color.dark_grey));
                email_login_field_layout.setVisibility(View.GONE);
                //foryou_txt.setTypeface(foryou_txt.getTypeface(), Typeface.BOLD);

                phone_login_txt.setTextColor(getResources().getColor(R.color.black));
                phone_login_view.setBackgroundColor(getResources().getColor(R.color.black));
                phone_login_field_layout.setVisibility(View.VISIBLE);
                // following_txt.setTypeface(following_txt.getTypeface(), Typeface.NORMAL);
            }
        });

        email_login_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email_username_txt.setTextColor(getResources().getColor(R.color.black));
                email_username_view.setBackgroundColor(getResources().getColor(R.color.black));
                email_login_field_layout.setVisibility(View.VISIBLE);
                //foryou_txt.setTypeface(foryou_txt.getTypeface(), Typeface.BOLD);

                phone_login_txt.setTextColor(getResources().getColor(R.color.dark_grey));
                phone_login_view.setBackgroundColor(getResources().getColor(R.color.dark_grey));
                phone_login_field_layout.setVisibility(View.GONE);
                // following_txt.setTypeface(following_txt.getTypeface(), Typeface.NORMAL);

            }
        });


        return view;
    }

    @OnClick({R.id.ivBack_LoginLay, R.id.fabDone_LoginLay, R.id.sign_up, R.id.forgot_pswd_txt})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.ivBack_LoginLay) {
//            onBackPressed();
        } else if (view.getId() == R.id.fabDone_LoginLay) {
            UserName = "" + etUserName_LoginLay.getText().toString().trim();
            password = "" + etPassword_LoginLay.getText().toString().trim();

            if (UserName.isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                return;
            }
            if (password.length() < 6) {
                Toast.makeText(getActivity(), getString(R.string.invalid_password), Toast.LENGTH_SHORT).show();
                return;
            }

            HashMap<String, Object> map = new HashMap<>();
            map.put("email", UserName);
            map.put("password", password);
            map.put("device_type", "Android");
            map.put("device_token", "123456");
            map.put("device_id", "1234567");
            presenter.postlogin(map);
        } else if (view.getId() == R.id.sign_up) {
//            Intent register = new Intent(getActivity(), Register_Activity.class);
//            startActivity(register);
        } else if (view.getId() == R.id.forgot_pswd_txt) {
            String UserName = "" + etUserName_LoginLay.getText().toString().trim();
            if (UserName.isEmpty()) {
                Toast.makeText(getActivity(), "Please enter your Email ID", Toast.LENGTH_SHORT).show();
            } else {
                HashMap<String, Object> map = new HashMap<>();
                map.put("email", UserName);
                presenter.reset(map);
            }
        }
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }

    @Override
    public void onSuccessLogin(Login_Pojo login_pojo) {
        String accessToken = "Bearer " + login_pojo.getToken();
        SharedHelper.putKey(getActivity(), "access_token", accessToken);
        SharedHelper.putKey(getActivity(), "first_name", login_pojo.getFirstName());
        SharedHelper.putKey(getActivity(), "last_name", login_pojo.getLastName());
        SharedHelper.putKey(getActivity(), "user_id", login_pojo.getId());
        SharedHelper.putKey(getActivity(), "logged_in", true);

        mLoginPresenter = new LoginPresenterchat(this);
        mLoginPresenter.login(getActivity(), UserName, "vlogy123");
    }


    @Override
    public void onSuccessReset(Reset_Pojo trendsResponse) {
        Intent change_password = new Intent(getActivity(), ForgetPasswordActivity.class);

        change_password.putExtra("user_id", "" + trendsResponse.getUserId());
        change_password.putExtra("otp", "" + trendsResponse.getOtp());

        startActivity(change_password);
        getActivity().finishAffinity();
        getActivity().finish();
    }

    @Override
    public void onLoginSuccess(String message) {
        getActivity().finishAffinity();
        ActivityManager.redirectToActivityWithoutBundle(context, MainActivity.class);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();
    }

    @Override
    public void onLoginFailure(String message) {
        mRegisterPresenter = new RegisterPresenter(this);
        mRegisterPresenter.register(Objects.requireNonNull(getActivity()), UserName, "vlogy123");
    }


    @Override
    public void onRegistrationSuccess(FirebaseUser firebaseUser) {
        mAddUserPresenter = new AddUserPresenter(this);
        mAddUserPresenter.addUser(getActivity(), firebaseUser);
    }

    @Override
    public void onRegistrationFailure(String message) {
    }

    @Override
    public void onAddUserSuccess(String message) {
        getActivity().finishAffinity();
        ActivityManager.redirectToActivityWithoutBundle(context, MainActivity.class);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();
    }

    @Override
    public void onAddUserFailure(String message) {
    }
}

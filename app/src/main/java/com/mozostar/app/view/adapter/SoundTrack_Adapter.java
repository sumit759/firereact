package com.mozostar.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.view.activity.videos_view.ViewVideosActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoundTrack_Adapter extends RecyclerView.Adapter<SoundTrack_Adapter.MyViewHolder> {
    Context context;
    ArrayList<Discover_Pojo.Video_> discover;

    public SoundTrack_Adapter(Context context,  ArrayList<Discover_Pojo.Video_>  cover_ArrayList) {
        this.context = context;
        this.discover = cover_ArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trendingchild_rowitem, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Glide.with(context).load("" + discover.get(i).getVideo()).into(myViewHolder.ivTrendImage_TrendChildLay);
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedHelper.putKey(context, "video_view", "guest_dub");

                Intent intent = new Intent(context, ViewVideosActivity.class);
//                intent.putExtra("list",cover_ArrayList );
                intent.putExtra("position",""+myViewHolder.getAdapterPosition());
                intent.putExtra("guest_id",""+discover.get(i).getUserId());
                context.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return discover.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivTrendImage_TrendChildLay)
        ImageView ivTrendImage_TrendChildLay;
        @BindView(R.id.llComments_TrendChildLay)
        LinearLayout llComments_TrendChildLay;
        @BindView(R.id.ivLogo_TrendChildLay)
        ImageView ivLogo_TrendChildLay;
        @BindView(R.id.tvComments_TrendChildLay)
        TextView tvComments_TrendChildLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            llComments_TrendChildLay.setVisibility(View.GONE);
            ivLogo_TrendChildLay.setVisibility(View.GONE);
            tvComments_TrendChildLay.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

package com.mozostar.app.view.activity.banner;

import com.mozostar.app.base.MvpView;

public interface BannerIView extends MvpView {
    void redirectHome();
}

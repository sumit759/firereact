package com.mozostar.app.view.activity;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.data.model.DeletePojo;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.activity.videos_view.ViewVideosIView;
import com.mozostar.app.view.activity.videos_view.ViewVideosPresenter;
import com.mozostar.app.view.adapter.CommentsAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExoPlayerActivity extends BaseActivity implements ViewVideosIView, Player.EventListener{

    @BindView(R.id.exo_video_player_act)
    PlayerView exo_video_player_act;

    SimpleExoPlayer previous_exoplayer;

    @Override
    public int getLayoutId() {
        return R.layout.activity_exo_player;
    }

    Context context;
    String position, guest_id, from, position_list,video;
    String TAG = "Exo";

    ViewVideosPresenter<ExoPlayerActivity> presenter = new ViewVideosPresenter<>();

    @Override
    public void initView() {
        ButterKnife.bind(this);
        context = ExoPlayerActivity.this;
        presenter.attachView(this);

        position = getIntent().getStringExtra("position");
        position_list = getIntent().getStringExtra("position_list");
        guest_id = getIntent().getStringExtra("guest_id");
        video = getIntent().getStringExtra("video");

        from = SharedHelper.getKey(this, "video_view");

        final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this, (TrackSelector) new DefaultTrackSelector());
        newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this, Util.getUserAgent(this, "TikTok")))
                .createMediaSource(Uri.parse(video)));
        newSimpleInstance.addListener(this);

        exo_video_player_act.setPlayer(newSimpleInstance);
        newSimpleInstance.setPlayWhenReady(true);
        previous_exoplayer = newSimpleInstance;
        exo_video_player_act.setControllerAutoShow(true);
        exo_video_player_act.setUseController(true);
        exo_video_player_act.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

      /*  if (!from.isEmpty()) {
            if (from.equals("profile_dub")) {
                presenter.getProfileDubList();
            } else if (from.equals("profile_like")) {
                presenter.getProfileList();
            } else if (from.equals("guest_dub")) {
                presenter.getDubList(guest_id);
            } else if (from.equals("guest_like")) {
                presenter.getList(guest_id);
            } else if (from.equals("channel_video_list")) {
                presenter.getChannelList(String.valueOf(SharedHelper.getIntKey(this, "id")));
            } else if (from.equals("hash_tag")) {
                presenter.getHashList();
            } else {
                Toast.makeText(this, "Somthing went wrong", Toast.LENGTH_LONG).show();
            }
        }*/
    }

    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

    }

    @Override
    public void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse) {
        ArrayList<ProfileVideoList_Pojo> list = new ArrayList<>();
        ArrayList<String> cover_ArrayList = new ArrayList<>();
        list = trendsResponse;
        int size = trendsResponse.size();
        for (int i = 0; i < size; i++) {
            cover_ArrayList.add(trendsResponse.get(i).getVideo());
            final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this, (TrackSelector) new DefaultTrackSelector());
            newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this, Util.getUserAgent(this, "TikTok")))
                    .createMediaSource(Uri.parse(cover_ArrayList.get(i))));
            newSimpleInstance.addListener(this);

            exo_video_player_act.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;
            exo_video_player_act.setControllerAutoShow(true);
            exo_video_player_act.setUseController(true);
        }

    }
    public void onResume() {
        super.onResume();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(true);
        }
    }

    public void stopPreviousVideoPlayer() {
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.removeListener(this);
            this.previous_exoplayer.release();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.release();
        }
    }

    @Override
    public void onSuccessLike(Object trendsResponse) {

    }

    @Override
    public void onSuccessComment(CommentPost_Pojo trendsResponse) {

    }

    @Override
    public void onSuccessComments(CommentsAdapter adapter) {

    }

    @Override
    public void onSuccessHash(Discover_Pojo trendsResponse) {

    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {

    }

    @Override
    public void onSuccessDelete(DeletePojo trendsResponse) {

    }

    @Override
    public void onSuccessChannelVideoList(ChannelVideoList_Pojo trendsResponse) {

    }

   /*000000000000000000000000000000000000000000000000000000000000000*/
    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

}
package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.mozostar.app.R;
import com.mozostar.app.view.fragment.homedashboard.Home_Fragment;
import com.mozostar.app.view.fragment.homedashboard.PagerAdapterOnClick;
import com.mozostar.app.view.fragment.homedashboard.Video_View_Fragment;

import java.util.ArrayList;



public class HomeScreen_PagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> slidercarlist_arraylist;
    PagerAdapterOnClick pagerAdapterClicK;


    public HomeScreen_PagerAdapter(Context context, Home_Fragment home_fragment, ArrayList<String> list_arraylist) {
        this.mContext = context;
        this.slidercarlist_arraylist = list_arraylist;
        this.pagerAdapterClicK = home_fragment;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public HomeScreen_PagerAdapter(Context context, Video_View_Fragment video_view_fragment, ArrayList<String> mVideoURLList) {
        this.mContext = context;
        this.slidercarlist_arraylist = mVideoURLList;
        this.pagerAdapterClicK = video_view_fragment;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return slidercarlist_arraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ConstraintLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.home_frag_rowitem, container, false);
        RelativeLayout rrGuestProfile_HomeFragLay = itemView.findViewById(R.id.rrGuestProfile_HomeFragLay);
        ImageView ivSoundTrack_HomeFragLay = itemView.findViewById(R.id.ivSoundTrack_HomeFragLay);

//        VideoView videoView = itemView.findViewById(R.id.video_view);
//        videoView.getVideoInfo().setBgColor(Color.BLACK).setAspectRatio(VideoInfo.AR_MATCH_PARENT);//config player
//        videoView.setVideoPath(""+slidercarlist_arraylist.get(position)).setFingerprint(position);
       // videoView.setVideoPath(""+slidercarlist_arraylist.get(position)).getPlayer().start();


//        rrGuestProfile_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pagerAdapterClicK.onClickGuestProfile();
//            }
//        });
//        ivSoundTrack_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pagerAdapterClicK.onClickSoundTrack();
//            }
//        });


        notifyDataSetChanged();

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout) object);
    }


}

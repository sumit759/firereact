package com.mozostar.app.view.activity.chat_list;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.ChatUserList_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChatPresenter  <V extends ChatIView> extends BasePresenter<V> implements ChatIPresenter<V>  {

    @SuppressLint("CheckResult")
    @Override
    public void getList() {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().getChatList();
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccess((ArrayList<ChatUserList_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }
}

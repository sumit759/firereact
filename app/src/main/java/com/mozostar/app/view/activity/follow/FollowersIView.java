package com.mozostar.app.view.activity.follow;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.Follower_list;
import com.mozostar.app.data.model.FollowingLIst_pojo;

import java.util.ArrayList;

public interface FollowersIView extends MvpView {
    void onSuccess(ArrayList<FollowingLIst_pojo> trendsResponse);

    void onSuccessFollower(ArrayList<Follower_list> trendsResponse);

    void onSuccessFollow(Follow_Pojo trendsResponse);
}

package com.mozostar.app.view.fragment.profilefragment;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.bigvideo.getlist.BigVideoGetListResponse;
import com.mozostar.app.data.model.bigvideo.getlist.VideosItem;
import com.mozostar.app.view.DividerGridItemDecoration;
import com.mozostar.app.view.activity.ExoPlayerActivity;
import com.mozostar.app.view.activity.bigvideo.BigVideoIView;
import com.mozostar.app.view.activity.bigvideo.BigVideoPresenter;
import com.mozostar.app.view.adapter.BigVideoGridAdapter;
import com.mozostar.app.view.fragment.guestfragment.GuestDub_Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileBigVideo_Fragment extends BaseFragment implements BigVideoIView,BigVideoGridAdapter.OnItemClickListener{
    private static String TAG = GuestDub_Fragment.class.getSimpleName();
    Context context;

    @BindView(R.id.profile_big_video_list)
    RecyclerView profile_big_video_list;

    BigVideoPresenter<ProfileBigVideo_Fragment> presenter = new BigVideoPresenter<ProfileBigVideo_Fragment>();

    BigVideoGridAdapter bigVideoGridAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile_big_video;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        context = getActivity();
        try {
            presenter.getBigVideoList(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        profile_big_video_list.setLayoutManager(new GridLayoutManager(context, 3));
        profile_big_video_list.setHasFixedSize(true);
        profile_big_video_list.addItemDecoration(new DividerGridItemDecoration(context));
        return view;
    }


    @Override
    public void onSuccessBigVideoList(BigVideoGetListResponse bigVideoGetListResponse) {
        try {
            if (bigVideoGetListResponse.getVideos().size() > 0) {
                bigVideoGridAdapter = new BigVideoGridAdapter(context, bigVideoGetListResponse.getVideos(), this);
                profile_big_video_list.setAdapter(bigVideoGridAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(int position, VideosItem videosItem) {
        try {
            Intent intent = new Intent(context, ExoPlayerActivity.class);
            intent.putExtra("position", "" + position);
            intent.putExtra("guest_id", "" + videosItem.getUserId());
            intent.putExtra("video", "" + videosItem.getVideo());
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

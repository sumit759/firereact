package com.mozostar.app.view.postdubvideo;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.VideoUpload_pojo;
import com.mozostar.app.data.model.bigvideo.postresponse.BigVideoUploadResponse;

public interface PostDubVideoIView extends MvpView {

    void onSuccessRegister(VideoUpload_pojo register_pojo);

    void onSuccessBigVideoUpload(BigVideoUploadResponse bigVideoUploadResponse);
}

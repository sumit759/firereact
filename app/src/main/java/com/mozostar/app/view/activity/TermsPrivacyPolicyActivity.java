package com.mozostar.app.view.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TermsPrivacyPolicyActivity extends BaseActivity {

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.terms_privacy_policy_tv)
    TextView terms_privacy_policy_tv;

    String comesFrom = "";

    @Override
    public int getLayoutId() {
        return R.layout.activity_terms_privacy_policy;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);

        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);
        comesFrom = getIntent().getStringExtra("fromAct");
        if (comesFrom.equalsIgnoreCase("terms")) {
            tvTitle_ToolBarLay.setText("Terms and conditions");
            terms_privacy_policy_tv.setText("Terms");
        } else if (comesFrom.equalsIgnoreCase("privacy_policy")) {
            tvTitle_ToolBarLay.setText("Privacy & Policy");
            terms_privacy_policy_tv.setText(" Privacy");
        }

    }

    @OnClick(R.id.back)
    public void onBackClick() {
        onBackPressed();
        finish();
    }
}
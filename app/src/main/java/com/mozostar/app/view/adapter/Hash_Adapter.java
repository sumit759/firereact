package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.model.HashSearch_Pojo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class Hash_Adapter extends RecyclerView.Adapter<Hash_Adapter.MyViewHolder> {
    Context context;
    ArrayList<String> stringArrayList;


    ArrayList<HashSearch_Pojo> discover_pojos;


    public Hash_Adapter(Context context, ArrayList<HashSearch_Pojo> discover_pojos) {
        this.context = context;
        this.discover_pojos = discover_pojos;
    }


    @NonNull
    @Override
    public Hash_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_adapter, parent, false);
        return new Hash_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Hash_Adapter.MyViewHolder myViewHolder, int i) {

//        if (discover_pojos.get(i).getImage() != null)
//            Glide.with(context).load(discover_pojos.get(i).getImage())
//                    .apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate()
//                            .error(R.drawable.user_profile)).into(myViewHolder.user_img);
//
//
//
//        if (discover_pojos.get(i).getUserBio() != null)
//            myViewHolder.user.setText("" + discover_pojos.get(i).getUserBio());

        HashSearch_Pojo hashSearch_pojo = discover_pojos.get(i);
        try {
            myViewHolder.user.setText(hashSearch_pojo.getTagName());
            myViewHolder.user_bio.setText(hashSearch_pojo.getCount());
            Glide.with(context).load(R.drawable.user_profile).into(myViewHolder.user_img);
          /*  myViewHolder.user_bio.setText(user_pojo.getUserBio());
            if (hashSearch_pojo.get() != null) {
                Glide.with(context).load("" + user_pojo.getImage()).into(myViewHolder.user_img);
            } else {

            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    public int getItemCount() {
        return discover_pojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.user_img)
        CircleImageView user_img;
        @BindView(R.id.user)
        TextView user;
        @BindView(R.id.user_bio)
        TextView user_bio;

        MyViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);


        }

        @Override
        public void onClick(View v) {

        }
    }
}

/*
    String search = SharedHelper.getKey(context, "search");
        if (search.equals("post_user")) {
                myViewHolder.user_img.setVisibility(View.VISIBLE);
                myViewHolder.user.setText("" + discover_pojos.get(i).getTagName());
                } else if (search.equals("hash")) {
                myViewHolder.user_img.setVisibility(View.GONE);
                myViewHolder.user.setText("" +discover_pojos.get(i).getTagName());
                }
                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {


        if (search.equals("post_user")) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("id", "" + discover_pojos.get(i).getId());
        returnIntent.putExtra("name", discover_pojos.get(i).getTagName());
        ((Activity) v.getContext()).setResult(Activity.RESULT_OK, returnIntent);
        ((Activity) v.getContext()).finish();
        } else if (search.equals("hash")) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("id", "" + discover_pojos.get(i).getId());
        returnIntent.putExtra("name", discover_pojos.get(i).getTagName());
        ((Activity) v.getContext()).setResult(Activity.RESULT_OK, returnIntent);
        ((Activity) v.getContext()).finish();
        } else {
        Intent guest = new Intent(context, GuestProfile_Activity.class);
        guest.putExtra("guest_id", "" + discover_pojos.get(i).getId());
        context.startActivity(guest);
        }
        }
        });*/

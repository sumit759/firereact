package com.mozostar.app.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.data.ChatUserList_Pojo;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.view.activity.chat.ChatActivity;
import com.mozostar.app.view.activity.chat.Constants;
import com.mozostar.app.view.activity.chat.models.User;
import com.mozostar.app.view.activity.guest.GuestProfile_Activity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    ArrayList<ChatUserList_Pojo> mlist_arraylist;
    List<User> chatUser;
    Context mContext;
    String from;
//        InteractionListener listener;

    public ChatListAdapter(Context context, ArrayList<ChatUserList_Pojo> list_arraylist, List<User> cahtUser) {
        mContext = context;
        mlist_arraylist = list_arraylist;
//        this.listener = (InteractionListener)context;
        this.chatUser = cahtUser;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ChatListAdapter.MyViewHolder myViewHolder, int i) {
        from = SharedHelper.getKey(mContext, "on_click_follow");


//        if (mlist_arraylist.get(i).getChatFollowing() != null) {
//            if (mlist_arraylist.get(i).getChatFollowing().getImage() != null)
//                Glide.with(mContext).load(mlist_arraylist.get(i).getChatFollowing().getImage()).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate().error(R.drawable.user_profile)).into(myViewHolder.prfl_img);
//
//            myViewHolder.prfl_name.setText(mlist_arraylist.get(i).getChatFollowing().getFirstName() + " " + mlist_arraylist.get(i).getChatFollowing().getFirstName());
//
//        }


            myViewHolder.prfl_name.setText(chatUser.get(i).name);




        int user_id = SharedHelper.getIntKey(mContext, "user_id");

        myViewHolder.prfl_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent guest = new Intent(mContext, GuestProfile_Activity.class);
                guest.putExtra("guest_id", "" + mlist_arraylist.get(i).getFollowerId());
                mContext.startActivity(guest);
            }
        });
        User user = chatUser.get(i);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chat = new Intent(mContext, ChatActivity.class);
                chat.putExtra(Constants.ARG_RECEIVER,user.name);
                chat.putExtra(Constants.ARG_RECEIVER_UID,user.uid);
                chat.putExtra(Constants.ARG_FIREBASE_TOKEN,user.firebaseToken);
//                chat.putExtra("request_id", ""+mlist_arraylist.get(i).getId());
//                chat.putExtra("user_id", ""+mlist_arraylist.get(i).getUserId());
//                Log.e("sendUserId",""+mlist_arraylist.get(i).getFollowerId());
                chat.putExtra("name", ""+chatUser.get(i).name);
                mContext.startActivity(chat);
            }
        });
    }

    @Override
    public int getItemCount() {
//        return mlist_arraylist.size();
        return chatUser.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.prfl_img)
        CircleImageView prfl_img;

        @BindView(R.id.prfl_name)
        TextView prfl_name;



        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

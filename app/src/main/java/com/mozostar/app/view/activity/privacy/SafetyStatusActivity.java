package com.mozostar.app.view.activity.privacy;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SafetyStatusActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.everyone_image_view)
    ImageView everyone_image_view;

    @BindView(R.id.friends_image_view)
    ImageView friends_image_view;

    @BindView(R.id.onlyme_image_view)
    ImageView onlyme_image_view;

    @BindView(R.id.everyone_layout)
    RelativeLayout everyone_layout;

    @BindView(R.id.friends_layout)
    RelativeLayout friends_layout;

    @BindView(R.id.onlyme_layout)
    RelativeLayout onlyme_layout;

    @BindView(R.id.tool_profile_title)
    TextView tool_profile_title;

    String likedStatus, commentStatus, reactStatus, actionActivity, commentActivity, reactActivity;
    int likedStatusCount, commentStatusCount, reactStatusCount;

    @Override
    public int getLayoutId() {
        return R.layout.activity_safety_status;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);

        actionActivity = getIntent().getStringExtra("action_activity");

        likedStatusCount = SharedHelper.getIntKey(SafetyStatusActivity.this,"likedStatusCount");
        commentStatusCount = SharedHelper.getIntKey(SafetyStatusActivity.this,"commentStatusCount");
        reactStatusCount = SharedHelper.getIntKey(SafetyStatusActivity.this,"reactStatusCount");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (actionActivity.equalsIgnoreCase("like_activity")){
            tool_profile_title.setText(getResources().getString(R.string.liked_safety_text));

            if (likedStatusCount == 0){
                everyone_image_view.setBackgroundResource(R.drawable.tick_ic);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }

            if (likedStatusCount == 1){
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.drawable.tick_ic);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }

            if (likedStatusCount == 2){
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.drawable.tick_ic);
            }

        }

        if (actionActivity.equalsIgnoreCase("comment_activity")){
            tool_profile_title.setText(getResources().getString(R.string.comment_safety_text));

            if (commentStatusCount == 0){
                everyone_image_view.setBackgroundResource(R.drawable.tick_ic);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }

            if (commentStatusCount == 1){
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.drawable.tick_ic);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }

            if (commentStatusCount == 2){
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.drawable.tick_ic);
            }

        }

        if (actionActivity.equalsIgnoreCase("react_activity")){
            tool_profile_title.setText(getResources().getString(R.string.react_safety_text));
            if (reactStatusCount == 0){
                everyone_image_view.setBackgroundResource(R.drawable.tick_ic);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }

            if (reactStatusCount == 1){
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.drawable.tick_ic);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }

            if (reactStatusCount == 2){
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.drawable.tick_ic);
            }

        }



        Log.e("like count",""+likedStatusCount);
        Log.e("comment count",""+commentStatusCount);
        Log.e("react count",""+reactStatusCount);




        everyone_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likedStatus = "Everyone";
                commentStatus = "Everyone";
                reactStatus = "Everyone";
                everyone_image_view.setBackgroundResource(R.drawable.tick_ic);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }
        });

        friends_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likedStatus = "Friends";
                commentStatus = "Friends";
                reactStatus = "Friends";
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.drawable.tick_ic);
                onlyme_image_view.setBackgroundResource(R.color.transparent);
            }
        });

        onlyme_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likedStatus = "Only me";
                commentStatus = "Only me";
                reactStatus = "Only me";
                everyone_image_view.setBackgroundResource(R.color.transparent);
                friends_image_view.setBackgroundResource(R.color.transparent);
                onlyme_image_view.setBackgroundResource(R.drawable.tick_ic);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.putExtra("likedStatus", likedStatus);
        intent.putExtra("commentStatus", commentStatus);
        intent.putExtra("reactStatus", reactStatus);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();  // optional depending on your needs
    }
}

package com.mozostar.app.view.activity.bigvideo;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.bigvideo.getlist.BigVideoGetListResponse;
import com.mozostar.app.data.network.APIClient;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BigVideoPresenter<V extends BigVideoIView> extends BasePresenter<V> implements BigVideoIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void getBigVideoList(boolean comingFrom) {
        ProgressDialog dialog = null;
        if (comingFrom) {
            dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
            dialog.show();
        }
        Observable modelObservable = APIClient.getAPIClient().getBigVideoList();
        ProgressDialog finalDialog = dialog;
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bigVideoList -> {
                            if (comingFrom) {
                                finalDialog.dismiss();
                            }
                            getMvpView().onSuccessBigVideoList((BigVideoGetListResponse) bigVideoList);
                        },
                        throwable -> {
                            if (comingFrom) {
                                finalDialog.dismiss();
                            }
                            getMvpView().onError((Throwable) throwable);
                        });
    }
}

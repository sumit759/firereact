package com.mozostar.app.view.device_video_list;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.view.DividerGridItemDecoration;
import com.mozostar.app.view.adapter.VideoGridAdapter;
import com.mozostar.app.view.videotrim.VideoTrimActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.mozostar.app.R;
import com.mozostar.app.common.VideoUtil;
import com.mozostar.app.model.LocalVideoModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class DvideoList_Activity extends BaseActivity implements DvideoListIView, VideoGridAdapter.OnItemClickListener {

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView title;
    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView option;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.dvla_recylerView)
    RecyclerView mRecyclerView;
    Context context;
    private static String TAG = DvideoList_Activity.class.getSimpleName();
    DvideoListPresenter<DvideoList_Activity> presenter = new DvideoListPresenter<>();

    private RxPermissions mRxPermissions;

    private List<LocalVideoModel> mLocalVideoModels = new ArrayList<>();
    private VideoGridAdapter mAdapter;

    Toolbar myToolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_device_video_lsit;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initView() {

        context = DvideoList_Activity.this;
        ButterKnife.bind(this);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(DvideoList_Activity.this, R.color.colorPrimary));
        setSupportActionBar(myToolbar);
        mRxPermissions = new RxPermissions(this);

        option.setVisibility(View.INVISIBLE);
        title.setText("Videos");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedHelper.putKey(context, "upload_big_video", false);
                finish();
            }
        });

        VideoUtil.getLocalVideoFiles(this)
                .subscribe(new Observer<ArrayList<LocalVideoModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        subscribe(d);
                    }

                    @Override
                    public void onNext(ArrayList<LocalVideoModel> localVideoModels) {
                        mLocalVideoModels = localVideoModels;
                        mAdapter.setData(mLocalVideoModels);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        init();
    }

    private void setSupportActionBar(Toolbar myToolbar) {

    }

    private void init() {
        mRxPermissions
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        subscribe(d);
                    }

                    @Override
                    public void onNext(Boolean granted) {
                        if (granted) {
                            mRecyclerView.setLayoutManager(new GridLayoutManager(context, 4));
                            mRecyclerView.setHasFixedSize(true);
                            mRecyclerView.addItemDecoration(new DividerGridItemDecoration(context));
                            mAdapter = new VideoGridAdapter(context, mLocalVideoModels);
                            mRecyclerView.setAdapter(mAdapter);
                            mAdapter.setOnItemClickListener(DvideoList_Activity.this);
                        } else {
                            Toast.makeText(context, "\n" + "Is it possible to give permission?", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void redirectHome() {

    }

    @Override
    public void onItemClick(int position, LocalVideoModel model) {
        Intent intent = new Intent(this, VideoTrimActivity.class);
        intent.putExtra("videoPath", model.getVideoPath());
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        SharedHelper.putKey(context, "upload_big_video", false);
        super.onBackPressed();
    }
}

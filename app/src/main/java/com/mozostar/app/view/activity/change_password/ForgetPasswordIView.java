package com.mozostar.app.view.activity.change_password;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Follow_Pojo;

public interface ForgetPasswordIView extends MvpView {
    void onSuccessReset(Follow_Pojo trendsResponse);
}

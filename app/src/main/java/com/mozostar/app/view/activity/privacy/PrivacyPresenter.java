package com.mozostar.app.view.activity.privacy;

import android.annotation.SuppressLint;
import android.util.Log;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.Safety_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PrivacyPresenter <V extends PrivacyIView> extends BasePresenter<V> implements PrivacyIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void getSafetyCount(HashMap<String, Object> obj) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().getSafetyCount(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(safetyResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccess((Safety_Pojo) safetyResponse);
                            Log.e("success Safety","success Safety");
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                            Log.e("failure Safety","failure Safety");
                        });

    }
}

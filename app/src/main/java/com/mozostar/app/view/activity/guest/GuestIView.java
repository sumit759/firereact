package com.mozostar.app.view.activity.guest;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.Guest_Pojo;

public interface GuestIView extends MvpView {
    void onSuccessProfile(Guest_Pojo trendsResponse);

    void onSuccessFollow(Follow_Pojo trendsResponse);
}

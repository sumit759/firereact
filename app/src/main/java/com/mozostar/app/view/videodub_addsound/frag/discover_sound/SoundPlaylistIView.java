package com.mozostar.app.view.videodub_addsound.frag.discover_sound;


import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.SoundsPlayList_Pojo;

import java.util.List;

public interface SoundPlaylistIView extends MvpView {
    void onSuccessSoundsPlayList(List<SoundsPlayList_Pojo> soundsPlayList_pojos);
}

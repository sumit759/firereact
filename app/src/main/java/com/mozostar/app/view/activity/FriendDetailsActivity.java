package com.mozostar.app.view.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class FriendDetailsActivity extends BaseActivity {

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.name_friend_details_tv)
    TextView name_friend_details_tv;

    @BindView(R.id.bio_friend_details_tv)
    TextView bio_friend_details_tv;

    @BindView(R.id.image_friend_details)
    CircleImageView image_friend_details;

    @BindView(R.id.report_layout_ll)
    LinearLayout report_layout_ll;

    @BindView(R.id.block_layout_ll)
    LinearLayout block_layout_ll;


    @Override
    public int getLayoutId() {
        return R.layout.activity_friend_details;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        ivMenu_ToolBarLay.setVisibility(View.GONE);
        tvTitle_ToolBarLay.setText("Details");
    }

    @OnClick(R.id.back)
    public void onBackClick() {
        onBackPressed();
    }
}
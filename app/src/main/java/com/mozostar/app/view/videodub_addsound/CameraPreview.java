package com.mozostar.app.view.videodub_addsound;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    SurfaceHolder mHolder;
    Camera camera;

    public CameraPreview(Context context, Camera camera){
        super(context);
        this.camera = camera;
        this.mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        try {
            Camera.Parameters parameters = camera.getParameters();

            parameters.set("orientation","portrait");
            camera.setParameters(parameters);
            camera.setDisplayOrientation(90);
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        if (mHolder.getSurface() == null){
            return;
        }
//        camera.startPreview();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}

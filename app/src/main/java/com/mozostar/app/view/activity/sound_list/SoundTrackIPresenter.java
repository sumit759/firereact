package com.mozostar.app.view.activity.sound_list;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface SoundTrackIPresenter <V extends SoundTrackIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getSoundList(HashMap<String, Object> obj);
}

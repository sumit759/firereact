package com.mozostar.app.view.fragment.profilefragment;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface ProfileVideoIPresenter<V extends ProfileVideoIView> extends MvpPresenter<V> {


    @SuppressLint("CheckResult")
    void getList();

    @SuppressLint("CheckResult")
    void getDubList();

    @SuppressLint("CheckResult")
    void getChanneList();

}

package com.mozostar.app.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.view.activity.videos_view.ViewVideosFragment;
import com.mozostar.app.view.adapter.CommentsAdapter;
import com.mozostar.app.view.fragment.Inbox_Fragment;
import com.mozostar.app.view.fragment.discover.Discover_Fragment;
import com.mozostar.app.view.fragment.homedashboard.Home_Fragment;
import com.mozostar.app.view.fragment.homedashboard.Listener;
import com.mozostar.app.view.fragment.profile.Profile_Fragment;
import com.mozostar.app.view.register.SocialRegisterActivity;
import com.mozostar.app.view.videodub_addsound.VideoDubRecord_Activity_new;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, InteractionListener, MainIView {
    Context context;
    private static String TAG = MainActivity.class.getSimpleName();
    public static final String FACEBOOK_PACKAGE_NAME = "com.facebook.katana";
    public static final String TWITTER_PACKAGE_NAME = "com.twitter.android";
    public static final String INSTAGRAM_PACKAGE_NAME = "com.instagram.android";
    public static final String PINTEREST_PACKAGE_NAME = "com.pinterest";
    public static final String WHATS_PACKAGE_NAME = "com.whatsapp";
    private static final int pick = 100;

//            ArrayList<Uri> videoUri = new ArrayList<Uri>();

    private BottomSheetBehavior sheetBehavior, commentsBehavior, shareBehaviour;

    @BindView(R.id.bottom_sheet)
    LinearLayout bottom_sheet;

    @BindView(R.id.iv_facebook)
    ImageView iv_facebook;

    @BindView(R.id.frag_lay)
    LinearLayout frag_lay;
    @BindView(R.id.rrTollbar)
    RelativeLayout rrTollbar;

    @BindView(R.id.comments_bottom_sheet)
    LinearLayout comments_bottom_sheet;

    @BindView(R.id.bottom_sheet_share)
    LinearLayout bottom_sheet_share;

    @BindView(R.id.post_comment_etx)
    EditText post_comment_etx;

    @BindView(R.id.post_comment_img)
    ImageView post_comment_img;

    @BindView(R.id.iv_whatsapp)
    ImageView iv_whatsapp;

    @BindView(R.id.comments_list)
    RecyclerView comments_list;

    @BindView(R.id.ivClose_BottomSheetLay)
    ImageView ivClose_BottomSheetLay;

    @BindView(R.id.ivClose_share)
    ImageView ivClose_share;

    @BindView(R.id.close_cmnt_sheet)
    ImageView close_cmnt_sheet;

    @BindView(R.id.tvSignIn_BottomSheetLay)
    TextView tvSignIn_BottomSheetLay;
    @BindView(R.id.btnSignUp_BottomSheetLay)
    Button btnSignUp_BottomSheetLay;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottom_navigation;

    @BindView(R.id.following_txt)
    TextView following_txt;

    @BindView(R.id.foryou_txt)
    TextView foryou_txt;

    @BindView(R.id.progressBarMain)
    LinearLayout progressBarMain;

    @BindView(R.id.progress_time_txt)
    TextView progress_time_txt;
    String title, description;

    Listener mlistener;

    Boolean loggedIn;
    MainPresenter<MainActivity> presenter = new MainPresenter<>();
    boolean logged_in;
    String videolist, userid;
    String video_id, user_id;

    FFmpeg ffmpeg;


    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void initView() {
        context = MainActivity.this;
        ButterKnife.bind(this);
        presenter.attachView(this);

        bottom_navigation.setOnNavigationItemSelectedListener(this);

        /*set third view view*/
        setThirdView();


        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        commentsBehavior = BottomSheetBehavior.from(comments_bottom_sheet);
        shareBehaviour = BottomSheetBehavior.from(bottom_sheet_share);
        progressBarMain.setVisibility(View.GONE);
        loggedIn = SharedHelper.getBoolKey(MainActivity.this, "logged_in", false);


        shareBehaviour.setHideable(true);
        shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
        shareBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        commentsBehavior.setHideable(true);

        commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        commentsBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        sheetBehavior.setHideable(true);//Important to add
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });


        Bundle i = getIntent().getExtras();

        if (i != null) {
            frag_lay.setVisibility(View.GONE);
            rrTollbar.setVisibility(View.GONE);


            String data = i.getString("frag");
            //bottom_navigation.setBackgroundColor(context.getColor(R.color.colorPrimary));

//            changeFragment(new Profile_Fragment(), "profile");
            Intent startAct = new Intent(MainActivity.this, Profile_Fragment.class);
            startActivity(startAct);
//            finish();

            bottom_navigation.setSelectedItemId(R.id.profile_img);

        } else {
            frag_lay.setVisibility(View.VISIBLE);
            rrTollbar.setVisibility(View.VISIBLE);

            bottom_navigation.setSelectedItemId(R.id.action_home);
            changeFragment(new Home_Fragment(), "home");
        }

      /*   saveImage();
        loadFFMpegBinary();   // Share video to set Watermark in shared video Image and Text
        String commands[]={"-y","-i",getVideoFilePath("test.mp4"),"-i", getVideoFilePath("mozostar.png"),"-filter_complex","[1:v]scale=100:100 [ovrl], [0:v][ovrl]overlay=main_w-overlay_w-5:main_h-overlay_h-5:enable='between(t,"+0+","+9600+")'","-codec:a","copy","-strict","-2","-c:v","libx264","-preset","ultrafast",getVideoFilePath("test1.mp4")};
        execFFmpegBinary(commands);

        //  Text watermark
        String ffmpegCmdString = "-y -i "+getVideoFilePath("test1.mp4")+" -vf drawtext=text='Fireact':fontfile=/storage/emulated/0/Movies/fontsfree_net_cor3.ttf:fontcolor=white:fontsize=32:x=w-tw-10:y=h-th-10 -preset ultrafast -async 1 " +getVideoFilePath("test2.mp4");
        String[] splitCmd = ffmpegCmdString.split(" ");
        execFFmpegBinary(splitCmd);

        // Audio Video Merge
        String cmd[] = {"-y", "-i", getVideoFilePath("test.mp4"), "-i", getVideoFilePath("song.mp3"), "-c", "copy", "-map", "0:v:0", "-map", "1:a:0", getVideoFilePath("test1.mp4")};
        execFFmpegBinary(cmd); */


    }

    private void setThirdView() {
        BottomNavigationMenuView mbottomNavigationMenuView =
                (BottomNavigationMenuView) bottom_navigation.getChildAt(0);

        View view = mbottomNavigationMenuView.getChildAt(2);

        BottomNavigationItemView itemView = (BottomNavigationItemView) view;

        View cart_badge = LayoutInflater.from(this)
                .inflate(R.layout.custom_item_layout,
                        mbottomNavigationMenuView, false);

        itemView.addView(cart_badge);
    }

    // startActivity(new Intent(MainActivity.this, FullScreenVideoAct.class));

    private void saveImage() {  // get Image from Drawable to store an Internal storage
        // TODO Change logo.png in drawable folder for watermark
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File file = new File(getVideoFilePath("mozostar.png"));
        if (!file.exists()) {
            try {
                FileOutputStream outStream = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadFFMpegBinary() {
        ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        }
    }

    private void execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Toast.makeText(MainActivity.this, "FAILED with output : " + s, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String s) {
                    Toast.makeText(MainActivity.this, "Success : " + s, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onProgress(String s) {
                    //  Log.d(TAG, "Started command : ffmpeg " + command);
                    Toast.makeText(MainActivity.this, "progress : " + s, Toast.LENGTH_SHORT).show();
                    //  progressDialog.setMessage("Processing\n"+s);
                }

                @Override
                public void onStart() {
                  /*
                    Log.d(TAG, "Started command : ffmpeg " + command);
                    progressDialog.setMessage("Processing...");
                    progressDialog.show();*/
                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Finished command : ffmpeg " + command);
                    // progressDialog.dismiss();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Support")
                .setMessage("Device Not Support")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create()
                .show();

    }

    private void changeFragment(Fragment fragment, String from) {
        Bundle arguments = new Bundle();
        arguments.putString("from", from);
        fragment.setArguments(arguments);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContainer_MainLay, fragment);
        transaction.commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_home:
                frag_lay.setVisibility(View.VISIBLE);
                rrTollbar.setVisibility(View.VISIBLE);

                changeFragment(new Home_Fragment(), "home");
                //                bottom_navigation.setBackgroundColor(context.getColor(R.color.colorPrimary));
                break;
            case R.id.action_discover:
                frag_lay.setVisibility(View.GONE);
                rrTollbar.setVisibility(View.GONE);

                changeFragment(new Discover_Fragment(), "discover");
                // bottom_navigation.setBackgroundColor(context.getColor(R.color.colorPrimary));
                break;
            case R.id.action_add:

                logged_in = SharedHelper.getBoolKey(MainActivity.this, "logged_in", false);
                if (logged_in) {
                    frag_lay.setVisibility(View.GONE);
                    rrTollbar.setVisibility(View.GONE);

                    // bottom_navigation.setBackgroundColor(context.getColor(R.color.colorPrimary));
                    // changeFragment(new Dashboard_Fragment());
                    if (ActivityManager.file_videopath.exists()) {
                        try {
                            FileUtils.deleteDirectory(ActivityManager.file_videopath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    SharedHelper.putKey(context, "channel_video_uplaod", false);

                    ActivityManager.redirectToActivityWithoutBundle(context, VideoDubRecord_Activity_new.class);
//                    startActivity(new Intent(context,BaseCameraActivity.class));
//                    ActivityManager.redirectToActivityWithoutBundle(context, VideoDubRecord_Activity.class);

                    overridePendingTransition(0, 0);

                    //return false;``

                } else {
                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }
                break;
            case R.id.action_inbox:
//                frag_lay.setVisibility(View.GONE);
//                rrTollbar.setVisibility(View.GONE);
                logged_in = SharedHelper.getBoolKey(MainActivity.this, "logged_in", false);
                if (logged_in) {
                    startActivity(new Intent(this, Inbox_Fragment.class));
//                    changeFragment(new Inbox_Fragment(), "inbox");
                } else {
                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }
                break;
            case R.id.action_profile:
//                frag_lay.setVisibility(View.GONE);
//                rrTollbar.setVisibility(View.GONE);
                // bottom_navigation.setBackgroundColor(context.getColor(R.color.colorPrimary));
                logged_in = SharedHelper.getBoolKey(MainActivity.this, "logged_in", false);
                if (logged_in) {
//                    changeFragment(new Profile_Fragment(), "profile");
                    Intent startAct = new Intent(MainActivity.this, Profile_Fragment.class);
                    startActivity(startAct);
                } else {
                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void share_sheet(String id, int i) {
        videolist = id;
        Log.i("share_sheet", "" + videolist);
        userid = "" + i;
        Log.i("share_sheet", "" + userid);
        String appPath = getFilesDir().getAbsolutePath();
        String fileName = userid + ".mp4";
        File outputFile = new File(appPath, fileName);
        Log.i("downloadVideo", "PATH: " + outputFile.getAbsolutePath());
        //    String commands[] = {"-y", "-i", getVideoFilePath(fileName), "-i", getVideoFilePath("mozostar.png"), "-filter_complex", "[1:v]scale=100:100 [ovrl], [0:v][ovrl]overlay=main_w-overlay_w-5:main_h-overlay_h-5:enable='between(t," + 0 + "," + 9600 + ")'", "-codec:a", "copy", "-strict", "-2", "-c:v", "libx264", "-preset", "ultrafast", getVideoFilePath(fileName + "_1")};
        // execFFmpegBinary(commands);
        if (outputFile.exists()) {
            try {
                File videoFile = new File(outputFile.getAbsolutePath());
                Uri videoURI = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                        ? FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", videoFile)
                        : Uri.fromFile(videoFile);
                ShareCompat.IntentBuilder.from(this)
                        .setStream(videoURI)
                        .setType("video/mp4")
                        .setChooserTitle("Share video...")
                        .startChooser();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            downloadVideo(videolist, appPath, fileName);
        }

        /*if (shareBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            shareBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
            presenter.getListVedio(id);
        } else {
            shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
        }*/
    }

    @Override
    public void comments_list(String id, int i) {
        // since this may be called from a non-UI thread, we can't update
        // the UI directly.
        video_id = id;
        position = "" + i;

        if (commentsBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            commentsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            presenter.getList(id);
        } else {
            commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
    }


    @Override
    public void behaviour() {
        // since this may be called from a non-UI thread, we can't update
        // the UI directly.

        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }
    }

    @Override
    public void hide_sheet() {
        commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }


    String position;

    @OnClick({R.id.foryou_txt, R.id.following_txt, R.id.ivClose_BottomSheetLay,
            R.id.close_cmnt_sheet, R.id.ivClose_share, R.id.tvSignIn_BottomSheetLay, R.id.btnSignUp_BottomSheetLay, R.id.iv_whatsapp, R.id.post_comment_img})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.ivClose_BottomSheetLay) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setHideable(true);//Important to add
                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        } else if (view.getId() == R.id.close_cmnt_sheet) {
            if (commentsBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                commentsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        } else if (view.getId() == R.id.ivClose_share) {
            if (shareBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                shareBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
            }


        } else if (view.getId() == R.id.iv_whatsapp) {
            shareAppWithWhatsApp();

        } else if (view.getId() == R.id.iv_facebook) {
            shareFacebook();
        } else if (view.getId() == R.id.tvSignIn_BottomSheetLay) {
            ActivityManager.redirectToActivityWithoutBundle(context, SocialRegisterActivity.class);
            overridePendingTransition(0, 0);
        } else if (view.getId() == R.id.btnSignUp_BottomSheetLay) {
            ActivityManager.redirectToActivityWithoutBundle(context, SocialRegisterActivity.class);
        } else if (view.getId() == R.id.post_comment_img) {
            String comments = post_comment_etx.getText().toString();
            user_id = "" + SharedHelper.getIntKey(MainActivity.this, "user_id");

            if (!TextUtils.isEmpty(comments)) {
                post_comment_etx.getText().clear();
                HashMap<String, Object> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("video_id", video_id);
                map.put("comment", comments);
                presenter.post_comment(map);
            } else {
                Toast.makeText(MainActivity.this, "Please write a comment and try to post it", Toast.LENGTH_LONG).show();
            }


        } else if (view.getId() == R.id.foryou_txt) {

            foryou_txt.setTextColor(getResources().getColor(R.color.white));
            foryou_txt.setTypeface(foryou_txt.getTypeface(), Typeface.BOLD);
            foryou_txt.setTextSize(18);
            following_txt.setTextColor(getResources().getColor(R.color.white));
            following_txt.setTypeface(following_txt.getTypeface(), Typeface.NORMAL);
            following_txt.setTextSize(18);
            changeFragment(new Home_Fragment(), "home");
        } else if (view.getId() == R.id.following_txt) {

            foryou_txt.setTextColor(getResources().getColor(R.color.white));
            foryou_txt.setTypeface(foryou_txt.getTypeface(), Typeface.NORMAL);
            foryou_txt.setTextSize(18);
            following_txt.setTextColor(getResources().getColor(R.color.white));
            following_txt.setTypeface(following_txt.getTypeface(), Typeface.BOLD);
            following_txt.setTextSize(18);

            changeFragment(new Home_Fragment(), "following");
        }
    }

    private void shareFacebook() {
        ArrayList<Uri> shareFacebook = new ArrayList<Uri>();
        Intent shareWhatsApp = new Intent();
        shareWhatsApp.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareWhatsApp.setPackage(FACEBOOK_PACKAGE_NAME);
        shareWhatsApp.setType("video/mp4");
        try {
            context.startActivity(shareWhatsApp);
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(context, "app not Installed ", Toast.LENGTH_SHORT).show();
        }

    }

    private void shareAppWithWhatsApp() {

        String path = videolist;
        Log.d("shareAppWithWhatsApp", "" + path);
        ContentValues content = new ContentValues(1000);
        content.put(MediaStore.Video.VideoColumns.DATE_ADDED, System.currentTimeMillis() / 1000);
        content.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
        content.put(MediaStore.Video.Media.DATA, path);

        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        Uri uri = contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content);

        Intent shareWhatsApp = new Intent();
        shareWhatsApp.setAction(Intent.ACTION_SEND);
        shareWhatsApp.setPackage(WHATS_PACKAGE_NAME);
        shareWhatsApp.putExtra(Intent.EXTRA_STREAM, uri);
        shareWhatsApp.setType("video/*");
        shareWhatsApp.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
//            context.startActivity(shareWhatsApp);
            startActivity(Intent.createChooser(shareWhatsApp, "Share"));
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(context, "app not Installed ", Toast.LENGTH_SHORT).show();
        }
       /* try{
            File videoFile = new File(videolist);
            Uri videoURI = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                    ? FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                    BuildConfig.APPLICATION_ID + ".provider", videoFile)
                    : Uri.fromFile(videoFile);
            ShareCompat.IntentBuilder.from(this)
                    .setStream(videoURI)
                    .setType("video/mp4")
                    .setChooserTitle("Share video...")
                    .startChooser();
        }catch (Exception e){
            e.printStackTrace();
        }*/

    }

    public void downloadVideo(String url, String path, String fileName) {
        try {
            progressBarMain.setVisibility(View.VISIBLE);
            progress_time_txt.setText("0%");
            int downloadId = PRDownloader.download(url, path, fileName)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {
                            long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                            progress_time_txt.setText(progressPercent + "%");
                        }
                    })
                    .start(new OnDownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            progressBarMain.setVisibility(View.GONE);
                            File outputFile = new File(path, fileName);
                            Log.i("downloadVideo", "onDownloadComplete: " + outputFile.getAbsolutePath());
                            //  String commands[]={"-y","-i",getVideoFilePath("test.mp4"),"-i", getVideoFilePath("mozostar.png"),"-filter_complex","[1:v]scale=100:100 [ovrl], [0:v][ovrl]overlay=main_w-overlay_w-5:main_h-overlay_h-5:enable='between(t,"+0+","+9600+")'","-codec:a","copy","-strict","-2","-c:v","libx264","-preset","ultrafast",getVideoFilePath("test1.mp4")};
                            //  execFFmpegBinary(commands);
                            try {
                                File videoFile = new File(outputFile.getAbsolutePath());
                                Uri videoURI = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                                        ? FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                                        BuildConfig.APPLICATION_ID + ".provider", videoFile)
                                        : Uri.fromFile(videoFile);
                                ShareCompat.IntentBuilder.from(MainActivity.this)
                                        .setStream(videoURI)
                                        .setType("video/mp4")
                                        .setChooserTitle("Share video...")
                                        .startChooser();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Error error) {
                            Log.i("downloadVideo", "onError: " + error.getServerErrorMessage());
                        }

                    });
            Log.i("downloadVideo", "" + downloadId);
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setHideable(true);//Important to add
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSuccess(CommentsAdapter adapter) {

        comments_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        comments_list.setAdapter(adapter);

    }

    @Override
    public void onSuccessComment(CommentPost_Pojo trendsResponse) {
//        String mCount = "" + comments_count_txt.getText().toString();
//        int count = Integer.parseInt(mCount) + 1;
//        comments_count_txt.setText("" + count);

        String from = SharedHelper.getKey(this, "comment_listener");

        if (from.equals("video")) {

        } else if (from.equals("home")) {

            commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            Home_Fragment fragment = (Home_Fragment) getSupportFragmentManager().findFragmentById(R.id.flContainer_MainLay);
            fragment.comment(position);
            Toast.makeText(MainActivity.this, "Comment posted successfully", Toast.LENGTH_LONG).show();

        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && requestCode == pick && data != null)
//        {
//
//            Uri uri = data.getData();
//            String path = getrealPathURI(this,uri);
//         }
//    }

//    private String getrealPathURI(MainActivity mainActivity, Uri uri)
//    {
//        return null;
//    }

    @Deprecated
    public void onAttach(Activity activity) {
        if (activity instanceof Listener) {
            //init the listener
            mlistener = (Listener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement InteractionListener");
        }
    }


    public void Fragment_call(int adapterPosition, Integer userId, String s) {
        Fragment f = new ViewVideosFragment();
        Bundle arguments = new Bundle();
        arguments.putString("from", s);
        arguments.putString("position", String.valueOf(adapterPosition));
        arguments.putString("guest_id", String.valueOf(userId));
        f.setArguments(arguments);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContainer_MainLay, f);
        transaction.commit();
    }

    public void BottamSheetAction(int i) {
        if (i == 1) {
            bottom_navigation.setVisibility(View.GONE);
        } else {
            bottom_navigation.setVisibility(View.VISIBLE);
        }
    }

    public static String getVideoFilePath(String str3) {
        return getAndroidMoviesFolder().getAbsolutePath() + "/" + str3 /*+ ".mp4"*/;
    }

    public static File getAndroidMoviesFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
    }
}

package com.mozostar.app.view.CountryPicker;


public interface CountryPickerListener {
    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID);
}

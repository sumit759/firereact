package com.mozostar.app.view.activity.settings;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Follow_Pojo;

public interface SettingsIView extends MvpView {

    void onSuccessUpgrade(Follow_Pojo trendsResponse);
}

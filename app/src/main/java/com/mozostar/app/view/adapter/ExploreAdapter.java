package com.mozostar.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.view.activity.ExoPlayerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.MyViewHolder> {
    Context context;
    ExploreChannelVideoResponse exploreChannelVideoResponse;

    public ExploreAdapter(Context context, ExploreChannelVideoResponse cover_ArrayList) {
        this.context = context;
        this.exploreChannelVideoResponse = cover_ArrayList;
    }

    @NonNull
    @Override
    public ExploreAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_rowitem, parent, false);
        return new ExploreAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ExploreAdapter.MyViewHolder myViewHolder, int i) {
        try {
            Glide.with(context).load(exploreChannelVideoResponse.getData().get(i).getVideo_image_url()).into(myViewHolder.explore_row_itemImage);
            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        SharedHelper.putKey(context, "video_view", "profile_dub");
                        Intent intent = new Intent(context, ExoPlayerActivity.class);
                        intent.putExtra("position", "" + myViewHolder.getAdapterPosition());
                        intent.putExtra("guest_id", "" + exploreChannelVideoResponse.getData().get(i).getUserId());
                        intent.putExtra("video", "" + exploreChannelVideoResponse.getData().get(i).getVideo());
                        context.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return exploreChannelVideoResponse.getData().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.explore_row_itemImage)
        ImageView explore_row_itemImage;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        @Override
        public void onClick(View v) {

        }
    }
}
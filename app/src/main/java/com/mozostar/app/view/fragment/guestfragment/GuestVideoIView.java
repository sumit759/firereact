package com.mozostar.app.view.fragment.guestfragment;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;

import java.util.ArrayList;

public interface GuestVideoIView extends MvpView {
    void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse);

    void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse);
}

package com.mozostar.app.view.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.view.adapter.InboxAdapter;
import com.mozostar.app.view.register.Register_Activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Inbox_Fragment extends BaseActivity {

    @BindView(R.id.sign_up)
    Button sign_up;

    @BindView(R.id.yesterday_likes_rv)
    RecyclerView yesterday_likes_rv;

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @Override
    public int getLayoutId() {
        return R.layout.inbox_frag;
    }

    Boolean logged_in;

    @Override
    public void initView() {

        ButterKnife.bind(this);

        /*set adapter*/
        setAdapter();

        logged_in = SharedHelper.getBoolKey(this, "logged_in", false);

        if (logged_in) {
            sign_up.setVisibility(View.GONE);
        } else {
            sign_up.setVisibility(View.VISIBLE);
        }

        imgBack.setOnClickListener(v -> onBackPressed());

    }

    private void setAdapter() {
        InboxAdapter inboxAdapter = new InboxAdapter(this);
        yesterday_likes_rv.setLayoutManager(new LinearLayoutManager(this));
        yesterday_likes_rv.setAdapter(inboxAdapter);
    }

    @OnClick({R.id.sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sign_up:
                Intent register = new Intent(this, Register_Activity.class);
                startActivity(register);
                break;
//            case R.id.chat_window:
//
//                GetUsersPresenter mGetUsersPresenter;
//
//                Intent inbox=new Intent(getContext(), ChatListActivity.class);
//                startActivity(inbox);
//                break;
        }
    }


}

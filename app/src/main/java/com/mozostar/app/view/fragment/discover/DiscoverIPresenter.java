package com.mozostar.app.view.fragment.discover;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface DiscoverIPresenter<V extends DiscoverIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getList();

    @SuppressLint("CheckResult")
    void getUserList(String user);
}

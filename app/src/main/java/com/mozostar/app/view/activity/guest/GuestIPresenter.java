package com.mozostar.app.view.activity.guest;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface GuestIPresenter <V extends GuestIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getProfiel(String id);

    @SuppressLint("CheckResult")
    void followup(HashMap<String, Object> obj);
}

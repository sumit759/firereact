package com.mozostar.app.view.activity.chat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;
import com.mozostar.app.R;
import com.mozostar.app.view.activity.chat.models.ChatModel;
import com.mozostar.app.base.BaseActivity;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;


public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;
    private List<ChatModel> mChats;
    Context context;

    public ChatRecyclerAdapter(Context context, List<ChatModel> chats) {
        this.mChats = chats;
        this.context = context;
    }

    public void add(ChatModel chat) {
        mChats.add(chat);
        notifyItemInserted(mChats.size() - 1);
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.item_mine_message, parent, false);
                viewHolder = new MyChatViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.item_other_message, parent, false);
                viewHolder = new OtherChatViewHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {
        if (TextUtils.equals(mChats.get(position).fromID,
                FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            configureMyChatViewHolder((MyChatViewHolder) holder, position);
        } else {
            configureOtherChatViewHolder((OtherChatViewHolder) holder, position);
        }
    }

    private void configureMyChatViewHolder(MyChatViewHolder myChatViewHolder, int position) {
        final ChatModel chat = mChats.get(position);
//        String alphabet = chat.sender.substring(0, 1);
        final String type = chat.type;
        if (type.equals("text")) {
            myChatViewHolder.txtChatMessage.setText(chat.content);
            myChatViewHolder.time.setText(String.valueOf(BaseActivity.getDisplayableTime(chat.timestamp)));

        }

    }

    private void configureOtherChatViewHolder(OtherChatViewHolder otherChatViewHolder, int position) {
        final ChatModel chat = mChats.get(position);
        final String type = chat.type;
        if (type.equals("text")) {
            otherChatViewHolder.txtChatMessage.setText(chat.content);
            otherChatViewHolder.time.setText(String.valueOf(BaseActivity.getDisplayableTime(chat.timestamp)));

        }
    }

    @Override
    public int getItemCount() {
        if (mChats != null) {
            return mChats.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.equals(mChats.get(position).fromID,
                FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            return VIEW_TYPE_ME;
        } else {
            return VIEW_TYPE_OTHER;
        }
    }

    private static class MyChatViewHolder extends RecyclerView.ViewHolder {
        private TextView txtChatMessage,time;
        public MyChatViewHolder(View itemView) {
            super(itemView);
            txtChatMessage = (TextView) itemView.findViewById(R.id.text);
            time = (TextView) itemView.findViewById(R.id.timestamp);

        }
    }

    private static class OtherChatViewHolder extends RecyclerView.ViewHolder {
        private TextView txtChatMessage, time;
        public OtherChatViewHolder(View itemView) {
            super(itemView);
            txtChatMessage = (TextView) itemView.findViewById(R.id.text);
            time = (TextView) itemView.findViewById(R.id.timestamp);
        }
    }
}

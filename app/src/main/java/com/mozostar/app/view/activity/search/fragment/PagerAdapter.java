package com.mozostar.app.view.activity.search.fragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mozostar.app.view.fragment.UserSearchFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                UserSearchFragment tab1 = new UserSearchFragment();
                return tab1;
            case 1:
                TopFragment tab2 = new TopFragment();
                return tab2;
            case 2:
                TopFragment tab3 = new TopFragment();
                return tab3;
            case 3:
                TopFragment tab4 = new TopFragment();
                return tab4;
           /* case 4:
                TopFragment tab5 = new TopFragment();
                return tab5;*/
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

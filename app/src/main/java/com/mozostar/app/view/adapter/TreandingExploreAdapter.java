package com.mozostar.app.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TreandingExploreAdapter extends RecyclerView.Adapter<TreandingExploreAdapter.MyViewHolder> {
    Context context;
    ArrayList<Drawable> discover;

    public TreandingExploreAdapter(Context context, ArrayList<Drawable> cover_ArrayList) {
        this.context = context;
        this.discover = cover_ArrayList;
    }

    @NonNull
    @Override
    public TreandingExploreAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trendingchild_rowitem, parent, false);
        return new TreandingExploreAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TreandingExploreAdapter.MyViewHolder myViewHolder, int i) {
        Glide.with(context).load(discover.get(i)).into(myViewHolder.ivTrendImage_TrendChildLay);
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return discover.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivTrendImage_TrendChildLay)
        ImageView ivTrendImage_TrendChildLay;
        @BindView(R.id.llComments_TrendChildLay)
        LinearLayout llComments_TrendChildLay;
        @BindView(R.id.ivLogo_TrendChildLay)
        ImageView ivLogo_TrendChildLay;
        @BindView(R.id.tvComments_TrendChildLay)
        TextView tvComments_TrendChildLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            llComments_TrendChildLay.setVisibility(View.GONE);
            ivLogo_TrendChildLay.setVisibility(View.GONE);
            tvComments_TrendChildLay.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
package com.mozostar.app.view.videodub_addsound.frag.discover_sound;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.SoundsPlayList_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.view.videodub_addsound.DiscoverDoneOnClick;
import com.mozostar.app.view.videodub_addsound.DiscoverPlaylistOnClick;
import com.mozostar.app.view.videodub_addsound.VideoDubRecord_Activity_new;
import com.mozostar.app.view.videodub_addsound.adapter.DiscoverPlaylist1_Adapter;
import com.mozostar.app.view.videodub_addsound.adapter.DiscoverPlaylist_Adapter;
import com.mozostar.app.view.addsounddetail.AddSound_Detail_Activity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class AddSound_Discover_frag extends BaseFragment implements DiscoverPlaylistOnClick, SoundPlaylistIView {
    Context context;
    private static String TAG = AddSound_Discover_frag.class.getSimpleName();

    @BindView(R.id.rvPlaylist_DiscSoundLay)
    RecyclerView rvPlaylist_DiscSoundLay;
    @BindView(R.id.rvPlaylist1_DiscSoundLay)
    RecyclerView rvPlaylist1_DiscSoundLay;

    SoundPlaylistPresenter<AddSound_Discover_frag> presenter = new SoundPlaylistPresenter<>();

    ArrayList<SoundsPlayList_Pojo> soundsPlayList_pojoArrayList = new ArrayList<SoundsPlayList_Pojo>();

    DiscoverPlaylist_Adapter discoverPlaylist_adapter;
    DiscoverPlaylist1_Adapter discoverPlaylist1_adapter;

    DiscoverDoneOnClick discoverDoneOnClick;

    @SuppressLint("ValidFragment")
    public AddSound_Discover_frag(VideoDubRecord_Activity_new videoDubRecord_activity_new) {
        discoverDoneOnClick = (DiscoverDoneOnClick) videoDubRecord_activity_new;
    }


    @Override
    public int getLayoutId() {
        return R.layout.discover_addsound_lay;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        //layoutManager = new LinearLayoutManager(context, 1, false);
        rvPlaylist_DiscSoundLay.setLayoutManager(new GridLayoutManager(context, 2));
        rvPlaylist_DiscSoundLay.setHasFixedSize(false);
        rvPlaylist_DiscSoundLay.setNestedScrollingEnabled(true);

        discoverPlaylist_adapter = new DiscoverPlaylist_Adapter(context, this, soundsPlayList_pojoArrayList);
        rvPlaylist_DiscSoundLay.setAdapter(discoverPlaylist_adapter);
        presenter.getSoundPlayList();

        rvPlaylist1_DiscSoundLay.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvPlaylist1_DiscSoundLay.setHasFixedSize(false);
        rvPlaylist1_DiscSoundLay.setNestedScrollingEnabled(true);
        discoverPlaylist1_adapter = new DiscoverPlaylist1_Adapter(context, this, soundsPlayList_pojoArrayList);
        rvPlaylist1_DiscSoundLay.setAdapter(discoverPlaylist1_adapter);
        return view;
    }

    @Override
    public void onClickPlaylistItem(int position, Sounds_Pojo sounds_pojo) {
        String selected_audioUrl = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getAudio();
        SharedHelper.putKey(activity(), "selected_audioUrl", selected_audioUrl);
        discoverDoneOnClick.onClickAddSoundDone(sounds_pojo);
    }

    @Override
    public void onClickPlaylistItem(int position) {

    }

    @Override
    public void onClickAllPlaylistItem(int position) {
        ActivityManager.redirectToActivityWithoutBundle(context, AddSound_Detail_Activity.class);
    }

    @Override
    public void onSuccessSoundsPlayList(List<SoundsPlayList_Pojo> soundsPlayList_pojos) {
        soundsPlayList_pojoArrayList.addAll(soundsPlayList_pojos);
        discoverPlaylist_adapter.notifyDataSetChanged();
        discoverPlaylist1_adapter.notifyDataSetChanged();
    }
}

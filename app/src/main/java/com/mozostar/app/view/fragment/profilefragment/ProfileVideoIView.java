package com.mozostar.app.view.fragment.profilefragment;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.ProfileChannelList_pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;

import java.util.ArrayList;

public interface ProfileVideoIView extends MvpView {
    void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse);

    void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse);

    void onSuccessChannel(ProfileChannelList_pojo trendsResponse);
}

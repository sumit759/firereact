package com.mozostar.app.view.activity.privacy;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface PrivacyIPresenter <V extends PrivacyIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getSafetyCount(HashMap<String, Object> obj);
}

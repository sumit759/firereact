package com.mozostar.app.view.fragment.homedashboard;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.model.VideoList_Pojo1;
import com.mozostar.app.data.model.VideoReportResponse;

import java.util.ArrayList;

public interface HomeIView  extends MvpView {

    void onSuccess(VideoList_Pojo1 historyList);
    void onError(Throwable e);

    void onSuccessLike(Object trendsResponse);

    void onSuccessDisLike(Object trendsResponse);

    void onSuccessFollowList(ArrayList<ProfileVideoList_Pojo> trendsResponse);

    void onSuccessFollow(Follow_Pojo trendsResponse);

    void onSuccessVideoReport(VideoReportResponse videoReportResponse);
}

package com.mozostar.app.view.fragment.discover;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseNavigationFragment;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.view.activity.search.SearchActivity;
import com.mozostar.app.view.adapter.DiscoverTrending_Adapter;
import com.mozostar.app.view.adapter.PromoBanner_PagerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Discover_Fragment extends BaseNavigationFragment implements DiscoverIView, BottomNavigationView.OnNavigationItemSelectedListener {
    Context context;
    private static String TAG = Discover_Fragment.class.getSimpleName();

    /*@BindView(R.id.rvTrending_DiscoverFragLay)
    RecyclerView rvTrending_DiscoverFragLay;*/
    @BindView(R.id.vpPromoBanner_HomeFragLay)
    ViewPager vpPromoBanner_HomeFragLay;
    @BindView(R.id.tlPromoBannerDot_HomeFragLay)
    TabLayout tlPromoBannerDot_HomeFragLay;
    /*   @BindView(R.id.rv_user)
       RecyclerView rv_user;*/
    @BindView(R.id.tvSearch_DiscoverFragLay)
    TextView tvSearch_DiscoverFragLay;

    String user;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottom_navigation;

    @BindView(R.id.following_txt)
    TextView following_txt;

    @BindView(R.id.foryou_txt)
    TextView foryou_txt;
    @BindView(R.id.opnedrewer)
    ImageView opnedrewer;


    /*use to set navigation drawer*/
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.rrAll)
    RelativeLayout rrAll;


    DiscoverPresenter<Discover_Fragment> presenter = new DiscoverPresenter<>();

    ArrayList<Integer> myImageList = new ArrayList<>();
    ArrayList<Discover_Pojo.Video> discover_pojos = new ArrayList<>();
    ArrayList<User_Pojo> user_pojos = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.discover_frag;
    }

    @OnClick({R.id.following_txt, R.id.foryou_txt, R.id.opnedrewer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.following_txt:
                foryou_txt.setTextColor(getResources().getColor(R.color.dark_grey));
                foryou_txt.setTypeface(foryou_txt.getTypeface(), Typeface.NORMAL);
                foryou_txt.setTextSize(16);
                following_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
                following_txt.setTypeface(following_txt.getTypeface(), Typeface.BOLD);
                following_txt.setTextSize(18);
                changeFragment(new ExploreDiscover_Fragment(), "explore");
                break;
            case R.id.foryou_txt:
                foryou_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
                foryou_txt.setTypeface(foryou_txt.getTypeface(), Typeface.BOLD);
                foryou_txt.setTextSize(18);
                following_txt.setTextColor(getResources().getColor(R.color.dark_grey));
                following_txt.setTypeface(following_txt.getTypeface(), Typeface.NORMAL);
                following_txt.setTextSize(16);
                changeFragment(new TrendingDiscover_Fragment(), "trending");
                break;
            case R.id.opnedrewer:
                openCloseDrawer();
                break;
        }
    }

    public void openCloseDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }

    }

    private void setDrawerLockModes() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.setScrimColor(Color.TRANSPARENT);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.open, R.string.close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                rrAll.setTranslationX(slideX);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);

//        presenter.getList();

        bottom_navigation.setOnNavigationItemSelectedListener(this);
        tvSearch_DiscoverFragLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedHelper.putKey(context, "search", "user");
                Intent inetent = new Intent(getContext(), SearchActivity.class);
                inetent.putExtra("search", "user");
                startActivity(inetent);
            }
        });

        changeFragment(new TrendingDiscover_Fragment(), "trending");
        setDrawerLockModes();

       /* tvSearch_DiscoverFragLay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0) {
                    user=tvSearch_DiscoverFragLay.getText().toString();
                    presenter.getUserList(user);
                    rvTrending_DiscoverFragLay.setVisibility(View.GONE);
                }else {
                    rvTrending_DiscoverFragLay.setVisibility(View.VISIBLE);
                    rv_user.setVisibility(View.GONE);
                    tvSearch_DiscoverFragLay.getText().clear();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
     /*   rvTrending_DiscoverFragLay.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvTrending_DiscoverFragLay.setItemAnimator(new DefaultItemAnimator());
        rvTrending_DiscoverFragLay.setNestedScrollingEnabled(true);


        rv_user.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rv_user.setItemAnimator(new DefaultItemAnimator());
        rv_user.setNestedScrollingEnabled(true);*/

        myImageList.add(R.drawable.banner1);
        myImageList.add(R.drawable.banner2);
        myImageList.add(R.drawable.banner3);

        vpPromoBanner_HomeFragLay.setAdapter(new PromoBanner_PagerAdapter(getActivity(), myImageList));
        tlPromoBannerDot_HomeFragLay.setupWithViewPager(vpPromoBanner_HomeFragLay, true);


        return view;
    }

    @Override
    public void onSuccess(Discover_Pojo trendsResponse) {


        discover_pojos = (ArrayList<Discover_Pojo.Video>) trendsResponse.getVideos();

        DiscoverTrending_Adapter homeScreen_adapter = new DiscoverTrending_Adapter(context, discover_pojos);
//        rvTrending_DiscoverFragLay.setAdapter(homeScreen_adapter);


    }

    @Override
    public void onSuccessUser(ArrayList<User_Pojo> trendsResponse) {
/*
        user_pojos=trendsResponse;
        rv_user.setVisibility(View.VISIBLE);
        User_Adapter homeScreen_adapter = new User_Adapter(context, user_pojos);
        rv_user.setAdapter(homeScreen_adapter);*/

    }

    @Override
    public void onSuccessExploreVideoList(ExploreChannelVideoResponse exploreList) {

    }

    private void changeFragment(Fragment fragment, String from) {

        Bundle arguments = new Bundle();
        arguments.putString("from", from);
        fragment.setArguments(arguments);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContainer, fragment);

        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}

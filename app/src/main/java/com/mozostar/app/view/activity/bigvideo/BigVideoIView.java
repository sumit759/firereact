package com.mozostar.app.view.activity.bigvideo;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.bigvideo.getlist.BigVideoGetListResponse;

public interface BigVideoIView extends MvpView {
    void onSuccessBigVideoList(BigVideoGetListResponse bigVideoGetListResponse);
}

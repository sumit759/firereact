package com.mozostar.app.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.common.UIUtils;
import com.mozostar.app.common.VideoUtil;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class VideoGridChannelAdapter extends RecyclerView.Adapter<VideoGridChannelAdapter.VideoHolder> {

    private Context mContext;
    private List<ChannelVideoList_Pojo.Videos> mDatas;
    private OnItemClickListener mOnItemClickListener;

    public VideoGridChannelAdapter(Context context, List<ChannelVideoList_Pojo.Videos> data) {
        mContext = context;
        mDatas = data;
    }

    public void setData(List<ChannelVideoList_Pojo.Videos> datas) {
        mDatas = datas;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDatas == null ? 0 : mDatas.size();
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VideoHolder(
            LayoutInflater.from(mContext).inflate(R.layout.item_grid_video, null, false));
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        ChannelVideoList_Pojo.Videos model = mDatas.get(position);
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(model.getVideo(), MediaStore.Video.Thumbnails.MINI_KIND);

        Glide.with(mContext)
                .asBitmap()
                .load(VideoUtil.getVideoFilePath(model.getVideo()))
            .into(holder.mIv);
        holder.mTvDuration.setVisibility(View.INVISIBLE);

        holder.itemView.setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(position, model);
                holder.rb.setChecked(true);

            }
        });

        holder.rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position, model);
                    holder.rb.setChecked(true);
                }
            }
        });
    }



    class VideoHolder extends RecyclerView.ViewHolder {

        ImageView mIv;
        TextView mTvDuration;
        RadioButton rb;


        public VideoHolder(View itemView) {
            super(itemView);
            mIv = itemView.findViewById(R.id.iv);
            mTvDuration = itemView.findViewById(R.id.tv_duration);
            rb = itemView.findViewById(R.id.iv_radio_option);
            int size = UIUtils.getScreenWidth() / 4;
            LayoutParams params = (LayoutParams) mIv.getLayoutParams();
            params.width = size;
            params.height = size;
            params.bottomMargin=5;
            params.topMargin=5;
            params.leftMargin=5;
            params.rightMargin=5;
            mIv.setLayoutParams(params);
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ChannelVideoList_Pojo.Videos model);
    }
}

package com.mozostar.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.view.activity.ExoPlayerActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverTrendingChild_Adapter extends RecyclerView.Adapter<DiscoverTrendingChild_Adapter.MyViewHolder> {
    Context context;
    List<Discover_Pojo.Video_> cover_ArrayList;
    DiscoverTrending_Adapter.MyViewHolder mmyViewHolder;
    int clickPosition;
    String from;


    public DiscoverTrendingChild_Adapter(Context context, List<Discover_Pojo.Video_> cover_ArrayList,
                                         int clickPosition, DiscoverTrending_Adapter.MyViewHolder myViewHolder) {
        this.context = context;
        this.cover_ArrayList = cover_ArrayList;
        this.clickPosition = clickPosition;
        this.mmyViewHolder = myViewHolder;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trendingchild_rowitem, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Glide.with(context).load("" + cover_ArrayList.get(i).getVideo()).into(myViewHolder.ivTrendImage_TrendChildLay);


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedHelper.putKey(context, "video_view", "hash_tag");


                Log.e("Position", "Parent: " + mmyViewHolder.getAdapterPosition()+" Child: " +i);
                Intent intent = new Intent(context, ExoPlayerActivity.class);
//                intent.putExtra("list",cover_ArrayList );
                intent.putExtra("position", "" + mmyViewHolder.getAdapterPosition());
                intent.putExtra("position_list", "" + myViewHolder.getAdapterPosition());
                intent.putExtra("guest_id", "" + cover_ArrayList.get(i).getUserId());
                intent.putExtra("video", "" + cover_ArrayList.get(i).getVideo());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return cover_ArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivTrendImage_TrendChildLay)
        ImageView ivTrendImage_TrendChildLay;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        @Override
        public void onClick(View v) {

        }
    }
}

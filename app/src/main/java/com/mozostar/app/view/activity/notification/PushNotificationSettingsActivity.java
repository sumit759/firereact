package com.mozostar.app.view.activity.notification;

import android.view.View;
import android.widget.ImageView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PushNotificationSettingsActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;

    @Override
    public int getLayoutId() {
        return R.layout.activity_push_notification;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}

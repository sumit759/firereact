package com.mozostar.app.view.fragment.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class QRCodeProfileActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.user_img)
    CircleImageView user_img;

    @BindView(R.id.user_profile_name)
    TextView user_profile_name;

    @BindView(R.id.qr_code_image)
    ImageView qr_code_image;

    String user_name, qr_code_url;

    @Override
    public int getLayoutId() {
        return R.layout.activity_qr_code;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            user_name = extras.getString("user_name");
            qr_code_url = extras.getString("qr_code_url");

            user_profile_name.setText(user_name);

            Glide.with(activity()).load(qr_code_url)
                    .into(qr_code_image);

        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}

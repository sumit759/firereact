package com.mozostar.app.view.fragment.profile;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Profile_Pojo;

import java.util.ArrayList;

public interface ProfileIView extends MvpView {
    void onSuccessProfile(ArrayList<Profile_Pojo> trendsResponse);
}

package com.mozostar.app.view.activity.channel;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.AddCategory_Pojo;
import com.mozostar.app.data.model.CategoryListPojo;

public interface ChannelIView extends MvpView {
    void onSuccessCategory(CategoryListPojo trendsResponse);
    void onSuccessCreate(AddCategory_Pojo trendsResponse);
}

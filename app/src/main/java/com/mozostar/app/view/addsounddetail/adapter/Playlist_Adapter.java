package com.mozostar.app.view.addsounddetail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.view.addsounddetail.PlaylistOnClick;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Playlist_Adapter extends RecyclerView.Adapter<Playlist_Adapter.MyViewHolder> {
    ArrayList<String> mlist_arraylist;
    Context mContext;
    PlaylistOnClick pagerAdapterClicK;

    public Playlist_Adapter(Context context, PlaylistOnClick addSound_discover_frag, ArrayList<String> list_arraylist) {
        mContext = context;
        pagerAdapterClicK = addSound_discover_frag;
        mlist_arraylist = list_arraylist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_child_itemrow, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.tvName_PlaylistChildLay.setText(mlist_arraylist.get(i));
        myViewHolder.ll_PlaylistChildLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagerAdapterClicK.onClickPlaylistItem(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mlist_arraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvName_PlaylistChildLay)
        TextView tvName_PlaylistChildLay;
        @BindView(R.id.ll_PlaylistChildLay)
        LinearLayout ll_PlaylistChildLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

package com.mozostar.app.view.activity.chat;

import com.google.gson.JsonObject;
import com.mozostar.app.base.MvpPresenter;

public interface ChatIPresenter<V extends ChatIView> extends MvpPresenter<V> {
    void sendFCM(JsonObject jsonObject);
}

package com.mozostar.app.view.videodub_addsound.frag.discover_sound;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.network.APIClient;
import com.mozostar.app.data.model.SoundsPlayList_Pojo;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SoundPlaylistPresenter<V extends SoundPlaylistIView> extends BasePresenter<V> implements SoundPlaylistIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void getSoundPlayList() {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().sound_playlist();
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessSoundsPlayList((List<SoundsPlayList_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }
}

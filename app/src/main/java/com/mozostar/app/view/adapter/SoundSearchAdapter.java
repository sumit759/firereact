package com.mozostar.app.view.adapter;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.data.model.Sounds_Pojo;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoundSearchAdapter extends RecyclerView.Adapter<SoundSearchAdapter.MyViewHolder> {

    Context context;
    ArrayList<Sounds_Pojo> soundsPojoArrayList;
    public static MediaPlayer mediaPlayerMain = null;
    MediaPlayer mPlayer;
    int sel_pos = -1;

    public SoundSearchAdapter(Context context, ArrayList<Sounds_Pojo> sounds_pojos) {
        this.context = context;
        this.soundsPojoArrayList = sounds_pojos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_child_itemrow, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Sounds_Pojo sounds_pojo = soundsPojoArrayList.get(i);
        myViewHolder.tvName_PlaylistChildLay.setText(sounds_pojo.getTitle());
        myViewHolder.tvDecrip_PlaylistChildLay.setText(sounds_pojo.getDescription());
        myViewHolder.tvDuration_PlaylistChildLay.setText("00:" + sounds_pojo.getDuration());
        Glide.with(context).load(BuildConfig.BASE_IMAGE_URL + sounds_pojo.getPicture())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_launcher_background)
                        .dontAnimate().error(R.drawable.ic_launcher_background))
                .into(myViewHolder.ivImg_PlaylistChildLay);

      /*  myViewHolder.ivDone_PlaylistChildLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sounds_Pojo sounds_pojo = soundsPojoArrayList.get(i);
                pagerAdapterClicK.onClickPlaylistItem(i,sounds_pojo);
                if (mPlayer != null) {
                    if (mPlayer.isPlaying()) {
                        mPlayer.stop();
                    }
                }
            }
        });*/

        myViewHolder.ivImg_PlaylistChildLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sounds_Pojo sounds_pojo = soundsPojoArrayList.get(i);
                String audioUrl = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getAudio();
                sel_pos = i;
                notifyDataSetChanged();
                if (mPlayer != null) {
                    if (mPlayer.isPlaying()) {
                        mPlayer.pause();
                        myViewHolder.ivPlayPause_PlaylistChildLay.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play_arrow));

                    }else {
                        mPlayer.start();
                    }
                }
                stream_audio(audioUrl, context);

            }
        });

        if (sel_pos == i) {
            myViewHolder.ivDone_PlaylistChildLay.setVisibility(View.GONE);
            myViewHolder.ivPlayPause_PlaylistChildLay.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_pause));
        } else {
            myViewHolder.ivDone_PlaylistChildLay.setVisibility(View.GONE);
            myViewHolder.ivPlayPause_PlaylistChildLay.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play_arrow));
        }
    }
    private void stream_audio(String audioUrl, Context mContext) {
        try {
            mPlayer = new MediaPlayer();
//            if (mPlayer.isPlaying()) {
//                mPlayer.stop();
//            }
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.setDataSource(audioUrl);
            mPlayer.prepare();
            mPlayer.start();
            mediaPlayerMain = mPlayer;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return soundsPojoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvName_PlaylistChildLay)
        TextView tvName_PlaylistChildLay;
        @BindView(R.id.tvDecrip_PlaylistChildLay)
        TextView tvDecrip_PlaylistChildLay;
        @BindView(R.id.tvDuration_PlaylistChildLay)
        TextView tvDuration_PlaylistChildLay;
        @BindView(R.id.ll_PlaylistChildLay)
        LinearLayout ll_PlaylistChildLay;
        @BindView(R.id.ivImg_PlaylistChildLay)
        ImageView ivImg_PlaylistChildLay;
        @BindView(R.id.ivDone_PlaylistChildLay)
        ImageView ivDone_PlaylistChildLay;
        @BindView(R.id.ivPlayPause_PlaylistChildLay)
        ImageView ivPlayPause_PlaylistChildLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        @Override
        public void onClick(View v) {

        }
    }
}

package com.mozostar.app.view.fragment.profile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.DrawerBaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Profile_Pojo;
import com.mozostar.app.view.activity.follow.FollowersActivity;
import com.mozostar.app.view.activity.settings.SettingsPrivacyActivity;
import com.mozostar.app.view.activity.update_profile.EditProfileActivity;
import com.mozostar.app.view.fragment.profilefragment.ProfileBigVideo_Fragment;
import com.mozostar.app.view.fragment.profilefragment.ProfileDub_Fragment;
import com.mozostar.app.view.fragment.profilefragment.ProfileLikes_Fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class Profile_Fragment extends DrawerBaseActivity implements ProfileIView {


    @BindView(R.id.user_img)
    CircleImageView user_img;

    @BindView(R.id.tool_profile)
    ImageView tool_profile;

    @BindView(R.id.user_prfl_name)
    TextView user_prfl_name;

    @BindView(R.id.user_prfl_bio)
    TextView user_prfl_bio;

    @BindView(R.id.following_count_txt)
    TextView following_count_txt;

    @BindView(R.id.followers_count_txt)
    TextView followers_count_txt;

    @BindView(R.id.like_count_txt)
    TextView like_count_txt;

    @BindView(R.id.vpContent_GuestProfLay)
    ViewPager vpContent_GuestProfLay;

    @BindView(R.id.tbList_GuestProfLay)
    TabLayout tbList_GuestProfLay;

    @BindView(R.id.edit_prfl_btn)
    Button edit_prfl_btn;

    @BindView(R.id.followin_lnr_lay)
    LinearLayout followin_lnr_lay;

    @BindView(R.id.followers_lay)
    LinearLayout followers_lay;

    @BindView(R.id.likes_lnr_lay)
    LinearLayout likes_lnr_lay;

    @BindView(R.id.settings_img)
    ImageView settings_img;

    @BindView(R.id.qr_code)
    ImageView qr_code;

    @BindView(R.id.find_friend_iv)
    ImageView find_friend_iv;

    String qr_url;
    int likedStatusCount, commentStatusCount, reactStatusCount;

    private int[] tabIcons = {
            R.drawable.ic_vertical_grid,
            R.drawable.ic_play_black_arrow,
            R.drawable.ic_guest_heart
    };

    ProfilePresenter<Profile_Fragment> presenter = new ProfilePresenter<>();

    /*use to set navigation drawer*/
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.llContent)
    LinearLayout llContent;

    @Override
    public int getLayoutId() {

        return R.layout.profile_frag;

    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        presenter.getProfiel();
        setupViewPager(vpContent_GuestProfLay);
        tbList_GuestProfLay.setupWithViewPager(vpContent_GuestProfLay);
        setupTabIcons();
        setDrawerLockModes();


    }


    public void openCloseRightDrawer() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {

            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END);
            } else {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        } else {
            onBackPressed();
        }
    }

    public void openCloseDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
        }

    }

    private void setDrawerLockModes() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    private void setupTabIcons() {
        Objects.requireNonNull(tbList_GuestProfLay.getTabAt(0)).setIcon(tabIcons[0]);
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                llContent.setTranslationX(slideX);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);


//        Objects.requireNonNull(tbList_GuestProfLay.getTabAt(1)).setIcon(tabIcons[1]);   //  Big Video
//        Objects.requireNonNull(tbList_GuestProfLay.getTabAt(2)).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ProfileDub_Fragment(), "ProfileDub");
        //  adapter.addFragment(new ProfileChannel_Fragment(), "ProfileChe");
        adapter.addFragment(new ProfileBigVideo_Fragment(), "ProfileBigVideo");
        adapter.addFragment(new ProfileLikes_Fragment(), "ProfileLikes");

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tbList_GuestProfLay));
        onTabSelectedListener(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return 1;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //  return mFragmentTitleList.get(position);
            return null;
        }
    }

    @Override
    public void onSuccessProfile(ArrayList<Profile_Pojo> trendsResponse) {

        if (trendsResponse.get(0).getImage() != null)
            Glide.with(activity()).load(trendsResponse.get(0).getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
                            .dontAnimate().error(R.drawable.user_profile)).into(user_img);

        if (trendsResponse.get(0).getSafetydetails() != null) {

            likedStatusCount = trendsResponse.get(0).getSafetydetails().getLikeStatus();
            commentStatusCount = trendsResponse.get(0).getSafetydetails().getCommentStatus();
            reactStatusCount = trendsResponse.get(0).getSafetydetails().getReactStatus();
            SharedHelper.putKey(this, "likedStatusCount", likedStatusCount);
            SharedHelper.putKey(this, "commentStatusCount", commentStatusCount);
            SharedHelper.putKey(this, "reactStatusCount", reactStatusCount);
        }


//        tool_profile.setText(trendsResponse.get(0).getFirstName() + " " + trendsResponse.get(0).getLastName());

        user_prfl_name.setText("@" + trendsResponse.get(0).getFirstName() + " " + trendsResponse.get(0).getLastName());

        following_count_txt.setText("" + trendsResponse.get(0).getUserfollowingCount());

        followers_count_txt.setText("" + trendsResponse.get(0).getUserfollowerCount());

        like_count_txt.setText("" + trendsResponse.get(0).getUserlikesCount());

        qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trendsResponse.get(0).getSafetydetails() != null) {

                    qr_url = trendsResponse.get(0).getSafetydetails().getQrcode();

                    Bundle bundle = new Bundle();
                    bundle.putString("user_name", trendsResponse.get(0).getFirstName() + " " + trendsResponse.get(0).getLastName());
                    bundle.putString("qr_code_url", qr_url);
                    ActivityManager.redirectToActivityWithBundle(Profile_Fragment.this, QRCodeProfileActivity.class, bundle);
                } else {
                    Toast.makeText(Profile_Fragment.this, "QR Code Not Available", Toast.LENGTH_LONG).show();
                }
                // Intent qrCodeIntent = new Intent(this, PushNotificationSettingsActivity.class);

                // startActivity(qrCodeIntent);
            }
        });

        Log.e("qrcode", "" + trendsResponse.get(0).getSafetydetails().getQrcode());

        if (trendsResponse.get(0).getUserBio() != null && !trendsResponse.get(0).getUserBio().equals("null"))
            user_prfl_bio.setText("" + trendsResponse.get(0).getUserBio());
//        else
//            user_prfl_bio.setVisibility(View.GONE);


    }

    @OnClick({R.id.edit_prfl_btn, R.id.followers_lay, R.id.followin_lnr_lay, R.id.settings_img, R.id.find_friend_iv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_prfl_btn:

                Intent update_prfl = new Intent(this, EditProfileActivity.class);
                startActivity(update_prfl);

                break;
            case R.id.followers_lay:
                SharedHelper.putKey(this, "on_click_follow", "follower");
                Intent follower = new Intent(this, FollowersActivity.class);
                startActivity(follower);
                break;

            case R.id.followin_lnr_lay:
                SharedHelper.putKey(this, "on_click_follow", "following");
                Intent following = new Intent(this, FollowersActivity.class);
                startActivity(following);
                break;

            case R.id.settings_img:
                Intent settingPrivacy = new Intent(this, SettingsPrivacyActivity.class);
                startActivity(settingPrivacy);
                break;

            case R.id.find_friend_iv:
                openCloseDrawer();
//                Intent findFriend = new Intent(this, FindFriendsActivity.class);
//                startActivity(findFriend);
                break;
        }

    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

}

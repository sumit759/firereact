package com.mozostar.app.view.activity.channel;

import androidx.annotation.NonNull;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.AddCategory_Pojo;
import com.mozostar.app.data.model.CategoryListPojo;
import com.mozostar.app.MvpApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class ChannelActivity extends BaseActivity implements ChannelIView {


    @BindView(R.id.create)
    TextView create;
    @BindView(R.id.cancel)
    TextView cancel;

    @BindView(R.id.upload_img)
    CircleImageView upload_img;

    @BindView(R.id.channel_category)
    TextView channel_category;

    @BindView(R.id.private_channel)
    Switch private_channel;

    @BindView(R.id.channel_name)
    EditText channel_name;
    ArrayAdapter<String> arrayAdapter;

    ChannelPresenter<ChannelActivity> presenter=new ChannelPresenter<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_channel;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);
        presenter.attachView(this);
        presenter.getCategory();

        id= ""+ SharedHelper.getIntKey(this,"user_id");

        upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        channel_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(ChannelActivity.this);
                builderSingle.setTitle("Select One Category");

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        channel_category.setText(arrayAdapter.getItem(which));
                        category=ids.get(which);
                       /* String strName = arrayAdapter.getItem(which);
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(ChannelActivity.this);
//                        builderInner.setMessage(strName);
//                        builderInner.setTitle("Your Selected Item is");
                        builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builderInner.show();*/
                    }
                });
                builderSingle.show();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name=channel_name.getText().toString();
                if (private_channel.isChecked()){
                    privacy="1";
                }else {
                    privacy="0";
                }

                if (TextUtils.isEmpty(name)){
                    Toast.makeText(ChannelActivity.this,"Please Enter The Channel Name",Toast.LENGTH_LONG).show();
                    return;
                }else if(TextUtils.isEmpty(category)){
                    Toast.makeText(ChannelActivity.this,"Please Selcet The Channel Category",Toast.LENGTH_LONG).show();
                    return;
                }else if(imgFile==null){
                    Toast.makeText(ChannelActivity.this,"Please Select The Channel Image",Toast.LENGTH_LONG).show();
                    return;
                }else {
                    HashMap<String, RequestBody> map = new HashMap<>();
                    map.put("name", toRequestBody(name));
                    map.put("user_id", toRequestBody(id));
                    map.put("privacy", toRequestBody(privacy));
                    map.put("channel_category", toRequestBody(category));

                    MultipartBody.Part filePart = null;

                    if (imgFile != null)
                        filePart = MultipartBody.Part.createFormData("image", imgFile.getName(), RequestBody.create(MediaType.parse("image*//*"), imgFile));

                    presenter.add_category(map, filePart);

                }

            }
        });

    }

    String name,privacy,id,category;

    ArrayList<String> ids=new ArrayList<>();

    public static RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    @Override
    public void onSuccessCategory(CategoryListPojo trendsResponse) {



        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.select_dialog_singlechoice);
        int size=trendsResponse.getData().size();
        for (int i=0;i<size;i++){
            arrayAdapter.add(trendsResponse.getData().get(i).getName());
            ids.add(""+trendsResponse.getData().get(i).getId());
        }

    }

    @Override
    public void onSuccessCreate(AddCategory_Pojo trendsResponse) {

        Toast.makeText(ChannelActivity.this,trendsResponse.getMessage(),Toast.LENGTH_LONG).show();
        onBackPressed();
        finish();

    }

    private void pickImage() {
        if (hasPermission(Manifest.permission.CAMERA) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            EasyImage.openChooserWithGallery(this, "", 0);
        } else {
            requestPermissionsSafely(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MvpApplication.ASK_MULTIPLE_PERMISSION_REQUEST_CODE);
        }
    }

    File imgFile = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, ChannelActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imgFile = imageFiles.get(0);
                Glide.with(activity()).load(Uri.fromFile(imgFile)).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate()
                        .error(R.drawable.user_profile))
                        .into(upload_img);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {

            }
        });
    }
}

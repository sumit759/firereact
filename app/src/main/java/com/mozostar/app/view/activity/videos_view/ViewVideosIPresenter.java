package com.mozostar.app.view.activity.videos_view;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface ViewVideosIPresenter <V extends ViewVideosIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getList(String id);

    @SuppressLint("CheckResult")
    void getDubList(String id);

    @SuppressLint("CheckResult")
    void getProfileDubList();

    @SuppressLint("CheckResult")
    void getProfileList();

    @SuppressLint("CheckResult")
    void like_video(HashMap<String, Object> obj);

    @SuppressLint("CheckResult")
    void post_comment(HashMap<String, Object> obj);

    @SuppressLint("CheckResult")
    void getCommentList(String id);

    @SuppressLint("CheckResult")
    void getHashList();

    @SuppressLint("CheckResult")
    void followup(HashMap<String, Object> obj);

    @SuppressLint("CheckResult")
    void delete_video(String id);

    @SuppressLint("CheckResult")
    void getChannelList(String id);
}

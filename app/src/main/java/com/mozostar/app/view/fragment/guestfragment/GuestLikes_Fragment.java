package com.mozostar.app.view.fragment.guestfragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.adapter.GuestLikes_Adapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuestLikes_Fragment extends BaseFragment implements GuestVideoIView {
    Context context;
    private static String TAG = GuestLikes_Fragment.class.getSimpleName();

    @BindView(R.id.rvTrending_GuestDubLay)
    RecyclerView rvTrending_GuestDubLay;

    GuestVideoPresenter<GuestLikes_Fragment> presenter = new GuestVideoPresenter<>();
    ArrayList<String> cover_ArrayList = new ArrayList<>();
    ArrayList<String> list = new ArrayList<>();

    String guest_id;
    public GuestLikes_Fragment() {
        // Required empty public constructor
    }

    @Override
    public int getLayoutId() {
        return R.layout.guestdub_lay;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        Bundle bundle = this.getArguments();
        guest_id = bundle.getString("guest_id");
        if (!TextUtils.isEmpty(guest_id))
            presenter.getList(guest_id);
        rvTrending_GuestDubLay.setLayoutManager(new GridLayoutManager(context, 3));
        rvTrending_GuestDubLay.setItemAnimator(new DefaultItemAnimator());
        // rvTrending_GuestDubLay.setNestedScrollingEnabled(true);

        return view;
    }

    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

        int size = trendsResponse.size();
        for (int i=0;i<size;i++){
            cover_ArrayList.add(trendsResponse.get(i).getVideo());
        }

        GuestLikes_Adapter guestLikes_adapter = new GuestLikes_Adapter(context,trendsResponse);
        rvTrending_GuestDubLay.setAdapter(guestLikes_adapter);
    }

    @Override
    public void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

    }
}

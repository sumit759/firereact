package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.R;
import com.mozostar.app.common.UIUtils;
import com.mozostar.app.common.VideoUtil;
import com.mozostar.app.data.model.bigvideo.getlist.VideosItem;

import java.util.List;

public class BigVideoGridAdapter extends RecyclerView.Adapter<BigVideoGridAdapter.VideoHolder> {

    private Context mContext;
    List<VideosItem> bigVideoArrayList;
    private OnItemClickListener mOnItemClickListener;

    public BigVideoGridAdapter(Context context, List<VideosItem> bigVideoList,OnItemClickListener mOnItemClick) {
        mContext = context;
        bigVideoArrayList = bigVideoList;
        mOnItemClickListener = mOnItemClick;
    }

    public void setData(List<VideosItem> bigVideoList) {
        bigVideoArrayList = bigVideoList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return bigVideoArrayList == null ? 0 : bigVideoArrayList.size();
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VideoHolder(
                LayoutInflater.from(mContext).inflate(R.layout.item_big_video_grid, null, false));
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        VideosItem videosItem = bigVideoArrayList.get(position);
        Glide.with(mContext)
                .load(videosItem.getVideoImageUrl()).apply(RequestOptions.placeholderOf(R.mipmap.ic_launcher))  // VideoUtil.getVideoFilePath(videosItem.getVideo())
                .into(holder.mIv);
        holder.itemView.setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(position, videosItem);
            }
        });
    }


    class VideoHolder extends RecyclerView.ViewHolder {

        ImageView mIv;
        TextView mTvDuration;
        //  RadioButton rb;

        public VideoHolder(View itemView) {
            super(itemView);
            mIv = itemView.findViewById(R.id.iv);
            mTvDuration = itemView.findViewById(R.id.tv_duration);
            //  rb = itemView.findViewById(R.id.iv_radio_option);
            int size = UIUtils.getScreenWidth() / 4;
            LayoutParams params = (LayoutParams) mIv.getLayoutParams();
            params.width = size;
            params.height = size;
            params.bottomMargin = 5;
            params.topMargin = 5;
            params.leftMargin = 5;
            params.rightMargin = 5;

            mIv.setLayoutParams(params);
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, VideosItem videosItem);
    }
}

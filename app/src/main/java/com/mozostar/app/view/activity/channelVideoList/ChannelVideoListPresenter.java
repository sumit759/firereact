package com.mozostar.app.view.activity.channelVideoList;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;
import com.mozostar.app.data.network.APIClient;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChannelVideoListPresenter<V extends ChannelVideoListIView> extends BasePresenter<V> implements ChannelVideoListIPresenter<V>  {

    @SuppressLint("CheckResult")
    @Override
    public void getList(String id) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().channel_video_list(id);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessChannelVideoList((ChannelVideoList_Pojo) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }
}

package com.mozostar.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.view.activity.hashtag_list.Hash_VideoList_Activity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverTrending_Adapter extends RecyclerView.Adapter<DiscoverTrending_Adapter.MyViewHolder> {
    Context context;
    ArrayList<String> stringArrayList;


    ArrayList<Discover_Pojo.Video> discover_pojos;

    int clickPosition;


    public DiscoverTrending_Adapter(Context context, ArrayList<Discover_Pojo.Video> discover_pojos) {
        this.context = context;
        this.discover_pojos = discover_pojos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_rowitem, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        int size = discover_pojos.get(i).getVideos().size();

        myViewHolder.videos_count.setText("" + size + " >");

        myViewHolder.videos_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String position=""+myViewHolder.getAdapterPosition();

//                Intent intent = new Intent(context, Hash_VideoList_Activity.class);
//                intent.putExtra("position", position);
//                context.startActivity(intent);
            }
        });


        List<Discover_Pojo.Video_> cover_ArrayList = discover_pojos.get(i).getVideos();


        DiscoverTrendingChild_Adapter homeScreen_adapter = new DiscoverTrendingChild_Adapter(context,
                cover_ArrayList, clickPosition,myViewHolder);


        myViewHolder.rvTrending_TrendRowItemLay.setAdapter(homeScreen_adapter);
        myViewHolder.tvTrendTitle_TrendRowItemLay.setText("" + discover_pojos.get(i).getHagtagName());


    }

    @Override
    public int getItemCount() {
        return discover_pojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.rvTrending_TrendRowItemLay)
        RecyclerView rvTrending_TrendRowItemLay;
        @BindView(R.id.tvTrendTitle_TrendRowItemLay)
        TextView tvTrendTitle_TrendRowItemLay;
        @BindView(R.id.videos_count)
        TextView videos_count;

        MyViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);


            rvTrending_TrendRowItemLay.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            rvTrending_TrendRowItemLay.setItemAnimator(new DefaultItemAnimator());
//

        }

        @Override
        public void onClick(View v) {

        }
    }
}

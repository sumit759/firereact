package com.mozostar.app.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mozostar.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RateReview extends AppCompatActivity {


    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_review);
        ButterKnife.bind(this);

        tvTitle_ToolBarLay.setText(R.string.rate_review);
        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
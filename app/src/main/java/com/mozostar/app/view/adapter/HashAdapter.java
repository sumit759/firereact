package com.mozostar.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.view.activity.videos_view.ViewVideosActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HashAdapter extends RecyclerView.Adapter<HashAdapter.MyViewHolder> {
    Context context;
    ArrayList<Discover_Pojo.Video_> cover_ArrayList;

    public HashAdapter(Context context, ArrayList<Discover_Pojo.Video_> cover_ArrayList) {
        this.context = context;
        this.cover_ArrayList = cover_ArrayList;
    }

    @NonNull
    @Override
    public HashAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trendingchild_rowitem, parent, false);
        return new HashAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HashAdapter.MyViewHolder myViewHolder, int i) {
        Glide.with(context).load("" + cover_ArrayList.get(i).getVideo()).into(myViewHolder.ivTrendImage_TrendChildLay);


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedHelper.putKey(context, "video_view", "hash_tag");

                Intent intent = new Intent(context, ViewVideosActivity.class);
                intent.putExtra("position", i);
                intent.putExtra("position_list", "" + myViewHolder.getAdapterPosition());
                intent.putExtra("guest_id", "" + cover_ArrayList.get(i).getUserId());

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return cover_ArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivTrendImage_TrendChildLay)
        ImageView ivTrendImage_TrendChildLay;
        @BindView(R.id.llComments_TrendChildLay)
        LinearLayout llComments_TrendChildLay;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            llComments_TrendChildLay.setVisibility(View.GONE);
        }


        @Override
        public void onClick(View v) {

        }
    }
}

package com.mozostar.app.view.login;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.Login_Pojo;
import com.mozostar.app.data.model.Reset_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class  LoginPresenter<V extends LoginIView> extends BasePresenter<V> implements LoginIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void postlogin(HashMap<String, Object> obj) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().login(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessLogin((Login_Pojo) trendsResponse);
                        },
                        throwable -> {
                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }


    @SuppressLint("CheckResult")
    @Override
    public void reset(HashMap<String, Object> obj) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().reset(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessReset((Reset_Pojo) trendsResponse);
                        },
                        throwable -> {
                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }
}

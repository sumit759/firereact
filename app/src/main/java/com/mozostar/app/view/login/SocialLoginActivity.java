package com.mozostar.app.view.login;

import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.view.register.SocialRegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialLoginActivity extends BaseActivity {

    @BindView(R.id.email_social_login_layout)
    RelativeLayout email_social_login_layout;

    @BindView(R.id.gmail_social_loginlayout)
    RelativeLayout gmail_social_loginlayout;

    @BindView(R.id.facebook_social_login_layout)
    RelativeLayout facebook_social_login_layout;

    @BindView(R.id.signup_link_layout)
    RelativeLayout signup_link_layout;


    @Override
    public int getLayoutId() {
        return R.layout.activity_social_login;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);

        email_social_login_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLoginActivity();
            }
        });
        facebook_social_login_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLoginActivity();
            }
        });
        gmail_social_loginlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               callLoginActivity();
            }
        });

        signup_link_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(SocialLoginActivity.this, SocialRegisterActivity.class);
                startActivity(register);
            }
        });
    }

    private void callLoginActivity() {
        Intent register = new Intent(SocialLoginActivity.this, Login_Activity.class);
        startActivity(register);
    }
}
package com.mozostar.app.view.fragment.homedashboard;

public interface PagerAdapterOnClick {
    void onClickGuestProfile();
    void onClickSoundTrack();
}

package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TextColor_recyler_Adapter extends RecyclerView.Adapter<TextColor_recyler_Adapter.MyViewHolder> {
    Context context;
    int[] imgList = {R.drawable.white_color_circle,R.drawable.black_color_circle,R.drawable.blue_color_circle,R.drawable.green_color_circle,R.drawable.orange_color_circle, R.drawable.pink_color_circle,R.drawable.purple_color_circle,R.drawable.yellow_color_circle};



    int clickPosition;


    public TextColor_recyler_Adapter(Context context) {
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.color_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.name.setVisibility(View.GONE);
        myViewHolder.image.setImageResource(imgList[i]);


        myViewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((VideoDubRecord_Activity_new)context).Filter_pos(i);

            }
        });



    }

    @Override
    public int getItemCount() {
        return imgList.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.filter_item_layout)
        LinearLayout itemLayout;
        @BindView(R.id.filter_item_text)
        TextView name;
        @BindView(R.id.filter_item_image)
        ImageView image;

        MyViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);
            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "ok", Toast.LENGTH_SHORT).show();
                }
            });


        }


    }
}

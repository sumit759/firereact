package com.mozostar.app.view.activity.bigvideo;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.common.DoubleClickListener;
import com.mozostar.app.common.SingletonClassVideoBlock;
import com.mozostar.app.custom_ui.VerticalViewPager;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.VideoList_Pojo1;
import com.mozostar.app.data.model.bigvideo.getlist.BigVideoGetListResponse;
import com.mozostar.app.data.model.bigvideo.getlist.VideosItem;
import com.mozostar.app.view.DividerGridItemDecoration;
import com.mozostar.app.view.activity.ExoPlayerActivity;
import com.mozostar.app.view.activity.guest.GuestProfile_Activity;
import com.mozostar.app.view.activity.sound_list.SoundTrack_Activity;
import com.mozostar.app.view.adapter.BigVideoGridAdapter;
import com.mozostar.app.view.adapter.BigVideoNewAdapter;
import com.mozostar.app.view.adapter.BigVideoNewPagerAdapter;
import com.mozostar.app.view.adapter.HomeScreen_Adapter;
import com.mozostar.app.view.adapter.HomeScreen_PagerAdapter;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class BigVideoListActivity extends BaseActivity implements BigVideoIView, EventListener, Player.EventListener {

    @BindView(R.id.big_video_list_rv)
    RecyclerView big_video_list_rv;

    @BindView(R.id.vvpBigVideoList)
    VerticalViewPager vvpBigVideoList;

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView title;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView option;

    @BindView(R.id.back)
    ImageView backArrow;

    Context context;
    BigVideoPresenter<BigVideoListActivity> presenter = new BigVideoPresenter<>();

    BigVideoGridAdapter bigVideoGridAdapter;
    private int currentPage = -1;
    private String position;

    @Override
    public int getLayoutId() {
        return (R.layout.activity_big_video_list);
    }

    private LinearLayoutManager layoutManager;

    BigVideoNewAdapter bigVideoNewAdapter;
    BigVideoNewPagerAdapter bigVideoNewPagerAdapter;

    List<VideosItem> bigVideosItemsList = new ArrayList<>();
    private ArrayList<String> mVideoURLList = new ArrayList<>();

    TextView titleVideoTV;
    TextView titleDescriptionVideoTV, header_name_big_videoTV, disLikeCountTv;
    ImageView iv_user_imag_big_video;

    @Override
    public void initView() {
        ButterKnife.bind(this);
        context = BigVideoListActivity.this;
        presenter.attachView(this);
        option.setVisibility(View.INVISIBLE);
        title.setText("Big Videos");
        try {
            presenter.getBigVideoList(true);
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            big_video_list_rv.setLayoutManager(layoutManager);
            big_video_list_rv.setHasFixedSize(false);
            new PagerSnapHelper().attachToRecyclerView(big_video_list_rv);
            Log.i("onSuccess", "" + mVideoURLList.size());
            bigVideoNewAdapter = new BigVideoNewAdapter(context, mVideoURLList);
            big_video_list_rv.setAdapter(bigVideoNewAdapter);

            bigVideoNewPagerAdapter = new BigVideoNewPagerAdapter(context, mVideoURLList);
            vvpBigVideoList.setAdapter(bigVideoNewPagerAdapter);


            vvpBigVideoList.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int newState) {

                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    View view = vvpBigVideoList.getChildAt(position);
                    PlayerView playerView = view.findViewById(R.id.player_view);
                    stopPreviousVideoPlayer();
                }

                public void onPageSelected(int position) {
                    // Check if this is the page you want.
                    Log.i("onPageSelected", "" + position);
                }
            });

            big_video_list_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                    if (i == RecyclerView.SCROLL_STATE_IDLE) {

                    }
                    super.onScrollStateChanged(recyclerView, i);
                }

                public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                    if (i > 0 || i2 < 0) {

                    }
                    super.onScrolled(recyclerView, i, i2);
                    int visiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                    if (visiblePosition > -1) {
                        View v = layoutManager.findViewByPosition(visiblePosition);  //do something
                        stopPreviousVideoPlayer();
                        setVideoPlayerToPlay(visiblePosition);
                    }
                }
            });

            if (!TextUtils.isEmpty(position)) {
                layoutManager.scrollToPosition(Integer.parseInt(position));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopPreviousVideoPlayer() {
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.removeListener(this);
            this.previous_exoplayer.release();
        }
    }


    @Override
    public void onSuccessBigVideoList(BigVideoGetListResponse bigVideoList) {
        try {
            bigVideosItemsList.addAll(bigVideoList.getVideos());
            big_video_list_rv.setVisibility(View.VISIBLE);
            for (int i = 0; i < bigVideoList.getVideos().size(); i++) {
                Log.i("onSuccess", " Video::: " + bigVideoList.getVideos().get(i).getVideo());
                mVideoURLList.add(bigVideoList.getVideos().get(i).getVideo());
            }
            bigVideoNewAdapter.notifyDataSetChanged();
            bigVideoNewPagerAdapter.notifyDataSetChanged();
           /* if (bigVideoList.getVideos().size() > 0) {
                bigVideoGridAdapter = new BigVideoGridAdapter(context, bigVideoList.getVideos(), this);
                big_video_list_rv.setAdapter(bigVideoGridAdapter);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.back})
    public void onBackArrow() {
        try {
            stopPreviousVideoPlayer();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            stopPreviousVideoPlayer();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onBackPressed();
    }


    SimpleExoPlayer previous_exoplayer;

    @SuppressLint("SetTextI18n")
    public void setVideoPlayerToPlay(int i) {
        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(32 * 1024, 64 * 1024, 1024, 1024).createDefaultLoadControl();
        final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this.context, (TrackSelector) new DefaultTrackSelector(), loadControl);
        newSimpleInstance.setRepeatMode(Player.REPEAT_MODE_ONE);
        HttpProxyCacheServer proxyServer = new HttpProxyCacheServer.Builder(getApplicationContext()).maxCacheSize(1024 * 1024 * 1024).build();
        String proxyURL = proxyServer.getProxyUrl(mVideoURLList.get(i));
        newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this.context,
                Util.getUserAgent(this.context, "Fireact"))).createMediaSource(Uri.parse(proxyURL)));
        newSimpleInstance.addListener(this);

        View findViewByPosition = layoutManager.findViewByPosition(i);
        PlayerView playerView = findViewByPosition.findViewById(R.id.player_viewBigVideo);
        titleVideoTV = findViewByPosition.findViewById(R.id.titleVideoTV);
        titleDescriptionVideoTV = findViewByPosition.findViewById(R.id.titleDescriptionVideoTV);
        header_name_big_videoTV = findViewByPosition.findViewById(R.id.header_name_big_videoTV);
        iv_user_imag_big_video = findViewByPosition.findViewById(R.id.iv_user_imag_big_video);
        disLikeCountTv = findViewByPosition.findViewById(R.id.disLikeCountTv);

        try {
            titleVideoTV.setText(bigVideosItemsList.get(i).getTitle());
            titleDescriptionVideoTV.setText(bigVideosItemsList.get(i).getDescription());
            header_name_big_videoTV.setText(bigVideosItemsList.get(i).getUserdetails().getFirstName());
            disLikeCountTv.setText(bigVideosItemsList.get(i).getDislikesCount());
            Glide.with(activity()).load(bigVideosItemsList.get(i).getVideoImageUrl()).apply(RequestOptions.placeholderOf(R.mipmap.ic_launcher).dontAnimate().error(R.mipmap.ic_launcher)).into(iv_user_imag_big_video);
        } catch (Exception e) {
            e.printStackTrace();
        }

        playerView.setPlayer(newSimpleInstance);
        newSimpleInstance.setPlayWhenReady(true);
        previous_exoplayer = newSimpleInstance;
        playerView.setControllerAutoShow(true);
        // playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        // playerView.setUseController(true);

    }


    public void onResume() {
        super.onResume();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(true);
        }
    }

    public void onPause() {
        super.onPause();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.release();
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

}
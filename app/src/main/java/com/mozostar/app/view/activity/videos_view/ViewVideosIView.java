package com.mozostar.app.view.activity.videos_view;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.data.model.DeletePojo;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.adapter.CommentsAdapter;

import java.util.ArrayList;

public interface ViewVideosIView extends MvpView {
    void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse);

    void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse);

    void onSuccessLike(Object trendsResponse);

    void onSuccessComment(CommentPost_Pojo trendsResponse);

    void onSuccessComments(CommentsAdapter adapter);

    void onSuccessHash(Discover_Pojo trendsResponse);

    void onSuccessFollow(Follow_Pojo trendsResponse);

    void onSuccessDelete(DeletePojo trendsResponse);

    void onSuccessChannelVideoList(ChannelVideoList_Pojo trendsResponse);
}

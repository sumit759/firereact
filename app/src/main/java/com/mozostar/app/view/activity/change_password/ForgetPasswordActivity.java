package com.mozostar.app.view.activity.change_password;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.view.login.Login_Activity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordActivity extends BaseActivity implements ForgetPasswordIView {

    @BindView(R.id.et_Otp)
    AppCompatEditText et_otp;

    @BindView(R.id.et_new_password)
    AppCompatEditText et_new_password;

    @BindView(R.id.et_confirm_password)
    AppCompatEditText et_confirm_password;

    @BindView(R.id.fabDone_PassLay)
    FloatingActionButton fabDone_PassLay;

    String email, cpassword, password, eotp, otp, user_id;

    private ForgetPasswordPresenter<ForgetPasswordActivity> presenter = new ForgetPasswordPresenter<>();


    @Override
    public int getLayoutId() {
        return R.layout.activity_forget_password;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);
        presenter.attachView(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            user_id = extras.getString("user_id");
            otp = extras.getString("otp");
        }


    }

    @OnClick({R.id.fabDone_PassLay})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.fabDone_PassLay:

                password = et_new_password.getText().toString();
                cpassword = et_confirm_password.getText().toString();
                eotp = et_otp.getText().toString();

                if (!cpassword.equals(password)) {
                    Toast.makeText(this, getString(R.string.invalid_password1), Toast.LENGTH_LONG).show();
                    return;
                } else if (!otp.equals(eotp)) {
                    Toast.makeText(this, "Invalid OTP", Toast.LENGTH_LONG).show();
                    return;
                } else if (otp.isEmpty()) {
                    Toast.makeText(this, "Enter OTP", Toast.LENGTH_LONG).show();
                    return;
                } else if (password.isEmpty()) {
                    Toast.makeText(this, "Enter password", Toast.LENGTH_LONG).show();
                    return;
                } else if (cpassword.isEmpty()) {
                    Toast.makeText(this, "Enter confirm password", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("id", user_id);
                    map.put("password", password);
                    presenter.reset(map);
                }

                break;

        }

    }

    @Override
    public void onSuccessReset(Follow_Pojo trendsResponse) {
        Intent login = new Intent(ForgetPasswordActivity.this, Login_Activity.class);
        startActivity(login);
        finishAffinity();
        finish();
    }
}

package com.mozostar.app.view.activity.channelVideoList;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;

public interface ChannelVideoListIView extends MvpView {
    void onSuccessChannelVideoList(ChannelVideoList_Pojo channelList_pojoList  );

}

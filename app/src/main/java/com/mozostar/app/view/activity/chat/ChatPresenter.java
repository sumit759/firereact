package com.mozostar.app.view.activity.chat;

import com.google.gson.JsonObject;
import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.network.APIClient;
import com.mozostar.app.data.network.ApiInterface;


import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChatPresenter<V extends ChatIView> extends BasePresenter<V> implements ChatIPresenter<V> {
    @Override
    public void sendFCM(JsonObject jsonObject) {
        ApiInterface FcmAPI = APIClient.getFcmRetrofit().create(ApiInterface.class);
        Observable modelObservable = FcmAPI.sendFcm(jsonObject);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse ->
                                getMvpView().onSuccessFCM(trendsResponse),
                        throwable ->
                                getMvpView().onError((Throwable) throwable));


    }
}

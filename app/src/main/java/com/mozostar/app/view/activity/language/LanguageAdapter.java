package com.mozostar.app.view.activity.language;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {
    Context context;
    List<String> langList = new ArrayList<>();

    public LanguageAdapter(Context context, List<String> languageList) {
        this.context = context;
        this.langList = languageList;
    }

    @NonNull
    @Override
    public LanguageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_adapter, parent, false);
        return new LanguageAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageAdapter.MyViewHolder myViewHolder, int i) {
        String language = langList.get(i);
        myViewHolder.language_name_tv.setText(language);
        myViewHolder.language_layout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(context, AppLanguageSelectActivity.class);
                intent.putExtra("language", language);
                intent.putExtra("id", i);
                context.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return langList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.language_name_tv)
        TextView language_name_tv;

        @BindView(R.id.language_arrow_iv)
        ImageView language_arrow_iv;

        @BindView(R.id.language_layout_ll)
        LinearLayout language_layout_ll;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        @Override
        public void onClick(View v) {

        }
    }
}

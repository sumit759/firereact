package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LikesAdapter extends RecyclerView.Adapter<LikesAdapter.MyViewHolder> {
    Context context;
    List<String> langList = new ArrayList<>();

    public LikesAdapter(Context context, List<String> languageList) {
        this.context = context;
        this.langList = languageList;
    }

    @NonNull
    @Override
    public LikesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.likes_adapter, parent, false);
        return new LikesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LikesAdapter.MyViewHolder myViewHolder, int i) {
        String language = langList.get(i);
    }

    @Override
    public int getItemCount() {
        return langList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.user_id)
        TextView user_id;

        @BindView(R.id.likes_user_following_tv)
        TextView likes_user_following_tv;

        @BindView(R.id.likes_profile_img)
        ImageView likes_profile_img;

        @BindView(R.id.likes_follow_btn)
        Button likes_follow_btn;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        @Override
        public void onClick(View v) {

        }
    }
}

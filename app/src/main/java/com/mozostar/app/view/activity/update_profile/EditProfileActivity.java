package com.mozostar.app.view.activity.update_profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Profile_Pojo;
import com.mozostar.app.data.model.UpdateProfile_Pojo;
import com.mozostar.app.MvpApplication;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class EditProfileActivity extends BaseActivity implements EditProfileIView, AuthenticationListener {

    @BindView(R.id.getYoutubeLink)
    TextView getYoutubeLink;

    @BindView(R.id.firstname_et)
    EditText firstname_et;

    @BindView(R.id.bio_et)
    EditText bio_et;

    @BindView(R.id.last_name_et)
    EditText last_name_et;

    @BindView(R.id.update_btn)
    Button update_btn;

    @BindView(R.id.pick_img_lay)
    FrameLayout pick_img_lay;

    @BindView(R.id.profile_img)
    CircleImageView profile_img;

    @BindView(R.id.insta_link_et)
    EditText insta_link_et;

    @BindView(R.id.youtube_link_et)
    EditText youtube_link_et;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.add_youtubeLink)
    ImageView add_youtubeLink;

//    @BindView(R.id.person_first_name_profile)
//    TextView person_first_name_profile;
//
//    @BindView(R.id.person_last_name_profile)
//    TextView person_last_name_profile;
//
//    @BindView(R.id.bio_name_profile)
//    TextView bio_name_profile;
//
//    @BindView(R.id.mobile_number_tv)
//    TextView mobile_number_tv;
//
//    @BindView(R.id.email_id_profile)
//    TextView email_id_profile;
//
//    @BindView(R.id.instagram_name_profile)
//    TextView instagram_name_profile;
//
//    @BindView(R.id.youtube_name_profile)
//    TextView youtube_name_profile;

    AuthenticationDialog authenticationDialog;

    File imgFile = null;

    private EditProfilePresenter<EditProfileActivity> presenter = new EditProfilePresenter<>();

    String f_name, l_name, insta_link, youtube_link, bio, link;

//    @BindView(R.id.name_layout_ll)
//    LinearLayout name_layout_ll;
//
//    @BindView(R.id.last_name_layout_ll)
//    LinearLayout last_name_layout_ll;
//
//    @BindView(R.id.mobile_layout_ll)
//    LinearLayout mobile_layout_ll;
//
//    @BindView(R.id.email_layout_ll)
//    LinearLayout email_layout_ll;
//
//    @BindView(R.id.bio_layout_ll)
//    LinearLayout bio_layout_ll;
//
//    @BindView(R.id.instagram_layout_ll)
//    LinearLayout instagram_layout_ll;
//
//    @BindView(R.id.youtube_layout_ll)
//    LinearLayout youtube_layout_ll;

    String firstNameStr, lastNameStr, bioStr, mobileStr, emailsStr, instagramStr = "";

    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_profile;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);
        presenter.attachView(this);

        presenter.getProfiel();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public static RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSuccessProfile(ArrayList<Profile_Pojo> trendsResponse) {

//        if (trendsResponse.get(0).getImage() != null)
//            Glide.with(activity()).load(trendsResponse.get(0).getImage()).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate().error(R.drawable.user_profile)).into(profile_img);
//
//        firstname_et.setText(trendsResponse.get(0).getFirstName());
//        last_name_et.setText(trendsResponse.get(0).getLastName());
//        if (!TextUtils.isEmpty(trendsResponse.get(0).getUserBio())) {
//            bio_et.setText("" + trendsResponse.get(0).getUserBio());
//        }
//        link = "" + trendsResponse.get(0).getYoutubeLink();
//
//        if (!link.isEmpty() && !link.equals("") && !link.equals("null")) {
//            getYoutubeLink.setText(link);
//            getYoutubeLink.setVisibility(View.VISIBLE);
//            youtube_link_et.setVisibility(View.GONE);
//            add_youtubeLink.setVisibility(View.VISIBLE);
//        } else {
//            youtube_link_et.setVisibility(View.VISIBLE);
//            getYoutubeLink.setVisibility(View.GONE);
//            add_youtubeLink.setVisibility(View.GONE);
//        }
//
//        person_first_name_profile.setText(trendsResponse.get(0).getFirstName());
//        person_last_name_profile.setText(trendsResponse.get(0).getLastName());
//        mobile_number_tv.setText(trendsResponse.get(0).getMobile());
//        email_id_profile.setText(trendsResponse.get(0).getEmail());
//        if (!TextUtils.isEmpty(trendsResponse.get(0).getUserBio()))
//            bio_name_profile.setText("" + trendsResponse.get(0).getUserBio());
//
//
//        if (!TextUtils.isEmpty(link)) {
//            youtube_name_profile.setText(link);
//        } else {
//            youtube_name_profile.setText("");
//        }
//
//        if (!TextUtils.isEmpty(trendsResponse.get(0).getInstagramLink())) {
//            instagram_name_profile.setText("" + trendsResponse.get(0).getInstagramLink());
//        } else {
//            instagram_name_profile.setText("");
//        }
//
//        tvTitle_ToolBarLay.setText("Edit Profile");
    }

//    @OnClick({R.id.pick_img_lay, R.id.update_btn, R.id.back, R.id.add_instaLink, R.id.getYoutubeLink, R.id.add_youtubeLink,
//            R.id.name_layout_ll, R.id.bio_layout_ll, R.id.instagram_layout_ll, R.id.youtube_layout_ll, R.id.last_name_layout_ll,
//            R.id.email_layout_ll, R.id.mobile_layout_ll,})
//    public void onViewClicked(View view) {
//        firstNameStr = person_first_name_profile.getText().toString();
//        lastNameStr = person_last_name_profile.getText().toString();
//        bioStr = bio_name_profile.getText().toString();
//        mobileStr = mobile_number_tv.getText().toString();
//        emailsStr = email_id_profile.getText().toString();
//        instagramStr = instagram_name_profile.getText().toString();
//        switch (view.getId()) {
//            case R.id.add_youtubeLink:
//                youtube_link_et.setVisibility(View.VISIBLE);
//                getYoutubeLink.setVisibility(View.GONE);
//                add_youtubeLink.setVisibility(View.GONE);
//                break;
//            case R.id.getYoutubeLink:
//                Intent viewIntent =
//                        new Intent("android.intent.action.VIEW",
//                                Uri.parse(link));
//                startActivity(viewIntent);
//                break;
//            case R.id.add_instaLink:
//                youtube_link_et.setVisibility(View.VISIBLE);
//                getYoutubeLink.setVisibility(View.GONE);
//                add_youtubeLink.setVisibility(View.GONE);
//                break;
//            case R.id.back:
//                onBackPressed();
//                break;
//            case R.id.pick_img_lay:
//                pickImage();
//                break;
//            case R.id.update_btn:
//                f_name = firstname_et.getText().toString();
//                l_name = last_name_et.getText().toString();
//                insta_link = insta_link_et.getText().toString();
//                youtube_link = youtube_link_et.getText().toString();
//                bio = bio_et.getText().toString();
//
//                if (!TextUtils.isEmpty(insta_link)) {
//                    if (!Patterns.WEB_URL.matcher(insta_link).matches()) {
//                        Toast.makeText(EditProfileActivity.this, "You not entered a link", Toast.LENGTH_LONG).show();
//
//                    }
//                } else {
//                    insta_link = null;
//                }
//
//                if (!TextUtils.isEmpty(youtube_link)) {
//                    if (!Patterns.WEB_URL.matcher(youtube_link).matches()) {
//                        Toast.makeText(EditProfileActivity.this, "You not entered a link", Toast.LENGTH_LONG).show();
//                    }
//                } else {
//                    youtube_link = null;
//                }
//                if (!TextUtils.isEmpty(f_name) && !TextUtils.isEmpty(l_name)) {
//                    MultipartBody.Part filePart = null;
//                    if (imgFile != null)
//                        filePart = MultipartBody.Part.createFormData("image", imgFile.getName(), RequestBody.create(MediaType.parse("image*//*"), imgFile));
//
//                    HashMap<String, RequestBody> map = new HashMap<>();
//                    map.put("first_name", toRequestBody(f_name));
//                    map.put("last_name", toRequestBody(l_name));
//                    map.put("user_bio", toRequestBody(bio));
//                    if (insta_link != null) {
//                        map.put("instagram_link", toRequestBody(insta_link));
//                    } else {
//                        map.put("instagram_link", toRequestBody(""));
//                    }
//                    if (youtube_link != null) {
//                        map.put("youtube_link", toRequestBody(youtube_link));
//                    } else {
//                        map.put("youtube_link", toRequestBody(""));
//                    }
//
//                    presenter.updateProfile(map, filePart);
//                } else {
//                    Toast.makeText(EditProfileActivity.this, "Don't let the fields empty", Toast.LENGTH_LONG).show();
//                }
//
//                break;
//            case R.id.name_layout_ll:
//                startActivity(new Intent(EditProfileActivity.this, EditProfileUpdateActivity.class).putExtra("from", "first_name")
//                        .putExtra("firstNameStr", firstNameStr).putExtra("lastNameStr", lastNameStr).putExtra("imgFile", imgFile));
//                overridePendingTransition(0, 0);
//                finish();
//                break;
//            case R.id.last_name_layout_ll:
//                startActivity(new Intent(EditProfileActivity.this, EditProfileUpdateActivity.class).putExtra("from", "last_name")
//                        .putExtra("firstNameStr", firstNameStr).putExtra("lastNameStr", lastNameStr).putExtra("imgFile", imgFile));
//                overridePendingTransition(0, 0);
//                finish();
//                break;
//            case R.id.bio_layout_ll:
//                startActivity(new Intent(EditProfileActivity.this, EditProfileUpdateActivity.class).putExtra("from", "bio")
//                        .putExtra("bioStr", bioStr).putExtra("firstNameStr", firstNameStr).putExtra("lastNameStr", lastNameStr).putExtra("imgFile", imgFile));
//                overridePendingTransition(0, 0);
//                finish();
//                break;
//            case R.id.mobile_layout_ll:
//                startActivity(new Intent(EditProfileActivity.this, EditProfileUpdateActivity.class).putExtra("from", "mobile")
//                        .putExtra("mobileStr", mobileStr).putExtra("firstNameStr", firstNameStr).putExtra("lastNameStr", lastNameStr).putExtra("imgFile", imgFile));
//                overridePendingTransition(0, 0);
//                finish();
//                break;
//            case R.id.email_layout_ll:
//                startActivity(new Intent(EditProfileActivity.this, EditProfileUpdateActivity.class).putExtra("from", "email")
//                        .putExtra("emailStr", emailsStr).putExtra("firstNameStr", firstNameStr).putExtra("lastNameStr", lastNameStr).putExtra("imgFile", imgFile));
//                overridePendingTransition(0, 0);
//                finish();
//                break;
//            case R.id.instagram_layout_ll:
//                startActivity(new Intent(EditProfileActivity.this, EditProfileUpdateActivity.class).putExtra("from", "instagram")
//                        .putExtra("instagramStr", instagramStr).putExtra("firstNameStr", firstNameStr).putExtra("lastNameStr", lastNameStr).putExtra("imgFile", imgFile));
//                overridePendingTransition(0, 0);
//                finish();
//                break;
//        }
//    }

    private void pickImage() {
        if (hasPermission(Manifest.permission.CAMERA) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            EasyImage.openChooserWithGallery(this, "", 0);
        } else {
            requestPermissionsSafely(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MvpApplication.ASK_MULTIPLE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, EditProfileActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imgFile = imageFiles.get(0);
                Glide.with(activity()).load(Uri.fromFile(imgFile)).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate().error(R.drawable.user_profile)).into(profile_img);
                 updateImage(imgFile);
              /*  File s = modify(imgFile);
                updateImage(s);*/
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {

            }
        });
    }

    public static File modify(File file) {
        String name = "";
        try {
            int i = file.getName().lastIndexOf('.');
            name = file.getName().substring(0, i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new File(file.getParent() + "/" + name + ".png");
    }

    private void updateImage(File imgFile) {
//        firstNameStr = person_first_name_profile.getText().toString();
//        lastNameStr = person_last_name_profile.getText().toString();
        if (!TextUtils.isEmpty(firstNameStr) && !TextUtils.isEmpty(lastNameStr)) {
            MultipartBody.Part filePart = null;
            // convertImage(imgFile);
            if (imgFile != null)
                //  RequestBody fileReqBody = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
                filePart = MultipartBody.Part.createFormData("image", imgFile.getName(), RequestBody.create(MediaType.parse("image/*"), imgFile));  // "image/*"
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("first_name", toRequestBody(firstNameStr));
            map.put("last_name", toRequestBody(lastNameStr));
            presenter.updateProfile(map, filePart);
        } else {
            Toast.makeText(EditProfileActivity.this, "Don't let the fields empty", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSuccessRegister(UpdateProfile_Pojo trendsResponse) {
        Toast.makeText(EditProfileActivity.this, "Profile Image Successfully Updated", Toast.LENGTH_LONG).show();
        presenter.getProfiel();
    }

    private void convertImage(File imgFile) {
        Bitmap bmp = BitmapFactory.decodeFile(imgFile.getName());// here you need to pass your existing file path
        FileOutputStream outStream;
        try {
            outStream = new FileOutputStream(imgFile);
// here you need to pass the format as I have passed as PNG so you can create the file with specific format
            bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTokenReceived(String auth_token) {
        Log.e("auth_token", auth_token);
        SharedHelper.putKey(EditProfileActivity.this, "insta_access_token", auth_token);
    }
}

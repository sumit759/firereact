package com.mozostar.app.view.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.view.login.Login_Activity;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.widget.NumberPicker.OnScrollListener.SCROLL_STATE_IDLE;

public class SocialRegisterActivity extends BaseActivity {

//    @BindView(R.id.email_layout)
//    RelativeLayout email_layout;

//    @BindView(R.id.facebook_layout)
//    public RelativeLayout facebook_layout;

//    @BindView(R.id.gmail_layout)
//    public RelativeLayout gmail_layout;

//    @BindView(R.id.login_layout)
//    RelativeLayout login_layout;

    @BindView(R.id.viewpager)
    ViewPager viewpager;


    @Override
    public int getLayoutId() {
        return R.layout.activity_social_register;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);

//        email_layout.setOnClickListener(v -> {
//            Intent register = new Intent(SocialRegisterActivity.this, Register_Activity.class);
//            startActivity(register);
//        });
//
//        facebook_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent register = new Intent(SocialRegisterActivity.this, Register_Activity.class);
//                startActivity(register);
//            }
//        });
//
//        gmail_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent register = new Intent(SocialRegisterActivity.this, Register_Activity.class);
//                startActivity(register);
//            }
//        });
//
//        login_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent register = new Intent(SocialRegisterActivity.this, Login_Activity.class);
//                startActivity(register);
//            }
//        });

        setupViewPager(viewpager);




    }
    @BindView(R.id.tabs)
    TabLayout tabs;
    private ViewPagerAdapter adapter;


    public void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(this, getSupportFragmentManager(), SCROLL_STATE_IDLE);
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);


    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<>();
        private Context mContext;

        public ViewPagerAdapter(Context context, @NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
            mContext = context;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        // This determines the fragment for each tab
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {

                return new Login_Activity();
            } else  {

                return new Register_Activity();
            }
        }

        // This determines the number of tabs
        @Override
        public int getCount() {
            return 2;
        }

//        // This determines the title for each tab
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            switch (position) {
                case 0:
                    return mContext.getString(R.string.sign_in);
                case 1:
                    return mContext.getString(R.string.sign_up);

                default:
                    return null;
            }
        }
    }





}

package com.mozostar.app.view.activity.channelVideoList;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface ChannelVideoListIPresenter<V extends ChannelVideoListIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getList(String id);

}

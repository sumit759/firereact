package com.mozostar.app.view.fragment.homedashboard;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.common.CustomVideoReportDialog;
import com.mozostar.app.common.DoubleClickListener;
import com.mozostar.app.common.SingletonClassVideoBlock;
import com.mozostar.app.custom_ui.VerticalViewPager;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.model.VideoList_Pojo1;
import com.mozostar.app.data.model.VideoReportResponse;
import com.mozostar.app.view.activity.InteractionListener;
import com.mozostar.app.view.activity.guest.GuestProfile_Activity;
import com.mozostar.app.view.activity.sound_list.SoundTrack_Activity;
import com.mozostar.app.view.adapter.HomeScreen_Adapter;
import com.mozostar.app.view.adapter.HomeScreen_PagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class Home_Fragment extends BaseFragment implements Listener, PagerAdapterOnClick, EventListener, Player.EventListener, HomeIView, PopupMenu.OnMenuItemClickListener, CustomVideoReportDialog.DialogInterface {

    private Context context;

    private static String TAG = Home_Fragment.class.getSimpleName();

    @BindView(R.id.rvHome_HomeFragLay)
    RecyclerView rvHome_HomeFragLay;

    @BindView(R.id.vvpHome_HomeFragLay)
    VerticalViewPager vvpHome_HomeFragLay;

 /*   @BindView(R.id.vvpHome_HomeFragLay)
    HorizontalViewPager vvpHome_HomeFragLay;*/

    @BindView(R.id.pbLoad_HomeFragLay)
    ProgressBar pbLoad_HomeFragLay;

    @BindView(R.id.no_data_lay)
    LinearLayout no_data_lay;

    ImageView music_01, music_02;

    LinearLayout home_menu_block_layout, layout_contentLL, linearLayout, posted_user_content_ll;


    private ProgressBar progress;
    private String from;
    private String position;
    String downloadedFilePath;

    private InteractionListener mListener;
    private Boolean liked = false;
    private Boolean disLiked = false;
    ImageView dislike_img;

    private int currentPage = -1;
    private LinearLayoutManager layoutManager;

    private HomePresenter<Home_Fragment> presenter = new HomePresenter<>();
    private final int PERMISSION_CONSTANT = 1000;

    private HomeScreen_PagerAdapter homeScreen_pagerAdapter;
    private ArrayList<String> mVideoURLList = new ArrayList<>();

    private List<VideoList_Pojo1.Data> mVideoDataList = new ArrayList<>();
    private List<ProfileVideoList_Pojo> list_follow = new ArrayList<>();
    private String user_id = "";

    private Boolean loggedIn;

    private int mNextPage = 0;

    @Override
    public int getLayoutId() {
        return R.layout.home_frag;
    }

    ProgressDialog mProgressDialog;
    View bottom_navigation;

    @Override
    public View initView(View view) {

        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);

        Bundle arguments = getArguments();
        from = arguments.getString("from");

        bottom_navigation = getActivity().findViewById(R.id.bottom_navigation);

        rvHome_HomeFragLay.setOnFlingListener(null);
        user_id = "" + SharedHelper.getIntKey(getContext(), "user_id");

        loggedIn = SharedHelper.getBoolKey(getContext(), "logged_in", false);
        if (!TextUtils.isEmpty(from)) {
            if (from.equals("home")) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("page", startPagesCount);
                presenter.getList(map);
            } else if (from.equals("following")) {

                if (loggedIn) {
                    presenter.getFollowList();
                } else {
                    no_data_lay.setVisibility(View.VISIBLE);
                    rvHome_HomeFragLay.setVisibility(View.GONE);
                    this.pbLoad_HomeFragLay.setVisibility(View.GONE);
                    mListener.behaviour();
                }
            }
        }

        SharedHelper.putKey(context, "upload_big_video", false);

        // Changed by thiru

        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvHome_HomeFragLay.setLayoutManager(layoutManager);
        rvHome_HomeFragLay.setHasFixedSize(false);
        new PagerSnapHelper().attachToRecyclerView(rvHome_HomeFragLay);
        Log.i("onSuccess", "" + mVideoURLList.size());
        homeScreen_adapter = new HomeScreen_Adapter(context, this, mVideoURLList);
        rvHome_HomeFragLay.setAdapter(homeScreen_adapter);

        homeScreen_pagerAdapter = new HomeScreen_PagerAdapter(context, this, mVideoURLList);
        vvpHome_HomeFragLay.setAdapter(homeScreen_pagerAdapter);

        //  vvpHome_HomeFragLay.setRotationY(180);

        vvpHome_HomeFragLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int newState) {

            }


            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                View view = vvpHome_HomeFragLay.getChildAt(position);
                PlayerView playerView = view.findViewById(R.id.player_view);
                stopPreviousVideoPlayer();
            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                Log.i("onPageSelected", "" + position);
            }
        });

        //  rvHome_HomeFragLay.setRotationY(180);
        rvHome_HomeFragLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                if (i == RecyclerView.SCROLL_STATE_IDLE) {
                    try {
                        if (home_menu_block_layout != null && posted_user_content_ll != null && layout_contentLL != null && linearLayout != null && bottom_navigation != null) {
                            home_menu_block_layout.setVisibility(View.VISIBLE);
                            posted_user_content_ll.setVisibility(View.VISIBLE);
                            layout_contentLL.setVisibility(View.VISIBLE);
                            linearLayout.setVisibility(View.VISIBLE);
                            bottom_navigation.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                super.onScrollStateChanged(recyclerView, i);
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                if (i > 0 || i2 < 0) {
                    try {
                        if (home_menu_block_layout != null && posted_user_content_ll != null && layout_contentLL != null && linearLayout != null && bottom_navigation != null) {
                            home_menu_block_layout.setVisibility(View.GONE);
                            posted_user_content_ll.setVisibility(View.GONE);
                            layout_contentLL.setVisibility(View.GONE);
                            linearLayout.setVisibility(View.GONE);
                            bottom_navigation.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                mListener.hide_sheet();
                super.onScrolled(recyclerView, i, i2);
                int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                if (computeVerticalScrollOffset != currentPage) {
                    currentPage = computeVerticalScrollOffset;
                    stopPreviousVideoPlayer();
                    setVideoPlayerToPlay(currentPage);
                    loadMore(currentPage);
                }
                /*For Horizontal Video Swipe*/
               /* int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset() / recyclerView.getWidth();
                if (computeHorizontalScrollOffset != currentPage) {
                    currentPage = computeHorizontalScrollOffset;
                    stopPreviousVideoPlayer();
                    setVideoPlayerToPlay(currentPage);
                    loadMore(currentPage);
                }*/

            }
        });

        if (!TextUtils.isEmpty(position)) {
            layoutManager.scrollToPosition(Integer.parseInt(position));
        }
        loadAdapter();

        return view;
    }


    public void loadMore(int page) {
        int currentPage = page + 1;
        Log.i("loadMore", "" + currentPage);
        if (currentPage == mVideoURLList.size()) {
            Log.i("loadMore", "load next");
            if (mNextPage != 0) {
                getVideoList();
            }
        }
    }

    public void getVideoList() {

        HashMap<String, Object> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("page", mNextPage);

        Log.i("loadMore", "load next.." + map);
        presenter.getList(map);
    }

    @Override
    public void onClickGuestProfile() {
        ActivityManager.redirectToActivityWithoutBundle(context, GuestProfile_Activity.class);
    }

    @Override
    public void onClickSoundTrack() {
        ActivityManager.redirectToActivityWithoutBundle(context, SoundTrack_Activity.class);
    }

    SimpleExoPlayer previous_exoplayer;

    @SuppressLint("SetTextI18n")
    public void setVideoPlayerToPlay(int i) {

        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(32 * 1024, 64 * 1024, 1024, 1024).createDefaultLoadControl();
        final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this.context, (TrackSelector) new DefaultTrackSelector(), loadControl);
        newSimpleInstance.setRepeatMode(Player.REPEAT_MODE_ONE);
        HttpProxyCacheServer proxyServer = new HttpProxyCacheServer.Builder(getContext()).maxCacheSize(1024 * 1024 * 1024).build();
        String proxyURL = proxyServer.getProxyUrl(mVideoURLList.get(i));
        newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this.context,
                Util.getUserAgent(this.context, "TikTok"))).createMediaSource(Uri.parse(proxyURL)));
        newSimpleInstance.addListener(this);

        View findViewByPosition = layoutManager.findViewByPosition(i);
        PlayerView playerView = findViewByPosition.findViewById(R.id.player_view);
        RelativeLayout relativeLayout = findViewByPosition.findViewById(R.id.master_click_rl);

        TextView like_count_txt = findViewByPosition.findViewById(R.id.like_count_txt);
        BaseActivity.comments_count_txt = findViewByPosition.findViewById(R.id.comments_count_txt);
        TextView description_txt = findViewByPosition.findViewById(R.id.description_txt);
        TextView user_txt = findViewByPosition.findViewById(R.id.user_txt);
        TextView sount_track_txt = findViewByPosition.findViewById(R.id.sount_track_txt);
        ImageView like_img = findViewByPosition.findViewById(R.id.like_img);
        dislike_img = findViewByPosition.findViewById(R.id.dislike_img);
        LinearLayout dislike_layout = findViewByPosition.findViewById(R.id.dislike_layout);
        TextView dislike_count_tv = findViewByPosition.findViewById(R.id.dislike_count_tv);
        ImageView comments_img = findViewByPosition.findViewById(R.id.comments_img);
        ImageView ivSoundTrack_HomeFragLay = findViewByPosition.findViewById(R.id.ivSoundTrack_HomeFragLay);
        ImageView share_img = findViewByPosition.findViewById(R.id.share_img);
        RelativeLayout rrGuestProfile_HomeFragLay = findViewByPosition.findViewById(R.id.rrGuestProfile_HomeFragLay);

        home_menu_block_layout = findViewByPosition.findViewById(R.id.home_menu_block_layout);
        ImageView iv_home_menu_block = findViewByPosition.findViewById(R.id.iv_home_menu_block);
        // ImageView report_video_img = findViewByPosition.findViewById(R.id.report_video_img);
        FrameLayout textView_add = findViewByPosition.findViewById(R.id.textView_add);
        layout_contentLL = findViewByPosition.findViewById(R.id.layout_contentLL);
        linearLayout = findViewByPosition.findViewById(R.id.linearLayout);
        posted_user_content_ll = findViewByPosition.findViewById(R.id.posted_user_content_ll);
        CircleImageView play_video_iv = findViewByPosition.findViewById(R.id.play_video_iv);

         /* ivSoundTrack_HomeFragLay.setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(context)
                .load(getResources().getDrawable(R.drawable.record))
                .into(ivSoundTrack_HomeFragLay);*/

//        play_video_iv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (loggedIn) {
//                    Intent intent = new Intent(getContext(), BigVideoListActivity.class);
//                    startActivity(intent);
//                } else {
//                    mListener.behaviour();
//                }
//            }
//        });

        music_01 = findViewByPosition.findViewById(R.id.music_01);
        music_02 = findViewByPosition.findViewById(R.id.music_02);
        callFirstAnimation();
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(5000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        ivSoundTrack_HomeFragLay.startAnimation(rotate);


        progress = findViewByPosition.findViewById(R.id.progress);

        CircleImageView user_img = findViewByPosition.findViewById(R.id.user_img);
        ImageView follow_img = findViewByPosition.findViewById(R.id.follow_img);

        if (from.equals("home")) {
            like_count_txt.setText("" + mVideoDataList.get(i).getLikesCount());
            dislike_count_tv.setText("" + mVideoDataList.get(i).getDislikes_count());
            BaseActivity.comments_count_txt.setText("" + mVideoDataList.get(i).getCommentsCount());
            description_txt.setText("" + mVideoDataList.get(i).getDescription());
            description_txt.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            description_txt.setSelected(true);
            description_txt.setSingleLine(true);

            if (mVideoDataList.get(i).getUserdetails().getImage() != null)
                Glide.with(context).load("" + mVideoDataList.get(i).getUserdetails().getImage())
                        .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
                                .dontAnimate().error(R.drawable.user_profile)).into(user_img);

            if (mVideoDataList.get(i).getFollowers() != null) {
                follow_img.setVisibility(View.GONE);
            } else {
                follow_img.setVisibility(View.VISIBLE);
            }

            follow_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        progress.setVisibility(View.VISIBLE);
                        follow_img.setVisibility(View.GONE);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                        map.put("follower_id", mVideoDataList.get(i).getUserdetails().getId());
                        presenter.followup(map);
                    } else {
                        mListener.behaviour();
                    }
                }
            });

            rrGuestProfile_HomeFragLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        Intent guest = new Intent(getContext(), GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + mVideoDataList.get(i).getUserdetails().getId());
                        startActivity(guest);
                    } else {
                        mListener.behaviour();
                    }
                }
            });
//
//            ivSoundTrack_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (loggedIn) {
//                        Intent sound_track = new Intent(getContext(), SoundTrack_Activity.class);
//                        sound_track.putExtra("audio_id", mVideoDataList.get(i).getAudio());
//                        sound_track.putExtra("category_id", "" + mVideoDataList.get(i).getCategoryId());
//                        startActivity(sound_track);
//                    } else {
//                        mListener.behaviour();
//                    }
//                }
//            });

            user_txt.setText("@" + "" + mVideoDataList.get(i).getUserdetails().getFirstName() + " " + mVideoDataList.get(i).getUserdetails().getLastName());

            if (mVideoDataList.get(i).getVideosound() != null)
                sount_track_txt.setText("" + mVideoDataList.get(i).getVideosound().getDescription());
            else
                sount_track_txt.setVisibility(View.GONE);


            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        if (liked == false) {
                            liked = true;
                            int count = mVideoDataList.get(i).getLikesCount() + 1;
                            like_count_txt.setText("" + count);
                            mVideoDataList.get(i).setLikesCount(count);
                            like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            map.put("status", "1");
                            presenter.like_video(map);

                        } else {

                            liked = false;
                            if (mVideoDataList.get(i).getLikesCount() != 0) {
                                int count = mVideoDataList.get(i).getLikesCount() - 1;
                                like_count_txt.setText("" + count);
                                mVideoDataList.get(i).setLikesCount(count);
                            }
                            like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            map.put("status", "0");
                            presenter.like_video(map);

                        }

                    } else {
                        mListener.behaviour();
                    }
                }
            });
            dislike_layout.setVisibility(View.GONE);
            dislike_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        if (disLiked == false) {
                            disLiked = true;
                            int count = mVideoDataList.get(i).getDislikes_count() + 1;
                            dislike_count_tv.setText("" + count);
                            mVideoDataList.get(i).setDislikes_count(count);
                            dislike_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_thumb_down_selected));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            presenter.disLike_video(map);


                        } else {
                            disLiked = false;
                            if (mVideoDataList.get(i).getDislikes_count() != 0) {
                                int count = mVideoDataList.get(i).getDislikes_count() - 1;
                                dislike_count_tv.setText("" + count);
                                mVideoDataList.get(i).setDislikes_count(count);
                            }
                            dislike_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_thumb_down));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            presenter.disLike_video(map);

                        }

                    } else {
                        mListener.behaviour();
                    }
                }
            });

            textView_add.setOnClickListener(new DoubleClickListener() {
                @Override
                public void onSingleClick(View v) {

                }

                @Override
                public void onDoubleClick(View v) {
                    if (loggedIn) {
                        if (liked == false) {
                            liked = true;
                            int count = mVideoDataList.get(i).getLikesCount() + 1;
                            like_count_txt.setText("" + count);
                            mVideoDataList.get(i).setLikesCount(count);
                            like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            map.put("status", "1");
                            presenter.like_video(map);

                        } else {

                            liked = false;
                            if (mVideoDataList.get(i).getLikesCount() != 0) {
                                int count = mVideoDataList.get(i).getLikesCount() - 1;
                                like_count_txt.setText("" + count);
                                mVideoDataList.get(i).setLikesCount(count);
                            }
                            like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            map.put("status", "0");
                            presenter.like_video(map);

                        }

                    } else {
                        mListener.behaviour();
                    }
                }
            });


            user_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        Intent guest = new Intent(getContext(), GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + mVideoDataList.get(i).getUserdetails().getId());
                        startActivity(guest);
                    } else {
                        mListener.behaviour();
                    }

                }
            });
            share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedHelper.putKey(getContext(), "share_listener", "home");
                    mListener.share_sheet("" + mVideoURLList.get(i), mVideoDataList.get(i).getId());
                    //  shareFileToOther(mVideoURLList.get(i), mVideoDataList.get(i).getId());
                    /* https:vlogy.s3-us-west-2.amazonaws.com/video/1594639630.mp4*/
                }
            });


            comments_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        SharedHelper.putKey(getContext(), "comment_listener", "home");
                        mListener.comments_list("" + mVideoDataList.get(i).getId(), i);
                    } else {
                        mListener.behaviour();
                    }
                }
            });


            iv_home_menu_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (loggedIn) {
                            VideoList_Pojo1.Data mVideoData = mVideoDataList.get(i);
                            SingletonClassVideoBlock.getInstance().setmVideoData(mVideoData);
                            callPopupMenu(v);
                        } else {
                            mListener.behaviour();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;
        } else {

            follow_img.setVisibility(View.GONE);
            dislike_layout.setVisibility(View.GONE);

            like_count_txt.setText("" + list_follow.get(i).getLikesCount());
            dislike_count_tv.setText("" + list_follow.get(i).getDislikes_count());
            BaseActivity.comments_count_txt.setText("" + list_follow.get(i).getCommentsCount());
            description_txt.setText("" + list_follow.get(i).getDescription());
            description_txt.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            description_txt.setSelected(true);
            description_txt.setSingleLine(true);

            user_txt.setText("@" + "" + list_follow.get(i).getUserdetails().getFirstName() + " " + list_follow.get(i).getUserdetails().getLastName());

            if (list_follow.get(i).getLikes() != null) {
                liked = false;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
            } else {
                liked = true;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));

            }

            if (list_follow.get(i).getVideosound() != null)
                sount_track_txt.setText("" + list_follow.get(i).getVideosound().getDescription());
            else
                sount_track_txt.setVisibility(View.GONE);

            int status = list_follow.get(i).getLikes().size();

            if (list_follow.get(i).getLikes() != null && status != 0) {
                liked = false;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
            } else {
                liked = true;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));

            }
            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!liked) {
                        liked = true;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                        int count = list_follow.get(i).getLikesCount() + 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                        map.put("video_id", list_follow.get(i).getId());
                        map.put("status", "1");
                        presenter.like_video(map);
                    } else {
                        liked = false;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                        int count = list_follow.get(i).getLikesCount() - 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                        map.put("video_id", list_follow.get(i).getId());
                        map.put("status", "0");
                        presenter.like_video(map);
                    }

                }
            });


            dislike_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        if (disLiked == false) {
                            disLiked = true;
                            int count = list_follow.get(i).getDislikes_count() + 1;
                            dislike_count_tv.setText("" + count);
                            list_follow.get(i).setDislikes_count(count);
                            dislike_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_thumb_down_selected));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", list_follow.get(i).getId());
                            presenter.disLike_video(map);


                        } else {
                            disLiked = false;
                            if (list_follow.get(i).getDislikes_count() != 0) {
                                int count = list_follow.get(i).getDislikes_count() - 1;
                                dislike_count_tv.setText("" + count);
                                list_follow.get(i).setDislikes_count(count);
                            }
                            dislike_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_thumb_down));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", list_follow.get(i).getId());
                            presenter.disLike_video(map);

                        }

                    } else {
                        mListener.behaviour();
                    }
                }
            });


            user_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent guest = new Intent(getContext(), GuestProfile_Activity.class);
                    guest.putExtra("guest_id", "" + list_follow.get(i).getUserdetails().getId());
                    startActivity(guest);

                }
            });

            comments_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    int count = list_follow.get(i).getCommentsCount() + 1;
//                    homeScreen_pagerAdapter.notifyDataSetChanged();
//                    comments_count_txt.setText(count);
                    SharedHelper.putKey(getContext(), "comment_listener", "home");
                    mListener.comments_list("" + list_follow.get(i).getId(), i);

                }
            });

            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;
        }
    }

    private void callFirstAnimation() {
        Animation animation1 = new TranslateAnimation(0, -100, 0, -50);
        animation1.setInterpolator(new LinearInterpolator());
        animation1.setDuration(2000);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                callSecondAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        music_01.startAnimation(animation1);
    }

    private void callSecondAnimation() {
        Animation animation2 = new TranslateAnimation(0, -100, 0, -50);
        animation2.setInterpolator(new LinearInterpolator());
        animation2.setDuration(2000);
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                callFirstAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        music_02.startAnimation(animation2);
    }


    private void shareFileToOther(String videoFile, Integer id) {
        try {
            String[] path = videoFile.split("/");
            String mp4Video = path[path.length - 1];
            if (isFilePresent(mp4Video)) {
                String alreadyDownloaded = getVideoFilePath(mp4Video);
                if (!TextUtils.isEmpty(alreadyDownloaded)) {
                    Uri uri = Uri.parse(alreadyDownloaded);
                    Intent videoshare = new Intent(Intent.ACTION_SEND);
                    videoshare.setType("*/*");
                    videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    videoshare.putExtra(Intent.EXTRA_STREAM, uri);
                    videoshare.putExtra(Intent.EXTRA_TEXT, "Fireact Video");
                    startActivity(videoshare);
                }
            } else {
                startDownload(videoFile, id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isFilePresent(String fileName) {
        String fileNamePresent = "";
        // String path = getContext().getFilesDir().getAbsolutePath() + "/" + fileName;
        try {
            fileNamePresent = getAndroidMoviesFolder().getAbsolutePath() + "/" + fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        File file = new File(fileNamePresent);
        return file.exists();
    }

    private void startDownload(String videoFile, Integer id) {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please Wait!");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);
        final DownloadTask downloadTask = new DownloadTask(getActivity());
        ;
        try {
            downloadTask.execute(videoFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
      /*  mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true); //cancel the task
            }
        });*/
    }


    private void callPopupMenu(View v) {
        try {
            PopupMenu popup = new PopupMenu(getContext(), v);          // This activity implements OnMenuItemClickListener
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.home_block_menu);
            popup.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.video_report_menu:
                showCustomPopupDialog(SingletonClassVideoBlock.getInstance().getmVideoData());
                return true;
            case R.id.video_block_menu:
                Toast.makeText(getContext(), "Coming Soon", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    private void showCustomPopupDialog(VideoList_Pojo1.Data mVideoData) {
        try {
            CustomVideoReportDialog customVideoReportDialog = new CustomVideoReportDialog(context, mVideoData, this);
            customVideoReportDialog.setCancelable(true);
            customVideoReportDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InteractionListener) {
            //init the listener
            mListener = (InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement InteractionListener");
        }

    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int i) {
        if (i == 2) {
            this.pbLoad_HomeFragLay.setVisibility(View.VISIBLE);
        } else if (i == 3) {
            this.pbLoad_HomeFragLay.setVisibility(View.GONE);
        }
        if (i == ExoPlayer.STATE_ENDED) {
            stopPreviousVideoPlayer();
            // setVideoPlayerToPlay(currentPage);
            loadMore(currentPage);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void stopPreviousVideoPlayer() {
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.removeListener(this);
            this.previous_exoplayer.release();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(true);
        }
    }

    public void onPause() {
        super.onPause();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.release();
        }
    }

    private HomeScreen_Adapter homeScreen_adapter;

    private int startPagesCount = 1;


    @Override
    public void onSuccess(VideoList_Pojo1 historyList) {

        //mVideoURLList.clear();
        //mVideoDataList.clear();
        mVideoDataList.addAll(historyList.getVideos().getData());
        int his_list = historyList.getVideos().getData().size();

        if (his_list == 0) {
            // mNextPage = 0;
            //   no_data_lay.setVisibility(View.VISIBLE);
            //  rvHome_HomeFragLay.setVisibility(View.GONE);
            // this.pbLoad_HomeFragLay.setVisibility(View.GONE);
            //Snackbar.make(rvHome_HomeFragLay,"",Snackbar.LENGTH_LONG).show();
        } else {
            mNextPage = historyList.getVideos().getCurrentPage() + 1;
            no_data_lay.setVisibility(View.GONE);
            rvHome_HomeFragLay.setVisibility(View.VISIBLE);
            for (int i = 0; i < his_list; i++) {
                Log.i("onSuccess", "" + historyList.getVideos().getData().get(i).getVideo());
                mVideoURLList.add(historyList.getVideos().getData().get(i).getVideo());
            }

            homeScreen_adapter.notifyDataSetChanged();
            homeScreen_pagerAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onSuccessLike(Object trendsResponse) {

        homeScreen_pagerAdapter.notifyDataSetChanged();
//        presenter.getList();
//        Toast.makeText(getContext(),"Liked the video",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccessDisLike(Object trendsResponse) {
        try {
            homeScreen_pagerAdapter.notifyDataSetChanged();
            String jsonInString = new Gson().toJson(trendsResponse);
            JSONObject mJSONObject = new JSONObject(jsonInString);
            /*String message = mJSONObject.getString("message");
            if (message.equalsIgnoreCase("Successfully Unlike")) {
                dislike_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_thumb_down_selected));
            } else if (message.equalsIgnoreCase("Successfully Like")) {
                dislike_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_thumb_down));
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessFollowList(ArrayList<ProfileVideoList_Pojo> trendsResponse) {
        mVideoURLList.clear();
        mVideoDataList.clear();
        list_follow = trendsResponse;
//        list = trendsResponse.getVideos();
        int his_list = trendsResponse.size();
        if (his_list == 0) {
            no_data_lay.setVisibility(View.VISIBLE);
            rvHome_HomeFragLay.setVisibility(View.GONE);
            this.pbLoad_HomeFragLay.setVisibility(View.GONE);

        } else {
            for (int i = 0; i < his_list; i++) {
                mVideoURLList.add(trendsResponse.get(i).getVideo());
            }
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rvHome_HomeFragLay.setLayoutManager(layoutManager);
            rvHome_HomeFragLay.setHasFixedSize(false);
            new PagerSnapHelper().attachToRecyclerView(rvHome_HomeFragLay);
            HomeScreen_Adapter homeScreen_adapter = new HomeScreen_Adapter(context, this, mVideoURLList);
            rvHome_HomeFragLay.setAdapter(homeScreen_adapter);

            homeScreen_pagerAdapter = new HomeScreen_PagerAdapter(context, this, mVideoURLList);
            vvpHome_HomeFragLay.setAdapter(homeScreen_pagerAdapter);
            homeScreen_adapter.notifyDataSetChanged();


            vvpHome_HomeFragLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int newState) {

                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    View view = vvpHome_HomeFragLay.getChildAt(position);
                    PlayerView playerView = view.findViewById(R.id.player_view);
                    stopPreviousVideoPlayer();

                }


                public void onPageSelected(int position) {
                    // Check if this is the page you want.
                }
            });

            rvHome_HomeFragLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
                public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                    super.onScrollStateChanged(recyclerView, i);
                }

                public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                    super.onScrolled(recyclerView, i, i2);
                    int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                    if (computeVerticalScrollOffset != currentPage) {
                        currentPage = computeVerticalScrollOffset;
                        stopPreviousVideoPlayer();
                        setVideoPlayerToPlay(currentPage);
                    }
                    /*For Horizontal Video Swipe*/
                    /*int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset() / recyclerView.getWidth();
                    if (computeHorizontalScrollOffset != currentPage) {
                        currentPage = computeHorizontalScrollOffset;
                        stopPreviousVideoPlayer();
                        if (i > 0) {
                        System.out.println("Scrolled Right");
                    } else if (i < 0) {
                        System.out.println("Scrolled Left");
                    } else {
                        System.out.println("No Horizontal Scrolled");
                    }   setVideoPlayerToPlay(currentPage);
                    }     */

                }
            });
        }
        if (!TextUtils.isEmpty(position)) {
            layoutManager.scrollToPosition(Integer.parseInt(position));
        }
    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessVideoReport(VideoReportResponse videoReportResponse) {
        try {
            Toast.makeText(getContext(), videoReportResponse.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadAdapter() {


    }

    @Override
    public void comment(String i) {

        int place = Integer.parseInt(i);

        String mCount = "" + BaseActivity.comments_count_txt.getText().toString();
        int count = Integer.parseInt(mCount) + 1;
        BaseActivity.comments_count_txt.setText("" + count);

        if (from.equals("home")) {

            mVideoDataList.get(place).setCommentsCount(count);
//            HashMap<String, Object> map = new HashMap<>();
//            map.put("user_id", user_id);
//
//            presenter.getList(map);
        } else if (from.equals("following")) {

            if (loggedIn) {
                list_follow.get(place).setCommentsCount(count);
//                presenter.getFollowList();
            } else {
                no_data_lay.setVisibility(View.VISIBLE);
                rvHome_HomeFragLay.setVisibility(View.GONE);
                this.pbLoad_HomeFragLay.setVisibility(View.GONE);
                mListener.behaviour();
            }
        }
    }

    @Override
    public void onSubmit(String reportTxt, VideoList_Pojo1.Data videoData) {
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
            map.put("video_id", videoData.getId());
            map.put("video_user_id", videoData.getUserId());
            map.put("report", reportTxt);
            presenter.report_video(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context context;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                /* https:vlogy.s3-us-west-2.amazonaws.com/video/1594639630.mp4*/
                String[] path = sUrl[0].split("/");
                String mp4Video = path[path.length - 1];

                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                downloadedFilePath = getVideoFilePath(mp4Video);
                output = new FileOutputStream(downloadedFilePath);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            else {
                if (downloadedFilePath != null) {
                    Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
                    Uri uri = Uri.parse(downloadedFilePath);
                    Intent videoshare = new Intent(Intent.ACTION_SEND);
                    videoshare.setType("*/*");
                    //  videoshare.setPackage("com.whatsapp");
                    videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    videoshare.putExtra(Intent.EXTRA_STREAM, uri);
                    videoshare.putExtra(Intent.EXTRA_TEXT, "Fireact Video");
                    startActivity(videoshare);
                }
            }
        }

    }


    public static String getVideoFilePath(String str3) {
        return getAndroidMoviesFolder().getAbsolutePath() + "/" + str3 /*+ ".mp4"*/;
    }

    public static File getAndroidMoviesFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
    }
}

/*+ new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date())*/

/*
    Intent intent = new Intent(Intent.ACTION_SEND);
                   intent.setType("text/plain");
                           intent.putExtra(Intent.EXTRA_SUBJECT, "Fireact");
                           intent.putExtra(Intent.EXTRA_TEXT, downloadedFilePath);
                           startActivity(Intent.createChooser(intent, "Choose Option"));
*/

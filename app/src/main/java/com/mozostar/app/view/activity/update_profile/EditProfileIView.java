package com.mozostar.app.view.activity.update_profile;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Profile_Pojo;
import com.mozostar.app.data.model.UpdateProfile_Pojo;

import java.util.ArrayList;

public interface EditProfileIView extends MvpView {
    void onSuccessRegister(UpdateProfile_Pojo trendsResponse);

    void onSuccessProfile(ArrayList<Profile_Pojo> trendsResponse);
}

package com.mozostar.app.view.videodub_addsound;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.carlosmuvi.segmentedprogressbar.SegmentedProgressBar;
import com.coremedia.iso.boxes.Container;
import com.daasuu.gpuv.camerarecorder.CameraRecordListener;
import com.daasuu.gpuv.camerarecorder.GPUCameraRecorderBuilder;
import com.daasuu.gpuv.camerarecorder.LensFacing;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.cameraView.SampleCameraGLView;
import com.mozostar.app.common.FilterType;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.view.activity.MainActivity;
import com.mozostar.app.view.activity.videos_view.AudioExtractor;
import com.mozostar.app.view.adapter.Filter_recyler_Adapter;
import com.mozostar.app.view.adapter.Stricker_recyler_Adapter;
import com.mozostar.app.view.adapter.TextColor_recyler_Adapter;
import com.mozostar.app.view.device_video_list.DvideoList_Activity;
import com.mozostar.app.view.postdubvideo.PostDubVideo_Activity;
import com.mozostar.app.view.videodub_addsound.frag.discover_sound.AddSound_Discover_frag;
import com.wonderkiln.camerakit.CameraView;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mozostar.app.view.videodub_addsound.adapter.DiscoverPlaylistChild_Adapter.mediaPlayerMain;

public class VideoDubRecord_Activity_new extends BaseActivity implements DiscoverDoneOnClick {
    //Rasheed code
    private SampleCameraGLView sampleGLView;
    protected com.daasuu.gpuv.camerarecorder.GPUCameraRecorder GPUCameraRecorder;
    private String filepath;
    private TextView recordBtn;
    protected LensFacing lensFacing = LensFacing.BACK;
    protected int cameraWidth = 1280;
    protected int cameraHeight = 720;
    protected int videoWidth = 720;
    protected int videoHeight = 1280;

    private boolean toggleClick = false;

    boolean record_process = false;


    //Rasheed code end

    @BindView(R.id.lay_filter)
    LinearLayout lay_filter;
    @BindView(R.id.rrTrim)
    RelativeLayout rrTrim;
    @BindView(R.id.filter_list)
    RecyclerView lv;


    /*stricker layout*/
    @BindView(R.id.recvStricker)
    RecyclerView recvStricker;
    @BindView(R.id.llStrickers)
    LinearLayout llStrickers;
    @BindView(R.id.llText)
    LinearLayout llText;

    @BindView(R.id.recvText)
    RecyclerView recvText;
    @BindView(R.id.text_add)
    TextView text_add;

    Context context;
    Camera camera;
    private static String TAG = VideoDubRecord_Activity_new.class.getSimpleName();
    boolean is_recording = false;
    boolean is_flash_on = false;

    private BottomSheetBehavior sheetBehavior;
    ProgressDialog progressDialog;
    private final int PERMISSION_CONSTANT = 1000;

    @BindView(R.id.llBottomSheet_VideoDubLay)
    LinearLayout llBottomSheet_VideoDubLay;
    @BindView(R.id.camera)
    CameraView cameraView;
    @BindView(R.id.ibVideoRecord_VideoDubLay)
    ImageButton ibVideoRecord_VideoDubLay;
    @BindView(R.id.ivBack_VideoDubLay)
    ImageButton ivBack_VideoDubLay;
    @BindView(R.id.ibRotateCamera_VideoDubLay)
    ImageButton ibRotateCamera_VideoDubLay;
    @BindView(R.id.ibFlashCamera_VideoDubLay)
    ImageButton ibFlashCamera_VideoDubLay;
    @BindView(R.id.ibFilterCamera_VideoDoubly)
    ImageButton ibFilterCamera_VideoDoubly;
    @BindView(R.id.ibSpeed_VideoDoubly)
    ImageButton ibSpeed_VideoDoubly;
    @BindView(R.id.ib_trim)
    ImageButton ib_trim;
    @BindView(R.id.txt_trim)
    TextView txt_trim;
    @BindView(R.id.text_effects)
    TextView text_effects;
    @BindView(R.id.ibEffects_VideoDoubly)
    ImageButton ibEffects_VideoDoubly;
    @BindView(R.id.ibDone_VideoDubLay)
    ImageButton ibDone_VideoDubLay;
    @BindView(R.id.ivClose_AddSoundLay)
    ImageButton ivClose_AddSoundLay;
    @BindView(R.id.tvAddSound_VideoDubLay)
    ImageButton tvAddSound_VideoDubLay;
    @BindView(R.id.ibText)
    ImageButton ibText;
    @BindView(R.id.flTextLayout)
    FrameLayout flTextLayout;
    @BindView(R.id.tbTypes_AddSoundLay)
    TabLayout tbTypes_AddSoundLay;
    @BindView(R.id.vpType_AddSoundLay)
    ViewPager vpType_AddSoundLay;
    @BindView(R.id.rl_filter)
    RelativeLayout rl_filter;
    @BindView(R.id.rl_capture)
    RelativeLayout rl_capture;
    @BindView(R.id.rl_camera)
    RelativeLayout rl_camera;
    @BindView(R.id.segmented_progressbar)
    SegmentedProgressBar segmented_progressbar;

    @BindView(R.id.effect_layout)
    LinearLayout effect_layout;

    @BindView(R.id.ib_button_rotationScreen)
    ImageButton ib_button_rotationScreen;

    @BindView(R.id.time_effects)
    LinearLayout time_effects;

    @BindView(R.id.upload_layout)
    LinearLayout upload_layout;
    @BindView(R.id.delete_layout)
    LinearLayout delete_layout;
    @BindView(R.id.llSpeed)
    RelativeLayout llSpeed;

    @BindView(R.id.point_fiveX)
    TextView point_fiveX;

    @BindView(R.id.point_threex)
    TextView point_threex;

    @BindView(R.id.oneX)
    TextView oneX;

    @BindView(R.id.twoX)
    TextView twoX;

    @BindView(R.id.threeX)
    TextView threeX;

    int REQUEST_TAKE_GALLERY_VIDEO = 106;
    private ViewPagerAdapter pagerAdapter;

    ArrayList<String> videopath_arraylist = new ArrayList<>();

    MediaPlayer mediaPlayer;
    String selected_audioUrl = "", Title;

    String path;
    int count = 0;

    private SurfaceHolder mHolder;

    String filter_effect;

    String fPath;

    @Override
    public int getLayoutId() {
        return R.layout.activity_videodubrecord;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initView() {
        context = VideoDubRecord_Activity_new.this;
        ButterKnife.bind(this);
        final Timer[] timerArr = {new Timer()};

        ibVideoRecord_VideoDubLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                record_process = true;
                if (ibVideoRecord_VideoDubLay.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_record_start).getConstantState()) {
                    timerArr[0] = new Timer();
                    timerArr[0].schedule(new TimerTask() {
                        public void run() {
                            runOnUiThread(() -> {
                                if (!is_recording) {
                                    upload_layout.setVisibility(View.GONE);
//                                    delete_layout.setVisibility(View.VISIBLE);
                                    count++;
                                    startOrStopRecording();
                                }
                            });
                        }
                    }, 200);
                } else {
                    timerArr[0].cancel();
                    if (is_recording) {
                        startOrStopRecording();
                    }
                }
            }

        });


        ibVideoRecord_VideoDubLay.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                record_process = true;
                if (ibVideoRecord_VideoDubLay.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_record_start).getConstantState()) {
                    timerArr[0] = new Timer();
                    timerArr[0].schedule(new TimerTask() {
                        public void run() {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    if (!is_recording) {
                                        count++;
                                        upload_layout.setVisibility(View.GONE);
//                                        delete_layout.setVisibility(View.VISIBLE);
                                        delete_layout.setVisibility(View.GONE);
                                        startOrStopRecording();
                                    }
                                }
                            });
                        }
                    }, 200);
                } else {
                    timerArr[0].cancel();
                    if (is_recording) {
                        startOrStopRecording();
                    }
                }
                return false;
            }
        });

        ibDone_VideoDubLay.setEnabled(false);
        ibDone_VideoDubLay.setImageResource(R.drawable.ic_arrow_white);
        // mediaPlayer = MediaPlayer.create(this, R.raw.sample_tone);

        pagerAdapter = new

                ViewPagerAdapter(getResources(), getSupportFragmentManager());
        vpType_AddSoundLay.setAdapter(pagerAdapter);
        tbTypes_AddSoundLay.setupWithViewPager(vpType_AddSoundLay);


        sheetBehavior = BottomSheetBehavior.from(llBottomSheet_VideoDubLay);
        sheetBehavior.setHideable(true);//Important to add
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        sheetBehavior.setHideable(true);//Important to add
                        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        if (!SharedHelper.getKey(this, "selected_audioUrl").

                isEmpty()) {
            onClickAddSoundDone(sounds_pojo);
        }

        final List<FilterType> filterTypes = FilterType.createFilterList();
        lv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        lv.setItemAnimator(new DefaultItemAnimator());
        Filter_recyler_Adapter filter_recyler_adapter = new Filter_recyler_Adapter(context, filterTypes);
        lv.setAdapter(filter_recyler_adapter);


        final List<FilterType> stricker = FilterType.createFilterList();
        recvStricker.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recvStricker.setItemAnimator(new DefaultItemAnimator());
        Stricker_recyler_Adapter stricker_recyler_adapter = new Stricker_recyler_Adapter(context, stricker);
        recvStricker.setAdapter(stricker_recyler_adapter);


        recvText.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recvText.setItemAnimator(new DefaultItemAnimator());
        TextColor_recyler_Adapter text_color_adapter = new TextColor_recyler_Adapter(context);
        recvText.setAdapter(text_color_adapter);


    }


    public void startOrStopRecording() {
        try {
            if (ActivityManager.file_videopath.mkdir()) {
                Log.e(TAG, "Directory created");
            } else {
                Log.e(TAG, "Directory not created");
            }
            Log.e(TAG, "selected_audioUrl: " + selected_audioUrl);

            if (!is_recording) {

                is_recording = true;
                ibVideoRecord_VideoDubLay.setImageDrawable(getResources().getDrawable(R.drawable.ic_record_stop));
                long tsLong = System.currentTimeMillis() / 1000;
                segmented_progressbar.setSegmentCount(count);
                if (count > 1) {
                    segmented_progressbar.resume();
                } else {
                    segmented_progressbar.playSegment(15000);
                }
                String ts = Long.toString(tsLong);
                // 123
                if (!selected_audioUrl.equals("")) {
                    stream_audio("" + ActivityManager.file_videopath + "/" + fileName);
                }

                StringBuilder sb = new StringBuilder();
                sb.append(ActivityManager.file_videopath);
                sb.append("/FUNNZY_");
                sb.append(ts);
                sb.append(".mp4");
                File file = new File(sb.toString());
                ArrayList<String> arrayList = videopath_arraylist;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(ActivityManager.file_videopath);
                sb2.append("/FUNNZY_");
                sb2.append(ts);
                sb2.append(".mp4");
                arrayList.add(sb2.toString());
//                cameraView.captureVideo(file);


                GPUCameraRecorder.start(file.getPath());
                ibDone_VideoDubLay.setEnabled(false);
                ibDone_VideoDubLay.setImageResource(R.drawable.ic_arrow_white);
            } else {
                is_recording = false;
                GPUCameraRecorder.stop();

                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    }
                }
                segmented_progressbar.pause();
                ibDone_VideoDubLay.setImageResource(R.drawable.ic_check_24);

//                ibDone_VideoDubLay.setBackgroundResource(R.drawable.ic_done);
                ibDone_VideoDubLay.setEnabled(true);
                ibVideoRecord_VideoDubLay.setImageDrawable(getResources().getDrawable(R.drawable.ic_record_start));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.tvAddSound_VideoDubLay, R.id.ibDoneFilter, R.id.ibText, R.id.ib_trim, R.id.ivBack_VideoDubLay, R.id.ibDone_VideoDubLay, R.id.ibRotateCamera_VideoDubLay,
            R.id.ibFlashCamera_VideoDubLay, R.id.ivClose_AddSoundLay, R.id.ibFilterCamera_VideoDoubly, R.id.ibEffects_VideoDoubly,
            R.id.upload_layout, R.id.effect_layout, R.id.point_threex, R.id.point_fiveX, R.id.oneX, R.id.twoX, R.id.threeX,
            R.id.delete_img_img, R.id.ibSpeed_VideoDoubly, R.id.ib_button_rotationScreen})
    public void onViewClicked(View view) {

        if (view.getId() == R.id.ibFilterCamera_VideoDoubly) {
            /*rl_filter.setVisibility(view.VISIBLE);
            intialize();
            rl_capture.setVisibility(view.VISIBLE);*/
//            ShowDialog("Filter", "Coming Soon!!");
        }

        switch (view.getId()) {
            case R.id.ibFilterCamera_VideoDoubly:
                if (lay_filter.getVisibility() == View.VISIBLE) {
                    rl_capture.setVisibility(View.VISIBLE);
                    time_effects.setVisibility(View.VISIBLE);
                    rrTrim.setVisibility(View.GONE);
                    lay_filter.setVisibility(View.GONE);
                    llStrickers.setVisibility(View.GONE);
                    llText.setVisibility(View.GONE);
                    flTextLayout.setVisibility(View.GONE);
                    llSpeed.setVisibility(View.GONE);

                    /*set all in white color*/
                    ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    txt_trim.setTextColor(getResources().getColor(R.color.white));
                    text_effects.setTextColor(getResources().getColor(R.color.white));
                    text_add.setTextColor(getResources().getColor(R.color.white));
                } else {
                    /*set one into the red color*/
                    ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.color_dark_select), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                    txt_trim.setTextColor(getResources().getColor(R.color.white));
                    text_add.setTextColor(getResources().getColor(R.color.white));
                    text_effects.setTextColor(getResources().getColor(R.color.white));

                    lay_filter.setVisibility(View.VISIBLE);
                    rl_capture.setVisibility(View.GONE);
                    rrTrim.setVisibility(View.GONE);
                    time_effects.setVisibility(View.GONE);
                    llStrickers.setVisibility(View.GONE);
                    llText.setVisibility(View.GONE);
                    flTextLayout.setVisibility(View.GONE);
                    llSpeed.setVisibility(View.GONE);
                }
                break;
        }

        if (view.getId() == R.id.tvAddSound_VideoDubLay) {
            if (is_recording) {
                Toast.makeText(context, "can't add music while video record", Toast.LENGTH_SHORT).show();
            } else if (record_process == true) {
                ShowDialog("Add Music", "Music addition not allowed in the middle of video recording. Please delete clip and try adding music");
            } else {
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }

        } else if (view.getId() == R.id.ibDoneFilter)
            if (lay_filter.getVisibility() == View.VISIBLE) {
                rl_capture.setVisibility(View.VISIBLE);
                time_effects.setVisibility(View.VISIBLE);
                rrTrim.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);

                /*set all in white color*/
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));

            } else {
                /*set one into the red color*/
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.color_dark_select), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));

                lay_filter.setVisibility(View.VISIBLE);
                rl_capture.setVisibility(View.GONE);
                rrTrim.setVisibility(View.GONE);
                time_effects.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);
            }

        else if (view.getId() == R.id.ibText) {
            if (llText.getVisibility() == View.VISIBLE) {
                rl_capture.setVisibility(View.VISIBLE);
                time_effects.setVisibility(View.VISIBLE);
                rrTrim.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);

                /*set all in white color*/
                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));

                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
            } else {
                /*set one into the red color*/
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.color_dark_select), android.graphics.PorterDuff.Mode.MULTIPLY);
                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.color_dark_select));

                rl_capture.setVisibility(View.GONE);
                time_effects.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                rrTrim.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.VISIBLE);
                flTextLayout.setVisibility(View.VISIBLE);
                llSpeed.setVisibility(View.GONE);
            }
        } else if (view.getId() == R.id.ib_trim) {
            if (rrTrim.getVisibility() == View.VISIBLE) {
                rl_capture.setVisibility(View.VISIBLE);
                time_effects.setVisibility(View.VISIBLE);
                rrTrim.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);

                /*set all in white color*/
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));

            } else {
                /*set one into the red color*/
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.color_dark_select), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                txt_trim.setTextColor(getResources().getColor(R.color.color_dark_select));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));

                rl_capture.setVisibility(View.GONE);
                time_effects.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                rrTrim.setVisibility(View.VISIBLE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);
            }
        } else if (view.getId() == R.id.ivBack_VideoDubLay) {
            onBackPressed();
        } else if (view.getId() == R.id.ibDone_VideoDubLay) {
            final_output_dub_video();
        } else if (view.getId() == R.id.ibRotateCamera_VideoDubLay) {
            flip_camera_front_back();
        } else if (view.getId() == R.id.ibFlashCamera_VideoDubLay) {


//            ShowDialog("Flash Light", "Coming Soon!!");

            if (is_flash_on) {
                is_flash_on = false;
                ibFlashCamera_VideoDubLay.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));
                if (GPUCameraRecorder != null && GPUCameraRecorder.isFlashSupport()) {
                    GPUCameraRecorder.switchFlashMode();
                    GPUCameraRecorder.changeAutoFocus();
                }
                return;
            } else {
                GPUCameraRecorder.switchFlashMode();
                GPUCameraRecorder.changeAutoFocus();
            }
            is_flash_on = true;
            ibFlashCamera_VideoDubLay.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off));


        } else if (view.getId() == R.id.ivClose_AddSoundLay) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setHideable(true);//Important to add
                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                if (mediaPlayer != null) {
                    mediaPlayerMain.stop();

                }
            }
        } else if (view.getId() == R.id.effect_layout) {
            ibFilterCamera_VideoDoubly.performClick();
//            ShowDialog("Video Effects", "Coming Soon!!");
            /*Intent intent = new Intent();
            intent.setType("video/mp4");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);*/

        } else if (view.getId() == R.id.upload_layout) {
//            Intent intent = new Intent();
//            intent.setType("video/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);
            SharedHelper.putKey(context, "channel_video_uplaod", false);
            SharedHelper.putKey(context, "upload_big_video", true);
            ActivityManager.redirectToActivityWithoutBundle(context, DvideoList_Activity.class);
            overridePendingTransition(0, 0);
        } else if (view.getId() == R.id.point_threex) {
            ShowDialog("Slow Motion Effects", "Coming Soon!!");
            /*Intent intent = new Intent();
            intent.setType("video/mp4");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);*/

        } else if (view.getId() == R.id.point_fiveX) {
            ShowDialog("Slow Motion Effects", "Coming Soon!!");

            /*Intent intent = new Intent();
            intent.setType("video/mp4");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);*/

        } else if (view.getId() == R.id.oneX) {
            ShowDialog("Normal Mode", "Coming Soon!!");
            /*Intent intent = new Intent();
            intent.setType("video/mp4");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);*/

        } else if (view.getId() == R.id.twoX) {
            ShowDialog("Fast Effects", "Coming Soon!!");
            /*Intent intent = new Intent();
            intent.setType("video/mp4");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);*/

        } else if (view.getId() == R.id.threeX) {
            ShowDialog("Fast Effects", "Coming Soon!!");
            /*Intent intent = new Intent();
            intent.setType("video/mp4");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);*/

        } else if (view.getId() == R.id.ibEffects_VideoDoubly) {
            if (llStrickers.getVisibility() == View.VISIBLE) {
                rl_capture.setVisibility(View.VISIBLE);
                time_effects.setVisibility(View.VISIBLE);
                rrTrim.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);

                /*set all in white color*/
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.white));
                text_add.setTextColor(getResources().getColor(R.color.white));

            } else {
                /*set one into the red color*/
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.color_dark_select), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);

                txt_trim.setTextColor(getResources().getColor(R.color.white));
                text_effects.setTextColor(getResources().getColor(R.color.color_dark_select));
                text_add.setTextColor(getResources().getColor(R.color.white));
                llStrickers.setVisibility(View.VISIBLE);
                rl_capture.setVisibility(View.GONE);
                time_effects.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                rrTrim.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);
            }
        } else if (view.getId() == R.id.delete_img_img) {
            ShowDialogD("Delete Frame", "are you sure you want delete clip?");
        } else if (view.getId() == R.id.ibSpeed_VideoDoubly) {
            if (llSpeed.getVisibility() == View.VISIBLE) {
                rl_capture.setVisibility(View.VISIBLE);
                time_effects.setVisibility(View.VISIBLE);
                rrTrim.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.GONE);

                /*set all in white color*/
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
            } else {
                /*set one into the red color*/
                ibSpeed_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.color_dark_select), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFilterCamera_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibRotateCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ib_trim.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibFlashCamera_VideoDubLay.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibEffects_VideoDoubly.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
                ibText.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);

                rl_capture.setVisibility(View.GONE);
                time_effects.setVisibility(View.GONE);
                lay_filter.setVisibility(View.GONE);
                rrTrim.setVisibility(View.GONE);
                llStrickers.setVisibility(View.GONE);
                llText.setVisibility(View.GONE);
                flTextLayout.setVisibility(View.GONE);
                llSpeed.setVisibility(View.VISIBLE);
            }
        } else if (view.getId() == R.id.ib_button_rotationScreen) {
            changeScreenOrientation();
        }
    }

    private void changeScreenOrientation() {
        try {
            int orientation = this.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void ShowDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void ShowDialogD(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Yes",
                (arg0, arg1) -> {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void flip_camera_front_back() {
        releaseCamera();
        if (lensFacing == LensFacing.BACK) {
            lensFacing = LensFacing.FRONT;
        } else {
            lensFacing = LensFacing.BACK;
        }
        toggleClick = true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();

                // OI FILE Manager
                path = selectedImageUri.toString();
                // MEDIA GALLERY
//                path = getPath(selectedImageUri);
                if (path != null) {


                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {
                MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @Override
    public void onStart() {
        super.onStart();
//          cameraView.start();
        setUpCamera();

    }

    @Override
    public void onResume() {
        super.onResume();
//        cameraView.start();
        setUpCamera();

    }

    @Override
    public void onPause() {
//        cameraView.stop();
        super.onPause();
        releaseCamera();

    }

    @Override
    public void onStop() {
//        cameraView.stop();
        super.onStop();
        releaseCamera();

    }

    public void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        super.onDestroy();
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
            }
//            cameraView.stop();
        } catch (Exception unused) {
            unused.printStackTrace();
        }
    }

   /* @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        cameraView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }*/

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }


    @Override
    public void onClickAddSoundDone(Sounds_Pojo sounds_pojo) {
        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setHideable(true);//Important to add
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        if (sounds_pojo != null) {
            selected_audioUrl = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getAudio();
            Title = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getTitle();
//            tvAddSound_VideoDubLay.setText(sounds_pojo.getTitle());
            SharedHelper.putKey(activity(), "audio_id_sound", "" + sounds_pojo.getId());
            SharedHelper.putKey(activity(), "categori_id_sound", "" + sounds_pojo.getCategoryId());
        }
        if (ActivityManager.file_videopath.exists()) {
            try {
                FileUtils.deleteDirectory(ActivityManager.file_videopath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        new DownloadFile().execute(selected_audioUrl);
    }

    public void onColorFilterEffect(View view) {
        Camera.Parameters parameters = camera.getParameters();
        switch (view.getId()) {
            case R.id.tv_none:
                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE);
                camera.setParameters(parameters);
                break;
            case R.id.tv_filter1:
                parameters.setColorEffect(Camera.Parameters.EFFECT_AQUA);
                camera.setParameters(parameters);
                break;

            case R.id.tv_filter2:
                parameters.setColorEffect(Camera.Parameters.EFFECT_NEGATIVE);
                camera.setParameters(parameters);
                break;
            case R.id.tv_filter3:
                parameters.setColorEffect(Camera.Parameters.EFFECT_SEPIA);
                camera.setParameters(parameters);
                break;
            case R.id.tv_filter4:
                parameters.setColorEffect(Camera.Parameters.EFFECT_BLACKBOARD);
                camera.setParameters(parameters);
                break;
        }
    }

    public void Filter_pos(int i) {
        final List<FilterType> filterTypes = FilterType.createFilterList();

        if (GPUCameraRecorder != null) {
            GPUCameraRecorder.setFilter(FilterType.createGlFilter(filterTypes.get(i), getApplicationContext()));
//            lay_filter.setVisibility(View.GONE);
        }

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public int getCount() {
            return 2;
        }

        public CharSequence getPageTitle(int i) {
            switch (i) {
                case 0:
                    return getString(R.string.hot_song);
                case 1:
                    return getString(R.string.my_favorites);
                default:
                    return null;
            }
        }

        public ViewPagerAdapter(Resources resources, FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new AddSound_Discover_frag(VideoDubRecord_Activity_new.this);
                case 1:
//                    return new AddSound_Favorite_frag();
                    return new AddSound_Discover_frag(VideoDubRecord_Activity_new.this);
                default:
                    return null;
            }
        }

        public Object instantiateItem(ViewGroup viewGroup, int i) {
            Fragment fragment = (Fragment) super.instantiateItem(viewGroup, i);
            registeredFragments.put(i, fragment);
            return fragment;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            registeredFragments.remove(i);
            super.destroyItem(viewGroup, i, obj);
        }

        public Fragment getRegisteredFragment(int i) {
            return (Fragment) registeredFragments.get(i);
        }
    }


    private void stream_audio(String audioUrl) {
        try {

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(audioUrl);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean final_output_dub_video() {
        progressDialog = new ProgressDialog(this);
        new Thread(new Runnable() {
            public void run() {
                String str = null;
                runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.setMessage("Please wait..");
                        progressDialog.show();
                    }
                });
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < videopath_arraylist.size(); i++) {
                    File file = new File((String) videopath_arraylist.get(i));
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(context, Uri.fromFile(file));
                    if ("yes".equals(mediaMetadataRetriever.extractMetadata(17)) && file.length() > 3000) {
                        Log.e("resp", (String) videopath_arraylist.get(i));
                        arrayList.add(videopath_arraylist.get(i));
                    }
                }
                Log.e("arrayList", "" + arrayList.size());
                try {
                    Movie[] movieArr = new Movie[arrayList.size()];
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        movieArr[i2] = MovieCreator.build((String) arrayList.get(i2));
                    }
                    LinkedList linkedList = new LinkedList();
                    LinkedList linkedList2 = new LinkedList();
                    for (Movie tracks : movieArr) {
                        for (Track track : tracks.getTracks()) {
                            if (track.getHandler().equals("soun")) {
                                linkedList2.add(track);
                            }
                            if (track.getHandler().equals("vide")) {
                                linkedList.add(track);
                            }
                        }
                    }
                    Movie movie = new Movie();
                    if (linkedList2.size() > 0) {
                        movie.addTrack(new AppendTrack((Track[]) linkedList2.toArray(new Track[linkedList2.size()])));
                    }
                    if (linkedList.size() > 0) {
                        movie.addTrack(new AppendTrack((Track[]) linkedList.toArray(new Track[linkedList.size()])));
                    }
                    Container build = new DefaultMp4Builder().build(movie);
                    if (!selected_audioUrl.equals("")) {
                        str = ActivityManager.outputfile;
                    } else {
                        str = ActivityManager.outputfile2;
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
                    build.writeContainer(fileOutputStream.getChannel());
                    fileOutputStream.close();
                    String finalStr = str;
                    runOnUiThread(() -> {
                        progressDialog.dismiss();
                        if (!selected_audioUrl.equals("")) {
                            video_merge_audio();

                        } else {
                            //Intent intent = new Intent(context, DubbedVideoPreview_Activity.class);
                            StringBuilder sb = new StringBuilder();
                            sb.append(ActivityManager.file_videopath);
                            sb.append("/output2.mp4");
//                                intent.putExtra("dubbed_video_path", finalStr);
//                                startActivity(intent);

                            extract_audio_from_video(finalStr);

                        }
                    });
                } catch (Exception unused) {
                    unused.printStackTrace();
                    progressDialog.dismiss();
                }
            }
        }).start();
        return true;
    }

    public void extract_audio_from_video(String finalStr) {
        StringBuilder sb = new StringBuilder();
        sb.append(ActivityManager.file_videopath);
        sb.append("/outputaudio.mp3");
        String outputfile = sb.toString();
        try {
            new AudioExtractor().genVideoUsingMuxer(finalStr, outputfile, -1, -1, true, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //StringBuilder sb = new StringBuilder();
        sb.append(ActivityManager.file_videopath);
        sb.append("/output2.mp4");
        Bundle bundle = new Bundle();
        bundle.putString("dubbed_video_path", finalStr);
        bundle.putString("dubbed_audio_path", outputfile);

        ActivityManager.redirectToActivityWithBundle(context, PostDubVideo_Activity.class, bundle);
    }

    public void video_merge_audio() {
        if (ActivityManager.file_videopath.mkdir()) {
            Log.e(TAG, "Directory created");
        } else {
            Log.e(TAG, "Directory not created");
        }

        String file = Environment.getExternalStorageDirectory().toString();
        StringBuilder sb = new StringBuilder();
        sb.append(ActivityManager.file_videopath);
        sb.append("/");
        //sb.final_output_dub_video(selected_audioUrl);
        sb.append(fileName);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(ActivityManager.file_videopath);
        sb3.append("/output.mp4");
        String sb4 = sb3.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(ActivityManager.file_videopath);
        sb5.append("/output2.mp4");
        String sb6 = sb5.toString();
        new Merge_Video_Audio(this).doInBackground(sb2, sb4, sb6);
    }

    private String fileName;

    private class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;

        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(VideoDubRecord_Activity_new.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // String timestamp = "" + new SimpleDateFormat("yyyy_MM_dd");
                long tsLong = System.currentTimeMillis() / 1000;
                String timestamp = Long.toString(tsLong);
                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                if (ActivityManager.file_videopath.mkdir()) {
                    Log.e(TAG, "Directory created");
                } else {
                    Log.e(TAG, "Directory not created");
                }
                // Output stream to write file
                OutputStream output = new FileOutputStream(ActivityManager.file_videopath + "/" + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + ActivityManager.file_videopath + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String message) {
            Log.e("PostExecute", "message " + message);
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }


    private void releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }

        if (GPUCameraRecorder != null) {
            GPUCameraRecorder.stop();
            GPUCameraRecorder.release();
            GPUCameraRecorder = null;
        }

        if (sampleGLView != null) {
            ((FrameLayout) findViewById(R.id.wrap_view)).removeView(sampleGLView);
            sampleGLView = null;
        }

    }

    private void setUpCameraView() {
        runOnUiThread(() -> {
            FrameLayout frameLayout = findViewById(R.id.wrap_view);
            frameLayout.removeAllViews();
            sampleGLView = null;
            sampleGLView = new SampleCameraGLView(getApplicationContext());
            sampleGLView.setTouchListener((event, width, height) -> {
                if (GPUCameraRecorder == null) return;
                GPUCameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
            });
            frameLayout.addView(sampleGLView);
        });
    }

    private void setFrameRate() {

    }

    private void setUpCamera() {
        setUpCameraView();
        GPUCameraRecorder = new GPUCameraRecorderBuilder(this, sampleGLView)
                //.recordNoFilter(true)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {
                        runOnUiThread(() -> {
                        });
                    }

                    @Override
                    public void onRecordComplete() {

                    }

                    @Override
                    public void onRecordStart() {
                        runOnUiThread(() -> {

                        });
                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("GPUCameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        if (toggleClick) {
                            runOnUiThread(() -> {
                                setUpCamera();
                            });
                        }
                        toggleClick = false;
                    }

                    @Override
                    public void onVideoFileReady() {

                    }
                })
                .videoSize(videoWidth, videoHeight)
                .cameraSize(cameraWidth, cameraHeight)
                .lensFacing(lensFacing)
                .build();


    }


    public static File getAndroidMoviesFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
    }


}

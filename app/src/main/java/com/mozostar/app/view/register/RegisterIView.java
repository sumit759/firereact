package com.mozostar.app.view.register;


import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Register_Pojo;

public interface RegisterIView extends MvpView {
    void onSuccessRegister(Register_Pojo register_pojo);
}

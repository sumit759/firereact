package com.mozostar.app.view.fragment.discover;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.data.model.User_Pojo;

import java.util.ArrayList;

public interface DiscoverIView extends MvpView {
    void onSuccess(Discover_Pojo trendsResponse);

    void onSuccessUser(ArrayList<User_Pojo> trendsResponse);

    void onSuccessExploreVideoList(ExploreChannelVideoResponse exploreList);
}

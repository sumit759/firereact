package com.mozostar.app.view.activity.chat.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    public String uid;
    public String email;
    public String firebaseToken;
    public String name;
    public String master_user_id;


    public User() {
    }


    public User(String uid, String email, String firebaseToken, String name,String master_user_id) {
        this.uid = uid;
        this.email = email;
        this.firebaseToken = firebaseToken;
        this.name=name;
        this.master_user_id=master_user_id;
    }
}

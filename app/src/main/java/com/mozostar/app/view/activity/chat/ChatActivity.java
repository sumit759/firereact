package com.mozostar.app.view.activity.chat;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.network.APIClient;
import com.mozostar.app.view.activity.chat.chatfcm.ChatContract;
import com.mozostar.app.view.activity.chat.chatfcm.ChatPresenterfcm;
import com.mozostar.app.view.activity.chat.models.ChatModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChatActivity extends BaseActivity implements ChatIView, ChatContract.View {
    ChatMessageAdapter mAdapter;
    String chatPath = null, id = "", name = "", reciver_id, reciverFcmToken;
    String sendUserId;

    @BindView(R.id.chat_lv)
    RecyclerView chatLv;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.send)
    ImageView send;
    @BindView(R.id.chat_controls_layout)
    LinearLayout chatControlsLayout;

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    FirebaseDatabase database;
    DatabaseReference myRef;
    public static String sender = "user";
    public static String reciver;
    private ChatRecyclerAdapter mChatRecyclerAdapter;
    chatAdapter chatAdapter;


    ChatPresenter<ChatActivity> presenter = new ChatPresenter<>();
    private ChatPresenterfcm mChatPresenter;


    @Override
    public int getLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        Bundle extras = getIntent().getExtras();
        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);

        if (extras != null) {

            reciver = extras.getString(Constants.ARG_RECEIVER);
            reciver_id = extras.getString(Constants.ARG_RECEIVER_UID);
            reciverFcmToken = extras.getString(Constants.ARG_FIREBASE_TOKEN);
            name = extras.getString("name");
//            chatPath = extras.getString("request_id", "");
//            id = extras.getString("user_id", "");
//            name = extras.getString("name", "");
//            sendUserId = extras.getString("sendUserId");
            tvTitle_ToolBarLay.setText(name);
//            initChatView(chatPath);
        }
        tvTitle_ToolBarLay.setText("Chat");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        mChatPresenter = new ChatPresenterfcm(this);
        mChatPresenter.getMessage(FirebaseAuth.getInstance().getCurrentUser().getUid(), reciver_id);

    }

    private void sendMessage(String message, String type) {
        String sender = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        String senderUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ChatModel chatModel = new ChatModel(sender,
                reciver,
                senderUid,
                reciver_id,
                message,
                System.currentTimeMillis(),
                type);
        mChatPresenter.sendMessage(this,
                chatModel,
                reciverFcmToken);
    }

    private void initChatView(String chatPath) {
        if (chatPath == null) {
            return;
        }

        message.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                String myText = message.getText().toString().trim();
                if (myText.length() > 0) {
//                    sendMessage(myText);
                }
                handled = true;
            }
            return handled;
        });

//        mAdapter = new ChatMessageAdapter(activity(), new ArrayList<Chat>());
//        chatLv.setAdapter(mAdapter);
//
//        database = FirebaseDatabase.getInstance();
//        myRef = database.getReference().child(chatPath);
//        myRef.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String prevChildKey) {
//                Chat chat = dataSnapshot.getValue(Chat.class);
//                mAdapter.add(chat);
//
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String prevChildKey) {
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String prevChildKey) {
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//            }
//        });
    }


    @OnClick(R.id.send)
    public void onViewClicked() {
        String myText = message.getText().toString();
        if (myText.length() > 0) {
//            sendMessage(myText);
            sendMessage(myText, "text");
            message.setText("");

        }
    }

    private void sendMessage(String messageStr) {
        Chat chat = new Chat();
        chat.setSender(sender);
        chat.setTimestamp(new Date().getTime());
        chat.setType("text");
        chat.setText(messageStr);
        myRef.push().setValue(chat);
        chatSend(messageStr, id);
        message.setText("");
    }

    void chatSend(String messageStr, String id) {
        HashMap<String, Object> map = new HashMap<>();
//        map.put("zipper_id", id);
//        map.put("request_id", chatPath);
        map.put("message", messageStr);
        map.put("user_id", "141");

        Observable modelObservable = APIClient.getAPIClient().chatPush(map);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> onSuccess(trendsResponse),
                        throwable -> onError((Throwable) throwable));

    }


    public void onSuccess(Object trendsResponse) {

        Log.e("Chat push", "SUCCESS");
    }

    public void onError(Throwable e) {
        Log.e("Chat push", "ERROR: " + e.getMessage());

    }

    @Override
    public void onSuccessFCM(Object object) {
        Log.e("onSuccessFCM", "onSuccessFCM");
    }

    @Override
    public void onSendMessageSuccess() {
        Toast.makeText(this, "sent", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSendMessageFailure(String message) {
        Toast.makeText(this, "sent_fail", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetMessagesSuccess(ChatModel chatModel) {
        if (mChatRecyclerAdapter == null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            chatLv.setLayoutManager(layoutManager);
            chatLv.setHasFixedSize(false);
            ArrayList<ChatModel> chatModels = new ArrayList<>();
            chatModels.add(chatModel);
            mChatRecyclerAdapter = new ChatRecyclerAdapter(ChatActivity.this, chatModels);
            chatLv.setAdapter(mChatRecyclerAdapter);
        } else {
            mChatRecyclerAdapter.add(chatModel);
            chatLv.smoothScrollToPosition(mChatRecyclerAdapter.getItemCount() - 1);
        }
    }

    @Override
    public void onGetMessagesFailure(String message) {
        Toast.makeText(this, "recive_fail", Toast.LENGTH_SHORT).show();
    }
}

package com.mozostar.app.view.activity.banner;

import android.os.Handler;

import com.mozostar.app.base.BasePresenter;

public class BannerPresenter <V extends BannerIView> extends BasePresenter<V> implements BannerIPresenter<V> {

    @Override
    public void handlerCall() {
        new Handler().postDelayed(() -> getMvpView().redirectHome(), 5000); //Timer is in ms here.
    }
}

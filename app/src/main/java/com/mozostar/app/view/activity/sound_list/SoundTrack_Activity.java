package com.mozostar.app.view.activity.sound_list;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.view.adapter.GuestDub_Adapter;
import com.mozostar.app.view.videodub_addsound.DiscoverPlaylistOnClick;
import com.mozostar.app.view.videodub_addsound.VideoDubRecord_Activity;
import com.mozostar.app.base.ActivityManager;


import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoundTrack_Activity extends BaseActivity implements SoundTrackIView, DiscoverPlaylistOnClick {
    Context context;
    private static String TAG = SoundTrack_Activity.class.getSimpleName();

    @BindView(R.id.rvSoundTrack_SoundTrackLay)
    RecyclerView rvSoundTrack_SoundTrackLay;
    @BindView(R.id.ctl_SoundTrackLay)
    CollapsingToolbarLayout ctl_SoundTrackLay;
    @BindView(R.id.flContainer_SoundTrackLay)
    FrameLayout flContainer_SoundTrackLay;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.anim_toolbar)
    Toolbar toolbar;
    @BindView(R.id.original_txt)
    TextView original_txt;
    @BindView(R.id.user_prfl_txt)
    TextView user_prfl_txt;
    @BindView(R.id.videos_count)
    TextView videos_count;

    private Menu collapseMenu;

    ArrayList<String> cover_ArrayList = new ArrayList<>();

    String sound_id, category_id, user;

    private boolean appBarExpanded = true;

    @BindView(R.id.sound_dub_btn)
    FloatingActionButton sound_dub_btn;

    SoundTrackPresenter<SoundTrack_Activity> presenter = new SoundTrackPresenter<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_soundtrack;
    }

    @Override
    public void initView() {
        context = SoundTrack_Activity.this;
        ButterKnife.bind(this);
        presenter.attachView(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctl_SoundTrackLay.setTitle("");
        toolbar.setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rvSoundTrack_SoundTrackLay.setLayoutManager(new GridLayoutManager(context, 3));
        rvSoundTrack_SoundTrackLay.setItemAnimator(new DefaultItemAnimator());

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            sound_id = extras.getString("audio_id");
            category_id = extras.getString("category_id");

            HashMap<String, Object> map = new HashMap<>();
            map.put("audio", sound_id);
            map.put("category_id", category_id);
            presenter.getSoundList(map);

        }

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) > 200) {
                    appBarExpanded = false;
                } else {
                    appBarExpanded = true;
                }
                invalidateOptionsMenu();
            }
        });

        sound_dub_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selected_audioUrl = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getAudio();
                SharedHelper.putKey(activity(), "selected_audioUrl", selected_audioUrl);
                SharedHelper.putKey(activity(), "selected_audioUrl", selected_audioUrl);

                Intent intent=new Intent(SoundTrack_Activity.this,VideoDubRecord_Activity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (collapseMenu != null && (!appBarExpanded || collapseMenu.size() != 1)) {
            flContainer_SoundTrackLay.setVisibility(View.GONE);
            ctl_SoundTrackLay.setTitle(user_prfl_txt.getText().toString());
            toolbar.setTitle(user_prfl_txt.getText().toString());
        } else {
            flContainer_SoundTrackLay.setVisibility(View.VISIBLE);
            ctl_SoundTrackLay.setTitle("");
            toolbar.setTitle("");
        }
        return super.onPrepareOptionsMenu(collapseMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sound_menu, menu);
        this.collapseMenu = menu;
//        this. collapseMenu.add("Share")
//                .setIcon(R.drawable.ic_add_circle)
//                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                Toast.makeText(this, "Setting menu clicked!", Toast.LENGTH_SHORT).show();
                break;
        }

        if (item.getTitle() == "Share") {
            Toast.makeText(this, "Share menu clicked!", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

        sounds_pojo=trendsResponse.get(0).getVideosound();

        original_txt.setText("Original Sound - " + trendsResponse.get(0).getVideosound().getDescription());
        user_prfl_txt.setText(trendsResponse.get(0).getUserdetails().getFirstName() + " " + trendsResponse.get(0).getUserdetails().getFirstName());
        int size = trendsResponse.size();
        videos_count.setText("" + size + " Videos");


        GuestDub_Adapter soundTrack_adapter = new GuestDub_Adapter(context, trendsResponse);
        rvSoundTrack_SoundTrackLay.setAdapter(soundTrack_adapter);

    }


    String selected_audioUrl = "",Title;

    /*@Override
    public void onClickAddSoundDone(Sounds_Pojo sounds_pojo) {

        selected_audioUrl = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getAudio();
        Title = BuildConfig.BASE_IMAGE_URL + sounds_pojo.getTitle();
//        tvAddSound_VideoDubLay.setText(sounds_pojo.getTitle());
        SharedHelper.putKey(activity(), "audio_id_sound", "" + sounds_pojo.getId());
        SharedHelper.putKey(activity(), "categori_id_sound", "" + sounds_pojo.getCategoryId());

        if (file_videopath.exists()) {
            try {
                FileUtils.deleteDirectory(file_videopath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        new DownloadFile().execute(selected_audioUrl);
    }
*/
    @Override
    public void onClickPlaylistItem(int position, Sounds_Pojo sounds_pojo) {

//        discoverDoneOnClick.onClickAddSoundDone(sounds_pojo);
    }

    @Override
    public void onClickPlaylistItem(int position) {

    }

    @Override
    public void onClickAllPlaylistItem(int position) {

    }

    private class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;

        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(SoundTrack_Activity.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        private String fileName;

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // String timestamp = "" + new SimpleDateFormat("yyyy_MM_dd");
                long tsLong = System.currentTimeMillis() / 1000;
                String timestamp = Long.toString(tsLong);
                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                if (ActivityManager.file_videopath.mkdir()) {
                    Log.e(TAG, "Directory created");
                } else {
                    Log.e(TAG, "Directory not created");
                }
                // Output stream to write file
                OutputStream output = new FileOutputStream(ActivityManager.file_videopath + "/" + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + ActivityManager.file_videopath + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String message) {
            Log.e("PostExecute", "message " + message);
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

}

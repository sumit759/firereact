package com.mozostar.app.view.activity.channel;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.AddCategory_Pojo;
import com.mozostar.app.data.model.CategoryListPojo;
import com.mozostar.app.data.network.APIClient;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChannelPresenter <V extends ChannelIView> extends BasePresenter<V> implements ChannelIPresenter<V>  {

    @SuppressLint("CheckResult")
    @Override
    public void getCategory() {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().getCategory();
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessCategory((CategoryListPojo) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    @Override
    public void add_category(Map<String, RequestBody> params, MultipartBody.Part file) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().add_category(params, file);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessCreate((AddCategory_Pojo) trendsResponse);
                        },
                        throwable -> {
                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }
}

package com.mozostar.app.view.activity.language;

import android.content.Intent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.view.activity.settings.SettingsPrivacyActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppLanguageSelectActivity extends BaseActivity {

    @BindView(R.id.language_back_image_view)
    ImageView language_back_image_view;

    @BindView(R.id.language_selected_text)
    TextView language_selected_text;

    @BindView(R.id.language_select_layout)
    RelativeLayout language_select_layout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_app_language_select;
    }

    String languageName;

    @Override
    public void initView() {
        ButterKnife.bind(this);
      /*  try {
            languageName = getIntent().getStringExtra("language");
            language_selected_text.setText(languageName);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @OnClick(R.id.language_back_image_view)
    public void cancelClick() {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @OnClick(R.id.language_select_layout)
    public void languageSelect() {
        startActivity(new Intent(AppLanguageSelectActivity.this, LanguageListActivity.class));
    }
}
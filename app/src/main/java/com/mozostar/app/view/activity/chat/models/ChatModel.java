package com.mozostar.app.view.activity.chat.models;

import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class ChatModel {
    public String sender;
    public String receiver;
    public String fromID;
    public String toID;
    public String content;
    public long timestamp;
    public String type;

    public ChatModel() {
    }

    public ChatModel(String sender, String receiver, String fromID, String toID, String content, long timestamp, String type) {
        this.sender = sender;
        this.receiver = receiver;
        this.fromID = fromID;
        this.toID = toID;
        this.content = content;
        this.timestamp = timestamp;
        this.type=type;
    }
}

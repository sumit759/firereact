package com.mozostar.app.view.fragment.guestfragment;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface GuestVideoIPresenter <V extends GuestVideoIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getList(String id);

    @SuppressLint("CheckResult")
    void getDubList(String id);
}

package com.mozostar.app.view.videodub_addsound;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;


import com.google.android.exoplayer2.metadata.id3.InternalFrame;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AACTrackImpl;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import com.mozostar.app.view.dubvideopreview.DubbedVideoPreview_Activity;
import com.mozostar.app.base.ActivityManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class Merge_Video_Audio extends AsyncTask<String, String, String> {
    String audio;
    Context context;
    String output;
    ProgressDialog progressDialog;
    public Runnable runnable = new Runnable() {
        public void run() {
            Merge_Video_Audio merge_Video_Audio;
            try {
                Movie build = MovieCreator.build(Merge_Video_Audio.this.video);
                ArrayList arrayList = new ArrayList();
                for (Track track : build.getTracks()) {
                    if (!"soun".equals(track.getHandler())) {
                        arrayList.add(track);
                    }
                }
                arrayList.add(Merge_Video_Audio.this.audio_merge_video(Merge_Video_Audio.this.video, new AACTrackImpl(new FileDataSourceImpl(Merge_Video_Audio.this.audio))));
                build.setTracks(arrayList);
                Container build2 = new DefaultMp4Builder().build(build);
                FileChannel channel = new FileOutputStream(new File(Merge_Video_Audio.this.output)).getChannel();
                build2.writeContainer(channel);
                channel.close();
                progressDialog.dismiss();
            } catch (Exception unused) {
                progressDialog.dismiss();
            } catch (Throwable th) {
                progressDialog.dismiss();
                throw th;
            }
            output_VideoAudioMerged();
        }
    };
    String video;

    Merge_Video_Audio(Context context2) {
        this.context = context2;
        this.progressDialog = new ProgressDialog(context2);
        this.progressDialog.setMessage("Please Wait...");
    }

    public void onPreExecute() {
        super.onPreExecute();
    }

    public String doInBackground(String... strArr) {
        try {
            this.progressDialog.show();
        } catch (Exception unused) {
        }
        this.audio = strArr[0];
        this.video = strArr[1];
        this.output = strArr[2];
        StringBuilder sb = new StringBuilder();
        sb.append(this.audio);
        sb.append(InternalFrame.ID);
        sb.append(this.video);
        sb.append("-----");
        sb.append(this.output);
        new Thread(this.runnable).start();
        return null;
    }

    public void onPostExecute(String str) {
        super.onPostExecute(str);
    }

    public void output_VideoAudioMerged() {
        Intent intent = new Intent(this.context, DubbedVideoPreview_Activity.class);
        long tsLong = System.currentTimeMillis() / 1000;
        String ts = Long.toString(tsLong);
        StringBuilder sb = new StringBuilder();
        sb.append(ActivityManager.file_videopath);
        sb.append("/output.mp4");

        StringBuilder sb1 = new StringBuilder();
        sb1.append(ActivityManager.file_videopath);
        sb1.append("/outputaudio.mp3");
        String outputfile = sb1.toString();
        intent.putExtra("dubbed_audio_path", outputfile);
        intent.putExtra("dubbed_video_path", sb.toString());
        this.context.startActivity(intent);
    }

    public Track audio_merge_video(String str, Track track) {
        try {
            IsoFile isoFile = new IsoFile(str);
            double duration = ((double) isoFile.getMovieBox().getMovieHeaderBox().getDuration()) / ((double) isoFile.getMovieBox().getMovieHeaderBox().getTimescale());
            int i = 0;
            long j = -1;
            long j2 = 0;
            double d = -1.0d;
            long j3 = -1;
            double d2 = 0.0d;
            while (i < track.getSampleDurations().length) {
                long j4 = track.getSampleDurations()[i];
                int i2 = (d2 > d ? 1 : (d2 == d ? 0 : -1));
                if (i2 > 0) {
                    if (d2 <= 0.0d) {
                        j3 = j2;
                    }
                }
                if (i2 > 0 && d2 <= duration) {
                    j = j2;
                }
                j2++;
                i++;
                d = d2;
                d2 = (((double) j4) / ((double) track.getTrackMetaData().getTimescale())) + d2;
            }
            CroppedTrack croppedTrack = new CroppedTrack(track, j3, j);
            return croppedTrack;
        } catch (IOException e) {
            e.printStackTrace();
            return track;
        }
    }
}

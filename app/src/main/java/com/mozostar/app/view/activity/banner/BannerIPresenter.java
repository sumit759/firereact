package com.mozostar.app.view.activity.banner;

import com.mozostar.app.base.MvpPresenter;

public interface BannerIPresenter  <V extends BannerIView> extends MvpPresenter<V> {
    void handlerCall();
}

package com.mozostar.app.view.activity.channel;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface ChannelIPresenter <V extends ChannelIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getCategory();

    @SuppressLint("CheckResult")
    void add_category(Map<String, RequestBody> params, MultipartBody.Part file);
}

package com.mozostar.app.view.fragment.profile;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface ProfileIPresenter <V extends ProfileIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getProfiel();
}

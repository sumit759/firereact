package com.mozostar.app.view.activity.guest;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.mozostar.app.R;
import com.mozostar.app.base.DrawerBaseActivity;
import com.mozostar.app.custom_ui.DialogManager;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.Guest_Pojo;
import com.mozostar.app.view.activity.chat.ChatActivity;
import com.mozostar.app.view.activity.chat.Constants;
import com.mozostar.app.view.activity.chat.models.User;
import com.mozostar.app.view.activity.chat.users.get.all.GetUsersContract;
import com.mozostar.app.view.activity.chat.users.get.all.GetUsersPresenter;
import com.mozostar.app.view.activity.follow.FollowersActivity;
import com.mozostar.app.view.fragment.guestfragment.GuestDub_Fragment;
import com.mozostar.app.view.fragment.guestfragment.GuestLikes_Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class GuestProfile_Activity extends DrawerBaseActivity implements GuestIView, GetUsersContract.View {
    Context context;
    private static String TAG = GuestProfile_Activity.class.getSimpleName();

    @BindView(R.id.tbList_GuestProfLay)
    TabLayout tbList_GuestProfLay;
    @BindView(R.id.vpContent_GuestProfLay)
    ViewPager vpContent_GuestProfLay;
    @BindView(R.id.user_txt)
    TextView user_txt;
    @BindView(R.id.guest_following_txt)
    TextView guest_following_txt;
    @BindView(R.id.guest_follower_txt)
    TextView guest_follower_txt;
    @BindView(R.id.followup_btn)
    Button followup_btn;
    @BindView(R.id.user_img)
    CircleImageView user_img;

    @BindView(R.id.follow_lay)
    LinearLayout follow_lay;

    @BindView(R.id.friends_lay)
    LinearLayout friends_lay;

    @BindView(R.id.ivClose_share)
    ImageView ivClose_share;

    @BindView(R.id.guest_likes_count)
    TextView guest_likes_count;

    @BindView(R.id.followin_lnr_lay)
    LinearLayout followin_lnr_lay;

    @BindView(R.id.followers_lay)
    LinearLayout followers_lay;

    @BindView(R.id.bottom_sheet_share)
    LinearLayout bottom_sheet_share;
    @BindView(R.id.find_friend_iv)
    ImageView find_friend_iv;

    @BindView(R.id.likes_lnr_lay)
    LinearLayout likes_lnr_lay;

    @BindView(R.id.message_btn)
    Button message_btn;

    @BindView(R.id.unfollow_img)
    ImageView unfollow_img;

    @BindView(R.id.insta_link)
    ImageView insta_link;

    @BindView(R.id.youtube_link)
    ImageView youtube_link;

    private BottomSheetBehavior shareBehaviour;
    String guest_id, youtube, insta;
    GuestPresenter<GuestProfile_Activity> presenter = new GuestPresenter<>();
    GetUsersPresenter mGetUsersPresenter;


    /*use to set navigation drawer*/
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.clAll)
    CoordinatorLayout clAll;


    private int[] tabIcons = {
            R.drawable.ic_vertical_grid,
            R.drawable.ic_guest_heart
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_guestprofile;
    }

    @Override
    public void initView() {
        context = GuestProfile_Activity.this;
        ButterKnife.bind(this);
        presenter.attachView(this);


        shareBehaviour = BottomSheetBehavior.from(bottom_sheet_share);

        shareBehaviour.setHideable(true);
        shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
        shareBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            guest_id = extras.getString("guest_id");

            setupViewPager(vpContent_GuestProfLay);
            tbList_GuestProfLay.setupWithViewPager(vpContent_GuestProfLay);
            if (!TextUtils.isEmpty(guest_id)) {

                presenter.getProfiel(guest_id);
            }
        }


        setupTabIcons();

        setDrawerLockModes();
        find_friend_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCloseDrawer();

            }
        });

    }

    public void openCloseDrawer() {

            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
            }

    }


    private void setDrawerLockModes() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.setScrimColor(Color.TRANSPARENT);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                clAll.setTranslationX(slideX);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    private void setupTabIcons() {
        Objects.requireNonNull(tbList_GuestProfLay.getTabAt(0)).setIcon(tabIcons[0]);
//        Objects.requireNonNull(tbList_GuestProfLay.getTabAt(1)).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {

        Bundle bundle = new Bundle();
        bundle.putString("guest_id", guest_id);

        GuestDub_Fragment guestDub_fragment = new GuestDub_Fragment();
        GuestLikes_Fragment guestLikes_fragment = new GuestLikes_Fragment();

        guestDub_fragment.setArguments(bundle);
        guestLikes_fragment.setArguments(bundle);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(guestDub_fragment, "GuestDub");
        adapter.addFragment(guestLikes_fragment, "GuestLikes");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onSuccessProfile(Guest_Pojo trendsResponse) {
        if (trendsResponse.getGuestDetails().get(0).getImage() != null)
            Glide.with(activity()).load(trendsResponse.getGuestDetails().get(0).getImage()).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate().error(R.drawable.user_profile)).into(user_img);

        user_txt.setText("@" + trendsResponse.getGuestDetails().get(0).getFirstName() + " " + trendsResponse.getGuestDetails().get(0).getLastName());
        guest_following_txt.setText("" + trendsResponse.getGuestDetails().get(0).getUserfollowingCount());
        guest_follower_txt.setText("" + trendsResponse.getGuestDetails().get(0).getUserfollowerCount());
        guest_likes_count.setText("" + trendsResponse.getGuestDetails().get(0).getUserlikesCount());

        if (trendsResponse.getGuestDetails().get(0).getInstagramLink() != null && !trendsResponse.getGuestDetails().get(0).getInstagramLink().equals("null"))
            insta = "" + trendsResponse.getGuestDetails().get(0).getInstagramLink();
        if (trendsResponse.getGuestDetails().get(0).getYoutubeLink() != null && !trendsResponse.getGuestDetails().get(0).getYoutubeLink().equals("null"))
            youtube = "" + trendsResponse.getGuestDetails().get(0).getYoutubeLink();

        if (insta == null && youtube == null) {
            youtube_link.setVisibility(View.GONE);
            insta_link.setVisibility(View.GONE);
        } else if (insta != null && youtube != null) {
            youtube_link.setVisibility(View.GONE);
            insta_link.setVisibility(View.VISIBLE);
        } else if (insta == null && youtube != null) {
            youtube_link.setVisibility(View.VISIBLE);
            insta_link.setVisibility(View.GONE);
        } else if (insta != null && youtube != null) {
            youtube_link.setVisibility(View.VISIBLE);
            insta_link.setVisibility(View.GONE);

        }
        if (trendsResponse.getFollowStatus() == 0) {
            message_btn.setVisibility(View.GONE);
            followup_btn.setVisibility(View.VISIBLE);
            followup_btn.setText("Follow");
        } else if (trendsResponse.getFollowStatus() == 1) {
            message_btn.setVisibility(View.VISIBLE);
            followup_btn.setVisibility(View.GONE);
        }


    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {
        Toast.makeText(GuestProfile_Activity.this, trendsResponse.getMessage(), Toast.LENGTH_LONG).show();
        presenter.getProfiel(guest_id);

    }

    @Override
    public void onGetAllUsersSuccess(List<User> users) {
        DialogManager.hideProgress();

        for (int i = 0; i < users.size(); i++) {
//            if(users.get(i).name.equals(tvTitle_ToolBarLay.getText().toString())){
            Intent chat = new Intent(context, ChatActivity.class);
            chat.putExtra(Constants.ARG_RECEIVER, users.get(i).name);
            chat.putExtra(Constants.ARG_RECEIVER_UID, users.get(i).uid);
            chat.putExtra(Constants.ARG_FIREBASE_TOKEN, users.get(i).firebaseToken);
            chat.putExtra("name", "" + users.get(i).name);
            startActivity(chat);
//            }
        }


    }

    @Override
    public void onGetAllUsersFailure(String message) {
        DialogManager.hideProgress();

    }

    @Override
    public void onGetChatUsersSuccess(List<User> users) {

    }

    @Override
    public void onGetChatUsersFailure(String message) {


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return 1;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //  return mFragmentTitleList.get(position);
            return null;
        }

    }

    @OnClick({ R.id.followin_lnr_lay, R.id.ivMenu_ToolBarLay, R.id.ivClose_share, R.id.message_btn,
            R.id.followup_btn, R.id.unfollow_img, R.id.insta_link, R.id.youtube_link, R.id.followers_lay})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.followin_lnr_lay:
                SharedHelper.putKey(GuestProfile_Activity.this, "on_click_follow", "guest_follwing");
                Intent follower = new Intent(GuestProfile_Activity.this, FollowersActivity.class);
                follower.putExtra("guest_id", guest_id);
                startActivity(follower);
                break;
            case R.id.followers_lay:
                SharedHelper.putKey(GuestProfile_Activity.this, "on_click_follow", "guest_follower");
                Intent following = new Intent(GuestProfile_Activity.this, FollowersActivity.class);
                following.putExtra("guest_id", guest_id);
                startActivity(following);
                break;
            case R.id.ivMenu_ToolBarLay:

                if (shareBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    shareBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
                }

                break;
            case R.id.ivClose_share:
                shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);

                break;
            case R.id.message_btn:
                Intent chat = new Intent(context, ChatActivity.class);
//                chat.putExtra(Constants.ARG_RECEIVER,users.get(i).name);
//                chat.putExtra(Constants.ARG_RECEIVER_UID,users.get(i).uid);
//                chat.putExtra(Constants.ARG_FIREBASE_TOKEN,users.get(i).firebaseToken);
//                chat.putExtra("name", ""+users.get(i).name);
                startActivity(chat);

//                mGetUsersPresenter = new GetUsersPresenter(this);
//                DialogManager.showProgress(this);
//                mGetUsersPresenter.getAllUsers();
//                Intent inbox=new Intent(this, ChatListActivity.class);
//                startActivity(inbox);
                break;
            case R.id.followup_btn:

                HashMap<String, Object> map = new HashMap<>();
                map.put("user_id", "" + SharedHelper.getIntKey(GuestProfile_Activity.this, "user_id"));
                map.put("follower_id", guest_id);
                presenter.followup(map);

                break;

            case R.id.unfollow_img:

                HashMap<String, Object> unfollow = new HashMap<>();
                unfollow.put("user_id", "" + SharedHelper.getIntKey(GuestProfile_Activity.this, "user_id"));
                unfollow.put("follower_id", guest_id);
                presenter.followup(unfollow);

                break;
            case R.id.insta_link:

                Uri uri = Uri.parse(insta);
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(insta)));
                }

                break;

            case R.id.youtube_link:

                if (insta == null && youtube != null) {
                    Intent viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(youtube));
                    startActivity(viewIntent);
                } else if (insta != null && youtube != null) {
                    Intent viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(youtube));
                    startActivity(viewIntent);

                }

                break;

        }
    }
}

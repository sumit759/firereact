package com.mozostar.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.UserVideosList_Pojo;
import com.mozostar.app.view.activity.ExoPlayerActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoSearchAdapter extends RecyclerView.Adapter<VideoSearchAdapter.MyViewHolder> {
    Context context;
    ArrayList<UserVideosList_Pojo> videosList_pojos;

    public VideoSearchAdapter(Context context, ArrayList<UserVideosList_Pojo> videosList_pojoArrayList) {
        this.context = context;
        this.videosList_pojos = videosList_pojoArrayList;
    }

    @NonNull
    @Override
    public VideoSearchAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trendingchild_rowitem, parent, false);
        return new VideoSearchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoSearchAdapter.MyViewHolder myViewHolder, int i) {
        Glide.with(context).load("" + videosList_pojos.get(i).getVideo_image_url()).into(myViewHolder.ivTrendImage_TrendChildLay);


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedHelper.putKey(context, "video_view", "guest_dub");

                Intent intent = new Intent(context, ExoPlayerActivity.class);
                intent.putExtra("position", i);
                intent.putExtra("position_list", "" + myViewHolder.getAdapterPosition());
                intent.putExtra("guest_id", "" + videosList_pojos.get(i).getUserId());
                intent.putExtra("video", "" + videosList_pojos.get(i).getVideo());

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return videosList_pojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivTrendImage_TrendChildLay)
        ImageView ivTrendImage_TrendChildLay;
        @BindView(R.id.llComments_TrendChildLay)
        LinearLayout llComments_TrendChildLay;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            llComments_TrendChildLay.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

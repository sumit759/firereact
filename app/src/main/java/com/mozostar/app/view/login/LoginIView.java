package com.mozostar.app.view.login;


import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Login_Pojo;
import com.mozostar.app.data.model.Reset_Pojo;

public interface LoginIView extends MvpView {
    void onSuccessLogin(Login_Pojo login_pojo);

    void onSuccessReset(Reset_Pojo trendsResponse);
}

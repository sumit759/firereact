package com.mozostar.app.view.activity.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.common.LogoutDialog;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.view.activity.ShareActivity;
import com.mozostar.app.view.activity.TermsPrivacyPolicyActivity;
import com.mozostar.app.view.activity.change_password.ChangePassword;
import com.mozostar.app.view.activity.channel.ChannelActivity;
import com.mozostar.app.view.activity.language.AppLanguageSelectActivity;
import com.mozostar.app.view.activity.notification.PushNotificationSettingsActivity;
import com.mozostar.app.view.activity.privacy.PrivacyActivity;
import com.mozostar.app.view.splash.Splash_Activity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class
SettingsPrivacyActivity extends BaseActivity implements LogoutDialog.DialogInterface, SettingsIView {

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.push_notification_layout)
    LinearLayout push_notification_layout;

    @BindView(R.id.logout_layout)
    LinearLayout logout_layout;

    @BindView(R.id.privacy_layout)
    LinearLayout privacy_layout;

    @BindView(R.id.channel_layout)
    LinearLayout channel_layout;

    @BindView(R.id.business_layout)
    LinearLayout upgrade;

    @BindView(R.id.share_layout_ll)
    LinearLayout share_layout_ll;

    @BindView(R.id.language_layout_ll)
    LinearLayout language_layout_ll;

    @BindView(R.id.terms_conditions_layout)
    LinearLayout terms_conditions_layout;

    @BindView(R.id.advertisement_layout)
    LinearLayout advertisement_layout;

    @BindView(R.id.privacy_policy_layout)
    LinearLayout privacy_policy_layout;
    @BindView(R.id.llChangePassword)
    LinearLayout llChangePassword;


    SettingsPresenter<SettingsPrivacyActivity> presenter = new SettingsPresenter<>();
    Context mContext;

    @Override
    public int getLayoutId() {
        return R.layout.activity_settings_privacy;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);
        presenter.attachView(this);
        mContext = SettingsPrivacyActivity.this;

        back.setOnClickListener(v -> onBackPressed());

        push_notification_layout.setOnClickListener(v -> {
            Intent settingPrivacy = new Intent(SettingsPrivacyActivity.this, PushNotificationSettingsActivity.class);
            startActivity(settingPrivacy);
        });

        share_layout_ll.setOnClickListener(v -> {
            Intent settingPrivacy = new Intent(SettingsPrivacyActivity.this, ShareActivity.class);
            startActivity(settingPrivacy);
        });

        llChangePassword.setOnClickListener(v -> {
            Intent settingPrivacy = new Intent(SettingsPrivacyActivity.this, ChangePassword.class);
            startActivity(settingPrivacy);
        });

        privacy_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingPrivacy = new Intent(SettingsPrivacyActivity.this, PrivacyActivity.class);
                startActivity(settingPrivacy); }
        });

        channel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SettingsPrivacyActivity.this, ChannelActivity.class));
            }
        });

        language_layout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsPrivacyActivity.this, AppLanguageSelectActivity.class));
            }
        });
        terms_conditions_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsPrivacyActivity.this, TermsPrivacyPolicyActivity.class).putExtra("fromAct", "terms"));
            }
        });
        privacy_policy_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsPrivacyActivity.this, TermsPrivacyPolicyActivity.class).putExtra("fromAct", "privacy_policy"));
            }
        });

        logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CustomViewDialog alert = new CustomViewDialog();
                //alert.showDialog(SettingsPrivacyActivity.this);
                callDialog();

            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("user_id", "" + SharedHelper.getIntKey(SettingsPrivacyActivity.this, "user_id"));
                presenter.upgrade(map);
            }
        });

    }

    public void callDialog() {
        try {
            LogoutDialog logoutDialog = new LogoutDialog(mContext, this);
            logoutDialog.setCancelable(false);
            logoutDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSuccessUpgrade(Follow_Pojo trendsResponse) {


        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setMessage(trendsResponse.getMessage());

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        alert.show();
    }


    @Override
    public void onYes() {
        SharedHelper.putKey(SettingsPrivacyActivity.this, "access_token", "");
        SharedHelper.putKey(SettingsPrivacyActivity.this, "logged_in", false);
        Toast.makeText(SettingsPrivacyActivity.this, "Logout Successfully", Toast.LENGTH_LONG).show();
        Intent mainIntent = new Intent(SettingsPrivacyActivity.this, Splash_Activity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        activity().finish();

    }

    @Override
    public void onNo() {

    }
}



package com.mozostar.app.view.activity.chat.chatfcm;

import android.content.Context;

import com.mozostar.app.view.activity.chat.models.ChatModel;


public interface ChatContract {
    interface View {
        void onSendMessageSuccess();

        void onSendMessageFailure(String message);

        void onGetMessagesSuccess(ChatModel chatModel);

        void onGetMessagesFailure(String message);
    }

    interface Presenter {
        void sendMessage(Context context, ChatModel chatModel, String receiverFirebaseToken);

        void getMessage(String senderUid, String receiverUid);
    }

    interface Interactor {
        void sendMessageToFirebaseUser(Context context, ChatModel chatModel, String receiverFirebaseToken);

        void getMessageFromFirebaseUser(String senderUid, String receiverUid);
    }

    interface OnSendMessageListener {
        void onSendMessageSuccess();

        void onSendMessageFailure(String message);
    }

    interface OnGetMessagesListener {
        void onGetMessagesSuccess(ChatModel chatModel);

        void onGetMessagesFailure(String message);
    }
}

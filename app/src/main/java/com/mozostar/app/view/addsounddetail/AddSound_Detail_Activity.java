package com.mozostar.app.view.addsounddetail;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.view.addsounddetail.adapter.Playlist_Adapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddSound_Detail_Activity extends BaseActivity implements PlaylistOnClick {
    Context context;
    private static String TAG = AddSound_Detail_Activity.class.getSimpleName();

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;
    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;
    @BindView(R.id.rvPlaylist_AddSoundDetailLay)
    RecyclerView rvPlaylist_AddSoundDetailLay;
    @BindView(R.id.back)
    ImageView back;


    ArrayList<String> list_arraylist = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_addsounds;
    }

    @Override
    public void initView() {
        context = AddSound_Detail_Activity.this;
        ButterKnife.bind(this);
        tvTitle_ToolBarLay.setText("Tamil Dance Hits");
        ivMenu_ToolBarLay.setVisibility(View.GONE);

        list_arraylist.add("Verithanam (From Bigil)");
        list_arraylist.add("Hey Amigo");
        list_arraylist.add("Paisa Nore (From Comali)");
        list_arraylist.add("Rowdy Baby");
        list_arraylist.add("Siriki (From Kaappaan)");
        list_arraylist.add("Rockstar Robber");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rvPlaylist_AddSoundDetailLay.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvPlaylist_AddSoundDetailLay.setHasFixedSize(false);
        Playlist_Adapter playlist_adapter = new Playlist_Adapter(context, this, list_arraylist);
        rvPlaylist_AddSoundDetailLay.setAdapter(playlist_adapter);
    }


    @Override
    public void onClickPlaylistItem(int position) {

    }
}


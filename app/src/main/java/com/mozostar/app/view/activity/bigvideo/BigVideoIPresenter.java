package com.mozostar.app.view.activity.bigvideo;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface BigVideoIPresenter<V extends BigVideoIView> extends MvpPresenter<V> {
    void getBigVideoList(boolean comingFrom);
}

package com.mozostar.app.view.activity.change_password;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mozostar.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePassword extends AppCompatActivity {

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;
    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;
    @BindView(R.id.back)
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);


        /*use to set toolbar data*/
        tvTitle_ToolBarLay.setText(R.string.change_password);
        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
package com.mozostar.app.view.videodub_addsound.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.data.model.SoundsPlayList_Pojo;
import com.mozostar.app.view.videodub_addsound.frag.discover_sound.AddSound_Discover_frag;
import com.mozostar.app.view.videodub_addsound.DiscoverPlaylistOnClick;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverPlaylist_Adapter extends RecyclerView.Adapter<DiscoverPlaylist_Adapter.MyViewHolder> {
    ArrayList<SoundsPlayList_Pojo> soundsPlayList_pojoArrayList;
    Context mContext;
    DiscoverPlaylistOnClick pagerAdapterClicK;

    public DiscoverPlaylist_Adapter(Context context, AddSound_Discover_frag addSound_discover_frag, ArrayList<SoundsPlayList_Pojo> list_arraylist) {
        mContext = context;
        pagerAdapterClicK = addSound_discover_frag;
        soundsPlayList_pojoArrayList = list_arraylist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_itemrow, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        SoundsPlayList_Pojo soundsPlayList_pojo = soundsPlayList_pojoArrayList.get(i);
        myViewHolder.tvPlayListName_PlaylistLay.setText(soundsPlayList_pojo.getTitle());

        myViewHolder.llPlayList_PlaylistLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pagerAdapterClicK.onClickPlaylistItem(i);
            }
        });

        Glide.with(mContext).load(BuildConfig.BASE_IMAGE_URL + soundsPlayList_pojo.getPicture())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_launcher_background)
                .dontAnimate().error(R.drawable.ic_launcher_background))
                .into(myViewHolder.ivPlayListName_PlaylistLay);
    }

    @Override
    public int getItemCount() {
        return soundsPlayList_pojoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvPlayListName_PlaylistLay)
        TextView tvPlayListName_PlaylistLay;
        @BindView(R.id.ivPlayListName_PlaylistLay)
        ImageView ivPlayListName_PlaylistLay;
        @BindView(R.id.llPlayList_PlaylistLay)
        LinearLayout llPlayList_PlaylistLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

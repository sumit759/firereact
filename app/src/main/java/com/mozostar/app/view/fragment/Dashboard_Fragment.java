package com.mozostar.app.view.fragment;

import android.content.Context;
import android.view.View;


import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;

import butterknife.ButterKnife;

public class Dashboard_Fragment extends BaseFragment {
    Context context;
    private static String TAG = Dashboard_Fragment.class.getSimpleName();

//    @BindView(R.id.camera)
//    CameraKitView cameraKitView;


    @Override
    public int getLayoutId() {
        return R.layout.dashboard_frag;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        //cameraKitView.onStart();
    }

}

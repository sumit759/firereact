package com.mozostar.app.view.videodub_addsound.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.data.model.SoundsPlayList_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.view.videodub_addsound.frag.discover_sound.AddSound_Discover_frag;
import com.mozostar.app.view.videodub_addsound.DiscoverPlaylistOnClick;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverPlaylist1_Adapter extends RecyclerView.Adapter<DiscoverPlaylist1_Adapter.MyViewHolder> {
    ArrayList<SoundsPlayList_Pojo> soundsPlayList_pojoArrayList;
    Context mContext;
    DiscoverPlaylistOnClick pagerAdapterClicK;


    public DiscoverPlaylist1_Adapter(Context context, AddSound_Discover_frag addSound_discover_frag, ArrayList<SoundsPlayList_Pojo> list_arraylist) {
        mContext = context;
        pagerAdapterClicK = addSound_discover_frag;
        soundsPlayList_pojoArrayList = list_arraylist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist1_itemrow, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        SoundsPlayList_Pojo soundsPlayList_pojo= soundsPlayList_pojoArrayList.get(i);
        myViewHolder.tvPlayListName_Playlist1Lay.setText(soundsPlayList_pojo.getName());

        List<Sounds_Pojo> sounds_pojoArrayList = soundsPlayList_pojo.getSounds();
        DiscoverPlaylistChild_Adapter discoverPlaylist_adapter = new DiscoverPlaylistChild_Adapter(mContext,pagerAdapterClicK, sounds_pojoArrayList);
        myViewHolder.rvPlaylistChild_Playlist1Lay.setAdapter(discoverPlaylist_adapter);

        myViewHolder.tvAll_Playlist1Lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagerAdapterClicK.onClickAllPlaylistItem(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return soundsPlayList_pojoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvPlayListName_Playlist1Lay)
        TextView tvPlayListName_Playlist1Lay;
        @BindView(R.id.tvAll_Playlist1Lay)
        TextView tvAll_Playlist1Lay;
        @BindView(R.id.rvPlaylistChild_Playlist1Lay)
        RecyclerView rvPlaylistChild_Playlist1Lay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            rvPlaylistChild_Playlist1Lay.setVisibility(View.VISIBLE);
            //rvPlaylistChild_Playlist1Lay.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(mContext);
            rvPlaylistChild_Playlist1Lay.setLayoutManager(gridLayoutManager);
            rvPlaylistChild_Playlist1Lay.setHasFixedSize(false);
            rvPlaylistChild_Playlist1Lay.setNestedScrollingEnabled(true);
        }
        @Override
        public void onClick(View v) {
        }
    }
}

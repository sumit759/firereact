package com.mozostar.app.view.activity.videos_view;

public interface AdapterOnClick {
    void onClickGuestProfile();
    void onClickSoundTrack();
}

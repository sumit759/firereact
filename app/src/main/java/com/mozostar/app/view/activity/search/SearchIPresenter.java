package com.mozostar.app.view.activity.search;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface SearchIPresenter <V extends SearchIView> extends MvpPresenter<V> {


    @SuppressLint("CheckResult")
    void getUserList(String user);

    @SuppressLint("CheckResult")
    void getHashList(String user);
}

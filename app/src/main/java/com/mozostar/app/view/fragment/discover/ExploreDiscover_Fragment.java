package com.mozostar.app.view.fragment.discover;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.cscs.raaz_asymmetric_grid_fuction.AsymmetricRecyclerView;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.view.adapter.ExploreAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

//Rasheed...
public class ExploreDiscover_Fragment extends BaseFragment implements DiscoverIView {

    int[] imgList = {R.drawable.thiller, R.drawable.two, R.drawable.thumbline, R.drawable.thumbnail2, R.drawable.thumbnail3, R.drawable.thumbnail4, R.drawable.thumbnail5, R.drawable.thumbnail6, R.drawable.thumbnail7, R.drawable.one, R.drawable.three, R.drawable.four,
            R.drawable.five, R.drawable.six, R.drawable.seven, R.drawable.eight,
            R.drawable.nine, R.drawable.ten};


    private final DemoUtils demoUtils = new DemoUtils();

    DiscoverPresenter<ExploreDiscover_Fragment> presenter = new DiscoverPresenter<>();

    @BindView(R.id.recyclerView_asymmetric)
    AsymmetricRecyclerView recyclerView;

    @BindView(R.id.exploreVideoFragmentRV)
    RecyclerView exploreVideoFragmentRV;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_explore;
    }

    @Override
    public View initView(View view) {
        ButterKnife.bind(this, view);

        presenter.attachView(this);
        presenter.getExploreVideoList();

       /* RecyclerViewAdapter adapter = new RecyclerViewAdapter(demoUtils.moarItems(imgList.length), imgList);
        recyclerView.setRequestedColumnCount(3);
        recyclerView.setDebugging(true);
        recyclerView.setRequestedHorizontalSpacing(Utils.dpToPx(getActivity(), 3));
        recyclerView.addItemDecoration(
                new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.recycler_padding)));
        recyclerView.setAdapter(new AsymmetricRecyclerViewAdapter<>(getActivity(), recyclerView, adapter));*/


        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL); // 2 Columns
        exploreVideoFragmentRV.setLayoutManager(layoutManager);


       /* exploreVideoFragmentRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        exploreVideoFragmentRV.setItemAnimator(new DefaultItemAnimator());*/


        return view;
    }

    @Override
    public void onSuccess(Discover_Pojo trendsResponse) {

    }

    @Override
    public void onSuccessUser(ArrayList<User_Pojo> trendsResponse) {

    }

    @Override
    public void onSuccessExploreVideoList(ExploreChannelVideoResponse exploreList) {
        try {
            if (exploreList.getData().size() > 0){
                ExploreAdapter exploreAdapter = new ExploreAdapter(getContext(),exploreList);
                exploreVideoFragmentRV.setAdapter(exploreAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*class RecyclerViewAdapter extends AGVRecyclerViewAdapter<ViewHolder> {
        private List<DemoItem> items;
        int[] image;

        RecyclerViewAdapter(List<DemoItem> items) {
            this.items = items;
        }

        public RecyclerViewAdapter(List<DemoItem> moarItems, int[] imgList) {
            this.items = moarItems;
            this.image = imgList;


        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Log.d("RecyclerViewActivity", "onCreateView");
            return new ViewHolder(parent, viewType);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Log.d("RecyclerViewActivity", "onBindView position=" + position);
//            holder.bind(items.get(position));

            holder.textView.setImageResource(image[position]);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public AsymmetricItem getItem(int position) {
            return items.get(position);
        }

        @Override
        public int getItemViewType(int position) {
            return position % 2 == 0 ? 1 : 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView textView;

        ViewHolder(ViewGroup parent, int viewType) {
            super(LayoutInflater.from(parent.getContext()).inflate(
                    viewType == 0 ? R.layout.adapter_item : R.layout.adapter_item_odd, parent, false));
            if (viewType == 0) {
                textView = itemView.findViewById(R.id.textview);
            } else {
                textView = itemView.findViewById(R.id.textview_old);
            }
        }

        void bind(AsymmetricItem item) {
//            textView.setText(String.valueOf(item.getPosition()));
        }
    }*/

}

package com.mozostar.app.view.splash;


import com.mozostar.app.base.MvpView;

public interface SplashIView extends MvpView {
    void redirectHome();
}
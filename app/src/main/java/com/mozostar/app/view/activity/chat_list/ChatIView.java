package com.mozostar.app.view.activity.chat_list;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.ChatUserList_Pojo;

import java.util.ArrayList;

public interface ChatIView extends MvpView {
    void onSuccess(ArrayList<ChatUserList_Pojo> trendsResponse);
}

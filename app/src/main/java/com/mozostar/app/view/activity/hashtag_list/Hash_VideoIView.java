package com.mozostar.app.view.activity.hashtag_list;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Discover_Pojo;

public interface Hash_VideoIView extends MvpView {

    void onSuccess(Discover_Pojo trendsResponse);

}

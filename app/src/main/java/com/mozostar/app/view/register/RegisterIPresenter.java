package com.mozostar.app.view.register;


import com.mozostar.app.base.MvpPresenter;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public interface RegisterIPresenter<V extends RegisterIView> extends MvpPresenter<V> {
    void postregister(Map<String, RequestBody> params, MultipartBody.Part file);

}

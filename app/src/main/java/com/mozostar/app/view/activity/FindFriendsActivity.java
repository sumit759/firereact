package com.mozostar.app.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FindFriendsActivity extends BaseActivity {

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.invite_friend_layout_ll)
    LinearLayout invite_friend_layout_ll;

    @Override
    public int getLayoutId() {
        return R.layout.activity_find_friends;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);
        tvTitle_ToolBarLay.setText("Find Friends");
        invite_friend_layout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindFriendsActivity.this, FriendDetailsActivity.class);
                startActivity(intent);
            }
        });

    }

    @OnClick(R.id.back)
    public void onClickBack() {
        onBackPressed();
    }

}
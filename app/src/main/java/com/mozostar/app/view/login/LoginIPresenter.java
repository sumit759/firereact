package com.mozostar.app.view.login;


import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;


public interface LoginIPresenter<V extends LoginIView> extends MvpPresenter<V> {
    void postlogin(HashMap<String, Object> obj);

    @SuppressLint("CheckResult")
    void reset(HashMap<String, Object> obj);
}

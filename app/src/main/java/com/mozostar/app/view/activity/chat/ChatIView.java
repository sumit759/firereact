package com.mozostar.app.view.activity.chat;


import com.mozostar.app.base.MvpView;

public interface ChatIView extends MvpView {
    void onSuccessFCM(Object object);

    void onError(Throwable throwable);
}

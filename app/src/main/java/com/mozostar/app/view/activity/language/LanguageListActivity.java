package com.mozostar.app.view.activity.language;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LanguageListActivity extends BaseActivity {

    @BindView(R.id.languageListRv)
    RecyclerView languageListRv;

    @BindView(R.id.cancel_language_tv)
    TextView cancel_language_tv;

    @BindView(R.id.header_name_tv)
    TextView header_name_tv;

    @BindView(R.id.done_language_tv)
    TextView done_language_tv;

    LinearLayoutManager layoutManager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_language_list;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        header_name_tv.setText("App Language");
        layoutManager = new LinearLayoutManager(LanguageListActivity.this, RecyclerView.VERTICAL, false);
        languageListRv.setLayoutManager(layoutManager);
        languageListRv.setHasFixedSize(false);
        List<String> languageList = new ArrayList<String>();
        languageList.add("English");
        languageList.add("Arabic");
        languageList.add("Chinese");
        LanguageAdapter languageAdapter = new LanguageAdapter(this, languageList);
        languageListRv.setAdapter(languageAdapter);

    }

    @OnClick(R.id.cancel_language_tv)
    public void onBackClick() {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
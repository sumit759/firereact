package com.mozostar.app.view.activity.search;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.model.HashSearch_Pojo;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.view.activity.search.fragment.PagerAdapter;
import com.mozostar.app.view.fragment.HashTagSearchFragment;
import com.mozostar.app.view.fragment.SoundSearchFragment;
import com.mozostar.app.view.fragment.UserSearchFragment;
import com.mozostar.app.view.fragment.VideoSearchFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity  /*implements SearchIView*/ {

    @BindView(R.id.rv_sear_lay)
    RecyclerView rv_sear_lay;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.tvSearch_DiscoverFragLay)
    EditText tvSearch_DiscoverFragLay;

    @BindView(R.id.ivClearprofileText)
    ImageView ivClearprofileText;

    @BindView(R.id.tab_layout)
    TabLayout tab_layout;

    @BindView(R.id.pager)
    ViewPager pager;

    @BindView(R.id.user_tab_text)
    TextView user_tab_text;

    @BindView(R.id.sounds_tab_text)
    TextView sounds_tab_text;

    @BindView(R.id.videos_tab_text)
    TextView videos_tab_text;

    @BindView(R.id.hashtag_tab_text)
    TextView hashtag_tab_text;

    public static String searchingKey = "";
    public static String currentFragment = "";

    //  SearchPresenter<SearchActivity> presenter = new SearchPresenter<>();
    ArrayList<User_Pojo> user_pojos = new ArrayList<>();
    ArrayList<HashSearch_Pojo> hash_pojos = new ArrayList<>();

    String user, search;

    @Override
    public int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    public void initView() {
        ButterKnife.bind(this);
        //  presenter.attachView(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            search = extras.getString("search");
        }
        // createFragments();
       /* rv_sear_lay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv_sear_lay.setItemAnimator(new DefaultItemAnimator());
        rv_sear_lay.setNestedScrollingEnabled(true);*/

        changeFragment(new UserSearchFragment(), "user", "");
        currentFragment = "user";
        tvSearch_DiscoverFragLay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    try {
                        searchingKey = tvSearch_DiscoverFragLay.getText().toString();
                        if (currentFragment.equalsIgnoreCase("user")) {
                            currentFragment = "user";
                            changeFragment(new UserSearchFragment(), "user", searchingKey);
                        } else if (currentFragment.equalsIgnoreCase("video")) {
                            currentFragment = "video";
                            changeFragment(new VideoSearchFragment(), "video", searchingKey);
                        } else if (currentFragment.equalsIgnoreCase("sound")) {
                            currentFragment = "sound";
                            changeFragment(new SoundSearchFragment(), "sound", searchingKey);
                        } else if (currentFragment.equalsIgnoreCase("hashtag")) {
                            currentFragment = "hashtag";
                            changeFragment(new HashTagSearchFragment(), "hashtag", searchingKey);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivClearprofileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSearch_DiscoverFragLay.setText("");
            }
        });

    }

    private void changeFragment(Fragment fragment, String from, String searchingKey) {
        Bundle arguments = new Bundle();
        arguments.putString("from", from);
        arguments.putString("searchingKey", searchingKey);
        fragment.setArguments(arguments);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.searchContainer, fragment);
        transaction.commit();
    }

    private void createFragments() {
        // tab_layout.addTab(tab_layout.newTab().setText("Top"));
        tab_layout.addTab(tab_layout.newTab().setText("Users"));
        tab_layout.addTab(tab_layout.newTab().setText("Videos"));
        tab_layout.addTab(tab_layout.newTab().setText("Sounds"));
        tab_layout.addTab(tab_layout.newTab().setText("Hashtags"));
        tab_layout.setTabGravity(TabLayout.GRAVITY_FILL);

        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tab_layout.getTabCount());
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout));

        tab_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

   /* @Override
    public void onSuccessUser(ArrayList<User_Pojo> trendsResponse) {
      *//*  user_pojos = trendsResponse;
        rv_sear_lay.setVisibility(View.VISIBLE);
        User_Adapter homeScreen_adapter = new User_Adapter(this, user_pojos);
        rv_sear_lay.setAdapter(homeScreen_adapter);*//*
    }

    @Override
    public void onSuccessHash(ArrayList<HashSearch_Pojo> trendsResponse) {
       *//* hash_pojos = trendsResponse;
        rv_sear_lay.setVisibility(View.VISIBLE);
        Hash_Adapter homeScreen_adapter = new Hash_Adapter(this, hash_pojos);
        rv_sear_lay.setAdapter(homeScreen_adapter);*//*
    }*/

    @OnClick({R.id.back})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    @OnClick({R.id.user_tab_text,R.id.videos_tab_text,R.id.sounds_tab_text,R.id.hashtag_tab_text})
    public void onTabClicked(View v) {
        switch (v.getId()) {
            case R.id.user_tab_text:
                currentFragment = "user";
                changeFragment(new UserSearchFragment(), "user", tvSearch_DiscoverFragLay.getText().toString());
              //  tvSearch_DiscoverFragLay.setText("");
                user_tab_text.setBackgroundResource(R.drawable.text_select_layout_bg);
                videos_tab_text.setBackground(null);
                sounds_tab_text.setBackground(null);
                hashtag_tab_text.setBackground(null);
                break;
            case R.id.videos_tab_text:
                currentFragment = "video";
                changeFragment(new VideoSearchFragment(), "video", tvSearch_DiscoverFragLay.getText().toString());
            //    tvSearch_DiscoverFragLay.setText("");
                user_tab_text.setBackground(null);
                videos_tab_text.setBackgroundResource(R.drawable.text_select_layout_bg);
                sounds_tab_text.setBackground(null);
                hashtag_tab_text.setBackground(null);
                break;
            case R.id.sounds_tab_text:
                currentFragment = "sound";
                changeFragment(new SoundSearchFragment(), "sound", tvSearch_DiscoverFragLay.getText().toString());
             //   tvSearch_DiscoverFragLay.setText("");
                user_tab_text.setBackground(null);
                videos_tab_text.setBackground(null);
                sounds_tab_text.setBackgroundResource(R.drawable.text_select_layout_bg);
                hashtag_tab_text.setBackground(null);
                break;
            case R.id.hashtag_tab_text:
                currentFragment = "hashtag";
                changeFragment(new HashTagSearchFragment(), "hashtag", tvSearch_DiscoverFragLay.getText().toString());
            //    tvSearch_DiscoverFragLay.setText("");
                user_tab_text.setBackground(null);
                videos_tab_text.setBackground(null);
                sounds_tab_text.setBackground(null);
                hashtag_tab_text.setBackgroundResource(R.drawable.text_select_layout_bg);
                break;
        }
    }
}

/*

 if (s.length() != 0) {
         user = tvSearch_DiscoverFragLay.getText().toString();
         int item = pager.getCurrentItem();
         if (!TextUtils.isEmpty(search)) {
         if (search.equals("user")) {
         Bundle bundle = new Bundle();
         bundle.putString("edttext", "FromActivity");
         UserSearchFragment fragobj = new UserSearchFragment();
         fragobj.setArguments(bundle);
         //  presenter.getUserList(user);
         } else if (search.equals("hash")) {
         presenter.getHashList(user);

         } else if (search.equals("post_user")) {
         presenter.getUserList(user);

         }
         }
         rv_sear_lay.setVisibility(View.GONE);
         } else {
         rv_sear_lay.setVisibility(View.GONE);
         tvSearch_DiscoverFragLay.getText().clear();
         }
*/

package com.mozostar.app.view.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mozostar.app.R;

import androidx.recyclerview.widget.RecyclerView;


public class NewAdapter extends RecyclerView.Adapter<NewAdapter.MasonryView> {

    private Context context;

    int[] imgList = {R.drawable.two,R.drawable.thumbline,R.drawable.thumbnail2,R.drawable.thumbnail3,R.drawable.thumbnail4,R.drawable.thumbnail5,R.drawable.thumbnail6,R.drawable.thumbnail7, R.drawable.one, R.drawable.three, R.drawable.four,
            R.drawable.five, R.drawable.six, R.drawable.seven, R.drawable.eight,
            R.drawable.nine, R.drawable.ten};



    public NewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public MasonryView onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        MasonryView masonryView = new MasonryView(layoutView);
        return masonryView;
    }

    @Override
    public void onBindViewHolder(MasonryView holder, int position) {
        holder.imageView.setImageResource(imgList[position]);
    }

    @Override
    public int getItemCount() {
        return imgList.length;
    }

    class MasonryView extends RecyclerView.ViewHolder {
        ImageView imageView;

        public MasonryView(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.img);

        }
    }
}

package com.mozostar.app.view.activity.channelVideoList;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;
import com.mozostar.app.view.DividerGridItemDecoration;
import com.mozostar.app.view.activity.ExoPlayerActivity;
import com.mozostar.app.view.adapter.VideoGridChannelAdapter;
import com.mozostar.app.view.device_video_list.DvideoList_Activity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelVideoListActivity extends BaseActivity implements ChannelVideoListIView, VideoGridChannelAdapter.OnItemClickListener {


    @BindView(R.id.tvTitle_ToolBarLay)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView option;

    @BindView(R.id.cvl_recylerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.cvl_add)
    ImageView add;

     List<ChannelVideoList_Pojo.Videos> ChannelVideoList = new ArrayList<>();
    private VideoGridChannelAdapter mAdapter;
    ArrayList<String> cover_ArrayList = new ArrayList<>();


    ChannelVideoListPresenter<ChannelVideoListActivity> presenter=new ChannelVideoListPresenter<>();
    Context context;


    @Override
    public int getLayoutId() {
        return R.layout.activity_channel_video_list;
    }

    @Override
    public void initView() {
        context = ChannelVideoListActivity.this;

        ButterKnife.bind(this);
        presenter.attachView(this);
        option.setVisibility(View.INVISIBLE);


        String id = String.valueOf(SharedHelper.getIntKey(context,"id"));

        presenter.getList(id);

        mRecyclerView.setLayoutManager(new GridLayoutManager(context, 4));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(context));

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityManager.redirectToActivityWithoutBundle(context, DvideoList_Activity.class);
            }
        });

        title.setText(getIntent().getExtras().getString("name"));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public void onSuccessChannelVideoList(ChannelVideoList_Pojo channelList_pojoList) {
        for (int i=0;i<channelList_pojoList.getVideos().size();i++){
            cover_ArrayList.add(channelList_pojoList.getVideos().get(i).getVideo());
        }

        ChannelVideoList = channelList_pojoList.getVideos();
        mAdapter = new VideoGridChannelAdapter(context, ChannelVideoList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(ChannelVideoListActivity.this);
    }

    @Override
    public void onItemClick(int position, ChannelVideoList_Pojo.Videos model) {

//        Intent intent = new Intent(context, VideoPreview_Activity.class);
//        intent.putExtra("dubbed_video_path",model.getVideo());
//        context.startActivity(intent);

                SharedHelper.putKey(context, "video_view", "channel_video_list");
               Intent intent = new Intent(context, ExoPlayerActivity.class);
//                intent.putExtra("list",cover_ArrayList );
                intent.putExtra("position",""+position);
                intent.putExtra("guest_id",""+model.getUserId());
                 intent.putExtra("video", "" + model.getVideo());
                context.startActivity(intent);

    }


}

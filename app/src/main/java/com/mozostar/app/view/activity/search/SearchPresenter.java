package com.mozostar.app.view.activity.search;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.HashSearch_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.data.model.UserVideosList_Pojo;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchPresenter<V extends SearchIView> extends BasePresenter<V> implements SearchIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void getUserList(String user) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().search_user(user);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessUser((ArrayList<User_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    @Override
    public void getHashList(String user) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().search_tag(user);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessHash((ArrayList<HashSearch_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }


    @SuppressLint("CheckResult")
    public void getVideoList(String user) {
        Observable modelObservable = APIClient.getAPIClient().searchVideo(user);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userVideoList -> {
                            getMvpView().onSuccessVideo((ArrayList<UserVideosList_Pojo>) userVideoList);
                        },
                        throwable -> {
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    public void getSoundList(String user) {
        Observable modelObservable = APIClient.getAPIClient().searchSound(user);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(soundsPojoList -> {
                            getMvpView().onSuccessSound((ArrayList<Sounds_Pojo>) soundsPojoList);
                        },
                        throwable -> {
                            getMvpView().onError((Throwable) throwable);
                        });

    }

}

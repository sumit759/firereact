package com.mozostar.app.view.splash;


import com.mozostar.app.base.MvpPresenter;

public interface SplashIPresenter <V extends SplashIView> extends MvpPresenter<V> {
    void handlerCall();
}
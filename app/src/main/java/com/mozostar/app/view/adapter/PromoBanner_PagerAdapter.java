package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.mozostar.app.R;

import java.util.ArrayList;
import java.util.List;

public class PromoBanner_PagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    List<Integer> mbannerContents;

    public PromoBanner_PagerAdapter(Context context, ArrayList<Integer> bannerContents) {
        this.mContext = context;
        this.mbannerContents = bannerContents;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mbannerContents.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);

    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        View itemView = mLayoutInflater.inflate(R.layout.bannerpromo_rowitem, container, false);
        ImageView ivImage_HeaderAdsRowitem = itemView.findViewById(R.id.ivImage_BannerPromoRowitem);
        ivImage_HeaderAdsRowitem.setImageResource(mbannerContents.get(i));

//        Glide.with(mContext).load("" + mbannerContents.get(position))
//                .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
//                        .dontAnimate().error(R.drawable.user_profile)).into(ivImage_HeaderAdsRowitem);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}

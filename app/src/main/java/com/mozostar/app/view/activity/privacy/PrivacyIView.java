package com.mozostar.app.view.activity.privacy;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.Safety_Pojo;

public interface PrivacyIView extends MvpView {
    void onSuccess(Safety_Pojo safetyResponse);
}

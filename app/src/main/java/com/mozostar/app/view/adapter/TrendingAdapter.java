package com.mozostar.app.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrendingAdapter  extends RecyclerView.Adapter<TrendingAdapter.MyViewHolder> {
    Context context;
    List<Drawable> cover_ArrayList;
    int clickPosition;
    String from;


    public TrendingAdapter(Context context, List<Drawable> cover_ArrayList) {
        this.context = context;
        this.cover_ArrayList = cover_ArrayList;
        this.clickPosition = clickPosition;

    }

    @NonNull
    @Override
    public TrendingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trendingchild_rowitem, parent, false);
        return new TrendingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TrendingAdapter.MyViewHolder myViewHolder, int i) {

        Glide.with(context).load(cover_ArrayList.get(i)).into(myViewHolder.ivTrendImage_TrendChildLay);


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });
    }

    @Override
    public int getItemCount() {
        return cover_ArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivTrendImage_TrendChildLay)
        ImageView ivTrendImage_TrendChildLay;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        @Override
        public void onClick(View v) {

        }
    }
}

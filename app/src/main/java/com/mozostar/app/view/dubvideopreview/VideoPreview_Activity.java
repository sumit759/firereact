package com.mozostar.app.view.dubvideopreview;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoPreview_Activity extends BaseActivity implements Player.EventListener {
    Context context;
    private static String TAG = VideoPreview_Activity.class.getSimpleName();

    String dubbed_video_path = "", dubbed_audio_path = "";


    @BindView(R.id.pvVideoPreview_DubVideoPreLay)
    PlayerView pvVideoPreview_DubVideoPreLay;
    @BindView(R.id.ivBack_DubVideoPreLay)
    ImageView ivBack_DubVideoPreLay;
    @BindView(R.id.btnNext_DubVideoPreLay)
    Button btnNext_DubVideoPreLay;

    @BindView(R.id.effects)
    RelativeLayout ef;

    @Override
    public int getLayoutId() {
        return R.layout.activity_dubbed_video_preview;
    }

    SimpleExoPlayer newSimpleInstance;

    @Override
    public void initView() {
        context = VideoPreview_Activity.this;
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        dubbed_video_path = bundle.getString("dubbed_video_path");

        ef.setVisibility(View.GONE);

        set_video_play();
    }

    private void set_video_play() {
        newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this.context, (TrackSelector) new DefaultTrackSelector());
        newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this.context, Util.getUserAgent(this.context, "TikTok")))
                .createMediaSource(Uri.parse(dubbed_video_path)));
        newSimpleInstance.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        pvVideoPreview_DubVideoPreLay.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        newSimpleInstance.addListener(this);
        pvVideoPreview_DubVideoPreLay.setPlayer(newSimpleInstance);
        newSimpleInstance.setPlayWhenReady(true);
    }

    @OnClick({R.id.ivBack_DubVideoPreLay, R.id.btnNext_DubVideoPreLay,R.id.text_add,
    R.id.camera_filter,R.id.music_filter,R.id.sticker_filter})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.ivBack_DubVideoPreLay) {
            onBackPressed();
        }
    }

    public void ShowDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);

    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void stopPreviousVideoPlayer() {
        if (newSimpleInstance != null) {

            newSimpleInstance.removeListener(this);
            newSimpleInstance.release();

        }
    }

    public void onResume() {
        super.onResume();
        if (newSimpleInstance != null) {
            newSimpleInstance.setPlayWhenReady(true);
        }
    }

    public void onPause() {
        super.onPause();
        if (newSimpleInstance != null) {
            newSimpleInstance.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (newSimpleInstance != null) {
            newSimpleInstance.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (newSimpleInstance != null) {
            newSimpleInstance.release();
        }
    }


}

package com.mozostar.app.view.fragment.homedashboard;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.model.VideoList_Pojo1;
import com.mozostar.app.data.model.VideoReportResponse;
import com.mozostar.app.data.network.APIClient;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomePresenter<V extends HomeIView> extends BasePresenter<V> implements HomeIPresenter<V> {

    @SuppressLint("CheckResult")
    @Override
    public void getList(HashMap<String, Object> params) {

        Observable modelObservable = APIClient.getAPIClient().video_list(params);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> getMvpView().onSuccess((VideoList_Pojo1) trendsResponse),
                        throwable -> getMvpView().onError((Throwable) throwable));
    }

    @SuppressLint("CheckResult")
    @Override
    public void getFollowList() {

//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().follow_video_list();
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessFollowList((ArrayList<ProfileVideoList_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    @Override
    public void like_video(HashMap<String, Object> obj) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().like(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessLike((Object) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void followup(HashMap<String, Object> obj) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().follow(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessFollow((Follow_Pojo) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }


    @SuppressLint("CheckResult")
    public void report_video(HashMap<String, Object> obj) {

        Observable modelObservable = APIClient.getAPIClient().reportVideo(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(videoReportRes -> {
                            getMvpView().onSuccessVideoReport((VideoReportResponse) videoReportRes);
                        },
                        throwable -> {
                            getMvpView().onError((Throwable) throwable);
                        });
    }
    @SuppressLint("CheckResult")
    @Override
    public void disLike_video(HashMap<String, Object> obj) {

        Observable modelObservable = APIClient.getAPIClient().disLike(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            getMvpView().onSuccessDisLike((Object) trendsResponse);
                        },
                        throwable -> {
                            getMvpView().onError((Throwable) throwable);
                        });
    }

}

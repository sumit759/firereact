package com.mozostar.app.view.activity.chat_list;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface ChatIPresenter  <V extends ChatIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getList();
}

package com.mozostar.app.view.videodub_addsound;

import com.mozostar.app.data.model.Sounds_Pojo;

public interface DiscoverPlaylistOnClick {
    void onClickPlaylistItem(int position, Sounds_Pojo sounds_pojo);
    void onClickPlaylistItem(int position);
    void onClickAllPlaylistItem(int position);
}

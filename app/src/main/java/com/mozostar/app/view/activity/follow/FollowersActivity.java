package com.mozostar.app.view.activity.follow;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.Follower_list;
import com.mozostar.app.data.model.FollowingLIst_pojo;
import com.mozostar.app.view.activity.FindFriendsActivity;
import com.mozostar.app.view.fragment.FollowPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class

FollowersActivity extends BaseActivity implements FollowersIView, InteractionListener {

    @BindView(R.id.rvPlaylist_FollowLay)
    RecyclerView rvPlaylist_FollowLay;


    @BindView(R.id.no_data)
    TextView no_data;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.tab_layout_follow)
    TabLayout tab_layout_follow;

    @BindView(R.id.pager_follow)
    ViewPager pager_follow;


    LinearLayoutManager layoutManager;

    String from, guest_id;

    InteractionListener mlistener;
//    ArrayList<FollowingLIst_pojo> cover_ArrayList=new ArrayList<>();

    FollowersPresenter<FollowersActivity> presenter = new FollowersPresenter<>();

    Bundle extras;

    @Override
    public int getLayoutId() {
        return R.layout.activity_followers;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);
        presenter.attachView(this);

        from = SharedHelper.getKey(FollowersActivity.this, "on_click_follow");
        layoutManager = new LinearLayoutManager(FollowersActivity.this, RecyclerView.VERTICAL, false);
        rvPlaylist_FollowLay.setLayoutManager(layoutManager);
        rvPlaylist_FollowLay.setHasFixedSize(false);
        ivMenu_ToolBarLay.setImageResource(R.drawable.ic_search_white);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ivMenu_ToolBarLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent startAct = new Intent(FollowersActivity.this, FindFriendsActivity.class);
//                startActivity(startAct);
//                finish();
            }
        });

        //  callList();
        createFragments();
    }

    private void callList() {
        extras = getIntent().getExtras();
        if (extras != null) {
            guest_id = extras.getString("guest_id");
            if (!TextUtils.isEmpty(guest_id)) {
                if (from.equals("guest_follower")) {
                    presenter.getGuestFollowersList(guest_id);

                    presenter.getGuestList(guest_id);
                    //   tvTitle_ToolBarLay.setText("Followers");
                } else if (from.equals("guest_follwing")) {
                    // tvTitle_ToolBarLay.setText("Following");
                    presenter.getGuestList(guest_id);

                    presenter.getGuestFollowersList(guest_id);
                }
            }
        } else {
            if (from.equals("follower")) {
                //  tvTitle_ToolBarLay.setText("Followers");
                presenter.getFollowersList();

                presenter.getList();
            } else if (from.equals("following")) {
                // tvTitle_ToolBarLay.setText("Following");
                presenter.getList();

                presenter.getFollowersList();
            }
        }

    }

    private void createFragments() {

        tab_layout_follow.addTab(tab_layout_follow.newTab().setText("Following"));
        tab_layout_follow.addTab(tab_layout_follow.newTab().setText("Followers"));

        tab_layout_follow.setTabGravity(TabLayout.GRAVITY_FILL);

        final FollowPagerAdapter adapter = new FollowPagerAdapter
                (getSupportFragmentManager(), tab_layout_follow.getTabCount());
        pager_follow.setAdapter(adapter);
        pager_follow.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout_follow));

        tab_layout_follow.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager_follow.setCurrentItem(tab.getPosition());
                // callList();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        if (from.equals("follower")) {
            pager_follow.setCurrentItem(1);
        } else if (from.equals("following")) {
            pager_follow.setCurrentItem(0);
        }
        if (from.equals("guest_follower")) {
            pager_follow.setCurrentItem(1);
        } else if (from.equals("guest_follwing")) {
            pager_follow.setCurrentItem(0);
        }

    }

    @Override
    public void onSuccess(ArrayList<FollowingLIst_pojo> trendsResponse) {
       /* int size = trendsResponse.size();
        if (trendsResponse != null && size != 0) {
            FollowAdapter homeScreen_adapter = new FollowAdapter(this, trendsResponse,this);
            rvPlaylist_FollowLay.setAdapter(homeScreen_adapter);
            no_data.setVisibility(View.GONE);
        } else {
            no_data.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public void onSuccessFollower(ArrayList<Follower_list> trendsResponse) {
       /* int size = trendsResponse.size();
        if (trendsResponse != null && size != 0) {
            FollowerAdapter homeScreen_adapter = new FollowerAdapter(this, trendsResponse);
            rvPlaylist_FollowLay.setAdapter(homeScreen_adapter);
            no_data.setVisibility(View.GONE);
        } else {
            no_data.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {
        //  callList();
    }

    @Override
    public void follow(String id) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("user_id", "" + SharedHelper.getIntKey(FollowersActivity.this, "user_id"));
        map.put("follower_id", id);
        presenter.followup(map);
    }

    @Deprecated
    public void onAttach(Activity activity) {
        if (activity instanceof InteractionListener) {
            //init the listener
            mlistener = (InteractionListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement InteractionListener");
        }
    }
}

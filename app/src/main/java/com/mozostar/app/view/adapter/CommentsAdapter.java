package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.R;
import com.mozostar.app.data.model.CommentsList_Pojo;

import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private List<CommentsList_Pojo> list;

    private Context context;


    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView comments, user_name_cmnts;
        private CircleImageView usaer_prfl;


        MyViewHolder(View view) {
            super(view);

            comments = view.findViewById(R.id.comments);
            user_name_cmnts=view.findViewById(R.id.user_name_cmnts);
            usaer_prfl=view.findViewById(R.id.usaer_prfl);

        }
    }


    public CommentsAdapter(Context context, List<CommentsList_Pojo> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public CommentsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comments_adapter, parent, false);

        return new CommentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.MyViewHolder holder, int position) {
        CommentsList_Pojo datum = list.get(position);


        holder.comments.setText(list.get(position).getComment());

        if (list.get(position).getUsersDetails().getImage() != null)
            Glide.with(context).load("" + list.get(position).getUsersDetails().getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
                            .dontAnimate().error(R.drawable.user_profile)).into(holder.usaer_prfl);

        holder.user_name_cmnts.setText(list.get(position).getUsersDetails().getFirstName() + " " + list.get(position).getUsersDetails().getLastName());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<CommentsList_Pojo> getList() {
        return this.list;
    }

}

package com.mozostar.app.view.videodub_addsound.adapter;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.BuildConfig;
import com.mozostar.app.R;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.view.videodub_addsound.DiscoverPlaylistOnClick;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverPlaylistChild_Adapter extends RecyclerView.Adapter<DiscoverPlaylistChild_Adapter.MyViewHolder> {
    public static MediaPlayer mediaPlayerMain = null;
    List<Sounds_Pojo> sounds_pojoList;
    Context mContext;
    DiscoverPlaylistOnClick pagerAdapterClicK;
    MediaPlayer mPlayer;
    int sel_pos = -1;

    public DiscoverPlaylistChild_Adapter(Context context, DiscoverPlaylistOnClick addSound_discover_frag, List<Sounds_Pojo> list_arraylist) {
        mContext = context;
        pagerAdapterClicK = addSound_discover_frag;
        sounds_pojoList = list_arraylist;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_child_itemrow, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Sounds_Pojo sounds_pojo = sounds_pojoList.get(i);
        myViewHolder.tvName_PlaylistChildLay.setText(sounds_pojo.getTitle());
        myViewHolder.tvDecrip_PlaylistChildLay.setText(sounds_pojo.getDescription());
        myViewHolder.tvDuration_PlaylistChildLay.setText("00:" + sounds_pojo.getDuration());
        Glide.with(mContext).load(BuildConfig.BASE_IMAGE_URL + sounds_pojo.getPicture())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_launcher_background)
                        .dontAnimate().error(R.drawable.ic_launcher_background))
                .into(myViewHolder.ivImg_PlaylistChildLay);

        myViewHolder.ivDone_PlaylistChildLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sounds_Pojo sounds_pojo = sounds_pojoList.get(i);
                pagerAdapterClicK.onClickPlaylistItem(i,sounds_pojo);
                if (mPlayer != null) {
                    if (mPlayer.isPlaying()) {
                        mPlayer.stop();
                    }
                }
            }
        });

        myViewHolder.llall.setOnClickListener(v -> {
            Sounds_Pojo sounds_pojo1 = sounds_pojoList.get(i);
            String audioUrl = BuildConfig.BASE_IMAGE_URL + sounds_pojo1.getAudio();
            if(sel_pos!=-1){
                sounds_pojoList.get(sel_pos).setIs_selected(false);
            }
            sounds_pojoList.get(i).setIs_selected(true);

            sel_pos = i;
            notifyDataSetChanged();
            if (mPlayer != null) {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                    myViewHolder.ivPlayPause_PlaylistChildLay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_play_arrow));

                }else {
                    mPlayer.start();
                }
            }
                stream_audio(audioUrl, mContext);

        });

        if (sounds_pojo.getIs_selected()) {
            myViewHolder.ivDone_PlaylistChildLay.setVisibility(View.VISIBLE);
            myViewHolder.ivPlayPause_PlaylistChildLay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_pause));
        } else {
            myViewHolder.ivDone_PlaylistChildLay.setVisibility(View.GONE);
            myViewHolder.ivPlayPause_PlaylistChildLay.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_play_arrow));
        }
    }

    private void stream_audio(String audioUrl, Context mContext) {
        try {
            mPlayer = new MediaPlayer();
//            if (mPlayer.isPlaying()) {
//                mPlayer.stop();
//            }
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.setDataSource(audioUrl);
            mPlayer.prepare();
            mPlayer.start();
            mediaPlayerMain = mPlayer;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return sounds_pojoList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvName_PlaylistChildLay)
        TextView tvName_PlaylistChildLay;
        @BindView(R.id.tvDecrip_PlaylistChildLay)
        TextView tvDecrip_PlaylistChildLay;
        @BindView(R.id.tvDuration_PlaylistChildLay)
        TextView tvDuration_PlaylistChildLay;
        @BindView(R.id.ll_PlaylistChildLay)
        RelativeLayout ll_PlaylistChildLay;
        @BindView(R.id.llall)
        LinearLayout llall;
        @BindView(R.id.ivImg_PlaylistChildLay)
        ImageView ivImg_PlaylistChildLay;
        @BindView(R.id.ivDone_PlaylistChildLay)
        TextView ivDone_PlaylistChildLay;
        @BindView(R.id.ivPlayPause_PlaylistChildLay)
        ImageView ivPlayPause_PlaylistChildLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

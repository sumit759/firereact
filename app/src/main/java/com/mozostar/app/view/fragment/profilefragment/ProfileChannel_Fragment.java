package com.mozostar.app.view.fragment.profilefragment;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.ProfileChannelList_pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.activity.channelVideoList.ChannelVideoListActivity;
import com.mozostar.app.view.adapter.ChannelListAdapter;
import com.mozostar.app.view.fragment.guestfragment.GuestDub_Fragment;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileChannel_Fragment extends BaseFragment implements ProfileVideoIView, ChannelListAdapter.OnItemClickListener {
    private static String TAG = GuestDub_Fragment.class.getSimpleName();
    Context context;
    @BindView(R.id.rvChannel_ProfileChaLay)
    RecyclerView rvChannel_ProfileChaLay;
    ChannelListAdapter mChannelListAdapter;
    ProfileVideoPresenter<ProfileChannel_Fragment> presenter = new ProfileVideoPresenter<>();
    ArrayList<String> cover_ArrayList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile_channel_;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        context = getActivity();
        mChannelListAdapter=new ChannelListAdapter(new ArrayList<>());
        rvChannel_ProfileChaLay.setHasFixedSize(true);
        rvChannel_ProfileChaLay.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvChannel_ProfileChaLay.setAdapter(mChannelListAdapter);
        presenter.getChanneList();
        mChannelListAdapter.setOnItemClickListener(this);
        return view;
    }


    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

    }

    @Override
    public void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

    }

    @Override
    public void onSuccessChannel(ProfileChannelList_pojo trendsResponse) {
        Log.e("ProfileChannelList_pojo", "" + trendsResponse.getMessage());
        mChannelListAdapter.ChannelListUpdate(trendsResponse.getData());
    }


    @Override
    public void onItemClick(ProfileChannelList_pojo.Datum myListData) {
        SharedHelper.putKey(context,"id",myListData.getId());
        SharedHelper.putKey(context,"channel_category",myListData.getChannel_category());

        SharedHelper.putKey(context,"channel_video_uplaod",true);
        Intent n = new Intent(context, ChannelVideoListActivity.class);
        n.putExtra("name",myListData.getName());
        startActivity(n);
    }


}

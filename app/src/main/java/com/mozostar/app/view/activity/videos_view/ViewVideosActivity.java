package com.mozostar.app.view.activity.videos_view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.custom_ui.VerticalViewPager;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.ChannelVideoList_Pojo;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.data.model.DeletePojo;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.activity.InteractionListener;
import com.mozostar.app.view.activity.MainActivity;
import com.mozostar.app.view.activity.sound_list.SoundTrack_Activity;
import com.mozostar.app.view.activity.guest.GuestProfile_Activity;
import com.mozostar.app.view.adapter.CommentsAdapter;
import com.mozostar.app.view.adapter.HomeScreen_Adapter;
import com.mozostar.app.view.adapter.HomeScreen_PagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ViewVideosActivity extends BaseActivity implements VideoViewListener, Player.EventListener, AdapterOnClick, ViewVideosIView {


    @BindView(R.id.rvHome_VideiViewLay)
    RecyclerView rvHome_VideiViewLay;

    @BindView(R.id.vvpHome_VideoViewLay)
    VerticalViewPager vvpHome_VideoViewLay;

    @BindView(R.id.pbLoad_VideoViewLay)
    ProgressBar pbLoad_VideoViewLay;

    @BindView(R.id.comments_bottom_sheet)
    LinearLayout comments_bottom_sheet;

    @BindView(R.id.bottom_sheet_share)
    LinearLayout bottom_sheet_share;

    @BindView(R.id.comments_list)
    RecyclerView comments_list;

    @BindView(R.id.post_comment_etx)
    EditText post_comment_etx;

    ProgressBar progress;

    private BottomSheetBehavior commentsBehavior, shareBehaviour;


    Boolean loggedIn;

    private InteractionListener mListener;
    Boolean liked = false;

    int currentPage = -1;
    LinearLayoutManager layoutManager;


    private HomeScreen_PagerAdapter homeScreen_pagerAdapter;
    ViewVideosPresenter<ViewVideosActivity> presenter = new ViewVideosPresenter<>();

    //    List<VideoList_Pojo.Video> list = new ArrayList<>();
    ArrayList<ProfileVideoList_Pojo> list = new ArrayList<>();
    ArrayList<Discover_Pojo.Video_> list_hash = new ArrayList<>();
    ArrayList<String> cover_ArrayList;
    String position, guest_id, from, position_list;

    @Override
    public int getLayoutId() {
        return R.layout.activity_view_videos;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);

        presenter.attachView(this);

        commentsBehavior = BottomSheetBehavior.from(comments_bottom_sheet);
        shareBehaviour = BottomSheetBehavior.from(bottom_sheet_share);


        shareBehaviour.setHideable(true);
        shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
        shareBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        commentsBehavior.setHideable(true);

        commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        commentsBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        // btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        loggedIn = SharedHelper.getBoolKey(ViewVideosActivity.this, "logged_in", false);

        cover_ArrayList = new ArrayList<>();

//        cover_ArrayList = (ArrayList<String>)getIntent().getSerializableExtra("list");


        position = getIntent().getStringExtra("position");
        position_list = getIntent().getStringExtra("position_list");
        guest_id = getIntent().getStringExtra("guest_id");

        from = SharedHelper.getKey(this, "video_view");

        if (!from.isEmpty()) {
            if (from.equals("profile_dub")) {
                presenter.getProfileDubList();
            } else if (from.equals("profile_like")) {
                presenter.getProfileList();
            } else if (from.equals("guest_dub")) {
                presenter.getDubList(guest_id);
            } else if (from.equals("guest_like")) {
                presenter.getList(guest_id);
            }else if(from.equals("channel_video_list")){
                presenter.getChannelList(String.valueOf(SharedHelper.getIntKey(this,"id")));
            }
            else if (from.equals("hash_tag")) {
                presenter.getHashList();
            } else {
                Toast.makeText(this, "Somthing went wrong", Toast.LENGTH_LONG).show();
            }
        }
//        if (!TextUtils.isEmpty(guest_id)) {
//            presenter.getDubList(guest_id);
//        }


    }

    @Override
    public void onClickGuestProfile() {

        ActivityManager.redirectToActivityWithoutBundle(ViewVideosActivity.this, GuestProfile_Activity.class);

    }

    @Override
    public void onClickSoundTrack() {

        ActivityManager.redirectToActivityWithoutBundle(ViewVideosActivity.this, SoundTrack_Activity.class);

    }

    SimpleExoPlayer previous_exoplayer;

    @SuppressLint("SetTextI18n")
    public void setVideoPlayerToPlay(int i) {

        final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this, (TrackSelector) new DefaultTrackSelector());
        newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this, Util.getUserAgent(this, "TikTok")))
                .createMediaSource(Uri.parse(cover_ArrayList.get(i))));
        newSimpleInstance.addListener(this);
        View findViewByPosition = layoutManager.findViewByPosition(i);
        PlayerView playerView = findViewByPosition.findViewById(R.id.player_view);

        TextView like_count_txt = findViewByPosition.findViewById(R.id.like_count_txt);
        comments_count_txt = findViewByPosition.findViewById(R.id.comments_count_txt);
        TextView description_txt = findViewByPosition.findViewById(R.id.description_txt);
        TextView user_txt = findViewByPosition.findViewById(R.id.user_txt);
        TextView sount_track_txt = findViewByPosition.findViewById(R.id.sount_track_txt);
        ImageView like_img = findViewByPosition.findViewById(R.id.like_img);
        ImageView comments_img = findViewByPosition.findViewById(R.id.comments_img);
        ImageView share_img = findViewByPosition.findViewById(R.id.share_img);
        ImageView delete_img = findViewByPosition.findViewById(R.id.delete_img);
        CircleImageView user_img = findViewByPosition.findViewById(R.id.user_img);
        ImageView follow_img = findViewByPosition.findViewById(R.id.follow_img);
        progress = findViewByPosition.findViewById(R.id.progress);

        ImageView ivSoundTrack_HomeFragLay = findViewByPosition.findViewById(R.id.ivSoundTrack_HomeFragLay);
        ivSoundTrack_HomeFragLay.setScaleType(ImageView.ScaleType.FIT_XY);

        Glide.with(this)
                .load(getResources().getDrawable(R.drawable.record))
                .into(ivSoundTrack_HomeFragLay);

        if (from.equals("hash_tag")) {

            if (list_hash.get(i).getUserdetails().getImage() != null)
                Glide.with(this).load("" + list_hash.get(i).getUserdetails().getImage())
                        .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
                                .dontAnimate().error(R.drawable.user_profile)).into(user_img);


            user_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SharedHelper.getIntKey(ViewVideosActivity.this, "user_id") == list_hash.get(i).getUserdetails().getId()) {
                        onBackPressed();
                    } else {
                        Intent guest = new Intent(ViewVideosActivity.this, GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + list_hash.get(i).getUserdetails().getId());
                        startActivity(guest);
                    }
                }
            });

            follow_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progress.setVisibility(View.VISIBLE);
                    follow_img.setVisibility(View.GONE);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("user_id", "" + SharedHelper.getIntKey(ViewVideosActivity.this, "user_id"));
                    map.put("follower_id", list_hash.get(i).getUserdetails().getId());
                    presenter.followup(map);
                }
            });


            if (SharedHelper.getIntKey(ViewVideosActivity.this, "user_id") == list_hash.get(i).getUserId()) {
                follow_img.setVisibility(View.GONE);
                delete_img.setVisibility(View.VISIBLE);

            } else {
                follow_img.setVisibility(View.VISIBLE);
                delete_img.setVisibility(View.GONE);
            }


            delete_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = "" + list_hash.get(i).getId();
                    presenter.delete_video(id);

                }
            });

//            ivSoundTrack_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (loggedIn) {
//                        Intent sound_track = new Intent(ViewVideosActivity.this, SoundTrack_Activity.class);
//                        sound_track.putExtra("audio_id", list_hash.get(i).getAudio());
//                        sound_track.putExtra("category_id", "" + list_hash.get(i).getCategoryId());
//                        startActivity(sound_track);
//                    } else {
//                        mListener.behaviour();
//                    }
//                }
//            });


            like_count_txt.setText("" + list_hash.get(i).getLikesCount());
            comments_count_txt.setText("" + list_hash.get(i).getCommentsCount());
            description_txt.setText("" + list_hash.get(i).getDescription());
            description_txt.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            description_txt.setSelected(true);
            description_txt.setSingleLine(true);

//            if (list_hash.get(i).getLikes()!=null&&list_hash.get(i).getLikes().size()!=0) {
//                liked = false;
//                like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
//            } else {
//                liked = true;
//                like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
//
//            }

            user_txt.setText("@" + "" + list_hash.get(i).getUserdetails().getFirstName() + " " + list_hash.get(i).getUserdetails().getLastName());

            if (list_hash.get(i).getVideosound() != null)
                sount_track_txt.setText("" + list_hash.get(i).getVideosound().getDescription());
            else
                sount_track_txt.setVisibility(View.GONE);

            share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (shareBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        shareBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
                    }

                }
            });

            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!liked) {

                        liked = true;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                        int count = list_hash.get(i).getLikesCount() + 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(ViewVideosActivity.this, "user_id"));
                        map.put("video_id", list_hash.get(i).getId());
                        map.put("status", "1");
                        presenter.like_video(map);
                    } else {
                        liked = false;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                        int count = list_hash.get(i).getLikesCount() - 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(ViewVideosActivity.this, "user_id"));
                        map.put("video_id", list_hash.get(i).getId());
                        map.put("status", "0");
                        presenter.like_video(map);
                    }
                }
            });

            user_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SharedHelper.getIntKey(ViewVideosActivity.this, "user_id") == list_hash.get(i).getUserId()) {
                        onBackPressed();
                    } else {
                        Intent guest = new Intent(ViewVideosActivity.this, GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + list_hash.get(i).getUserdetails().getId());
                        startActivity(guest);
                    }

                }
            });


            comments_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (commentsBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        commentsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        video_id = "" + list_hash.get(i).getId();
                        presenter.getCommentList(video_id);
                    } else {
                        commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    }
                    SharedHelper.putKey(ViewVideosActivity.this, "comment_listener", "video");
//                mListener.comments_list("" + list.get(i).getId(), i);

                }

            });

            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;

        } else if(list != null){


            if (list.get(i).getUserdetails().getImage() != null)
                Glide.with(this).load("" + list.get(i).getUserdetails().getImage())
                        .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
                                .dontAnimate().error(R.drawable.user_profile)).into(user_img);

            if (SharedHelper.getIntKey(ViewVideosActivity.this, "user_id") == list.get(i).getUserId()) {
                follow_img.setVisibility(View.GONE);
                delete_img.setVisibility(View.VISIBLE);

            } else {
                delete_img.setVisibility(View.GONE);
                follow_img.setVisibility(View.VISIBLE);
            }

            delete_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = "" + list.get(i).getId();
                    presenter.delete_video(id);
                }
            });

//            ivSoundTrack_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (loggedIn) {
//                        Intent sound_track = new Intent(ViewVideosActivity.this, SoundTrack_Activity.class);
//                        sound_track.putExtra("audio_id", list.get(i).getAudio());
//                        sound_track.putExtra("category_id", "" + list.get(i).getCategoryId());
//                        startActivity(sound_track);
//                    } else {
//                        mListener.behaviour();
//                    }
//                }
//            });

            like_count_txt.setText("" + list.get(i).getLikesCount());
            comments_count_txt.setText("" + list.get(i).getCommentsCount());
            description_txt.setText("" + list.get(i).getDescription());
            description_txt.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            description_txt.setSelected(true);
            description_txt.setSingleLine(true);

            if (list.get(i).getLikes() != null && list.get(i).getLikes().size() != 0) {
                liked = false;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
            } else {
                liked = true;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));

            }

            user_txt.setText("@" + "" + list.get(i).getUserdetails().getFirstName() + " " + list.get(i).getUserdetails().getLastName());

            if (list.get(i).getVideosound() != null)
                sount_track_txt.setText("" + list.get(i).getVideosound().getDescription());
            else
                sount_track_txt.setVisibility(View.GONE);

            share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (shareBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        shareBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
                    }

                }
            });

            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!liked) {

                        liked = true;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                        int count = list.get(i).getLikesCount() + 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(ViewVideosActivity.this, "user_id"));
                        map.put("video_id", list.get(i).getId());
                        map.put("status", "1");
                        presenter.like_video(map);
                    } else {
                        liked = false;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                        int count = list.get(i).getLikesCount() - 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(ViewVideosActivity.this, "user_id"));
                        map.put("video_id", list.get(i).getId());
                        map.put("status", "0");
                        presenter.like_video(map);
                    }
                }
            });

            user_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SharedHelper.getIntKey(ViewVideosActivity.this, "user_id") == list.get(i).getUserId()) {
                        onBackPressed();
                    } else {
                        Intent guest = new Intent(ViewVideosActivity.this, GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + list.get(i).getUserdetails().getId());
                        startActivity(guest);
                    }
                }
            });

            user_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (SharedHelper.getIntKey(ViewVideosActivity.this, "user_id") == list.get(i).getUserId()) {
                        onBackPressed();
                    } else {
                        Intent guest = new Intent(ViewVideosActivity.this, GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + list.get(i).getUserdetails().getId());
                        startActivity(guest);
                    }

                }
            });


            comments_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (commentsBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        commentsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        video_id = "" + list.get(i).getId();
                        presenter.getCommentList(video_id);
                    } else {
                        commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    }
                    SharedHelper.putKey(ViewVideosActivity.this, "comment_listener", "video");
//                mListener.comments_list("" + list.get(i).getId(), i);

                }

            });

            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;
        }
    }


    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == 2) {
            this.pbLoad_VideoViewLay.setVisibility(View.VISIBLE);
        } else if (playbackState == 3) {
            this.pbLoad_VideoViewLay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void onResume() {
        super.onResume();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(true);
        }
    }

    public void stopPreviousVideoPlayer() {
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.removeListener(this);
            this.previous_exoplayer.release();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.release();
        }
    }


    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

    }

    String video_id, user_id;

    @OnClick({R.id.close_cmnt_sheet, R.id.ivClose_share, R.id.post_comment_img})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.close_cmnt_sheet) {
            if (commentsBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                commentsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        } else if (view.getId() == R.id.ivClose_share) {
            if (shareBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                shareBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                shareBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        } else if (view.getId() == R.id.post_comment_img) {
            String comments = post_comment_etx.getText().toString();
            user_id = "" + SharedHelper.getIntKey(ViewVideosActivity.this, "user_id");

            if (!TextUtils.isEmpty(comments)) {
                post_comment_etx.getText().clear();
                HashMap<String, Object> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("video_id", video_id);
                map.put("comment", comments);
                presenter.post_comment(map);
            } else {
                Toast.makeText(ViewVideosActivity.this, "Please write a comment and try to post it", Toast.LENGTH_LONG).show();
            }


        }
    }

    @Override
    public void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

        list = trendsResponse;
        int size = trendsResponse.size();
        for (int i = 0; i < size; i++) {
            cover_ArrayList.add(trendsResponse.get(i).getVideo());
        }
        if (cover_ArrayList.size() != 0) {
            layoutManager = new LinearLayoutManager(ViewVideosActivity.this, LinearLayoutManager.VERTICAL, false);
            rvHome_VideiViewLay.setLayoutManager(layoutManager);
            rvHome_VideiViewLay.setHasFixedSize(false);
            new PagerSnapHelper().attachToRecyclerView(rvHome_VideiViewLay);
            HomeScreen_Adapter homeScreen_adapter = new HomeScreen_Adapter(ViewVideosActivity.this, this, cover_ArrayList);
            rvHome_VideiViewLay.setAdapter(homeScreen_adapter);

            if (cover_ArrayList != null && cover_ArrayList.size() != 0) {
                vvpHome_VideoViewLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    public void onPageScrollStateChanged(int newState) {

                    }

                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        View view = vvpHome_VideoViewLay.getChildAt(position);
                        PlayerView playerView = view.findViewById(R.id.player_view);

                        stopPreviousVideoPlayer();

                    }


                    public void onPageSelected(int position) {
                        //Check if this is the page you want.
                    }
                });

                rvHome_VideiViewLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                        super.onScrollStateChanged(recyclerView, i);
                    }

                    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                        super.onScrolled(recyclerView, i, i2);
                        int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                        if (computeVerticalScrollOffset != currentPage) {
                            currentPage = computeVerticalScrollOffset;
                            stopPreviousVideoPlayer();
                            setVideoPlayerToPlay(currentPage);
                        }
                    }
                });

                if (!TextUtils.isEmpty(position)) {
                    layoutManager.scrollToPosition(Integer.parseInt(position));
                }


            }
        }

    }

    @Override
    public void onSuccessLike(Object trendsResponse) {

    }

    @Override
    public void onSuccessComment(CommentPost_Pojo trendsResponse) {

        Toast.makeText(ViewVideosActivity.this, "Comment posted successfully", Toast.LENGTH_LONG).show();
        int place = Integer.parseInt(position);

        String mCount = "" + comments_count_txt.getText().toString();
        int count = Integer.parseInt(mCount) + 1;
        comments_count_txt.setText("" + count);

        list.get(place).setCommentsCount(count);
//

        if (commentsBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            commentsBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            commentsBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

    }

    @Override
    public void onSuccessComments(CommentsAdapter adapter) {
        comments_list.setLayoutManager(new LinearLayoutManager(ViewVideosActivity.this, LinearLayoutManager.VERTICAL, false));
        comments_list.setAdapter(adapter);
    }

    @Override
    public void onSuccessHash(Discover_Pojo trendsResponse) {
        list_hash = (ArrayList<Discover_Pojo.Video_>) trendsResponse.getVideos().get(Integer.parseInt(position)).getVideos();
        int size = list_hash.size();
        for (int i = 0; i < size; i++) {
            cover_ArrayList.add(trendsResponse.getVideos().get(Integer.parseInt(position)).getVideos().get(i).getVideo());
        }
        if (cover_ArrayList.size() != 0) {
            layoutManager = new LinearLayoutManager(ViewVideosActivity.this, LinearLayoutManager.VERTICAL, false);
            rvHome_VideiViewLay.setLayoutManager(layoutManager);
            rvHome_VideiViewLay.setHasFixedSize(false);
            new PagerSnapHelper().attachToRecyclerView(rvHome_VideiViewLay);
            HomeScreen_Adapter homeScreen_adapter = new HomeScreen_Adapter(ViewVideosActivity.this, this, cover_ArrayList);
            rvHome_VideiViewLay.setAdapter(homeScreen_adapter);

            if (cover_ArrayList != null && cover_ArrayList.size() != 0) {
                vvpHome_VideoViewLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    public void onPageScrollStateChanged(int newState) {

                    }

                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        View view = vvpHome_VideoViewLay.getChildAt(position);
                        PlayerView playerView = view.findViewById(R.id.player_view);

                        stopPreviousVideoPlayer();

                    }


                    public void onPageSelected(int position) {
                        // Check if this is the page you want.
                    }


                });

                rvHome_VideiViewLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                        super.onScrollStateChanged(recyclerView, i);
                    }

                    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                        super.onScrolled(recyclerView, i, i2);
                        int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                        if (computeVerticalScrollOffset != currentPage) {
                            currentPage = computeVerticalScrollOffset;
                            stopPreviousVideoPlayer();
                            setVideoPlayerToPlay(currentPage);
                        }
                    }
                });

                if (!TextUtils.isEmpty(position_list)) {

                    layoutManager.scrollToPosition(Integer.parseInt(position_list));

                }
            }
        }
    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {

        progress.setVisibility(View.GONE);

    }

    @Override
    public void onSuccessDelete(DeletePojo trendsResponse) {
        Toast.makeText(this, trendsResponse.getMessage(), Toast.LENGTH_LONG);
        Intent intent = new Intent(ViewVideosActivity.this, MainActivity.class);
        intent.putExtra("frag", "profile");
        startActivity(intent);
        finish();

    }

    @Override
    public void onSuccessChannelVideoList(ChannelVideoList_Pojo trendsResponse) {

        for(int i= 0; i<trendsResponse.getVideos().size(); i++){
            ProfileVideoList_Pojo profileVideoList_pojo = new ProfileVideoList_Pojo();

            profileVideoList_pojo.setAudio(trendsResponse.getVideos().get(i).getAudio());
            profileVideoList_pojo.setCategoryId(0);



        }

//        list = trendsResponse.getVideos();
        list = null;
        int size = trendsResponse.getVideos().size();
        for (int i = 0; i < size; i++) {
            cover_ArrayList.add(trendsResponse.getVideos().get(i).getVideo());
        }
        if (cover_ArrayList.size() != 0) {
            layoutManager = new LinearLayoutManager(ViewVideosActivity.this, LinearLayoutManager.VERTICAL, false);
            rvHome_VideiViewLay.setLayoutManager(layoutManager);
            rvHome_VideiViewLay.setHasFixedSize(false);
            new PagerSnapHelper().attachToRecyclerView(rvHome_VideiViewLay);
            HomeScreen_Adapter homeScreen_adapter = new HomeScreen_Adapter(ViewVideosActivity.this, this, cover_ArrayList);
            rvHome_VideiViewLay.setAdapter(homeScreen_adapter);

            if (cover_ArrayList != null && cover_ArrayList.size() != 0) {
                vvpHome_VideoViewLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    public void onPageScrollStateChanged(int newState) {

                    }

                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        View view = vvpHome_VideoViewLay.getChildAt(position);
                        PlayerView playerView = view.findViewById(R.id.player_view);

                        stopPreviousVideoPlayer();

                    }


                    public void onPageSelected(int position) {
                        //Check if this is the page you want.
                    }
                });

                rvHome_VideiViewLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                        super.onScrollStateChanged(recyclerView, i);
                    }

                    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                        super.onScrolled(recyclerView, i, i2);
                        int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                        if (computeVerticalScrollOffset != currentPage) {
                            currentPage = computeVerticalScrollOffset;
                            stopPreviousVideoPlayer();
                            setVideoPlayerToPlay(currentPage);
                        }
                    }
                });

                if (!TextUtils.isEmpty(position)) {
                    layoutManager.scrollToPosition(Integer.parseInt(position));
                }


            }
        }


    }


    @Override
    public void comments_list(String i) {

    }
}

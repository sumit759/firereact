package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.mozostar.app.R;
import com.mozostar.app.view.fragment.homedashboard.Home_Fragment;
import com.mozostar.app.view.fragment.homedashboard.PagerAdapterOnClick;
import com.mozostar.app.view.fragment.homedashboard.Video_View_Fragment;

import java.util.ArrayList;


public class BigVideoNewPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> slidercarlist_arraylist;
    PagerAdapterOnClick pagerAdapterClicK;

    public BigVideoNewPagerAdapter(Context context, ArrayList<String> list_arraylist) {
        this.mContext = context;
        this.slidercarlist_arraylist = list_arraylist;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BigVideoNewPagerAdapter(Context context, Video_View_Fragment video_view_fragment, ArrayList<String> mVideoURLList) {
        this.mContext = context;
        this.slidercarlist_arraylist = mVideoURLList;
        this.pagerAdapterClicK = video_view_fragment;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return slidercarlist_arraylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ConstraintLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.big_video_row_item, container, false);
        notifyDataSetChanged();
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout) object);
    }
}

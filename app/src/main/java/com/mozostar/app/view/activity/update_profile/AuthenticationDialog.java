package com.mozostar.app.view.activity.update_profile;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;

import com.mozostar.app.R;

public class AuthenticationDialog extends Dialog {
    private final String request_url;
    private final String redirect_url;
    private AuthenticationListener listener;

    public AuthenticationDialog(@NonNull Context context, AuthenticationListener listener) {
        super(context);
        this.listener = listener;
        this.redirect_url = context.getResources().getString(R.string.redirect_url);
//        this.request_url = context.getResources().getString(R.string.base_url) +
//                "oauth/authorize/?client_id=" +
//                context.getResources().getString(R.string.client_id) +
//                "&redirect_uri=" + redirect_url +
//                "&response_type=token&display=touch&scope=public_content";
        //this.request_url="https://api.instagram.com/oauth/authorize/?client_id=211d39be80329ea63d1adb19fe07d413&redirect_uri=https://www.goappx.com&response_type=code";
        //this.request_url="https://api.instagram.com/oauth/authorize/?app_id=347081802737370&redirect_uri=https://www.goappx.com&scope=user_profile,user_media&response_type=code";
        this.request_url = "https://www.instagram.com/oauth/authorize/?app_id=" + context.getResources().getString(R.string.app_id)
                + "&redirect_uri=" + context.getResources().getString(R.string.redirect_url)
                + "&scope=user_profile,user_media&response_type=code";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.auth_dialog);
        initializeWebView();
    }

    private void initializeWebView() {
        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(request_url);
        webView.setWebViewClient(webViewClient);
    }

    WebViewClient webViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(redirect_url)) {
                if (url.contains("code=")) {
                    Uri uri = Uri.EMPTY.parse(url);
                    String access_token = uri.getQueryParameter("code");
                    access_token = access_token.substring(access_token.lastIndexOf("=") + 1);
                    Log.e("access_token", access_token);
                    listener.onTokenReceived(access_token);
                    AuthenticationDialog.this.dismiss();
                }else {
                    AuthenticationDialog.this.dismiss();
                }
                return true;
            }
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.contains("code=")) {
                Uri uri = Uri.EMPTY.parse(url);
                String access_token = uri.getEncodedFragment();
                access_token = access_token.substring(access_token.lastIndexOf("=") + 1);
                Log.e("access_token", access_token);
                listener.onTokenReceived(access_token);
                dismiss();
            } else if (url.contains("?error")) {
                Log.e("access_token", "getting error fetching access token");
                dismiss();
            }
        }
    };
}

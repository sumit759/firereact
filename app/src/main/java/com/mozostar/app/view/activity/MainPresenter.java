package com.mozostar.app.view.activity;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.CommentPost_Pojo;
import com.mozostar.app.data.model.CommentsList_Pojo;
import com.mozostar.app.data.network.APIClient;
import com.mozostar.app.view.adapter.CommentsAdapter;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter<V extends MainIView> extends BasePresenter<V> implements MainIPresenter<V> {

    @SuppressLint("CheckResult")
    @Override
    public void getList(String id) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().comments_list(id);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer() {
                               @Override
                               public void accept(Object trendsResponse) throws Exception {
//                                   dialog.dismiss();
                                   List<CommentsList_Pojo> pastDelivery_pojo = (List<CommentsList_Pojo>) trendsResponse;
                                   CommentsAdapter adapter = new CommentsAdapter(getMvpView().activity(), pastDelivery_pojo);
                                   getMvpView().onSuccess(adapter);
                               }
                           },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void post_comment(HashMap<String, Object> obj) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().post_comment(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessComment((CommentPost_Pojo) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }

    public void getListVedio(String id) {

    }
}

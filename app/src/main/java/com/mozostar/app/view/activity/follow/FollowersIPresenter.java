package com.mozostar.app.view.activity.follow;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface FollowersIPresenter <V extends FollowersIView> extends MvpPresenter<V> {
    @SuppressLint("CheckResult")
    void getList();

    @SuppressLint("CheckResult")
    void getFollowersList();

    @SuppressLint("CheckResult")
    void getGuestList(String id);

    @SuppressLint("CheckResult")
    void getGuestFollowersList(String id);

    @SuppressLint("CheckResult")
    void followup(HashMap<String, Object> obj);
}

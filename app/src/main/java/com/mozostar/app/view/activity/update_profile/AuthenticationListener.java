package com.mozostar.app.view.activity.update_profile;

public interface AuthenticationListener {
    void onTokenReceived(String auth_token);

}

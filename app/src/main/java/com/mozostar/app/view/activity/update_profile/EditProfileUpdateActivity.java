package com.mozostar.app.view.activity.update_profile;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Profile_Pojo;
import com.mozostar.app.data.model.UpdateProfile_Pojo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditProfileUpdateActivity extends BaseActivity implements EditProfileIView, AuthenticationListener {

    @BindView(R.id.cancel_action_tv)
    TextView cancel_action_tv;

    @BindView(R.id.save_action_tv)
    TextView save_action_tv;

    @BindView(R.id.header_name_tv)
    TextView header_name_tv;

    /*FirstName*/
    @BindView(R.id.name_edit_layout_ll)
    LinearLayout name_edit_layout_ll;

    @BindView(R.id.update_profile_field_et)
    AppCompatEditText update_profile_field_et;

    @BindView(R.id.ivClearprofileText)
    ImageView ivClearprofileText;

    @BindView(R.id.length_profile_tv)
    TextView length_profile_tv;

    /*LastName*/
    @BindView(R.id.last_name_edit_layout_ll)
    LinearLayout last_name_edit_layout_ll;

    @BindView(R.id.update_last_name_profile_field_et)
    AppCompatEditText update_last_name_profile_field_et;

    @BindView(R.id.ivClearLast_name)
    ImageView ivClearLast_name;

    @BindView(R.id.last_name_length_profile_tv)
    TextView last_name_length_profile_tv;

    /*Bio */

    @BindView(R.id.bio_edit_layout_ll)
    LinearLayout bio_edit_layout_ll;

    @BindView(R.id.bio_update_profile_field_et)
    AppCompatEditText bio_update_profile_field_et;

    @BindView(R.id.bio_ivClearprofileText)
    ImageView bio_ivClearprofileText;

    @BindView(R.id.bio_length_profile_tv)
    TextView bio_length_profile_tv;


    /*Insta*/
    @BindView(R.id.insta_youtube_edit_ll)
    LinearLayout insta_youtube_edit_ll;

    @BindView(R.id.insta_youtube_profile_field_et)
    AppCompatEditText insta_youtube_profile_field_et;

    @BindView(R.id.insta_youtube_clearTV)
    ImageView insta_youtube_clearTV;

    @BindView(R.id.insta_youtube_length_tv)
    TextView insta_youtube_length_tv;

    /*Mobile*/
    @BindView(R.id.mobile_edit_ll)
    LinearLayout mobile_edit_ll;

    @BindView(R.id.mobile_profile_field_et)
    AppCompatEditText mobile_profile_field_et;

    @BindView(R.id.mobile_clearTV)
    ImageView mobile_clearTV;

    @BindView(R.id.mobile_length_tv)
    TextView mobile_length_tv;

    /*Email*/
    @BindView(R.id.email_edit_ll)
    LinearLayout email_edit_ll;

    @BindView(R.id.email_profile_field_et)
    AppCompatEditText email_profile_field_et;

    @BindView(R.id.email_clearTV)
    ImageView email_clearTV;

    @BindView(R.id.email_length_tv)
    TextView email_length_tv;

    private EditProfilePresenter<EditProfileUpdateActivity> presenter = new EditProfilePresenter<>();

    String comeFrom;
    String firstNameStr, lastNameStr, bioStr, mobileStr, emailsStr, instagramStr = "";
    File imgFile = null;
    MultipartBody.Part filePart = null;

    @Override
    public int getLayoutId() {
        return R.layout.activity_edit_profile_update;
    }

    Context context;

    @Override
    public void initView() {
        ButterKnife.bind(this);
        presenter.attachView(this);
        context = EditProfileUpdateActivity.this;
        try {
            comeFrom = getIntent().getStringExtra("from");
            imgFile = (File) getIntent().getSerializableExtra("imgFile");
            if (imgFile != null) {
                filePart = MultipartBody.Part.createFormData("image", imgFile.getName(), RequestBody.create(MediaType.parse("image*//*"), imgFile));
            }
            if (comeFrom.equalsIgnoreCase("first_name")) {

                header_name_tv.setText("First Name");
                name_edit_layout_ll.setVisibility(View.VISIBLE);
                last_name_edit_layout_ll.setVisibility(View.GONE);
                bio_edit_layout_ll.setVisibility(View.GONE);
                insta_youtube_edit_ll.setVisibility(View.GONE);
                email_edit_ll.setVisibility(View.GONE);
                mobile_edit_ll.setVisibility(View.GONE);

                firstNameStr = getIntent().getStringExtra("firstNameStr");
                lastNameStr = getIntent().getStringExtra("lastNameStr");
                update_profile_field_et.setText(firstNameStr);
                length_profile_tv.setText("" + update_profile_field_et.length() + "/20");

            } else if (comeFrom.equalsIgnoreCase("last_name")) {

                header_name_tv.setText("Last Name");
                name_edit_layout_ll.setVisibility(View.GONE);
                last_name_edit_layout_ll.setVisibility(View.VISIBLE);
                bio_edit_layout_ll.setVisibility(View.GONE);
                insta_youtube_edit_ll.setVisibility(View.GONE);
                email_edit_ll.setVisibility(View.GONE);
                mobile_edit_ll.setVisibility(View.GONE);

                firstNameStr = getIntent().getStringExtra("firstNameStr");
                lastNameStr = getIntent().getStringExtra("lastNameStr");
                update_last_name_profile_field_et.setText(lastNameStr);
                last_name_length_profile_tv.setText("" + update_last_name_profile_field_et.length() + "/20");

            } else if (comeFrom.equalsIgnoreCase("bio")) {

                header_name_tv.setText("Bio");
                name_edit_layout_ll.setVisibility(View.GONE);
                insta_youtube_edit_ll.setVisibility(View.GONE);
                last_name_edit_layout_ll.setVisibility(View.GONE);
                bio_edit_layout_ll.setVisibility(View.VISIBLE);
                email_edit_ll.setVisibility(View.GONE);
                mobile_edit_ll.setVisibility(View.GONE);

                firstNameStr = getIntent().getStringExtra("firstNameStr");
                lastNameStr = getIntent().getStringExtra("lastNameStr");
                bioStr = getIntent().getStringExtra("bioStr");
                bio_update_profile_field_et.setText(bioStr);
                bio_length_profile_tv.setText("" + bio_update_profile_field_et.length() + "/50");


            } else if (comeFrom.equalsIgnoreCase("mobile")) {
                header_name_tv.setText("Mobile");
                name_edit_layout_ll.setVisibility(View.GONE);
                last_name_edit_layout_ll.setVisibility(View.GONE);
                insta_youtube_edit_ll.setVisibility(View.GONE);
                bio_edit_layout_ll.setVisibility(View.GONE);
                email_edit_ll.setVisibility(View.GONE);
                mobile_edit_ll.setVisibility(View.VISIBLE);

                firstNameStr = getIntent().getStringExtra("firstNameStr");
                lastNameStr = getIntent().getStringExtra("lastNameStr");
                mobileStr = getIntent().getStringExtra("mobileStr");
                mobile_profile_field_et.setText(mobileStr);
                mobile_length_tv.setText("" + mobile_profile_field_et.length() + "/10");

            } else if (comeFrom.equalsIgnoreCase("email")) {
                header_name_tv.setText("Email");
                name_edit_layout_ll.setVisibility(View.GONE);
                last_name_edit_layout_ll.setVisibility(View.GONE);
                bio_edit_layout_ll.setVisibility(View.GONE);
                insta_youtube_edit_ll.setVisibility(View.GONE);
                mobile_edit_ll.setVisibility(View.GONE);
                email_edit_ll.setVisibility(View.VISIBLE);

                firstNameStr = getIntent().getStringExtra("firstNameStr");
                lastNameStr = getIntent().getStringExtra("lastNameStr");
                emailsStr = getIntent().getStringExtra("emailStr");
                email_profile_field_et.setText(emailsStr);
                email_length_tv.setText("" + email_profile_field_et.length() + "/50");

            } else if (comeFrom.equalsIgnoreCase("instagram")) {
                header_name_tv.setText("Instagram");
                name_edit_layout_ll.setVisibility(View.GONE);
                last_name_edit_layout_ll.setVisibility(View.GONE);
                bio_edit_layout_ll.setVisibility(View.GONE);
                email_edit_ll.setVisibility(View.GONE);
                mobile_edit_ll.setVisibility(View.GONE);
                insta_youtube_edit_ll.setVisibility(View.VISIBLE);

                firstNameStr = getIntent().getStringExtra("firstNameStr");
                lastNameStr = getIntent().getStringExtra("lastNameStr");
                instagramStr = getIntent().getStringExtra("instagramStr");
                insta_youtube_profile_field_et.setText(instagramStr);
                insta_youtube_length_tv.setText("" + insta_youtube_profile_field_et.length() + "/50");
            }
            try {
                initialView();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.cancel_action_tv)
    public void cancelBtnClick() {
        startActivity(new Intent(EditProfileUpdateActivity.this, EditProfileActivity.class));
        overridePendingTransition(0, 0);
        finish();
    }

    @OnClick(R.id.save_action_tv)
    public void saveActionClick() {
        validateFields();
    }

    private void validateFields() {
        HashMap<String, RequestBody> map = new HashMap<>();
        if (comeFrom.equalsIgnoreCase("first_name")) {
            firstNameStr = update_profile_field_et.getText().toString();
            if (!TextUtils.isEmpty(firstNameStr)) {
                map.put("first_name", toRequestBody(firstNameStr));
                map.put("last_name", toRequestBody(lastNameStr));
                callUpdate(map);
            } else {
                Toast.makeText(EditProfileUpdateActivity.this, "First name is Empty", Toast.LENGTH_LONG).show();
            }
        }
        if (comeFrom.equalsIgnoreCase("last_name")) {
            lastNameStr = update_last_name_profile_field_et.getText().toString();
            if (!TextUtils.isEmpty(lastNameStr)) {
                map.put("first_name", toRequestBody(firstNameStr));
                map.put("last_name", toRequestBody(lastNameStr));
                callUpdate(map);
            } else {
                Toast.makeText(EditProfileUpdateActivity.this, "Last name is Empty", Toast.LENGTH_LONG).show();
            }
        } else if (comeFrom.equalsIgnoreCase("bio")) {
            bioStr = bio_update_profile_field_et.getText().toString();
            if (!TextUtils.isEmpty(bioStr)) {
                map.put("first_name", toRequestBody(firstNameStr));
                map.put("last_name", toRequestBody(lastNameStr));
                map.put("user_bio", toRequestBody(bioStr));
                callUpdate(map);
            } else {
                Toast.makeText(EditProfileUpdateActivity.this, "Bio is Empty", Toast.LENGTH_LONG).show();
            }
        } else if (comeFrom.equalsIgnoreCase("mobile")) {
            mobileStr = mobile_profile_field_et.getText().toString();
            if (!TextUtils.isEmpty(mobileStr)) {
                map.put("first_name", toRequestBody(firstNameStr));
                map.put("last_name", toRequestBody(lastNameStr));
                map.put("mobile", toRequestBody(mobileStr));
                callUpdate(map);
            } else {
                Toast.makeText(EditProfileUpdateActivity.this, "Mobile is Empty", Toast.LENGTH_LONG).show();
            }

        } else if (comeFrom.equalsIgnoreCase("email")) {
            emailsStr = email_profile_field_et.getText().toString();
            if (!TextUtils.isEmpty(emailsStr)) {
                map.put("first_name", toRequestBody(firstNameStr));
                map.put("last_name", toRequestBody(lastNameStr));
                map.put("email", toRequestBody(emailsStr));
                callUpdate(map);
            } else {
                Toast.makeText(EditProfileUpdateActivity.this, "Email is Empty", Toast.LENGTH_LONG).show();
            }

        } else if (comeFrom.equalsIgnoreCase("instagram")) {
            instagramStr = insta_youtube_profile_field_et.getText().toString();
            if (!TextUtils.isEmpty(instagramStr)) {
                if (!Patterns.WEB_URL.matcher(instagramStr).matches()) {
                    Toast.makeText(EditProfileUpdateActivity.this, "You not entered a link", Toast.LENGTH_LONG).show();
                } else {
                    map.put("first_name", toRequestBody(firstNameStr));
                    map.put("last_name", toRequestBody(lastNameStr));
                    map.put("instagram_link", toRequestBody(instagramStr));
                    callUpdate(map);
                }
            } else {
                Toast.makeText(EditProfileUpdateActivity.this, "Instagram link is Empty", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void callUpdate(HashMap<String, RequestBody> map) {
        try {
            presenter.updateProfile(map, filePart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(EditProfileUpdateActivity.this, EditProfileActivity.class));
        overridePendingTransition(0, 0);
        finish();
    }

    public static RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    private void initialView() {

        update_profile_field_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                length_profile_tv.setText(s.toString().length() + "/20");  // "" + count + "/20"
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ivClearprofileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_profile_field_et.setText("");
            }
        });

        update_last_name_profile_field_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                last_name_length_profile_tv.setText(s.toString().length() + "/20"); // "" + count + "/20"
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ivClearLast_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update_last_name_profile_field_et.setText("");
            }
        });

        bio_update_profile_field_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bio_length_profile_tv.setText(s.toString().length() + "/50");  // "" + count + "/50"
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        bio_ivClearprofileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bio_update_profile_field_et.setText("");
            }
        });

        email_profile_field_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                email_length_tv.setText(s.toString().length() + "/50");  // "" + count + "/50"
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        email_clearTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_profile_field_et.setText("");
            }
        });

        mobile_profile_field_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mobile_length_tv.setText(s.toString().length() + "/10" );  // "" + count + "/10"
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mobile_clearTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobile_profile_field_et.setText("");
            }
        });

        insta_youtube_profile_field_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                insta_youtube_length_tv.setText(s.toString().length() + "/50");   // "" + count + "/50"
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        insta_youtube_clearTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insta_youtube_profile_field_et.setText("");
            }
        });

    }


    @Override
    public void onTokenReceived(String auth_token) {
        Log.d("auth_token", auth_token);
        SharedHelper.putKey(EditProfileUpdateActivity.this, "insta_access_token", auth_token);
    }

    @Override
    public void onSuccessRegister(UpdateProfile_Pojo trendsResponse) {
        Toast.makeText(EditProfileUpdateActivity.this, "Successfully updated", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccessProfile(ArrayList<Profile_Pojo> trendsResponse) {

    }
}
package com.mozostar.app.view.activity.settings;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SettingsPresenter<V extends SettingsIView> extends BasePresenter<V> implements SettingsIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void upgrade(HashMap<String, Object> obj) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().activateBusiness(obj);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessUpgrade((Follow_Pojo) trendsResponse );
                        },
                        throwable -> {
                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }
}

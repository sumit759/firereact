package com.mozostar.app.view.activity;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;


public interface MainIPresenter <V extends MainIView> extends MvpPresenter<V> {
    void getList(String id);

    @SuppressLint("CheckResult")
    void post_comment(HashMap<String, Object> obj);
}

package com.mozostar.app.view.fragment.discover;

import android.annotation.SuppressLint;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.ExploreChannelVideoResponse;
import com.mozostar.app.data.model.User_Pojo;
import com.mozostar.app.data.network.APIClient;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DiscoverPresenter<V extends DiscoverIView> extends BasePresenter<V> implements DiscoverIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void getList() {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().trending_list();
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccess((Discover_Pojo) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    @Override
    public void getUserList(String user) {
//        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
//        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().search_user(user);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
//                            dialog.dismiss();
                            getMvpView().onSuccessUser((ArrayList<User_Pojo>) trendsResponse);
                        },
                        throwable -> {
//                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });

    }

    @SuppressLint("CheckResult")
    public void getExploreVideoList() {
        Observable modelObservable = APIClient.getAPIClient().exploreChannelVideo();
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(exploreVideoList -> {
                            getMvpView().onSuccessExploreVideoList((ExploreChannelVideoResponse) exploreVideoList);
                        },
                        throwable -> {
                            getMvpView().onError((Throwable) throwable);
                        });
    }

}

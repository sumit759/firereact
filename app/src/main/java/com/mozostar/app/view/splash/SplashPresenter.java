package com.mozostar.app.view.splash;

import android.os.Handler;

import com.mozostar.app.base.BasePresenter;

public class SplashPresenter <V extends SplashIView> extends BasePresenter<V> implements SplashIPresenter<V> {

    @Override
    public void handlerCall() {
        new Handler().postDelayed(() -> getMvpView().redirectHome(), 5000); //Timer is in ms here.
    }
}

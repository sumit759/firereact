package com.mozostar.app.view.activity.settings;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface SettingsIPresenter<V extends SettingsIView> extends MvpPresenter<V> {

    @SuppressLint("CheckResult")
    void upgrade(HashMap<String, Object> obj);
}

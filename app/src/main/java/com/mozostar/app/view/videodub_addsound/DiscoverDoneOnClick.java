package com.mozostar.app.view.videodub_addsound;

import com.mozostar.app.data.model.Sounds_Pojo;

public interface DiscoverDoneOnClick {
    void onClickAddSoundDone(Sounds_Pojo sounds_pojo);
}

package com.mozostar.app.view.fragment.homedashboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.custom_ui.VerticalViewPager;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.data.model.VideoList_Pojo1;
import com.mozostar.app.data.model.VideoReportResponse;
import com.mozostar.app.view.activity.InteractionListener;
import com.mozostar.app.view.activity.guest.GuestProfile_Activity;
import com.mozostar.app.view.activity.sound_list.SoundTrack_Activity;
import com.mozostar.app.view.adapter.HomeScreen_Adapter;
import com.mozostar.app.view.adapter.HomeScreen_PagerAdapter;
import com.mozostar.app.base.BaseActivity;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class Video_View_Fragment extends BaseFragment implements Listener, PagerAdapterOnClick, EventListener, Player.EventListener, HomeIView {

    private Context context;

    private static String TAG = Video_View_Fragment.class.getSimpleName();

    @BindView(R.id.rvHome_HomeFragLay)
    RecyclerView rvHome_HomeFragLay;
    @BindView(R.id.vvpHome_HomeFragLay)
    VerticalViewPager vvpHome_HomeFragLay;
    @BindView(R.id.pbLoad_HomeFragLay)
    ProgressBar pbLoad_HomeFragLay;
    @BindView(R.id.no_data_lay)
    LinearLayout no_data_lay;

    private ProgressBar progress;
    private String from;
    private String position;

    private InteractionListener mListener;
    private Boolean liked = false;

    private int currentPage = -1;
    private LinearLayoutManager layoutManager;

    private HomePresenter<Video_View_Fragment> presenter = new HomePresenter<>();
    private final int PERMISSION_CONSTANT = 1000;

    private HomeScreen_PagerAdapter homeScreen_pagerAdapter;
    private ArrayList<String> mVideoURLList = new ArrayList<>();

    private List<VideoList_Pojo1.Data> mVideoDataList = new ArrayList<>();
    private List<ProfileVideoList_Pojo> list_follow = new ArrayList<>();
    private String user_id = "";

    private Boolean loggedIn;

    ArrayList<ProfileVideoList_Pojo> list = new ArrayList<>();
    ArrayList<Discover_Pojo.Video_> list_hash = new ArrayList<>();
    ArrayList<String> cover_ArrayList;
    String  guest_id,  position_list;

    private int mNextPage = 0;

    @Override
    public int getLayoutId() {
        return R.layout.home_frag;
    }

    @Override
    public View initView(View view) {

        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);


        rvHome_HomeFragLay.setOnFlingListener(null);
        user_id = "" + SharedHelper.getIntKey(getContext(), "user_id");

        loggedIn = SharedHelper.getBoolKey(getContext(), "logged_in", false);


        Bundle arguments = getArguments();
        from = arguments.getString("from");
        position = arguments.getString("position");
        position_list = arguments.getString("position_list");
        guest_id = arguments.getString("guest_id");

        if (!TextUtils.isEmpty(from)) {
            if (from.equals("home")) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("user_id", user_id);
                map.put("page", startPagesCount);
                presenter.getList(map);
            } else if (from.equals("following")) {

                if (loggedIn) {
                    presenter.getFollowList();
                } else {
                    no_data_lay.setVisibility(View.VISIBLE);
                    rvHome_HomeFragLay.setVisibility(View.GONE);
                    this.pbLoad_HomeFragLay.setVisibility(View.GONE);
                    mListener.behaviour();
                }
            }
        }

        // Changed by thiru

        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvHome_HomeFragLay.setLayoutManager(layoutManager);
        rvHome_HomeFragLay.setHasFixedSize(false);
        new PagerSnapHelper().attachToRecyclerView(rvHome_HomeFragLay);
        Log.i("onSuccess", "" + mVideoURLList.size());
        homeScreen_adapter = new HomeScreen_Adapter(context, this, mVideoURLList);
        rvHome_HomeFragLay.setAdapter(homeScreen_adapter);

        homeScreen_pagerAdapter = new HomeScreen_PagerAdapter(context, this, mVideoURLList);
        vvpHome_HomeFragLay.setAdapter(homeScreen_pagerAdapter);


        vvpHome_HomeFragLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int newState) {

            }


            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                View view = vvpHome_HomeFragLay.getChildAt(position);
                PlayerView playerView = view.findViewById(R.id.player_view);

                stopPreviousVideoPlayer();

            }


            public void onPageSelected(int position) {
                // Check if this is the page you want.
                Log.i("onPageSelected", "" + position);
            }
        });

        rvHome_HomeFragLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                super.onScrollStateChanged(recyclerView, i);
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                mListener.hide_sheet();
                super.onScrolled(recyclerView, i, i2);
                int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                if (computeVerticalScrollOffset != currentPage) {
                    currentPage = computeVerticalScrollOffset;
                    stopPreviousVideoPlayer();
                    setVideoPlayerToPlay(currentPage);
                    loadMore(currentPage);
                }
            }
        });

        if (!TextUtils.isEmpty(position)) {
            layoutManager.scrollToPosition(Integer.parseInt(position));
        }
        loadAdapter();

        return view;
    }

    public void loadMore(int page) {
        int currentPage = page + 1;
        Log.i("loadMore", "" + currentPage);
        if (currentPage == mVideoURLList.size()) {
            Log.i("loadMore", "load next");
            if(mNextPage != 0){
                getVideoList();
            }
        }

    }

    public void getVideoList() {

        HashMap<String, Object> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("page", mNextPage);

        Log.i("loadMore", "load next.."+map);
        presenter.getList(map);
    }

    @Override
    public void onClickGuestProfile() {
        ActivityManager.redirectToActivityWithoutBundle(context, GuestProfile_Activity.class);

    }

    @Override
    public void onClickSoundTrack() {
        ActivityManager.redirectToActivityWithoutBundle(context, SoundTrack_Activity.class);
    }

    SimpleExoPlayer previous_exoplayer;

    @SuppressLint("SetTextI18n")
    public void setVideoPlayerToPlay(int i) {

        DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(32 * 1024, 64 * 1024, 1024, 1024).createDefaultLoadControl();
        final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this.context, (TrackSelector) new DefaultTrackSelector(), loadControl);
        newSimpleInstance.setRepeatMode(Player.REPEAT_MODE_ONE);
        HttpProxyCacheServer proxyServer = new HttpProxyCacheServer.Builder(getContext()).maxCacheSize(1024 * 1024 * 1024).build();
        String proxyURL = proxyServer.getProxyUrl(mVideoURLList.get(i));
        newSimpleInstance.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(this.context,
                Util.getUserAgent(this.context, "TikTok"))).createMediaSource(Uri.parse(proxyURL)));
        newSimpleInstance.addListener(this);


        View findViewByPosition = layoutManager.findViewByPosition(i);
        PlayerView playerView = findViewByPosition.findViewById(R.id.player_view);

        TextView like_count_txt = findViewByPosition.findViewById(R.id.like_count_txt);
        BaseActivity.comments_count_txt = findViewByPosition.findViewById(R.id.comments_count_txt);
        TextView description_txt = findViewByPosition.findViewById(R.id.description_txt);
        TextView user_txt = findViewByPosition.findViewById(R.id.user_txt);
        TextView sount_track_txt = findViewByPosition.findViewById(R.id.sount_track_txt);
        ImageView like_img = findViewByPosition.findViewById(R.id.like_img);
        ImageView comments_img = findViewByPosition.findViewById(R.id.comments_img);
        ImageView ivSoundTrack_HomeFragLay = findViewByPosition.findViewById(R.id.ivSoundTrack_HomeFragLay);
        ImageView share_img = findViewByPosition.findViewById(R.id.share_img);
        RelativeLayout rrGuestProfile_HomeFragLay = findViewByPosition.findViewById(R.id.rrGuestProfile_HomeFragLay);

        ivSoundTrack_HomeFragLay.setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(context)
                .load(getResources().getDrawable(R.drawable.record))
                .into(ivSoundTrack_HomeFragLay);

        progress = findViewByPosition.findViewById(R.id.progress);

        CircleImageView user_img = findViewByPosition.findViewById(R.id.user_img);
        ImageView follow_img = findViewByPosition.findViewById(R.id.follow_img);

        if (from.equals("home")) {

            like_count_txt.setText("" + mVideoDataList.get(i).getLikesCount());
            BaseActivity.comments_count_txt.setText("" + mVideoDataList.get(i).getCommentsCount());
            description_txt.setText("" + mVideoDataList.get(i).getDescription());
            description_txt.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            description_txt.setSelected(true);
            description_txt.setSingleLine(true);

            if (mVideoDataList.get(i).getUserdetails().getImage() != null)
                Glide.with(context).load("" + mVideoDataList.get(i).getUserdetails().getImage())
                        .apply(RequestOptions.placeholderOf(R.drawable.user_profile)
                                .dontAnimate().error(R.drawable.user_profile)).into(user_img);

            if (mVideoDataList.get(i).getFollowers() != null) {
                follow_img.setVisibility(View.GONE);
            } else {
                follow_img.setVisibility(View.VISIBLE);
            }

            follow_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progress.setVisibility(View.VISIBLE);
                    follow_img.setVisibility(View.GONE);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                    map.put("follower_id", mVideoDataList.get(i).getUserdetails().getId());
                    presenter.followup(map);
                }
            });

            rrGuestProfile_HomeFragLay.setOnClickListener(v -> {
                if (loggedIn) {
                    Intent guest = new Intent(getContext(), GuestProfile_Activity.class);
                    guest.putExtra("guest_id", "" + mVideoDataList.get(i).getUserdetails().getId());
                    startActivity(guest);
                } else {
                    mListener.behaviour();
                }
            });

            ivSoundTrack_HomeFragLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        Intent sound_track = new Intent(getContext(), SoundTrack_Activity.class);
                        sound_track.putExtra("audio_id", mVideoDataList.get(i).getAudio());
                        sound_track.putExtra("category_id", "" + mVideoDataList.get(i).getCategoryId());
                        startActivity(sound_track);
                    } else {
                        mListener.behaviour();
                    }
                }
            });

            user_txt.setText("@" + "" + mVideoDataList.get(i).getUserdetails().getFirstName() + " " + mVideoDataList.get(i).getUserdetails().getLastName());

            if (mVideoDataList.get(i).getVideosound() != null)
                sount_track_txt.setText("" + mVideoDataList.get(i).getVideosound().getDescription());
            else
                sount_track_txt.setVisibility(View.GONE);


            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (loggedIn) {
                        if (liked == false) {

                            liked = true;
                            int count = mVideoDataList.get(i).getLikesCount() + 1;
                            like_count_txt.setText("" + count);
                            mVideoDataList.get(i).setLikesCount(count);
                            like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            map.put("status", "1");
                            presenter.like_video(map);

                        } else {

                            liked = false;
                            if (mVideoDataList.get(i).getLikesCount() != 0) {
                                int count = mVideoDataList.get(i).getLikesCount() - 1;
                                like_count_txt.setText("" + count);
                                mVideoDataList.get(i).setLikesCount(count);
                            }
                            like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                            map.put("video_id", mVideoDataList.get(i).getId());
                            map.put("status", "0");
                            presenter.like_video(map);

                        }

                    } else {
                        mListener.behaviour();
                    }
                }
            });

            user_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
                        Intent guest = new Intent(getContext(), GuestProfile_Activity.class);
                        guest.putExtra("guest_id", "" + mVideoDataList.get(i).getUserdetails().getId());
                        startActivity(guest);
                    } else {
                        mListener.behaviour();
                    }

                }
            });

            share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SharedHelper.putKey(getContext(), "share_listener", "home");
                    mListener.share_sheet("" + mVideoURLList.get(i), mVideoDataList.get(i).getId());

                }
            });

            comments_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedIn) {
//                        int count = list.get(i).getCommentsCount() + 1;
//                        comments_count_txt.setText("" + count);
                        SharedHelper.putKey(getContext(), "comment_listener", "home");
                        mListener.comments_list("" + mVideoDataList.get(i).getId(), i);
                    } else {
                        mListener.behaviour();
                    }
                }
            });

            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;
        } else {

            follow_img.setVisibility(View.GONE);

            like_count_txt.setText("" + list_follow.get(i).getLikesCount());
            BaseActivity.comments_count_txt.setText("" + list_follow.get(i).getCommentsCount());
            description_txt.setText("" + list_follow.get(i).getDescription());
            description_txt.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            description_txt.setSelected(true);
            description_txt.setSingleLine(true);

            user_txt.setText("@" + "" + list_follow.get(i).getUserdetails().getFirstName() + " " + list_follow.get(i).getUserdetails().getLastName());

            if (list_follow.get(i).getLikes() != null) {
                liked = false;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
            } else {
                liked = true;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));

            }

            if (list_follow.get(i).getVideosound() != null)
                sount_track_txt.setText("" + list_follow.get(i).getVideosound().getDescription());
            else
                sount_track_txt.setVisibility(View.GONE);

            int status = list_follow.get(i).getLikes().size();

            if (list_follow.get(i).getLikes() != null && status != 0) {
                liked = false;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
            } else {
                liked = true;
                like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));

            }
            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!liked) {

                        liked = true;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.liked));
                        int count = list_follow.get(i).getLikesCount() + 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                        map.put("video_id", list_follow.get(i).getId());
                        map.put("status", "1");
                        presenter.like_video(map);
                    } else {
                        liked = false;
                        like_img.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselect_like));
                        int count = list_follow.get(i).getLikesCount() - 1;
                        like_count_txt.setText("" + count);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
                        map.put("video_id", list_follow.get(i).getId());
                        map.put("status", "0");
                        presenter.like_video(map);
                    }

                }
            });

            user_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent guest = new Intent(getContext(), GuestProfile_Activity.class);
                    guest.putExtra("guest_id", "" + list_follow.get(i).getUserdetails().getId());
                    startActivity(guest);

                }
            });

            comments_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    int count = list_follow.get(i).getCommentsCount() + 1;
//                    homeScreen_pagerAdapter.notifyDataSetChanged();
//                    comments_count_txt.setText(count);
                    SharedHelper.putKey(getContext(), "comment_listener", "home");
                    mListener.comments_list("" + list_follow.get(i).getId(), i);

                }
            });

            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(true);
            previous_exoplayer = newSimpleInstance;

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InteractionListener) {
            //init the listener
            mListener = (InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement InteractionListener");
        }

    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int i) {
        if (i == 2) {
            this.pbLoad_HomeFragLay.setVisibility(View.VISIBLE);
        } else if (i == 3) {
            this.pbLoad_HomeFragLay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void stopPreviousVideoPlayer() {
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.removeListener(this);
            this.previous_exoplayer.release();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(true);
        }
    }

    public void onPause() {
        super.onPause();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.previous_exoplayer != null) {
            this.previous_exoplayer.release();
        }
    }

    private HomeScreen_Adapter homeScreen_adapter;

    private int startPagesCount = 1;


    @Override
    public void onSuccess(VideoList_Pojo1 historyList) {

        //mVideoURLList.clear();
        //mVideoDataList.clear();
        mVideoDataList.addAll(historyList.getVideos().getData());
        int his_list = historyList.getVideos().getData().size();

        if (his_list == 0) {
           // mNextPage = 0;
            //   no_data_lay.setVisibility(View.VISIBLE);
            //  rvHome_HomeFragLay.setVisibility(View.GONE);
            // this.pbLoad_HomeFragLay.setVisibility(View.GONE);
            //Snackbar.make(rvHome_HomeFragLay,"",Snackbar.LENGTH_LONG).show();
        } else {
            mNextPage = historyList.getVideos().getCurrentPage() + 1;
            no_data_lay.setVisibility(View.GONE);
            rvHome_HomeFragLay.setVisibility(View.VISIBLE);
            for (int i = 0; i < his_list; i++) {
                Log.i("onSuccess", "" + historyList.getVideos().getData().get(i).getVideo());
                mVideoURLList.add(historyList.getVideos().getData().get(i).getVideo());
            }

            homeScreen_adapter.notifyDataSetChanged();
            homeScreen_pagerAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onSuccessLike(Object trendsResponse) {

        homeScreen_pagerAdapter.notifyDataSetChanged();
//        presenter.getList();
//        Toast.makeText(getContext(),"Liked the video",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccessDisLike(Object trendsResponse) {

    }

    @Override
    public void onSuccessFollowList(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

        mVideoURLList.clear();
        mVideoDataList.clear();
        list_follow = trendsResponse;
//        list = trendsResponse.getVideos();
        int his_list = trendsResponse.size();
        if (his_list == 0) {
            no_data_lay.setVisibility(View.VISIBLE);
            rvHome_HomeFragLay.setVisibility(View.GONE);
            this.pbLoad_HomeFragLay.setVisibility(View.GONE);

        } else {
            for (int i = 0; i < his_list; i++) {
                mVideoURLList.add(trendsResponse.get(i).getVideo());
            }
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rvHome_HomeFragLay.setLayoutManager(layoutManager);
            rvHome_HomeFragLay.setHasFixedSize(false);
            new PagerSnapHelper().attachToRecyclerView(rvHome_HomeFragLay);
            HomeScreen_Adapter homeScreen_adapter = new HomeScreen_Adapter(context, this, mVideoURLList);
            rvHome_HomeFragLay.setAdapter(homeScreen_adapter);

            homeScreen_pagerAdapter = new HomeScreen_PagerAdapter(context, this, mVideoURLList);
            vvpHome_HomeFragLay.setAdapter(homeScreen_pagerAdapter);
            homeScreen_adapter.notifyDataSetChanged();


            vvpHome_HomeFragLay.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                public void onPageScrollStateChanged(int newState) {

                }

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    View view = vvpHome_HomeFragLay.getChildAt(position);
                    PlayerView playerView = view.findViewById(R.id.player_view);
                    stopPreviousVideoPlayer();

                }


                public void onPageSelected(int position) {
                    // Check if this is the page you want.
                }
            });

            rvHome_HomeFragLay.addOnScrollListener(new RecyclerView.OnScrollListener() {
                public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                    super.onScrollStateChanged(recyclerView, i);
                }

                public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                    super.onScrolled(recyclerView, i, i2);
                    int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                    if (computeVerticalScrollOffset != currentPage) {
                        currentPage = computeVerticalScrollOffset;

                        stopPreviousVideoPlayer();
                        setVideoPlayerToPlay(currentPage);
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(position)) {
            layoutManager.scrollToPosition(Integer.parseInt(position));
        }
    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessVideoReport(VideoReportResponse videoReportResponse) {

    }

    private void loadAdapter() {


    }

    @Override
    public void comment(String i) {

        int place = Integer.parseInt(i);

        String mCount = "" + BaseActivity.comments_count_txt.getText().toString();
        int count = Integer.parseInt(mCount) + 1;
        BaseActivity.comments_count_txt.setText("" + count);

        if (from.equals("home")) {

            mVideoDataList.get(place).setCommentsCount(count);
//            HashMap<String, Object> map = new HashMap<>();
//            map.put("user_id", user_id);
//
//            presenter.getList(map);
        } else if (from.equals("following")) {

            if (loggedIn) {
                list_follow.get(place).setCommentsCount(count);
//                presenter.getFollowList();
            } else {
                no_data_lay.setVisibility(View.VISIBLE);
                rvHome_HomeFragLay.setVisibility(View.GONE);
                this.pbLoad_HomeFragLay.setVisibility(View.GONE);
                mListener.behaviour();
            }
        }
    }
}

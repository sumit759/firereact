package com.mozostar.app.view.activity.search;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.HashSearch_Pojo;
import com.mozostar.app.data.model.Sounds_Pojo;
import com.mozostar.app.data.model.UserVideosList_Pojo;
import com.mozostar.app.data.model.User_Pojo;

import java.util.ArrayList;

public interface SearchIView extends MvpView {

    void onSuccessUser(ArrayList<User_Pojo> trendsResponse);

    void onSuccessHash(ArrayList<HashSearch_Pojo> trendsResponse);

    void onSuccessVideo(ArrayList<UserVideosList_Pojo> videoResponseList);

    void onSuccessSound(ArrayList<Sounds_Pojo> sounds_pojoArrayList);
}

package com.mozostar.app.view.postdubvideo;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.BasePresenter;
import com.mozostar.app.data.model.VideoUpload_pojo;
import com.mozostar.app.data.model.bigvideo.postresponse.BigVideoUploadResponse;
import com.mozostar.app.data.network.APIClient;

import java.util.ArrayList;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PostDubVideoPresenter<V extends PostDubVideoIView> extends BasePresenter<V> implements PostDubVideoIPresenter<V> {
    @SuppressLint("CheckResult")
    @Override
    public void postregister(Map<String, RequestBody> params, MultipartBody.Part file, ArrayList<RequestBody> tag) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().video_upload(params, file, tag);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessRegister((VideoUpload_pojo) trendsResponse);
                        },
                        throwable -> {
                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void postregister(Map<String, RequestBody> params, MultipartBody.Part file, MultipartBody.Part file1, ArrayList<RequestBody> tag) {
      //  ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
    //    dialog.show();
        Observable modelObservable = APIClient.getAPIClient().video_upload(params, file, file1, tag);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            //   dialog.dismiss();
                            getMvpView().onSuccessRegister((VideoUpload_pojo) trendsResponse);
                        },
                        throwable -> {
                            //    dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void postregister_channel(Map<String, RequestBody> params, MultipartBody.Part file, ArrayList<RequestBody> tag) {
        ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.show();
        Observable modelObservable = APIClient.getAPIClient().video_upload_channal(params, file, tag);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trendsResponse -> {
                            dialog.dismiss();
                            getMvpView().onSuccessRegister((VideoUpload_pojo) trendsResponse);
                        },
                        throwable -> {
                            dialog.dismiss();
                            getMvpView().onError((Throwable) throwable);
                        });
    }

    @SuppressLint("CheckResult")
    @Override
    public void postBigVideo(Map<String, RequestBody> params, MultipartBody.Part file, ArrayList<RequestBody> tag) {
     /*   ProgressDialog dialog = ProgressDialog.show(getMvpView().activity(), "", "Please wait...", true);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.show();*/
        Observable modelObservable = APIClient.getAPIClient().uploadBigVideo(params, file, tag);
        modelObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bigVideoUploadRes -> {
                            getMvpView().onSuccessBigVideoUpload((BigVideoUploadResponse) bigVideoUploadRes);
                        },
                        throwable -> {
                            getMvpView().onError((Throwable) throwable);
                        });
    }
}
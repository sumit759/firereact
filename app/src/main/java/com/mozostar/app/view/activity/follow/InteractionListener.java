package com.mozostar.app.view.activity.follow;

public interface InteractionListener {

    void follow(String id);
}

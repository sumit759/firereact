package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.view.activity.videos_view.AdapterOnClick;
import com.mozostar.app.view.activity.videos_view.ViewVideosActivity;
import com.mozostar.app.view.activity.videos_view.ViewVideosFragment;
import com.mozostar.app.view.fragment.homedashboard.Home_Fragment;
import com.mozostar.app.view.fragment.homedashboard.PagerAdapterOnClick;
import com.mozostar.app.view.fragment.homedashboard.Video_View_Fragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BigVideoNewAdapter extends RecyclerView.Adapter<BigVideoNewAdapter.MyViewHolder> {
    ArrayList<String> mlist_arraylist;
    Context mContext;
    PagerAdapterOnClick pagerAdapterClicK;
    AdapterOnClick adapterOnClick;

    public BigVideoNewAdapter(Context context,  ArrayList<String> list_arraylist) {
        mContext = context;
        mlist_arraylist = list_arraylist;
    }


    public BigVideoNewAdapter(Context context, ViewVideosActivity home_fragment, ArrayList<String> list_arraylist) {
        mContext = context;
        adapterOnClick = home_fragment;
        mlist_arraylist = list_arraylist;
    }

    public BigVideoNewAdapter(Context context, Video_View_Fragment viewVideosFragment, ArrayList<String> cover_arrayList) {
        mContext = context;
        mlist_arraylist = cover_arrayList;
    }

    public BigVideoNewAdapter(Context context, ViewVideosFragment home_fragment, ArrayList<String> list_arraylist) {
        mContext = context;
        adapterOnClick = home_fragment;
        mlist_arraylist = list_arraylist;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.big_video_row_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return mlist_arraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

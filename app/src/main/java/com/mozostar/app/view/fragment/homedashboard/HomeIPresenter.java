package com.mozostar.app.view.fragment.homedashboard;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.HashMap;

public interface HomeIPresenter<V extends HomeIView> extends MvpPresenter<V> {
    void getList(HashMap<String, Object> params);

    @SuppressLint("CheckResult")
    void getFollowList();

    void like_video(HashMap<String, Object> obj);

    void disLike_video(HashMap<String, Object> obj);

    @SuppressLint("CheckResult")
    void followup(HashMap<String, Object> obj);
}

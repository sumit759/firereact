package com.mozostar.app.view.fragment.profilefragment;


import android.content.Context;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.ProfileChannelList_pojo;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.DividerGridItemDecoration;
import com.mozostar.app.view.adapter.ProfileDub_Adapter;
import com.mozostar.app.view.fragment.guestfragment.GuestDub_Fragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileDub_Fragment extends BaseFragment implements ProfileVideoIView {

    Context context;
    private static String TAG = GuestDub_Fragment.class.getSimpleName();

    @BindView(R.id.rvTrending_ProfileDubLay)
    RecyclerView rvTrending_ProfileDubLay;

    ProfileVideoPresenter<ProfileDub_Fragment> presenter = new ProfileVideoPresenter<>();
    ArrayList<String> cover_ArrayList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile_dub_;
    }

    @Override
    public View initView(View view) {
        ButterKnife.bind(this,view);
        presenter.attachView(this);
        context = getActivity();

        rvTrending_ProfileDubLay.setLayoutManager(new GridLayoutManager(context, 3));
       /* rvTrending_ProfileDubLay.setHasFixedSize(true);
        rvTrending_ProfileDubLay.addItemDecoration(new DividerGridItemDecoration(context));
        rvTrending_ProfileDubLay.setItemAnimator(new DefaultItemAnimator());*/

        rvTrending_ProfileDubLay.setHasFixedSize(true); // bala added 28072020
        rvTrending_ProfileDubLay.addItemDecoration(new DividerGridItemDecoration(context));

        presenter.getDubList();
        return view;
    }

    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {



    }

    @Override
    public void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

        int size = trendsResponse.size();

        for (int i=0;i<size;i++){
            cover_ArrayList.add(trendsResponse.get(i).getVideo());
        }



        ProfileDub_Adapter guestLikes_adapter = new ProfileDub_Adapter(context,trendsResponse);
        rvTrending_ProfileDubLay.setAdapter(guestLikes_adapter);
    }

    @Override
    public void onSuccessChannel(ProfileChannelList_pojo trendsResponse) {

    }


}

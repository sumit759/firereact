package com.mozostar.app.view.activity.banner;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toolbar;

import androidx.core.content.ContextCompat;

import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.common.Utilities;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.view.activity.MainActivity;
import com.mozostar.app.view.login.Login_Activity;
import com.mozostar.app.view.splash.Splash_Activity;

public class BannerActivity extends BaseActivity implements BannerIView {

    Context context;
    private static String TAG = Splash_Activity.class.getSimpleName();
    BannerPresenter<BannerActivity> presenter = new BannerPresenter<>();
    Toolbar myToolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_banner;
    }

    @Override
    public void initView() {
        context = BannerActivity.this;
        init();
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setStatusBarColor(ContextCompat.getColor(BannerActivity.this, R.color.colorPrimary));

        setSupportActionBar(myToolbar);
        Utilities.printV("FCM TOKEN===>", SharedHelper.getFCMKey(this, "device_token"));
        Utilities.printV("FCM TOKEN ID===>", SharedHelper.getFCMKey(this, "device_id"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);
    }

    private void setSupportActionBar(Toolbar myToolbar) {
    }

    private void init() {
        new Handler().postDelayed(() -> {

            Boolean loggedIn = SharedHelper.getBoolKey(BannerActivity.this, "logged_in", false);
//            if (loggedIn) {

            ActivityManager.redirectToActivityWithoutBundle(context, MainActivity.class);
            overridePendingTransition(0, 0);
            finishAffinity();
            finish();
//                presenter.profile();
//            } else {

//                ActivityManager.redirectToActivityWithoutBundle(context, Login_Activity.class);
//                overridePendingTransition(0, 0);
//                finish();

//            }
        }, 2000);
    }

    @Override
    public void redirectHome() {
        if (SharedHelper.getKey(this, "loggged_in").equalsIgnoreCase("true")) {
            startActivity(new Intent(activity(), MainActivity.class));
            finishAffinity();
        } else {
            startActivity(new Intent(activity(), Login_Activity.class));
            finishAffinity();
            finish();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }
}
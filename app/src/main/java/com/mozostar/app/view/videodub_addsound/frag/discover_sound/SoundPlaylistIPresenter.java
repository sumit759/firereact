package com.mozostar.app.view.videodub_addsound.frag.discover_sound;


import com.mozostar.app.base.MvpPresenter;


public interface SoundPlaylistIPresenter<V extends SoundPlaylistIView> extends MvpPresenter<V> {
    void getSoundPlayList();
}

package com.mozostar.app.view.postdubvideo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.common.ProgressRequestBody;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.VideoUpload_pojo;
import com.mozostar.app.data.model.bigvideo.postresponse.BigVideoUploadResponse;
import com.mozostar.app.view.activity.MainActivity;
import com.mozostar.app.view.activity.search.SearchActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import retrofit2.Response;

public class PostDubVideo_Activity extends BaseActivity implements PostDubVideoIView, ProgressRequestBody.UploadCallbacks {
    Context context;
    private static String TAG = PostDubVideo_Activity.class.getSimpleName();

    @BindView(R.id.tvTitle_ToolBarLay)
    TextView tvTitle_ToolBarLay;

    @BindView(R.id.ivMenu_ToolBarLay)
    ImageView ivMenu_ToolBarLay;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.post_btn)
    Button post_btn;

    @BindView(R.id.video_thumb)
    ImageView video_thumb;

    @BindView(R.id.et_description)
    EditText et_description;

    @BindView(R.id.Privacy)
    TextView Privacy;

    @BindView(R.id.get_hash_list)
    ImageView get_hash_list;


    //  @BindView(R.id.progressBarVideoUpload)
    ProgressBar progressBarVideoUpload;
    //  @BindView(R.id.progressBar_per)
    TextView progressBar_per;
    // @BindView(R.id.progressDialogLayout)
    RelativeLayout progressDialogLayout;
    Dialog dialog = null;


    private PostDubVideoPresenter<PostDubVideo_Activity> presenter = new PostDubVideoPresenter<>();


    String path, audio_path, desc, title,
            audio, audio_id, video, view_type, category_id;
    int user_id;

    ArrayList<RequestBody> tag = new ArrayList<>();

    MultipartBody.Part vFile, aFile;
    boolean uploadBigVideo;

    @Override
    public int getLayoutId() {
        return R.layout.activity_post_dub_video;
    }

    @Override
    public void initView() {

        context = PostDubVideo_Activity.this;
        ButterKnife.bind(this);
        presenter.attachView(this);
        tvTitle_ToolBarLay.setText("Post Video");
        ivMenu_ToolBarLay.setVisibility(View.INVISIBLE);

        Bundle extras = getIntent().getExtras();

        audio_id = SharedHelper.getKey(PostDubVideo_Activity.this, "audio_id_sound");
        category_id = SharedHelper.getKey(PostDubVideo_Activity.this, "categori_id_sound");

        // SharedHelper.getKey(PostDubVideo_Activity.this, "upload_big_video");

        Privacy.setText("My Followers");
        view_type = "1";

        String hash = SharedHelper.getKey(context, "hash_list");

        if (!TextUtils.isEmpty(hash) && !hash.equals("")) {
            et_description.setText(hash + " ");
        }


        if (extras != null) {

            path = extras.getString("dubbed_video_path");
            audio_path = extras.getString("dubbed_audio_path");

            Log.e("@@@@@@@@ path ", "path " + path);
            Log.e("@@@@@@@@ audio_path ", "audio_path " + audio_path);

            title = path.substring(path.lastIndexOf("/") + 1);
            File videoFile = new File(path);
            int file_size = Integer.parseInt(String.valueOf(videoFile.length() / 1024));

            // Get length of file in bytes
            long fileSizeInBytes = videoFile.length();
// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            long fileSizeInKB = fileSizeInBytes / 1024;

            Log.e("File_sizeKB", "" + fileSizeInKB);
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            long fileSizeInMB = fileSizeInKB / 1024;

            Log.e("File_sizeMB", "" + fileSizeInMB);

//            if (fileSizeInMB > 27) {
//
//            }


            if (!TextUtils.isEmpty(audio_path)) {
                RequestBody audioBody = RequestBody.create(MediaType.parse("audio/mp3"), audio_path);
                aFile = MultipartBody.Part.createFormData("audio", audio, audioBody);
            }
            // RequestBody videoBody = RequestBody.create(MediaType.parse("video/mp4"), videoFile);
            //   vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), videoBody);  // Bala removed
            try {
                if (SharedHelper.getBoolKey(context, "upload_big_video", true)) { // Bala added for video upload percentage 29072020
                    ProgressRequestBody fileBody = new ProgressRequestBody(videoFile, "video/mp4", this);
                    vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), fileBody);
                } else {
                    ProgressRequestBody fileBody = new ProgressRequestBody(videoFile, "video/mp4", this);
                    vFile = MultipartBody.Part.createFormData("video", videoFile.getName(), fileBody);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            user_id = SharedHelper.getIntKey(PostDubVideo_Activity.this, "user_id");


        }


        Bitmap bmThumbnail;

        bmThumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);

        video_thumb.setImageBitmap(bmThumbnail);


    }

    public static RequestBody toRequestBody(String value) {

        return RequestBody.create(MediaType.parse("text/plain"), value);
    }


    @OnClick({R.id.post_btn, R.id.Privacy, R.id.get_user_list, R.id.get_hash_list, R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.post_btn:
                desc = et_description.getText().toString();
                if (SharedHelper.getBoolKey(context, "channel_video_uplaod", true) == true) {

                    HashMap<String, RequestBody> map = new HashMap<>();

                    map.put("description", toRequestBody(desc));
                    map.put("title", toRequestBody(title));
                    map.put("view_type", toRequestBody(view_type));
                    map.put("user_id", toRequestBody(String.valueOf(user_id)));
                    map.put("channel_category", toRequestBody(String.valueOf(SharedHelper.getIntKey(context, "channel_category"))));
                    map.put("channel", toRequestBody(String.valueOf(SharedHelper.getIntKey(context, "id"))));
                    presenter.postregister_channel(map, vFile, tag);

                } else if (SharedHelper.getBoolKey(context, "upload_big_video", true) == true) {

                    HashMap<String, RequestBody> map = new HashMap<>();
                    map.put("description", toRequestBody(desc));
                    map.put("title", toRequestBody(title));
                    map.put("audio", toRequestBody("0"));
                    map.put("category_id", toRequestBody("0"));
                    map.put("view_type", toRequestBody(view_type));
                    map.put("user_id", toRequestBody(String.valueOf(user_id)));
                    map.put("audio_type", toRequestBody("normal"));
                    showDialog(PostDubVideo_Activity.this, "Please Wait!");
                    presenter.postBigVideo(map, vFile, tag);
                } else {
                    if (!TextUtils.isEmpty(audio_path)) {
                        HashMap<String, RequestBody> map = new HashMap<>();
                        map.put("description", toRequestBody(desc));
                        map.put("title", toRequestBody(title));
//                    map.put("audio", toRequestBody(audio));
                        map.put("view_type", toRequestBody(view_type));
                        map.put("category_id", toRequestBody(""));
                        map.put("user_id", toRequestBody(String.valueOf(user_id)));
                        map.put("audio_type", toRequestBody("upload"));
                        showDialog(PostDubVideo_Activity.this, "Please Wait!");
                        presenter.postregister(map, vFile, aFile, tag);
                    } else {
                        HashMap<String, RequestBody> map = new HashMap<>();
                        map.put("description", toRequestBody(desc));
                        map.put("title", toRequestBody(title));
                        map.put("audio", toRequestBody(audio_id));
                        map.put("view_type", toRequestBody(view_type));
                        map.put("category_id", toRequestBody(category_id));
                        map.put("user_id", toRequestBody(String.valueOf(user_id)));
                        map.put("audio_type", toRequestBody("normal"));
                        presenter.postregister(map, vFile, tag);
                    }
                }

                break;

            case R.id.Privacy:

                AlertDialog.Builder adb = new AlertDialog.Builder(this);

                CharSequence items[] = new CharSequence[]{"My Followers", "My Firends", "Private"};

                adb.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface d, int n) {
                        if (n == 0) {

                            view_type = "1";
                            Privacy.setText("My Followers");
                            d.dismiss();

                        } else if (n == 1) {

                            view_type = "2";
                            Privacy.setText("My Firends");
                            d.dismiss();

                        } else if (n == 2) {

                            view_type = "3";
                            Privacy.setText("Private");
                            d.dismiss();

                        }
                    }

                });
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                adb.setTitle("Set Views");
                adb.show();

                break;
            case R.id.get_user_list:

                SharedHelper.putKey(context, "search", "post_user");
                Intent i = new Intent(this, SearchActivity.class);
                i.putExtra("search", "post_user");
                startActivityForResult(i, 0);
                break;

            case R.id.get_hash_list:
                SharedHelper.putKey(context, "search", "hash");
                Intent hash = new Intent(this, SearchActivity.class);
                hash.putExtra("search", "hash");
                startActivityForResult(hash, 0);
                break;
            case R.id.back:
                finish();
                break;

        }
    }


    @Override
    public void onSuccessRegister(VideoUpload_pojo register_pojo) {
        try {
            SharedHelper.putKey(PostDubVideo_Activity.this, "hash_list", "");
            Intent main = new Intent(PostDubVideo_Activity.this, MainActivity.class);
            Toast.makeText(PostDubVideo_Activity.this, "Success", Toast.LENGTH_LONG);
            startActivity(main);
            finishAffinity();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccessBigVideoUpload(BigVideoUploadResponse bigVideoUploadResponse) {
        try {
            if (dialog != null) {
                dialog.dismiss();
            }
            Toast.makeText(this, bigVideoUploadResponse.getMessage(), Toast.LENGTH_SHORT).show();
            SharedHelper.putKey(context, "upload_big_video", false);
            Intent main = new Intent(PostDubVideo_Activity.this, MainActivity.class);
            startActivity(main);
            finishAffinity();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                String name = data.getStringExtra("name");
                String id = data.getStringExtra("id");
                tag.add(toRequestBody(id));
                et_description.append(name);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public void showDialog(Activity activity, String msg) {
        try {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setTitle(msg);
            dialog.setContentView(R.layout.video_upload_progressbar_layout);
            progressBarVideoUpload = (ProgressBar) dialog.findViewById(R.id.progressBarVideoUpload);
            progressBar_per = dialog.findViewById(R.id.progressBar_per);
            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {
        try {
            progressBarVideoUpload.setProgress(percentage);
            progressBar_per.setText("" + percentage + "%");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError() {
        try {
            Toast.makeText(this, "Error Uploading", Toast.LENGTH_SHORT).show();
            if (dialog != null) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinish() {
        try {
            if (dialog != null) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            Response response = ((HttpException) e).response();
            if (dialog != null) {
                dialog.dismiss();
            }
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                if (jObjError.has("message"))
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();
                else if (jObjError.has("error"))
                    if (jObjError.has("error")) {
                        Toast.makeText(activity(), jObjError.optString("error"), Toast.LENGTH_SHORT).show();
                    } else {
                        String email = jObjError.getString("email");
                        JSONArray jsonArray = new JSONArray(email);
                        Toast.makeText(activity(), jsonArray.getString(0).toString(), Toast.LENGTH_SHORT).show();
                    }
                else
                    Toast.makeText(activity(), jObjError.toString(), Toast.LENGTH_SHORT).show();
            } catch (Exception exp) {
                Log.e("Error", exp.getMessage());
                Toast.makeText(activity(), exp.getMessage(), Toast.LENGTH_SHORT).show();
            }

            if (response.code() == 401) {

                JSONObject jObjError = null;
                try {
                    jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();

                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}

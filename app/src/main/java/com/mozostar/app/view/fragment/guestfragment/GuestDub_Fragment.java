package com.mozostar.app.view.fragment.guestfragment;

import android.content.Context;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;
import com.mozostar.app.view.adapter.GuestDub_Adapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuestDub_Fragment extends BaseFragment implements GuestVideoIView {
    Context context;
    private static String TAG = GuestDub_Fragment.class.getSimpleName();

    @BindView(R.id.rvTrending_GuestDubLay)
    RecyclerView rvTrending_GuestDubLay;

    GuestVideoPresenter<GuestDub_Fragment> presenter = new GuestVideoPresenter<>();
    ArrayList<String> cover_ArrayList = new ArrayList<>();

    String guest_id;
    public GuestDub_Fragment() {
        // Required empty public constructor
    }
    @Override
    public int getLayoutId() {
        return R.layout.guestdub_lay;
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);

        Bundle bundle = this.getArguments();
        guest_id = bundle.getString("guest_id");

        if (!TextUtils.isEmpty(guest_id))
            presenter.getDubList(guest_id);

        rvTrending_GuestDubLay.setLayoutManager(new GridLayoutManager(context, 3));
        rvTrending_GuestDubLay.setItemAnimator(new DefaultItemAnimator());
       // rvTrending_GuestDubLay.setNestedScrollingEnabled(true);


        return view;
    }

    @Override
    public void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse) {

    }

    @Override
    public void onSuccessDub(ArrayList<ProfileVideoList_Pojo> trendsResponse) {
        int size = trendsResponse.size();

        for (int i=0;i<size;i++){
            cover_ArrayList.add(trendsResponse.get(i).getVideo());
        }



        GuestDub_Adapter guestDub_adapter = new GuestDub_Adapter(context,trendsResponse);
        rvTrending_GuestDubLay.setAdapter(guestDub_adapter);

    }
}

package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.view.activity.videos_view.AdapterOnClick;
import com.mozostar.app.view.activity.videos_view.ViewVideosActivity;
import com.mozostar.app.view.fragment.homedashboard.PagerAdapterOnClick;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverView_Adapter extends RecyclerView.Adapter<DiscoverView_Adapter.MyViewHolder> {
    ArrayList<String> mlist_arraylist;
    Context mContext;
    PagerAdapterOnClick pagerAdapterClicK;
    AdapterOnClick adapterOnClick;

    public DiscoverView_Adapter(Context context, ViewVideosActivity home_fragment, ArrayList<String> list_arraylist) {
        mContext = context;
        adapterOnClick = home_fragment;
        mlist_arraylist = list_arraylist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_frag_rowitem, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

//        myViewHolder.rrGuestProfile_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pagerAdapterClicK.onClickGuestProfile();
//            }
//        });
//
//        myViewHolder.ivSoundTrack_HomeFragLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pagerAdapterClicK.onClickSoundTrack();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mlist_arraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.rrGuestProfile_HomeFragLay)
        RelativeLayout rrGuestProfile_HomeFragLay;
        @BindView(R.id.ivSoundTrack_HomeFragLay)
        ImageView ivSoundTrack_HomeFragLay;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
//            ivSoundTrack_HomeFragLay.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.clockwise_rotation));
        }

        @Override
        public void onClick(View v) {

        }
    }
}

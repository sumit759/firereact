package com.mozostar.app.view.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mozostar.app.R;
import com.mozostar.app.data.model.ProfileChannelList_pojo;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class ChannelListAdapter extends RecyclerView.Adapter<ChannelListAdapter.ViewHolder>{
    private ArrayList<ProfileChannelList_pojo.Datum>  listdata ;
    private ChannelListAdapter.OnItemClickListener mOnItemClickListener;


    // RecyclerView recyclerView;
    public ChannelListAdapter(ArrayList<ProfileChannelList_pojo.Datum> listdata) {
        this.listdata = listdata;
    }
    public void  ChannelListUpdate(ArrayList<ProfileChannelList_pojo.Datum> listdata) {
        this.listdata = listdata;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.channel_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ProfileChannelList_pojo.Datum myListData = listdata.get(position);
        holder.textView.setText(myListData.getName());
        //holder.imageView.setImageResource(myListData.getUserId()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(myListData);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return listdata.size(); }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }

    public void setOnItemClickListener(ChannelListAdapter.OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    public interface OnItemClickListener {
        void onItemClick( ProfileChannelList_pojo.Datum myListData);
    }
}

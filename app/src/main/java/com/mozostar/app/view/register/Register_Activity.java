package com.mozostar.app.view.register;

import android.Manifest;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mozostar.app.MvpApplication;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Register_Pojo;
import com.mozostar.app.view.CountryPicker.Country;
import com.mozostar.app.view.CountryPicker.CountryPicker;
import com.mozostar.app.view.CountryPicker.CountryPickerListener;
import com.mozostar.app.view.login.SocialLoginActivity;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class Register_Activity extends BaseFragment implements RegisterIView {
    Context context;
    private static String TAG = Register_Activity.class.getSimpleName();

    @BindView(R.id.etEmail_RegisterLay)
    AppCompatEditText etEmail_RegisterLay;
    @BindView(R.id.etFirstName_RegisterLay)
    AppCompatEditText etFirstName_RegisterLay;
    @BindView(R.id.etLastName_RegisterLay)
    AppCompatEditText etLastName_RegisterLay;
    @BindView(R.id.etMobileNumber_RegisterLay)
    AppCompatEditText etMobileNumber_RegisterLay;
    @BindView(R.id.etPassword_RegisterLay)
    AppCompatEditText etPassword_RegisterLay;
    @BindView(R.id.etCPassword_RegisterLay)
    AppCompatEditText etCPassword_RegisterLay;
    @BindView(R.id.tvDOB_RegisterLay)
    LinearLayout tvDOB_RegisterLay;
    @BindView(R.id.txtYear)
    TextView txtYear;
    @BindView(R.id.txtMonth)
    TextView txtMonth;
    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.tvGender_RegisterLay)
    AppCompatTextView tvGender_RegisterLay;
    @BindView(R.id.fabDone_RegisterLay)
    LinearLayout fabDone_RegisterLay;
    @BindView(R.id.ivBack_RegisterLay)
    ImageView ivBack_RegisterLay;
    @BindView(R.id.login_txt)
    TextView login_txt;
    @BindView(R.id.country_image)
    ImageView countryImage;

    @BindView(R.id.profile_img)
    CircleImageView profile_img;

    @BindView(R.id.country_number)
    TextView countryNumber;
    @BindView(R.id.facebook_layout)
    RelativeLayout facebook_layout;
    @BindView(R.id.gmail_layout)
    RelativeLayout gmail_layout;

    @BindView(R.id.register_to_login_layout)
    RelativeLayout register_to_login_layout;
    private RegisterPresenter<Register_Activity> presenter = new RegisterPresenter<>();

    String country_code;

    private CountryPicker mCountryPicker;

    @Override
    public int getLayoutId() {
        return (R.layout.activity_register);
    }

    @Override
    public View initView(View view) {
        context = getActivity();
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        mCountryPicker = CountryPicker.newInstance("Select Country");
        // You can limit the displayed countries
        List<Country> countryList = Country.getAllCountries();
        Collections.sort(countryList, new Comparator<Country>() {
            @Override
            public int compare(Country s1, Country s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        mCountryPicker.setCountriesList(countryList);
        setListener();
        return view;
    }

    String Dob = "";

    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
//                mCountryNameTextView.setText(name);
//                mCountryIsoCodeTextView.setText(code);
                countryNumber.setText(dialCode);

                countryImage.setImageResource(flagDrawableResID);
                mCountryPicker.dismiss();
            }
        });
       /* countryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getFragmentManager(), "COUNTRY_PICKER");
            }
        });
        countryNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getFragmentManager(), "COUNTRY_PICKER");
            }
        });*/
        getUserCountryInfo();
    }

    private void getUserCountryInfo() {
        Locale current = getResources().getConfiguration().locale;
        Country country = Country.getCountryFromSIM(getActivity());
        if (country != null) {
            countryImage.setImageResource(country.getFlag());
            countryNumber.setText(country.getDialCode());
            country_code = country.getDialCode();
        } else {
            Country us = new Country("US", "United States", "+1", R.drawable.flag_us);
            countryImage.setImageResource(us.getFlag());
            countryNumber.setText(us.getDialCode());
            country_code = us.getDialCode();
            //Toast.makeText(Login.this, "Required Sim", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.fabDone_RegisterLay, R.id.facebook_layout, R.id.gmail_layout, R.id.tvGender_RegisterLay, R.id.tvDOB_RegisterLay,
            R.id.pick_img_lay, R.id.ivBack_RegisterLay, R.id.register_to_login_layout /*R.id.login_txt*/, R.id.country_image, R.id.country_number})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.tvDOB_RegisterLay) {
            dialog_date_picker();
        } else if (view.getId() == R.id.facebook_layout) {

//            ((SocialRegisterActivity) getActivity()).facebook_layout.performClick();

        } else if (view.getId() == R.id.gmail_layout) {
//            ((SocialRegisterActivity) getActivity()).gmail_layout.performClick();
        } else if (view.getId() == R.id.ivBack_RegisterLay) {
//            getActivity().onBackPressed();
        } else if (view.getId() == R.id.pick_img_lay) {
            pickImage();
        } else if (view.getId() == R.id.country_number) {
            mCountryPicker.show(getActivity().getFragmentManager(), "COUNTRY_PICKER");
        } else if (view.getId() == R.id.country_image) {
            mCountryPicker.show(getActivity().getFragmentManager(), "COUNTRY_PICKER");
        } else if (view.getId() == R.id.register_to_login_layout) {  /*R.id.login_txt*/
            Intent login = new Intent(getActivity(), SocialLoginActivity.class);
            startActivity(login);
            getActivity().finishAffinity();
            getActivity().finish();
        } else if (view.getId() == R.id.fabDone_RegisterLay) {
            String UserName = "" + etEmail_RegisterLay.getText().toString().trim();
            String FirstName = "" + etFirstName_RegisterLay.getText().toString().trim();
            String LastName = "" + etLastName_RegisterLay.getText().toString().trim();
            // String Dob = "" + tvDOB_RegisterLay.getText().toString().trim();
            String MobileNumber = "" + etMobileNumber_RegisterLay.getText().toString().trim();
            String password = "" + etPassword_RegisterLay.getText().toString().trim();
            String cpassword = "" + etCPassword_RegisterLay.getText().toString().trim();

            if (UserName.isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.invalid_email), Toast.LENGTH_SHORT).show();
                return;
            } else if (FirstName.isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.invalid_firstname), Toast.LENGTH_SHORT).show();
                return;
            } else if (LastName.isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.invalid_lastname), Toast.LENGTH_SHORT).show();
                return;
            } else if (MobileNumber.isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.invalid_mobile), Toast.LENGTH_SHORT).show();
                return;
            } else if (password.length() < 6) {
                Toast.makeText(getActivity(), getString(R.string.invalid_password), Toast.LENGTH_SHORT).show();
                return;
            } else if (!cpassword.equals(password)) {
                Toast.makeText(getActivity(), getString(R.string.invalid_password1), Toast.LENGTH_SHORT).show();
                return;
            } else {

                MultipartBody.Part filePart = null;
                if (imgFile != null) {
                    filePart = MultipartBody.Part.createFormData("image", imgFile.getName(), RequestBody.create(MediaType.parse("image/*"), imgFile));
                }
                  /*else {
                    filePart = MultipartBody.Part.createFormData("image", "", RequestBody.create(MediaType.parse("image/*"), imgFile));
                }*/
                HashMap<String, RequestBody> map = new HashMap<>();
                map.put("email", toRequestBody(UserName));
                map.put("first_name", toRequestBody(FirstName));
                map.put("last_name", toRequestBody(LastName));
                map.put("mobile", toRequestBody(MobileNumber));
                map.put("device_type", toRequestBody("android"));
                map.put("device_token", toRequestBody(SharedHelper.getFCMKey(getActivity(), "device_token")));
                map.put("device_id", toRequestBody(SharedHelper.getFCMKey(getActivity(), "device_id")));
                map.put("password", toRequestBody(password));
                map.put("country_code", toRequestBody(country_code));
                map.put("login_type", toRequestBody("0"));
                presenter.postregister(map, filePart);

            }
        }
    }

    public static RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    private void dialog_date_picker() {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        if (layoutInflater != null) {
            LayoutInflater cloneInContext = layoutInflater.cloneInContext(new ContextThemeWrapper(context, 16973931));
            if (cloneInContext != null) {
                View inflate = cloneInContext.inflate(R.layout.view_date_time_picker, null, false);
                final DatePicker datePicker = (DatePicker) inflate.findViewById(R.id.date_picker);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                //datePicker.setMinDate(calendar.getTime().getTime() - 100);
                datePicker.setMaxDate(new Date().getTime());
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH) + 1;
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
                    public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
                    }
                });
                datePicker.updateDate(year, month, day);

                AlertDialog create = new Builder(context, 16973935).setView(inflate)
                        .setPositiveButton(context.getString(R.string.done), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Calendar instance = Calendar.getInstance();
                                instance.set(Calendar.YEAR, datePicker.getYear());
                                instance.set(Calendar.MONTH, datePicker.getMonth() + 1);
                                instance.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                                int month = datePicker.getMonth() + 1;
                                Dob = datePicker.getDayOfMonth() + "-" + month + "-" + datePicker.getYear();
//                                tvDOB_RegisterLay.setText(datePicker.getYear() + "-" + datePicker.getMonth() + "-" + datePicker.getDayOfMonth());

                                txtDate.setText("DD-"+String.valueOf(datePicker.getDayOfMonth()));
                                txtMonth.setText("MM-"+String.valueOf(month));
                                txtYear.setText("YY-"+String.valueOf(datePicker.getYear()));
                                txtDate.setTextColor(getResources().getColor(R.color.white));
                                txtMonth.setTextColor(getResources().getColor(R.color.white));
                                txtYear.setTextColor(getResources().getColor(R.color.white));
                                dialogInterface.dismiss();
                            }
                        }).setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).create();

                create.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                create.show();

            }
        }
    }

    private void pickImage() {
        if (hasPermission(Manifest.permission.CAMERA, getActivity()) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity())) {
            EasyImage.openChooserWithGallery(this, "", 0);
        } else {
            requestPermissionsSafely(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MvpApplication.ASK_MULTIPLE_PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onSuccessRegister(Register_Pojo register_pojo) {

        Toast.makeText(getActivity(), "Register Successfull", Toast.LENGTH_LONG).show();

        Intent login = new Intent(getActivity(), SocialRegisterActivity.class);
        startActivity(login);
        getActivity().finish();

    }

    File imgFile = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imgFile = imageFiles.get(0);
                Glide.with(activity()).load(Uri.fromFile(imgFile)).apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate().error(R.drawable.user_profile)).into(profile_img);
            }


            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {

            }
        });
    }
}

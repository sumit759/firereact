package com.mozostar.app.view.activity.hashtag_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mozostar.app.R;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Discover_Pojo;
import com.mozostar.app.view.activity.sound_list.SoundTrack_Activity;
import com.mozostar.app.view.adapter.SoundTrack_Adapter;
import com.mozostar.app.view.videodub_addsound.VideoDubRecord_Activity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Hash_VideoList_Activity extends BaseActivity implements Hash_VideoIView {

    Context context;
    private static String TAG = SoundTrack_Activity.class.getSimpleName();

    @BindView(R.id.rvSoundTrack_SoundTrackLay)
    RecyclerView rvSoundTrack_SoundTrackLay;
    @BindView(R.id.ctl_SoundTrackLay)
    CollapsingToolbarLayout ctl_SoundTrackLay;
    @BindView(R.id.flContainer_SoundTrackLay)
    FrameLayout flContainer_SoundTrackLay;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.anim_toolbar)
    Toolbar toolbar;
    @BindView(R.id.original_txt)
    TextView original_txt;
    @BindView(R.id.user_prfl_txt)
    TextView user_prfl_txt;
    @BindView(R.id.videos_count)
    TextView videos_count;
    @BindView(R.id.img_view)
    ImageView img_view;
    @BindView(R.id.hash_dub_video)
    FloatingActionButton hash_dub_video;


    ArrayList<Discover_Pojo.Video_> discover;
    Discover_Pojo discover_hash;
    ArrayList<String> list = new ArrayList<>();

    private Menu collapseMenu;

    int position;

    String hash;
    private boolean appBarExpanded = true;

    Hash_VideoPresenter<Hash_VideoList_Activity> presenter = new Hash_VideoPresenter<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_hash__video_list_;
    }

    @Override
    public void initView() {

        ButterKnife.bind(this);

        presenter.attachView(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctl_SoundTrackLay.setTitle("");
        toolbar.setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        rvSoundTrack_SoundTrackLay.setLayoutManager(new GridLayoutManager(context, 3));
        rvSoundTrack_SoundTrackLay.setItemAnimator(new DefaultItemAnimator());

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) > 200) {
                    appBarExpanded = false;
                } else {
                    appBarExpanded = true;
                }
                invalidateOptionsMenu();
            }
        });

        Glide.with(Hash_VideoList_Activity.this).load(getResources().getDrawable(R.drawable.music)).into(img_view);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            position = Integer.parseInt(extras.getString("position"));
            presenter.getList();


        }

        hash_dub_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Hash_VideoList_Activity.this, VideoDubRecord_Activity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (collapseMenu != null && (!appBarExpanded || collapseMenu.size() != 1)) {
            flContainer_SoundTrackLay.setVisibility(View.GONE);
            ctl_SoundTrackLay.setTitle(user_prfl_txt.getText().toString());
            toolbar.setTitle(user_prfl_txt.getText().toString());
        } else {
            flContainer_SoundTrackLay.setVisibility(View.VISIBLE);
            ctl_SoundTrackLay.setTitle("");
            toolbar.setTitle("");
        }
        return super.onPrepareOptionsMenu(collapseMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sound_menu, menu);
        this.collapseMenu = menu;
//        this. collapseMenu.add("Share")
//                .setIcon(R.drawable.ic_add_circle)
//                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                Toast.makeText(this, "Setting menu clicked!", Toast.LENGTH_SHORT).show();
                break;
        }

        if (item.getTitle() == "Share") {
            Toast.makeText(this, "Share menu clicked!", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(Discover_Pojo trendsResponse) {
        discover = (ArrayList<Discover_Pojo.Video_>) trendsResponse.getVideos().get(position).getVideos();
        SharedHelper.putKey(Hash_VideoList_Activity.this, "hash_list", "" + trendsResponse.getVideos().get(position).getHagtagName());
        original_txt.setText("" + trendsResponse.getVideos().get(position).getHagtagName());
        user_prfl_txt.setText("" + trendsResponse.getVideos().get(position).getHagtagName());
//        int size = discover.size();
//        for (int i=0;i<size;i++){
//            list.add(trendsResponse.getVideos().get(position).getVideos().get(i).getVideo());
//        }

        SoundTrack_Adapter soundTrack_adapter = new SoundTrack_Adapter(Hash_VideoList_Activity.this, discover);
        rvSoundTrack_SoundTrackLay.setAdapter(soundTrack_adapter);
    }
}

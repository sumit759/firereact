package com.mozostar.app.view.activity.sound_list;

import com.mozostar.app.base.MvpView;
import com.mozostar.app.data.model.ProfileVideoList_Pojo;

import java.util.ArrayList;

public interface SoundTrackIView extends MvpView {
    void onSuccess(ArrayList<ProfileVideoList_Pojo> trendsResponse);
}

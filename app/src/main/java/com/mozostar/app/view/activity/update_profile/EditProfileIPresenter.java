package com.mozostar.app.view.activity.update_profile;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public interface EditProfileIPresenter<V extends EditProfileIView> extends MvpPresenter<V> {
    void updateProfile(Map<String, RequestBody> params, MultipartBody.Part file);

    @SuppressLint("CheckResult")
    void getProfiel();
}

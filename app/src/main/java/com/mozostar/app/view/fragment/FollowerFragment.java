package com.mozostar.app.view.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mozostar.app.R;
import com.mozostar.app.base.BaseFragment;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.data.model.Follow_Pojo;
import com.mozostar.app.data.model.Follower_list;
import com.mozostar.app.data.model.FollowingLIst_pojo;
import com.mozostar.app.view.activity.follow.FollowersIView;
import com.mozostar.app.view.activity.follow.FollowersPresenter;
import com.mozostar.app.view.activity.follow.InteractionListener;
import com.mozostar.app.view.adapter.FollowerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowerFragment extends BaseFragment implements FollowersIView, InteractionListener {

    @BindView(R.id.follower_frag_rv)
    RecyclerView follower_frag_rv;

    @BindView(R.id.search_follower_et)
    EditText search_follower_et;

    @BindView(R.id.no_data_follower)
    TextView no_data_follower;

    FollowersPresenter<FollowerFragment> presenter = new FollowersPresenter<>();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_follower;
    }

    Boolean logged_in;
    Bundle extras;
    String from, guest_id;
    LinearLayoutManager layoutManager;
    @Override
    public View initView(View view) {
        ButterKnife.bind(this, view);
        presenter.attachView(this);

        extras =  getActivity().getIntent().getExtras();
        from = SharedHelper.getKey(getActivity(), "on_click_follow");
        layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        follower_frag_rv.setLayoutManager(layoutManager);
        follower_frag_rv.setHasFixedSize(false);


        callList();

        return null;
    }

    private void callList() {
        if (extras != null) {
            guest_id = extras.getString("guest_id");
            if (!TextUtils.isEmpty(guest_id)) {
                if (from.equals("guest_follower")) {
                    presenter.getGuestFollowersList(guest_id);

                    presenter.getGuestList(guest_id);
                    //   tvTitle_ToolBarLay.setText("Followers");
                } else if (from.equals("guest_follwing")) {
                    // tvTitle_ToolBarLay.setText("Following");
                    presenter.getGuestList(guest_id);

                    presenter.getGuestFollowersList(guest_id);
                }
            }
        } else {
            if (from.equals("follower")) {
                //  tvTitle_ToolBarLay.setText("Followers");
                presenter.getFollowersList();

                presenter.getList();
            } else if (from.equals("following")) {
                // tvTitle_ToolBarLay.setText("Following");
                presenter.getList();

                presenter.getFollowersList();
            }
        }
    }


    @Override
    public void onSuccess(ArrayList<FollowingLIst_pojo> trendsResponse) {   //Following

    }

    @Override
    public void onSuccessFollower(ArrayList<Follower_list> trendsResponse) {  //Follower
        int size = trendsResponse.size();
        if (trendsResponse != null && size != 0) {
            FollowerAdapter homeScreen_adapter = new FollowerAdapter(getContext(), trendsResponse);
            follower_frag_rv.setAdapter(homeScreen_adapter);
            no_data_follower.setVisibility(View.GONE);
        } else {
            no_data_follower.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccessFollow(Follow_Pojo trendsResponse) {
        callList();
    }

    @Override
    public void follow(String id) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("user_id", "" + SharedHelper.getIntKey(getContext(), "user_id"));
        map.put("follower_id", id);
        presenter.followup(map);
    }

}

package com.mozostar.app.view.postdubvideo;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;

import com.mozostar.app.base.MvpPresenter;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface PostDubVideoIPresenter <V extends PostDubVideoIView> extends MvpPresenter<V> {
//    void postregister(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part filename);
//
//    @SuppressLint("CheckResult")
//    void postregister(Map<String, RequestBody> params, MultipartBody.Part file, MultipartBody.Part file1);

    @SuppressLint("CheckResult")
    void postregister(Map<String, RequestBody> params, MultipartBody.Part file, ArrayList<RequestBody> tag);

    @SuppressLint("CheckResult")
    void postregister(Map<String, RequestBody> params, MultipartBody.Part file, MultipartBody.Part file1, ArrayList<RequestBody> tag);

    @SuppressLint("CheckResult")
    void postregister_channel(Map<String, RequestBody> params, MultipartBody.Part file, ArrayList<RequestBody> tag);

    void postBigVideo(Map<String, RequestBody> params, MultipartBody.Part file, ArrayList<RequestBody> tag);



}

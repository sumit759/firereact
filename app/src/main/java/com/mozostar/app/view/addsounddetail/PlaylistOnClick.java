package com.mozostar.app.view.addsounddetail;

public interface PlaylistOnClick {
    void onClickPlaylistItem(int position);
}

package com.mozostar.app.view.fragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FollowPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    public FollowPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FollowingFragment tab1 = new FollowingFragment();
                return tab1;
            case 1:
                FollowerFragment tab2 = new FollowerFragment();
                return tab2;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

package com.mozostar.app.view.activity.hashtag_list;

import android.annotation.SuppressLint;

import com.mozostar.app.base.MvpPresenter;

public interface Hash_VideoIPresenter <V extends Hash_VideoIView> extends MvpPresenter<V> {

    @SuppressLint("CheckResult")
    void getList();

}

package com.mozostar.app.view.splash;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toolbar;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.mozostar.app.R;
import com.mozostar.app.base.ActivityManager;
import com.mozostar.app.base.BaseActivity;
import com.mozostar.app.common.Utilities;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.view.activity.intro.WelcomeActivity;
import com.mozostar.app.view.login.Login_Activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Splash_Activity extends BaseActivity implements SplashIView {
    Context context;
    private static String TAG = Splash_Activity.class.getSimpleName();
    SplashPresenter<Splash_Activity> presenter = new SplashPresenter<Splash_Activity>();

    Toolbar myToolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initView() {
        context = Splash_Activity.this;
        init();
//        printHashKey();
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setStatusBarColor(ContextCompat.getColor(Splash_Activity.this, R.color.colorPrimary));

        setSupportActionBar(myToolbar);
        Utilities.printV("FCM TOKEN===>", SharedHelper.getFCMKey(this, "device_token"));
        Utilities.printV("FCM TOKEN ID===>", SharedHelper.getFCMKey(this, "device_id"));
    }


    private void setSupportActionBar(Toolbar myToolbar) {
    }

    private void init() {
        new Handler().postDelayed(() -> {

            ActivityManager.redirectToActivityWithoutBundle(context, WelcomeActivity.class);
            overridePendingTransition(0, 0);
            finishAffinity();
            finish();

            /*ActivityManager.redirectToActivityWithoutBundle(context, BannerActivity.class);
                overridePendingTransition(0, 0);
                finish();*/

//            Boolean loggedIn = SharedHelper.getBoolKey(Splash_Activity.this, "logged_in", false);
//            if (loggedIn) {
//
//                ActivityManager.redirectToActivityWithoutBundle(context, WelcomeActivity.class);
//                overridePendingTransition(0, 0);
//                finish();
////                presenter.profile();
//            } else {
//
//                ActivityManager.redirectToActivityWithoutBundle(context, Login_Activity.class);
//                overridePendingTransition(0, 0);
//                finish();
//
//             }
        }, 3000);
    }

    @Override
    public void redirectHome() {
        if (SharedHelper.getKey(this, "loggged_in").equalsIgnoreCase("true"))
            startActivity(new Intent(activity(), WelcomeActivity.class));
        else
            startActivity(new Intent(activity(), Login_Activity.class));
        finish();
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    @Override
    protected void onStart() {

        super.onStart();

    }


}

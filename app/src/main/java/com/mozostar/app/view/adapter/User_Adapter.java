package com.mozostar.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mozostar.app.R;
import com.mozostar.app.data.model.User_Pojo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class User_Adapter extends RecyclerView.Adapter<User_Adapter.MyViewHolder> {
    Context context;
    ArrayList<String> stringArrayList;


    ArrayList<User_Pojo> discover_pojos;

    public User_Adapter(Context context, ArrayList<User_Pojo> discover_pojos) {
        this.context = context;
        this.discover_pojos = discover_pojos;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        User_Pojo user_pojo = discover_pojos.get(i);
        try {
            myViewHolder.user.setText(user_pojo.getFirstName() + " " + user_pojo.getLastName());
            myViewHolder.user_bio.setText(user_pojo.getUserBio());
            if (user_pojo.getImage() != null) {
                Glide.with(context).load("" + user_pojo.getImage()).into(myViewHolder.user_img);
            } else {
                Glide.with(context).load(R.drawable.user_profile).into(myViewHolder.user_img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return discover_pojos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.user_img)
        CircleImageView user_img;
        @BindView(R.id.user)
        TextView user;
        @BindView(R.id.user_bio)
        TextView user_bio;

        MyViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);


        }

        @Override
        public void onClick(View v) {

        }
    }
}



  /* if (discover_pojos.get(i).getImage() != null)
            Glide.with(context).load(discover_pojos.get(i).getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.user_profile).dontAnimate()
                            .error(R.drawable.user_profile)).into(myViewHolder.user_img);


        if (discover_pojos.get(i).getUserBio() != null)
            myViewHolder.user.setText("" + discover_pojos.get(i).getUserBio());

        String search = SharedHelper.getKey(context, "search");
        if (search.equals("post_user")) {
            myViewHolder.user_img.setVisibility(View.VISIBLE);
            myViewHolder.user.setText("" + discover_pojos.get(i).getFirstName() + " " + discover_pojos.get(i).getLastName());
        } else if (search.equals("hash")) {
            myViewHolder.user_img.setVisibility(View.GONE);
//            myViewHolder.user.setText("" +discover_pojos.get(i).get);
        } else if (search.equals("user")) {
            myViewHolder.user_img.setVisibility(View.VISIBLE);
            myViewHolder.user.setText("" + discover_pojos.get(i).getFirstName() + " " + discover_pojos.get(i).getLastName());
        }
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (search.equals("post_user")) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("id", " " + discover_pojos.get(i).getId());
                    returnIntent.putExtra("name", " @" + discover_pojos.get(i).getFirstName());
                    ((Activity) v.getContext()).setResult(Activity.RESULT_OK, returnIntent);
                    ((Activity) v.getContext()).finish();
                } else if (search.equals("hash")) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("id", "" + discover_pojos.get(i).getId());
                    returnIntent.putExtra("name", discover_pojos.get(i).getFirstName());
                    ((Activity) v.getContext()).setResult(Activity.RESULT_OK, returnIntent);
                    ((Activity) v.getContext()).finish();
                } else if (search.equals("user")) {
                    Intent guest = new Intent(context, GuestProfile_Activity.class);
                    guest.putExtra("guest_id", "" + discover_pojos.get(i).getId());
                    context.startActivity(guest);
                }
            }
        });*/
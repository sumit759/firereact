package com.mozostar.app.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.navigation.NavigationView;
import com.mozostar.app.R;
import com.mozostar.app.common.LogoutDialog;
import com.mozostar.app.data.SharedHelper;
import com.mozostar.app.view.activity.MainActivity;
import com.mozostar.app.view.activity.RateReview;
import com.mozostar.app.view.activity.settings.SettingsPrivacyActivity;
import com.mozostar.app.view.fragment.profile.ProfilePresenter;
import com.mozostar.app.view.fragment.profile.Profile_Fragment;
import com.mozostar.app.view.splash.Splash_Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import retrofit2.HttpException;
import retrofit2.Response;


public abstract class BaseNavigationFragment extends Fragment implements MvpView , LogoutDialog.DialogInterface, NavigationView.OnNavigationItemSelectedListener {
    View view;
    ProgressDialog progressDialog;

    @BindView(R.id.nav_view)
    NavigationView nav_view;

    ProfilePresenter<Profile_Fragment> presenter = new ProfilePresenter<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(getLayoutId(), container, false);
            initView(view);
        }
        progressDialog = new ProgressDialog(activity());
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);


        nav_view.setNavigationItemSelectedListener(this);
//        View headerView = nav_view.getHeaderView(0);
//        TextView navUsername = (TextView) headerView.findViewById(R.id.txtName);
//        navUsername.setText("Your Text Here");

        return view;
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                Intent startAct3 = new Intent(getActivity(), MainActivity.class);
                startActivity(startAct3);

                break;
            case R.id.nav_profile:
                Intent startAct = new Intent(getActivity(), Profile_Fragment.class);
                startActivity(startAct);
                break;
            case R.id.nav_privacy_setting:
                Intent startAct2 = new Intent(getActivity(), SettingsPrivacyActivity.class);
                startActivity(startAct2);
                break;
            case R.id.nav_review:
                Intent startAct4 = new Intent(getActivity(), RateReview.class);
                startActivity(startAct4);
                break;
            case R.id.nav_logout:

                try {
                    LogoutDialog logoutDialog = new LogoutDialog(getActivity(), this);
                    logoutDialog.setCancelable(false);
                    logoutDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

        return false;
    }


    @Override
    public void onYes() {
        SharedHelper.putKey(getActivity(), "access_token", "");
        SharedHelper.putKey(getActivity(), "logged_in", false);
        Toast.makeText(getActivity(), "Logout Successfully", Toast.LENGTH_LONG).show();
        Intent mainIntent = new Intent(getActivity(), Splash_Activity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        activity().finish();
    }

    @Override
    public void onNo() {

    }

    public abstract int getLayoutId();

    public abstract View initView(View view);

    @Override
    public FragmentActivity activity() {
        return getActivity();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission, Activity activity) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void showLoading() {
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        progressDialog.dismiss();
    }


//    public NumberFormat getNumberFormat() {
//        String currencyCode = SharedHelper.getKey(MvpApplication.getInstance(), "currency_code", "USD");
//        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
//        numberFormat.setCurrency(Currency.getInstance(currencyCode));
//        numberFormat.setMinimumFractionDigits(2);
//        return numberFormat;
//    }

    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e instanceof HttpException) {
            Response response = ((HttpException) e).response();
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                if (jObjError.has("message"))
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();
                else if (jObjError.has("error"))
                    if (jObjError.has("error")) {
                        Toast.makeText(activity(), jObjError.optString("error"), Toast.LENGTH_SHORT).show();
                    } else {
                        String email = jObjError.getString("email");
                        JSONArray jsonArray = new JSONArray(email);
                        Toast.makeText(activity(), jsonArray.getString(0).toString(), Toast.LENGTH_SHORT).show();
                    }
                else
                    Toast.makeText(activity(), jObjError.toString(), Toast.LENGTH_SHORT).show();
            } catch (Exception exp) {
                Log.e("Error", exp.getMessage());
                Toast.makeText(activity(), exp.getMessage(), Toast.LENGTH_SHORT).show();
            }

            if (response.code() == 401) {

                JSONObject jObjError = null;
                try {
                    jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();

                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

//                SharedHelper.clearSharedPreferences(this);
//                finishAffinity();
//                startActivity(new Intent(activity(), OnboardActivity.class));
//                overridePendingTransition(0, 0);
            }
        }
    }


    private void errorValid(String field, JSONObject jObjErrors) throws JSONException {
        String error = jObjErrors.optJSONArray(field).get(0).toString();
        switch (error) {
            case "validation.unique":
                Toast.makeText(activity(), field + " already exits", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(activity(), jObjErrors.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void datePicker(DatePickerDialog.OnDateSetListener dateSetListener) {
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(activity(), dateSetListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.show();
    }

    public void timePicker(TimePickerDialog.OnTimeSetListener timeSetListener) {
        Calendar myCalendar = Calendar.getInstance();
        TimePickerDialog mTimePicker = new TimePickerDialog(getContext(), timeSetListener,
                myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), false);
        mTimePicker.show();
    }
}

package com.mozostar.app.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import retrofit2.HttpException;
import retrofit2.Response;


public abstract class BaseBottomSheetDialogFragment extends BottomSheetDialogFragment implements MvpView {

    View view;

    public abstract int getLayoutId();

    public abstract void initView(View view);

    ProgressDialog progressDialog;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(getLayoutId(), container, false);
            initView(view);
        }
        progressDialog = new ProgressDialog(activity());
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);

        return view;
    }

    @Override
    public Activity activity() {
        return getActivity();
    }

    @Override
    public void showLoading() {
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        progressDialog.dismiss();
    }


    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e instanceof HttpException) {
            Response response = ((HttpException) e).response();
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                if (jObjError.has("message"))
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();
                else if (jObjError.has("error"))
                    if (jObjError.has("error")) {
                        Toast.makeText(activity(), jObjError.optString("error"), Toast.LENGTH_SHORT).show();
                    } else {
                        String email = jObjError.getString("email");
                        JSONArray jsonArray = new JSONArray(email);
                        Toast.makeText(activity(), jsonArray.getString(0).toString(), Toast.LENGTH_SHORT).show();
                    }
                else
                    Toast.makeText(activity(), jObjError.toString(), Toast.LENGTH_SHORT).show();
            } catch (Exception exp) {
                Log.e("Error", exp.getMessage());
                Toast.makeText(activity(), exp.getMessage(), Toast.LENGTH_SHORT).show();
            }

            if (response.code() == 401) {

                JSONObject jObjError = null;
                try {
                    jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();

                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

//                SharedHelper.clearSharedPreferences(this);
//                finishAffinity();
//                startActivity(new Intent(activity(), OnboardActivity.class));
//                overridePendingTransition(0, 0);
            }
        }
    }

}

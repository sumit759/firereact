package com.mozostar.app.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.HttpException;
import retrofit2.Response;


public abstract class BaseFragment extends Fragment implements MvpView {
    View view;
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(getLayoutId(), container, false);
            initView(view);
        }
        progressDialog = new ProgressDialog(activity());
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);

        return view;
    }

    public abstract int getLayoutId();

    public abstract View initView(View view);

    @Override
    public FragmentActivity activity() {
        return getActivity();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission, Activity activity) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void showLoading() {
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        progressDialog.dismiss();
    }


    @Override
    public void onError(Throwable e) {
        hideLoading();
        if (e instanceof HttpException) {
            Response response = ((HttpException) e).response();
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                if (jObjError.has("message"))
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();
                else if (jObjError.has("error"))
                    if (jObjError.has("error")) {
                        Toast.makeText(activity(), jObjError.optString("error"), Toast.LENGTH_SHORT).show();
                    } else {
                        String email = jObjError.getString("email");
                        JSONArray jsonArray = new JSONArray(email);
                        Toast.makeText(activity(), jsonArray.getString(0).toString(), Toast.LENGTH_SHORT).show();
                    }
                else
                    Toast.makeText(activity(), jObjError.toString(), Toast.LENGTH_SHORT).show();
            } catch (Exception exp) {
                Log.e("Error", exp.getMessage());
                Toast.makeText(activity(), exp.getMessage(), Toast.LENGTH_SHORT).show();
            }

            if (response.code() == 401) {

                JSONObject jObjError = null;
                try {
                    jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(activity(), jObjError.optString("message"), Toast.LENGTH_SHORT).show();

                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

//                SharedHelper.clearSharedPreferences(this);
//                finishAffinity();
//                startActivity(new Intent(activity(), OnboardActivity.class));
//                overridePendingTransition(0, 0);
            }
        }
    }


    private void errorValid(String field, JSONObject jObjErrors) throws JSONException {
        String error = jObjErrors.optJSONArray(field).get(0).toString();
        switch (error) {
            case "validation.unique":
                Toast.makeText(activity(), field + " already exits", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(activity(), jObjErrors.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void datePicker(DatePickerDialog.OnDateSetListener dateSetListener) {
        Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(activity(), dateSetListener, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.show();
    }

    public void timePicker(TimePickerDialog.OnTimeSetListener timeSetListener) {
        Calendar myCalendar = Calendar.getInstance();
        TimePickerDialog mTimePicker = new TimePickerDialog(getContext(), timeSetListener,
                myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), false);
        mTimePicker.show();
    }
}

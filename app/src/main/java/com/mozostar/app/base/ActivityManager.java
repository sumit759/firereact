package com.mozostar.app.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.io.File;

public class ActivityManager {
    public static File file_videopath = new File(Environment.getExternalStorageDirectory(), "Funnzy");

    public static String outputfile;
    public static String outputfile2;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(file_videopath);
        sb.append("/output.mp4");
        outputfile = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(file_videopath);
        sb2.append("/output2.mp4");
        outputfile2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(file_videopath);
        sb3.append("/output-filtered.mp4");
    }
    
    public static void replaceFragmentWithoutBundle(AppCompatActivity activity, int frameId, Fragment inputFragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(frameId, inputFragment).commit();
    }

    public static void replaceFragmentWithoutBundleBackStack(AppCompatActivity activity, int frameId, Fragment inputFragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(frameId, inputFragment).addToBackStack(null).commit();
    }

    public static void replaceFragmentWithBundle(AppCompatActivity activity, Fragment inputFragment, int frameId) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(frameId, inputFragment).commit();
    }

    public static void replaceFragmentWithBundleBackStack(AppCompatActivity activity, int frameId, Fragment inputFragment, Bundle bundle) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        inputFragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(frameId, inputFragment).addToBackStack(null).commit();
    }

    public static void replaceFragmentBackStack(AppCompatActivity activity, int frameId, Fragment inputFragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(frameId, inputFragment).addToBackStack(null).commit();
    }

    public static void redirectToActivityWithoutBundle(Context activity, Class<? extends AppCompatActivity> redirectToActivity) {
        Intent redirectTo = new Intent(activity, redirectToActivity);
        activity.startActivity(redirectTo);

    }

    public static void redirectToActivityFlag(Context activity, Class<? extends AppCompatActivity> redirectToActivity) {
        Intent redirectTo = new Intent(activity, redirectToActivity);
        redirectTo.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        redirectTo.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        redirectTo.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(redirectTo);
    }

    public static void redirectToActivityWithBundle(Context activity, Class<? extends AppCompatActivity> redirectToActivity, Bundle bundle) {
        Intent redirectTo = new Intent(activity, redirectToActivity);
        redirectTo.putExtras(bundle);
        activity.startActivity(redirectTo);
    }


}

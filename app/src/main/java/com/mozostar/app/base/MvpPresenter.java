package com.mozostar.app.base;

import android.app.Activity;

import com.mozostar.app.MvpApplication;


public interface MvpPresenter<V extends MvpView> {
    Activity activity();
    MvpApplication appContext();
    void attachView(V mvpView);
    void detachView();

}
